; Variables - General
Var /GLOBAL IncompatibleOS
Var /GLOBAL BuildNumber
Var /GLOBAL sonicexemd5
Var /GLOBAL Permission1
Var /GLOBAL Permission2
Var /GLOBAL DirectXSetupError
Var /GLOBAL InstallPath
Var /GLOBAL InstallPathTemp
Var /GLOBAL InstallPathTemp2
Var /GLOBAL InstallPathTemp3
Var /GLOBAL NetFrameworkVersion
Var /GLOBAL Whatev
Var /GLOBAL DirTextThing
Var /GLOBAL DownloadErrorCode
Var /GLOBAL InstallType
Var /GLOBAL ModLoaderSettingMode
Var /GLOBAL EnableCustomIcon
Var /GLOBAL CustomIconNumber
Var /GLOBAL GuideMode
Var /GLOBAL PreserveModSettings
Var /GLOBAL EnableWindowTitle
Var /GLOBAL Final_SADX
Var /GLOBAL Final_Shortcuts
Var /GLOBAL Final_Launcher
Var /GLOBAL DotNetExecutable
Var /GLOBAL ModManagerArchive
Var /GLOBAL ConvToolsArchive
Var /GLOBAL CheckFolderResult
Var /GLOBAL UserName
Var /GLOBAL ModLoaderPath
Var /GLOBAL ManagerAppDataPath

; Variables - Mod Loader settings
Var /GLOBAL Res_HZ
Var /GLOBAL Res_V
Var /GLOBAL ScreenMode
Var /GLOBAL VSync

; Variables - Screen compare code
Var /GLOBAL ModScreenNumber
Var /GLOBAL ModScreenOff

; Variables - Mod sorting
Var /GLOBAL ModIndex
Var /GLOBAL INST_ADX
Var /GLOBAL INST_LANTERN
Var /GLOBAL INST_SADXFE
Var /GLOBAL INST_INPUTMOD
Var /GLOBAL INST_SMOOTHCAM
Var /GLOBAL INST_PAUSEHIDE
Var /GLOBAL INST_FRAMELIMIT
Var /GLOBAL INST_DLCS
Var /GLOBAL INST_STEAM
Var /GLOBAL INST_DCMODS
Var /GLOBAL INST_SNDOVERHAUL
Var /GLOBAL INST_SA1CHARS
Var /GLOBAL INST_HDGUI
Var /GLOBAL INST_TIMEOFDAY
Var /GLOBAL INST_SUPERSONIC
Var /GLOBAL INST_ONIONBLUR
Var /GLOBAL INST_SADXWTR
Var /GLOBAL INST_IDLECHATTER

; Variables - Mod update functions
Var /GLOBAL UpdatesFound
Var /GLOBAL WhichUpdates
Var /GLOBAL UpdateFilename

; Variables - Mod install functions
Var /GLOBAL Modname
Var /GLOBAL ModFilename
Var /GLOBAL ModInstallSuccess
Var /GLOBAL ModInstallNoSubFolderMode

; Variables - Dialogs
Var hCtl_TypeSel
Var hCtl_TypeSel_CheckBox_ShowAdvanced
Var hCtl_TypeSel_GroupBox_InstallerMode
Var hCtl_TypeSel_Option_Guide
Var hCtl_TypeSel_Option_Preset
Var hCtl_TypeSel_Option_Custom
Var hCtl_TypeSel_GroupBox_Presets
Var hCtl_TypeSel_Option_DC
Var hCtl_TypeSel_Option_Enhanced
Var hCtl_TypeSel_Option_Minimal
Var hCtl_TypeSel_Option_Redist
Var hCtl_TypeSel_Option_SteamConv
Var hCtl_TypeSel_CheckBox_Preserve
Var hCtl_TypeSel_GroupBox_TypeDesc
Var hCtl_TypeSel_TypeComment
Var hCtl_TypeSel_CheckBox_InstallIcon
Var hCtl_TypeSel_SelectTypeText
Var hCtl_TypeSel_Font1
Var hCtl_TypeSel_CheckBox_WindowTitle
Var hCtl_TypeSel_RadioButton_SettingsFailsafe
Var hCtl_TypeSel_RadioButton_SettingsOptimal
Var hCtl_TypeSel_Checkbox_ManagerClassic
Var hCtl_sel
Var hCtl_sel_OfflineText
Var hCtl_sel_DontCheckUpdates
Var hCtl_sel_CheckUpdates
Var hCtl_sel_UpdatesText
Var hCtl_FinishPage
Var hCtl_FinishPage_CheckBox_RunLauncher
Var hCtl_FinishPage_CheckBox_RunSADX
Var hCtl_FinishPage_FinishTitle
Var hCtl_FinishPage_FinishMessage
Var hCtl_FinishPage_Banner
Var hCtl_FinishPage_Banner_hImage
Var hCtl_FinishPage_Font1
Var hCtl_FinishPage_CheckBox_CreateShortcuts
Var hCtl_FinishPage_LinkMoreMods
Var hCtl_ScreenCompare
Var hCtl_ScreenCompare_CheckBox1
Var hCtl_ScreenCompare_ModDescription
Var hCtl_ScreenCompare_Bitmap1
Var hCtl_ScreenCompare_Bitmap1_hImage
Var hCtl_Additional
Var hCtl_Additional_ModsDesc
Var hCtl_Additional_CheckBox_SoundOverhaul
Var hCtl_Additional_CheckBox_DLCs
Var hCtl_Additional_CheckBox_SuperSonic
Var hCtl_iconselect
Var hCtl_iconselect_Label1
Var hCtl_iconselect_Bitmap_SADXPC
Var hCtl_iconselect_Bitmap_SADXPC_hImage
Var hCtl_iconselect_Bitmap_SADXGC
Var hCtl_iconselect_Bitmap_SADXGC_hImage
Var hCtl_iconselect_Bitmap_SA1Save
Var hCtl_iconselect_Bitmap_SA1Save_hImage
Var hCtl_iconselect_Bitmap_SA1Box
Var hCtl_iconselect_Bitmap_SA1Box_hImage
Var hCtl_iconselect_Button_SADXPC
Var hCtl_iconselect_Button_SADXGC
Var hCtl_iconselect_Button_SA1Box
Var hCtl_iconselect_Button_SA1Save
Var hCtl_WelcomePage
Var hCtl_WelcomePage_Welcome
Var hCtl_WelcomePage_Version
Var hCtl_WelcomePage_WelcomeText
Var hCtl_WelcomePage_Banner
Var hCtl_WelcomePage_Banner_hImage
Var hCtl_WelcomePage_Font1