!insertmacro LANGFILE "French" = "Français" "Francais"

;General strings
${LangFileString} MUI_TEXT_LICENSE_TITLE "Information"
${LangFileString} MUI_TEXT_LICENSE_SUBTITLE "Merci de prendre connaissance des notes suivantes avant d'utiliser ce programme."
${LangFileString} MUI_INNERTEXT_LICENSE_TOP "Faites défiler vers le bas ou appuyez sur Page suivante pour voir le reste des notes."
${LangFileString} MUI_INNERTEXT_LICENSE_BOTTOM "Veuillez sauvegarder votre dossier SADX avant d'utiliser ce programme d'installation. Cliquez sur Continuer pour poursuivre l'installation."
${LangFileString} MUI_INNERTEXT_LICENSE_BOTTOM_CHECKBOX " "
${LangFileString} MUI_TEXT_COMPONENTS_TITLE "Choisissez les composants"
${LangFileString} MUI_TEXT_COMPONENTS_SUBTITLE "Choisissez les mods et fonctionnalités que vous souhaitez installer."
${LangFileString} MUI_INNERTEXT_COMPONENTS_DESCRIPTION_INFO "Déplacez le curseur de la souris sur un composant pour voir sa description."
${LangFileString} MUI_INNERTEXT_COMPONENTS_DESCRIPTION_TITLE "Description"
${LangFileString} MUI_TEXT_DIRECTORY_TITLE "Choisissez l'emplacement d'installation"
${LangFileString} MUI_TEXT_DIRECTORY_SUBTITLE "Sélectionnez le dossier SADX à utiliser par le programme d'installation."
${LangFileString} MUI_TEXT_INSTALLING_TITLE "Installation"
${LangFileString} MUI_TEXT_INSTALLING_SUBTITLE "Veuillez patienter pendant que SADX Mod Installer installe les fichiers."
${LangFileString} MUI_BUTTONTEXT_FINISH "&Finish"

;Install types and profile names
LangString INSTTYPE_DC 1036 "Dreamcast mods"
LangString INSTTYPE_SADX 1036 "SADX + améliorations"
LangString INSTTYPE_MIN 1036 "Minimum/Vanilla"
LangString PROFILENAME_DC 1036 "Dreamcast"
LangString PROFILENAME_SADX 1036 "SADX Amélioré"
LangString PROFILENAME_MIN 1036 "Minimum"
LangString PROFILENAME_CUSTOM 1036 "Mode personnalisée"

;Section names
LangString SECTIONNAME_REMOVEMODS 1036 "Supprime tous les mods actifs"
LangString SECTIONNAME_PERMISSIONS 1036 "Vérifie et répare les permissions du dossier SADX"
LangString SECTIONNAME_MODLOADER 1036 "SADX Mod Loader par MainMemory & SonicFreak94"
LangString SECTIONNAME_LAUNCHER 1036 "SADX Launcher par PkR"
LangString SECTIONNAME_DEPENDENCIES 1036 "Dépendences du Mod Loader"
LangString SECTIONNAME_VCC 1036 "Visual C++ runtimes"
LangString SECTIONNAME_DX9 1036 "DirectX 9.0c"
LangString SECTIONNAME_BUGFIXES 1036 "Correction de bugs et contrôle/camera mods"
LangString SECTIONNAME_SADXFE 1036 "SADX: Fixed Edition par SonicFreak94"
LangString SECTIONNAME_INPUTMOD 1036 "Input Mod par SonicFreak94"
LangString SECTIONNAME_FRAMELIMIT 1036 "Frame Limiter par SonicFreak94"
LangString SECTIONNAME_CCEF 1036 "Camera Fix par VeritasDL & SonicFreak94"
LangString SECTIONNAME_DCMODS 1036 "Dreamcast mods"
LangString SECTIONNAME_DCCONV 1036 "Dreamcast Conversion par PkR"
LangString SECTIONNAME_SA1CHARS 1036 "Dreamcast Characters Pack par ItsEasyActually"
LangString SECTIONNAME_LANTERN 1036 "Lantern Engine par SonicFreak94"
LangString SECTIONNAME_DLCS 1036 "Sonic Adventure DLCs par PkR"
LangString SECTIONNAME_ENH 1036 "Amélioration et gameplay mods"
LangString SECTIONNAME_SMOOTH 1036 "Smooth Camera par SonicFreak94"
LangString SECTIONNAME_ONION 1036 "Onion Skin Blur par SonicFreak94"
LangString SECTIONNAME_IDLE 1036 "Idle Chatter par SonicFreak94"
LangString SECTIONNAME_PAUSE 1036 "Pause Hide par SonicFreak94"
LangString SECTIONNAME_STEAM 1036 "Steam Achievements par MainMemory"
LangString SECTIONNAME_SUPER 1036 "Super Sonic par Kell & SonicFreak94"
LangString SECTIONNAME_TIME 1036 "Time of Day par PkR"
LangString SECTIONNAME_ADX 1036 "ADX voices and music (pour la version 2004)"
LangString SECTIONNAME_SND 1036 "Sound Overhaul 3 par PkR"
LangString SECTIONNAME_HDGUI 1036 "HD GUI 2 par PkR & others"
LangString SECTIONNAME_SADXWTR 1036 "SADX Style Water by PkR & SteG"
LangString SECTIONNAME_MANAGERCLASSIC 1036 "SADX Mod Manager Classic by PkR"

;Guide Mode descriptions and checkboxes
LangString GUIDE_INFO_LANTERN 1036 "Lantern Engine mod recrée le système d'éclairage de la version Dreamcast pour la version PC de SADX. Il supprime l'effet brillant excessif sur les personnages et rend l'éclairage des niveaux, des objets et des personnages plus dynamique."
LangString GUIDE_INFO_DCCONV 1036 "Dreamcast Conversion est une refonte complète du jeu pour qu'il ressemble à la version Dreamcast de SA1. Il corrige de nombreux bugs et restaure les niveaux, les textures, les modèles d'objets, les effets spéciaux et l'UI de la Dreamcast."
LangString GUIDE_INFO_DXWTR 1036 "DC Conversion contient également une option pour activer des textures d'eau alternatives qui sont similaires à l'eau d'origine de SADX, mais de meilleures qualités. Certaines personnes préférent utiliser ces textures dans les niveaux Dreamcast."
LangString GUIDE_INFO_SA1CHARS 1036 "DC Characters restaure les modèles de personnages Dreamcast dans la version PC. Bien que les personnages dans SADX ont plus de polygones, certaines personnes préférent les modèles d'origine Dreamcast pour leurs textures ou esthétiques."
LangString GUIDE_INFO_ONION 1036 "Le mod Onion Skin Blur recrée l'effet «mouvement flou» sur les pieds de Sonic et les queues de Tails. Cet effet était à l'origine dans la version Dreamcast japonaise de SA1, mais il a été supprimé dans les versions ultérieures."
LangString GUIDE_INFO_HDGUI 1036 "HD GUI 2 remplace la plupart des textures GUI (menus, HUD, icone des capsules, etc.) par des textures en haute résolution personnalisées."
LangString GUIDE_INST_LANTERN 1036 "Installer Lantern Engine par SonicFreak94"
LangString GUIDE_INST_DCCONV 1036 "Installer Dreamcast Conversion par PkR"
LangString GUIDE_INST_DXWTR 1036 "Activer SADX Style Water textures par SteG"
LangString GUIDE_INST_SA1CHARS 1036 "Installer Dreamcast Characters Pack par ItsEasyActually"
LangString GUIDE_INST_ONION 1036 "Installer Onion Skin Blur par SonicFreak94"
LangString GUIDE_INST_HDGUI 1036 "Installer HD GUI 2 par PkR, Dark Sonic, Sonikko et SPEEPSHighway"

;Download strings
LangString D_DSOUND 1036 "Téléchargement de DirectSound wrapper DLL..."
LangString D_DLCS 1036 "Téléchargement du contenu des DLCs Dreamcast..."
LangString D_D3D8 1036 "Téléchargement de D3D8to9 DLL..."
LangString D_DCCONV 1036 "Téléchargement de Dreamcast Conversion..."
LangString D_STEAM 1036 "Téléchargement de Steam Achievements..."
LangString D_RESOURCES 1036 "Téléchargement de custom resources..."
LangString D_DX9 1036 "Téléchargement de DirectX Setup..."
LangString D_VC2010 1036 "Téléchargement de VS2010 runtimes..."
LangString D_VC2012 1036 "Téléchargement de VS2012 runtimes..."
LangString D_VC2013 1036 "Téléchargement de VS2013 runtimes..."
LangString D_VC2019 1036 "Téléchargement de VS2019 runtimes..."
LangString D_LAUNCHER 1036 "Téléchargement de SADX Launcher..."
LangString D_NET 1036 "Téléchargement de .NET Web Setup..."
LangString D_CHRMODELS 1036 "Téléchargement de CHRMODELS..."
LangString D_STEAMTOOLS 1036 "Téléchargement de tools..."
LangString D_2004BIN 1036 "Téléchargement de SADX 2004 binaries..."
LangString D_2004EXE 1036 "Téléchargement de SADX 2004 EXE file..."
LangString D_VOICES 1036 "Téléchargement de additional voices..."
LangString D_MODLOADER 1036 "Téléchargement de SADX Mod Loader..."
LangString D_FILELIST 1036 "Téléchargement de la liste des fichiers..."
LangString D_INSTCHK 1036 "Vérification des mises à jours du programme d'Installation..."
LangString D_GENERIC 1036 "Téléchargement de $Modname..."
LangString D_UPDATE 1036 "Vérification de $UpdateFilename ..."
LangString D_INSTALLER 1036 "Téléchargement du programme d'Installation..."
LangString D_INSTALLER_R 1036 "Lancement de la nouvelle version..."

;Headers
LangString MISC_INSTALLER 1036 "SADX Mod Installer"
LangString HEADER_GUIDE_TITLE 1036 "Guide de séléction de Mod"
LangString HEADER_GUIDE_TEXT 1036 "Cochez la case si vous souhaitez installer ce mod. Cliquez sur la capture d'écran pour une comparaison plus détaillée."
LangString HEADER_ADD_TITLE 1036 "Mods Additionnels" 
LangString HEADER_ADD_TEXT 1036 "Cochez les cases des mods que vous souhaitez installer."
LangString HEADER_UPDATES_TITLE 1036 "Vérifier les mises à jour"
LangString HEADER_UPDATES_TEXT 1036 "Il est recommandé de maintenir à jour les mods et le Mod Loader."
LangString HEADER_SET_TITLE 1036 "Configuration du Mod Loader"
LangString HEADER_SET_TEXT 1036 "Paramètre de SADX/Mod Loader. Vous pouvez toujours les modifier ultérieurement dans le Mod Manager."
LangString HEADER_ICON_TITLE 1036 "Sélectionnez votre icône EXE"
LangString HEADER_ICON_TEXT 1036 "Sélectionnez une icône personnalisée pour le fichier EXE SADX."
LangString HEADER_TYPE_TITLE 1036 "Type d'Installation"
LangString HEADER_TYPE_TEXT 1036 "Sélectionnez le type d'installation."

;Mod Loader settings
LangString SET_TRUE 1036 "Activé"
LangString SET_FALSE 1036 "Désactivé"
LangString SET_DISP 1036 "Options d'affichage"
LangString SET_RES 1036 "Résolution"
LangString SET_SCREEN 1036 "Type d'affichage"
LangString SET_WINDOWED 1036 "Fenêtré"
LangString SET_BORDERLESS 1036 "Plein écran sans bordure"
LangString SET_FULLSCREEN 1036 "Plein écran"
LangString SET_SCALE 1036 "Mise à l'échelle de l'affichage (sous-échantillonnage)"
LangString SET_VSYNC 1036 "VSync"
LangString SET_DETAIL 1036 "Niveau de détail"
LangString SET_HIGH 1036 "Elevé (meilleur)"
LangString SET_LOW 1036 "Faible"
LangString SET_LOWEST 1036 "Le plus faible"
LangString SET_FRAMERATE 1036 "Framerate"
LangString SET_OTHER 1036 "Autre options"
LangString SET_PAUSE 1036 "Pause quand inactif (ALT+TAB)"
LangString SET_UI 1036 "Mise à l'échelle de l'interface (recommandé)"
LangString SET_UPD 1036 "Options des mise à jour"
LangString SET_CHECKMODUPD 1036 "Vérifier les mises à jours des mods"
LangString SET_CHECKUPD 1036 "Vérifier les mises à jours"
LangString SET_FREQ 1036 "Fréquence des mises à jours"
LangString SET_DAYS 1036 "Jours"
LangString SET_WEEKS 1036 "Semaines"
LangString SET_HOURS 1036 "Heures"
LangString SET_ALWAYS 1036 "Toujours"

;Options
LangString OPTIONNAME_INSTMODE 1036 "Mode d'Installation"
LangString OPTIONNAME_GUIDE 1036 "Mode guidé - En cas de doute, sélectionnez cette option"
LangString OPTIONNAME_PRESET 1036 "Mode préréglé"
LangString OPTIONNAME_PRESETS 1036 "Préréglé"
LangString OPTIONNAME_ADVANCED 1036 "Montrez les options avancées"
LangString OPTIONNAME_PRESERVE 1036 "Conservez les paramètres individuels des mods (Si il y en a)"
LangString OPTIONNAME_OPTIMAL 1036 "Utiliser les paramètres optimaux du Mod Loader"
LangString OPTIONNAME_MANUAL 1036 "Configurer les paramètres manuellement"
LangString OPTIONNAME_FAILSAFE 1036 "Utiliser les paramètres sécurité intégrée du Mod Loader"
LangString OPTIONNAME_ICONSEL 1036 "Selectionner l'icône souhaitée pour l'exécutable de SADX."
LangString OPTIONNAME_ICON_DX 1036 "Icône de sauvegarde de la version gamecube de SADX"
LangString OPTIONNAME_ICON_HD 1036 "Icône HD par Lester LJSTAR"
LangString OPTIONNAME_ICON_SA1 1036 "SA1 box art icon par PkR"
LangString OPTIONNAME_ICON_VM 1036 "SA1 icône de VMU recréé par McAleeCh"
LangString OPTIONNAME_UPDATES 1036 "Vérifier les mises à jours"
LangString OPTIONNAME_NOUPDATES 1036 "Ne pas vérifier les mises à jours: Télécharger uniquement les fichiers nécessaire (si il y en a)"
LangString OPTIONNAME_WTITLE 1036 "Changer le titre de la fenêtre en $\"Sonic Adventure$\""
LangString OPTIONNAME_ICON 1036 "Choisissez une icône personnalisée pour sonic.exe"
LangString OPTIONNAME_MANAGERCLASSIC 1036 "Use the Classic Mod Manager"

;Messages
LangString MSG_UPDATES 1036 "Voulez-vous vérifier les mises à jour?$\r$\n$\r$\n$\r$\n$\r$\n$\r$\n\
Ce programme d'Installation, SADX Mod Loader et les mods sont tous en cours de réalisation. Les mises à jour corrigent des bugs et ajoutent de nouvelles fonctionnalités.$\r$\n\
$\r$\nGarder vos mods à jour pour recevoir du nouveau contenu et ainsi éviter d'éventuels problèmes."
LangString MSG_OFFLINE 1036 "Pour une Installation hors-ligne, choisissez la deuxième option."
LangString MSG_FOLDER_FOUND 1036 "Le programme d'installation a détecté le dossier suivant en tant qu'emplacement d'installation de SADX.$\r$\n\
$\r$\nPour l'installer dans un autre dossier, cliquez sur Parcourir et sélectionnez un autre dossier."
LangString MSG_FOLDER_NOTFOUND 1036 "Le programme d'installation n'a pas pu détecter l'emplacement d'installation de SADX. Veuillez choisir l'emplacement d'installation manuellement."
LangString MSG_DEFAULT 1036 "Vos paramètres du Mod Loader seront réinitialisés en failsafe par défaut.$\r$\n\
$\r$\nRésolution: 640x480$\r$\n\
Plein écran: activé$\r$\n\
Plein écran sans bordure: désactivé$\r$\n\
VSync: off$\r$\n\
Filtrage de Texture et mipmaps: désactivé$\r$\n\
Mise à l'echelle de l'affichage: désactivé$\r$\n\
Mise à l'échelle de l'interface: désactivé$\r$\n\
Arrière plan et mise à l'échelle FMV: étiré$\r$\n\
$\r$\n\
Continuer?"
LangString MSG_INSTALLERUPDATE 1036 "Mises à jour trouvées pour l'installateur! Effectuer une mise à jour automatique?$\r$\n\
$\r$\nLa nouvelle version se relancera automatiquement."
LangString MSG_FOUNDUPD 1036 "Mises à jour trouvées pour $UpdatesFound package(s)!$\r$\n\
$\r$\n$WhichUpdates$\r$\n\
Le dossier instdata a été vidé et les dernières versions des packages ci-dessus seront téléchargées à la place."
LangString MSG_CHECKUPDATES 1036 "Vérification des mises à jours du Mod Loader..."
LangString MSG_PROFILE 1036 "Sélectionnez votre type SADX . Vous pouvez utiliser un préréglage de mod commun ou sélectionner les mods manuellement."
LangString MSG_START 1036 "Bienvenue dans SADX Mod Installer!"
LangString MSG_WELCOME 1036 "Ce programme installera SADX Mod Loader par MainMemory et une sélection de mods de plusieurs créateurs.$\r$\n$\r$\nUne Installation par défaut avec tous les mods ajoutera environ 1,3 Go à la taille de votre dossier SADX.$\r$\n$\r$\nSi vous utilisez la version Web de ce programme d'Installation, assurez-vous qu'au moins 1 Go est disponible sur le disque dur avec sadx_setup.exe.$\r$\n$\r$\nCliquer sur Suivant pour continuer."
LangString MSG_COMPLETE 1036 "Installation terminée!"
LangString MSG_FINISH 1036 "Vous pouvez maintenant jouer à SADX avec les mods que vous avez installé.$\r$\n\
$\r$\nLes fichiers utilisés par ce programme d'Installation ont été enregistrés dans le dossier «instdata». Vous pouvez le conserver pour utiliser le programme d'Installation hors-ligne ultérieurement, ou le supprimer pour économiser de l'espace disque."

;Errors
LangString ERR_REQUIREDOS 1036 "Incompatible OS detected. This program requires Windows 7 SP1 or later to work properly.$\r$\nContinue?"
LangString ERR_NET_MISSING 1036 "The Steam conversion tool requires .NET Framework to be installed.$\r$\nPlease install .NET Framework manually and run the installer again."
LangString ERR_D_RESOURCES 1036 "Échec du téléchargement des resource data: $DownloadErrorCode. Continuer?"
LangString ERR_D_DSOUND 1036 "Échec du téléchargement: $DownloadErrorCode. Sound Overhaul ne fonctionnera pas correctement jusqu'à ce que dsound.dll soit placé dans le dossier principal de SADX."
LangString ERR_D_STEAM 1036 "Échec du téléchargement de Steam Achievements: $DownloadErrorCode. Continuer l'installation des autres mods?"
LangString ERR_D_DLCS 1036 "SA1 DLC échec du téléchargement: $DownloadErrorCode. Continuer l'installation des autres mods?"
LangString ERR_D_D3D8 1036 "Échec du téléchargement: $DownloadErrorCode. Lantern Engine ne fonctionnera pas correctement tant que d3d8.dll ne sera pas placé dans le dossier principal de SADX."
LangString ERR_D_DCCONV 1036 "Échec du téléchargement de DC Conversion: $DownloadErrorCode. Continuer l'installation des autres mods?"
LangString ERR_D_DX9 1036 "Échec du téléchargement: $DownloadErrorCode. Merci d'installer DirectX manuellement."
LangString ERR_D_VC2010 1036 "Échec du téléchargement: $DownloadErrorCode. Merci d'installer Visual C++ 2010 runtimes manuellement."
LangString ERR_D_VC2012 1036 "Échec du téléchargement: $DownloadErrorCode. Merci d'installer Visual C++ 2012 runtimes manuellement."
LangString ERR_D_VC2013 1036 "Échec du téléchargement: $DownloadErrorCode. Merci d'installer Visual C++ 2013 runtimes manuellement."
LangString ERR_D_VC2019 1036 "Échec du téléchargement: $DownloadErrorCode. Merci d'installer Visual C++ 2019 runtimes manuellement."
LangString ERR_D_LAUNCHER 1036 "Échec du téléchargement: $DownloadErrorCode. Merci de télécharger le launcher manuellement ou de lancer le programme d'installation à nouveau.$\r$\n\
$\r$\n\
Si vous obtenez régulièrement cette erreur, veuillez télécharger une version hors-ligne du programme d'installation"
LangString ERR_D_NET 1036 "Échec du téléchargement: $DownloadErrorCode. Merci d'installer .NET Framework manuellement."
LangString ERR_SIZE 1036 "Taille de $UpdateFilename non trouvé"
LangString ERR_FOLDER 1036 "Le programme d'installation a détecté que certains fichiers dans votre installation SADX sont manquants.$\r$\n\
$\r$\n\
Assurez-vous que vous installez le Mod Loader dans le dossier principal de SADX (où se trouve sonic.exe ou Sonic Adventure DX.exe)."
LangString ERR_2004CHECK 1036 "Le programme d'installation a détecté que SADX 2004 EXE a échoué au téléchargement ou à un checksum incorrect. $\r$\n\
Merci de vérifier votre connexion Internet et de relancer le programme d'installation.$\r$\n\
$\r$\n\
Si vous obtenez régulièrement cette erreur, veuillez télécharger une version hors-ligne du programme d'installation."
LangString ERR_MODLOADER 1036 "Le programme d'installation a détecté que le Mod Loader n'a pas été téléchargé ou installé correctement. $\r$\n\
Merci de vérifier votre connexion Internet et de relancer le programme d'installation.$\r$\n\
$\r$\n\
Si vous obtenez régulièrement cette erreur, veuillez télécharger une version hors-ligne du programme d'installation."
LangString ERR_DOWNLOAD_TOOLS 1036 "Le programme d'installation a détecté que certains outils n'ont pas pu être téléchargé. $\r$\n\
Merci de vérifier votre connexion Internet et de relancer le programme d'installation.$\r$\n\
$\r$\n\
Si vous obtenez régulièrement cette erreur, veuillez télécharger une version hors-ligne du programme d'installation."
LangString ERR_DOWNLOAD_2004BIN 1036 "Le programme d'installation a détecté que certains fichiers de SADX 2004 n'ont pas pu être téléchargé.$\r$\n\
Merci de vérifier votre connexion Internet et de relancer le programme d'installation.$\r$\n\
$\r$\n\
Si vous obtenez régulièrement cette erreur, veuillez télécharger une version hors-ligne du programme d'installation."
LangString ERR_MODDOWNLOAD 1036 "$ModName Échec du téléchargement: $DownloadErrorCode. Continuer l'installation des autres mods?"
LangString ERR_DOWNLOAD_FILE 1036 "Échec de téléchargement du fichier. Réessayer?"
LangString ERR_MODDATE 1036 "Impossible de vérifier la dernière date de modification du manifeste pour le mod $ModFilename. Assurez-vous que le mod a été installé correctement."
LangString ERR_MISSINGFILES 1036 "Le programme d'installation a détecté que certains fichiers sont manquants dans votre installation SADX.$\r$\n\
$\r$\n\
Assurez-vous que vous installez le Mod Loader dans le dossier principal de SADX (où se trouve sonic.exe ou Sonic Adventure DX.exe).$\r$\n\
$\r$\n\
Si vous avez des fichiers mixtes provenant de différentes versions de SADX, le programme d'installation peut ne pas détecter correctement votre version. Pour résoudre ce problème, faites une installation propre du jeu."
LangString ERR_DOWNLOAD_CANCEL_Q 1036 "Annuler le téléchargement?"
LangString ERR_DOWNLOAD_CANCEL_A 1036 "Annuler"
LangString ERR_DOWNLOAD_FATAL 1036 "Échec du téléchargement: $DownloadErrorCode. Veuillez relancer le programme d'installation.$\r$\n\
$\r$\n\
Si vous obtenez régulièrement cette erreur, veuillez télécharger une version hors-ligne du programme d'installation."
LangString ERR_DOWNLOAD_RETRY 1036 "Échec du téléchargement. Voulez-vous essayer à nouveau?"
LangString ERR_PERMISSION 1036 "Erreur lors de la définition des autorisations du dossier SADX!$\r$\n\
$\r$\n\
Veuillez accéder à votre dossier SADX et paramétrer les autorisations d'accès manuellement."
LangString ERR_CHRMODELS 1036 "Echec du téléchargement de CHRMODELS.dll: $DownloadErrorCode $\r$\n\
Merci de vérifier votre connexion Internet et de relancer le programme d'installation.$\r$\n\
$\r$\n\
Si vous obtenez régulièrement cette erreur, veuillez télécharger une version hors-ligne du programme d'installation."
LangString ERR_CHRMODELSBAD 1036 "Le programme d'installation a détecté un CHRMODELS.DLL modifié. $\r$\n\
$\r$\n\
Il sera sauvegardé en tant que CHRMODELS.bak et remplacé par un CHRMODELS.DLL standard. Ceci est nécessaire pour que le Mod Loader puisse fonctionner correctement.$\r$\n\
$\r$\n\
Si vous utilisez d'anciens mods qui modifient CHRMODELS.DLL, veuillez noter qu'ils ne sont pas pris en charge par le Mod Loader."
LangString ERR_EXEMD5 1036 "Le hash MD5 du sonic.exe téléchargé est incorrect. Veuillez relancer le programme d'installation.$\r$\n\
$\r$\n\
Si vous obtenez régulièrement cette erreur, veuillez télécharger une version hors-ligne du programme d'installation."

;Mod descriptions
LangString DESC_ADD_ALL 1036 "Ces mods introduisent des changements de gameplay et des améliorations non visuelles.$\r$\n\
$\r$\nSélectionnez les mods que vous souhaitez installer."
LangString DESC_ADD_SND 1036 "Installe Sound Overhaul pour améliorer la qualité et le volume du son et restaurer les sons manquants"
LangString DESC_ADD_DLCS 1036 "Installe le mod DLCs pour ajouter des défis de Sonic Adventure provenant de la version Dreamcast"
LangString DESC_ADD_SUPER 1036 "Installe le mod Super Sonic pour pouvoir se transformer en Super Sonic après avoir fini l'histoire principale"
LangString DESC_REMOVEMODS 1036 "Supprime tous les mods du dossier mods avant de continuer.$\r$\n\
$\r$\n\
ATTENTION! C'est une option dangereuse car elle supprimera tous vos mods.$\r$\n\
Utilisez-la uniquement si vous rencontrez des problèmes avec les mods actuels et que vous souhaitez recommencer."
LangString DESC_MODLOADER 1036 "Installe ou met à jour SADX Mod Loader (requis).$\r$\n\
$\r$\n\
La version Steam de SADX sera convertie en version 2004 avant l'installation."
LangString DESC_LAUNCHER 1036 "Installe SADX Launcher (requis).$\r$\n\
$\r$\n\
SADX Launcher est un outil pour modifier les paramètres du Mod Loader et configurer les contrôles de Input Mod."
LangString DESC_NET 1036 "Installe ou met à jour .NET Framework, qui est requis pour que SADX Mod Manager fonctionne correctement. $\r$\n\
$\r$\n\
Le programme d'installation vérifie si .NET Framework est déjà installé avant de le télécharger."
LangString DESC_RUNTIME 1036 "Installez/met à jour les runtimes Visual C ++ 2010, 2012, 2013 et 2015/2017/2019, qui sont nécessaires pour que les mods basés sur les DLL fonctionnent correctement. $\r$\n\
$\r$\n\
Le programme d'installation vérifie si les runtimes Visual C ++ sont déjà installés avant de les télécharger."
LangString DESC_DIRECTX 1036 "Met à jour les runtimes DirectX, qui sont nécessaires pour SADX et le mod Lantern Engine. $\r$\n\
$\r$\n\
Le programme d'installation vérifie si DirectX 9.0c est déjà installé avant de le télécharger."
LangString DESC_PERMISSIONS 1036 "Prend le contrôle du dossier SADX et défini des autorisations récursives. Cela empêche des erreurs d'autorisation lors de l'activation ou de la désactivation du Mod Loader sans droits administrateur.$\r$\n\
$\r$\n\
Le correctif d'autorisation s'exécute uniquement lorsque SADX est installé dans le dossier Program Files."
LangString DESC_ADXAUDIO 1036 "Musiques et voix au format ADX de meilleure qualité provenant de la version Dreamcast. $\r$\n\
$\r$\n\
Ce n'est pas nécessaire lors de l'installation de la version Steam car elle contient déjà les voix et les musiques au format ADX."
LangString DESC_SADXFE 1036 "Un mod contenant diverses corrections de bugs et améliorations de la version PC originale de SADX.$\r$\n\
$\r$\n\
Recommandé!"
LangString DESC_INPUTMOD 1036 "Refonte complète du système d'input de SADX avec reconfiguration des contrôles. Résout les problèmes des manettes utilisant XInput et DInput. $\r$\n\
$\r$\n\
Recommandé!"
LangString DESC_SMOOTHCAM 1036 "Rend la caméra plus fluide lors d'une utilisation à la première personne.$\r$\n\
$\r$\n\
Utillisable avec Input Mod pour des mouvements encore plus fluide."
LangString DESC_FRAMELIMIT 1036 "Un limiteur de framerate plus précis.$\r$\n\
$\r$\n\
Résout les problèmes de framerate (comme le jeu fonctionnant à 63 fps au lieu de 60) sur certains ordinateurs.$\r$\n\
$\r$\n\
Rend le jeu plus fluide, mais peut avoir un léger impact sur les performances des anciens systèmes."
LangString DESC_PAUSEHIDE 1036 "Masque le menu Pause lorsque vous appuyez sur X + Y comme sur Dreamcast. Le DC Conversion inclut déjà cela."
LangString DESC_ONIONBLUR 1036 "Ajoute un effet de «mouvements flou» aux pieds de Sonic (comme dans la version japonaise Dreamcast de SA1) et aux queues de Tails."
LangString DESC_DLCS 1036 "Contenu téléchargeable exclusif à la version Dreamcast recréé en tant que mod pour SADX. $\r$\n\
$\r$\n\
Voir la configuration du mod pour plus de détails."
LangString DESC_STEAM 1036 "Prise en charge des succès Steam dans la version 2004. $\r$\n\
$\r$\n\
Vous devez posséder SADX sur Steam pour que ce mod fonctionne."
LangString DESC_LANTERN 1036 "Un mod pour SADX qui réimplémente l'éclairage de la version Dreamcast de Sonic Adventure. $\r$\n\
$\r$\n\
Recommandé!"
LangString DESC_DCMODS 1036 "Niveaux Dreamcast de Sonic Adventure, modèles d'objets, effets, boss, GUI, etc."
LangString DESC_SNDOVERHAUL 1036 "Corrige plusieurs problèmes de son et remplace la majorité des effets sonores par des sons de meilleure qualité extraits de la version Dreamcast."
LangString DESC_HDGUI 1036 "Remplace la plupart des éléments de l'interface graphique, tels que le HUD, les boutons, les icônes de la vie, les menus, etc. par des assets avec des résolutions plus élevés."
LangString DESC_TIMEOFDAY 1036 "Vous pouvez changer l'heure de la journée en prenant le train entre Station Square et les Mystic Ruins."
LangString DESC_DCCONV 1036 "Mods qui ajoutent / remplacent divers assets du jeu et rendent SADX plus semblable à la version Dreamcast de SA1."
LangString DESC_BUGFIXES 1036 "Mods qui corrigent des problèmes ou ajoutent des améliorations techniques sans modifications majeures des assets ou du gameplay."
LangString DESC_SA1CHARS 1036 "Modèles de personnages de la version Dreamcast"
LangString DESC_MODLOADERSTUFF 1036 "Diverses dépendances nécessaires pour que le Mod Loader et les mods fonctionnent correctement."
LangString DESC_SUPERSONIC 1036 "Permet de se transformer en Super Sonic après avoir fini la dernière histoire."
LangString DESC_CCEF 1036 "Empêche la caméra de se réinitialiser au chargement d'un level. Inclus dans SADXFE. $\r$\n\
$\r$\n\
Utile pour les speedrunners qui n'utilisent pas SADXFE."
LangString DESC_ENHANCEMENTS 1036 "Mods qui ajoutent de nouvelles fonctionnalités de gameplay ou améliore SADX vanilla."
LangString DESC_IDLECHATTER 1036 "Appuyez sur Z pour entendre ce que le personnage dit sur le niveau."
LangString DESC_SADXWTR 1036 "Adds alternative water textures to some levels and re-enables the ocean wave effect in Emerald Coast."
LangString DESC_MANAGERCLASSIC 1036 "Alternative version of the SADX Mod Manager that works on Windows XP."

;Other descriptions
LangString DESC_DESC 1036 "Déscription"
LangString DESC_PRESERVE 1036 "L'activation de cette fonctionnalité préservera config.ini lors de la mise à jour et la réinstallation des mods."
LangString DESC_ICON 1036 "L'exécutable sera patché avec l'une des icônes personnalisées disponibles dans ce programme d'installation."
LangString DESC_DCMODS_ALL 1036 "Ces mods donnent à SADX une apparence plus proche de la version originale Dreamcast de SA1.$\r$\n\
Mods inclus: Onion Blur, Dreamcast Conversion, DC Characters, DLCs, Sound Overhaul, HD GUI, Lantern Engine, Time of Day, Input Mod, Idle Chatter, Smooth Camera, Frame Limit, Super Sonic."
LangString DESC_SADX_ALL 1036 "Expérience SADX vanilla améliorée.$\r$\n\
Mods inclus: SADX: Fixed Edition, Onion Blur, Enhanced Emerald Coast, Sound Overhaul, HD GUI, Time of Day, Input Mod, Idle Chatter, Pause Hide, Smooth Camera, Frame Limit, Super Sonic."
LangString DESC_MIN_ALL 1036 "Seuls les mods essentiels / compatibles speedrun seront installés.$\r$\n\
Mods inclus: Frame Limit."
LangString DESC_CUSTOM_ALL 1036 "Installation personnalisée.$\r$\n\
Vous pourrez sélectionner les mods à installer et configurer les paramètres du Mod Loader."
LangString DESC_GUIDE_ALL 1036 "Mode guidé.$\r$\n\
Le programme d'installation affichera des captures d'écran avec comparaison pour vous aider à décider quel mod vous souhaitez installer."
LangString DESC_MANUAL 1036 "Vous pourrez sélectionner la résolution, la fréquence d'images, clipping, VSync et d'autres options manuellement."
LangString DESC_OPTIMAL 1036 "Le programme d'installation détectera la résolution de votre écran et configurera le jeu et le Mod Loader pour une meilleure qualité visuelle."
LangString DESC_FAILSAFE 1036 "Utilisez ceci si le jeu plante au démarrage.$\r$\n\
La résolution sera réinitialisée en plein écran 640 x 480 et toutes les améliorations telles que le plein écran sans bordure seront désactivées."
LangString DESC_WINDOWTITLE 1036 "Le titre de la fenêtre du jeu sera défini sur $\"Sonic Adventure$\" au lieu de $\"SonicAdventureDXPC$\".$\r$\n\
$\r$\n\
ATTENTION! N'ACTIVEZ PAS CETTE OPTION SI VOUS COMPTEZ UTILISER FUSION'S CHAO EDITOR!"
LangString DESC_MOREMODS 1036 "Plus de mods SADX"
LangString DESC_SHORTCUTS 1036 "Créer des raccourcis sur le bureau"
LangString DESC_RUNSADX 1036 "Démarre Sonic Adventure DX"
LangString DESC_RUNLAUNCHER 1036 "Modifier les commandes et les paramètres avec SADX Launcher"
;The resources below are only available in English. Please indicate that in your translation
LangString DESC_DREAMCASTIFY 1036 "Dreamcastify - un site web concernant les downgrades de SADX (Anglais uniquement)"
LangString DESC_DISCORD 1036 "Rejoignez le discord de SADX modding (Anglais uniquement)"

;Detail output
LangString DE_SND 1036 "Conversion des soundbanks au format SADX 2004 (Cela peut prendre un moment)..."
LangString DE_MOV 1036 "Conversion des SFD movies en MPG (Cela peut prendre un moment)..."
LangString DE_VOICES 1036 "Téléchargement des voix additionnelles..."
LangString DE_INSTDATA 1036 "Création du dossier instdata..."
LangString DE_7Z 1036 "Copie 7Z..."
LangString DE_OWNER 1036 "Prise de contrôle du dossier SADX: $Permission1"
LangString DE_PERM 1036 "Permissions du dossier SADX: $Permission2"
LangString DE_REALL 1036 "Suppression des mods..."
LangString DE_PRGFILES 1036 "Vérification si SADX est dans Program Files..."
LangString DE_RECU 1036 "SADX est dans Program Files. Définition des autorisations de dossier récursif..."
LangString DE_2004FOUND 1036 "Version 2004 détéctée (sonic.exe)"
LangString DE_CHRM 1036 "CHRMODELS.DLL trouvée.  MD5..."
LangString DE_MANIFEST 1036 "Date de manifeste pour $ModFilename: $2-$1-$0T$4:$5:$6Z"
LangString DE_PREVIOUS_DX 1036 "Chargement des options précédentes de SADX..."
LangString DE_PREVIOUS_ML 1036 "Chargement des options précédentes du Mod Loader..."
LangString DE_TAKEOWN 1036 "Prise de contrôle de $R9: $0"
LangString DE_PERMSET1 1036 "Définition des permissions pour $R9 (1): $0"
LangString DE_PERMSET2 1036 "Définition des permissions pour $R9 (2): $0"
LangString DE_PERMSET3 1036 "Définition des permissions pour $R9 (3): $0"
LangString DE_E_GENERIC 1036 "Extraction de $Modname..."
LangString DE_B_CHR 1036 "Sauvegarde de l'ancien CHRMODELS..."
LangString DE_D_CHR 1036 "Téléchargement de CHRMODELS... (~1.7MB)"
LangString DE_E_CHR 1036 "Extraction de CHRMODELS..."
LangString DE_C_CHR 1036 "Vérification intégrale de CHRMODELS..."
LangString DE_B_EXE 1036 "Sauvegarde de sonic.exe..."
LangString DE_E_EXE 1036 "Extraction du fichier EXE SADX 2004..."
LangString DE_C_EXE 1036 "Vérification MD5 sde sonic.exe..."
LangString DE_E_BIN 1036 "Extraction de SADX 2004 binaries..."
LangString DE_C_BIN 1036 "Vérication de SADX 2004 binaries..."
LangString DE_E_ML 1036 "Extraction de SADX Mod Loader..."
LangString DE_I_ML 1036 "Installation/Mise à jour de SADX Mod Loader..."
LangString DE_EXEFOUND 1036 "sonic.exe utilisable trouvée, procédure de l'installation du Mod Loader."
LangString DE_EXEUNK 1036 "sonic.exe inconnu, procéderure de l'installation de l'EXE US + Mod Loader."
LangString DE_DETECT 1036 "Détection du type d'installation de SADX."
LangString DE_2010FOUND 1036 "2010 version détectée (Sonic Adventure DX.exe)"
LangString DE_C_2010 1036 "Vérification de l'intégralité de l'installation de SADX 2010..."
LangString DE_E_TOOLS 1036 "Extraction de tools..."
LangString DE_C_TOOLS 1036 "Vérification de tools..."
LangString DE_E_SCR 1036 "Extraction des scripts..."
LangString DE_E_VOICES 1036 "Installation des voix additionnelles..."
LangString DE_SAVE 1036 "Copie des sauvegardes de SADX 2010 dans le dossier SAVEDATA de SADX 2004..."
LangString DE_DPI 1036 "Ajout d'exception d'optimisations DPI Elevé / plein écran pour sonic.exe..."
LangString DE_FSOPT 1036 "Ajout de l'optimisation plein écran et exception DPI pour sonic.exe..."
LangString DE_FSOPTNOTFOUND 1036 "Optimisations plein écran non présente, Ajout de l'exception DPI..."
LangString DE_CLEANUP 1036 "Nettoyage..."
LangString DE_E_LAUNCHER 1036 "Extraction SADX Launcher..."
LangString DE_CHECKNET 1036 " la version de .NET Framework..."
LangString DE_E_NET 1036 "Installation de .NET Framework..."
LangString DE_NETPRESENT 1036 ".NET Framework est déjà installé. ($NetFrameworkVersion)."
LangString DE_C_VCC 1036 "Vérification de Visual C++ runtimes et Installation de ce dernier si nécessaire..."
LangString DE_E_VC2010 1036 "Extraction de Visual C++ 2010 runtime..."
LangString DE_I_VC2010 1036 "Installation de Visual C++ 2010 runtime..."
LangString DE_E_VC2012 1036 "Extraction de Visual C++ 2012 runtime..."
LangString DE_I_VC2012 1036 "Installation de Visual C++ 2012 runtime..."
LangString DE_E_VC2013 1036 "Extraction de Visual C++ 2013 runtime..."
LangString DE_I_VC2013 1036 "Installation de Visual C++ 2013 runtime..."
LangString DE_E_VC2019 1036 "Extraction de Visual C++ 2019 runtime..."
LangString DE_I_VC2019 1036 "Installation de Visual C++ 2019 runtime..."
LangString DE_C_DX9 1036 "Vérification de DirectX 9.0c and Installation de if necessary..."
LangString DE_I_DX9_1 1036 "Lancement de DirectX Setup (1)..."
LangString DE_I_DX9_2 1036 "Lancement de DirectX Setup (2)..."
LangString DE_B_SADXFE 1036 "Sauvegarde des paramètres de SADXFE..."
LangString DE_R_SADXFE 1036 "Restauration de la configuration de SADXFE..."
LangString DE_B_INPUT 1036 "Sauvegarde des paramètres de Input Mod..."
LangString DE_R_INPUT 1036 "Restauration de la configuration de Input Mod..."
LangString DE_B_GAMEC 1036 "Sauvegarde de la base de donnée du contrôleur de jeu..."
LangString DE_R_GAMEC 1036 "Restauration de la base de donnée du contrôleur de jeu..."
LangString DE_B_DCCONV 1036 "Sauvegarde des paramètres de Dreamcast Conversion..."
LangString DE_E_DCCONV 1036 "Extraction de Dreamcast Conversion..."
LangString DE_R_DCCONV 1036 "Restauration de la configuration de Dreamcast Conversion..."
LangString DE_DCTITLE_ON 1036 "Activation de DC Conversion window title..."
LangString DE_DCTITLE_OFF 1036 "Désactivation de DC Conversion window title..."
LangString DE_WTR_ON 1036 "Activation de SADX Style Water..."
LangString DE_WTR_OFF 1036 "Désactivation de SADX Style Water..."
LangString DE_B_SA1CHARS 1036 "Sauvegarde des paramètres de Dreamcast Characters..."
LangString DE_R_SA1CHARS 1036 "Restauration de la configuration de Dreamcast Characters..."
LangString DE_B_DLCS 1036 "Sauvegarde des paramètres de Dreamcast DLCs..."
LangString DE_E_DLCS 1036 "Extraction de SA1 DLCs..."
LangString DE_R_DLCS 1036 "Restauration Dreamcast DLCs config..."
LangString DE_E_STEAM 1036 "Extraction de Steam Achievements..."
LangString DE_E_DSOUND 1036 "Ajout de DirectSound DLL wrapper..."
LangString DE_I_DSOUNDINI 1036 "Ajout de dsound.ini..."
LangString DE_MODORDER 1036 "Configuration de l'ordre des mods..."
LangString DE_B_MLINI 1036 "Sauvegarde de SADXModLoader.ini..."
LangString DE_I_MLINI 1036 "Ajout de SADXModLoader.ini..."
LangString DE_B_SADXINI 1036 "Sauvegarde de sonicDX.ini..."
LangString DE_I_SADXINI 1036 "Ajout de sonicDX.ini..."
LangString DE_E_RESOURCES 1036 "Extraction de resource data..."
LangString DE_I_RESOURCES 1036 "Patching sonic.exe avec des ressources personnalisées..."
LangString DE_B_SADXWTR 1036 "Backing up SADX Style Water settings..."
LangString DE_R_SADXWTR 1036 "Restoring SADX Style Water settings..."

;Mod names (generic installer)
LangString MOD_SADXFE 1036 "SADXFE"
LangString MOD_INPUT 1036 "Input Mod"
LangString MOD_FRAME 1036 "Frame Limiter"
LangString MOD_CCEF 1036 "Camera Code Error Fix"
LangString MOD_SA1CHARS 1036 "Dreamcast Characters"
LangString MOD_LANTERN 1036 "Lantern Engine"
LangString MOD_SMOOTHCAM 1036 "Smooth Camera"
LangString MOD_ONION 1036 "Onion Skin Blur"
LangString MOD_IDLE 1036 "Idle Chatter"
LangString MOD_PAUSE 1036 "Pause Hide"
LangString MOD_SUPER 1036 "Super Sonic"
LangString MOD_TIME 1036 "Time Of Day"
LangString MOD_ADX 1036 "ADX Audio"
LangString MOD_SND 1036 "Sound Overhaul"
LangString MOD_HDGUI 1036 "HD GUI"
LangString MOD_SADXWTR 1036 "SADX Style Water"

;inetc stuff
LangString INETC_DOWNLOADING 1036 "Téléchargement de %s" 
LangString INETC_CONNECT 1036 "Connexion ..."
LangString INETC_SECOND 1036 "seconde"
LangString INETC_MINUTE 1036 "minute"
LangString INETC_HOUR 1036 "heure"
LangString INETC_PLURAL 1036 "s" 
LangString INETC_PROGRESS 1036 "%dkB (%d%%) of %dkB @ %d.%01dkB/s"
LangString INETC_REMAINING 1036 "(%d %s%s restantes)"

;Unused strings
${LangFileString} MUI_TEXT_WELCOME_INFO_TITLE " "
${LangFileString} MUI_TEXT_WELCOME_INFO_TEXT " "
${LangFileString} MUI_TEXT_FINISH_TITLE "  "
${LangFileString} MUI_TEXT_FINISH_SUBTITLE "  "
${LangFileString} MUI_TEXT_ABORT_TITLE " "
${LangFileString} MUI_TEXT_ABORT_SUBTITLE " "
${LangFileString} MUI_TEXT_FINISH_INFO_TITLE " "
${LangFileString} MUI_TEXT_FINISH_INFO_TEXT " "
${LangFileString} MUI_TEXT_FINISH_INFO_REBOOT " "
${LangFileString} MUI_TEXT_FINISH_REBOOTNOW " "
${LangFileString} MUI_TEXT_FINISH_REBOOTLATER " "
${LangFileString} MUI_TEXT_FINISH_RUN " "
${LangFileString} MUI_TEXT_FINISH_SHOWREADME " "
${LangFileString} MUI_INNERTEXT_LICENSE_BOTTOM_CHECKBOX " "
${LangFileString} MUI_INNERTEXT_LICENSE_BOTTOM_RADIOBUTTONS " "

;Strings added in the 2022 update
LangString INSTTYPE_STEAMCONV 1036 "Steam to 2004"
LangString INSTTYPE_REDIST 1036 "Redists"
LangString DESC_STEAMCONV_ALL 1036 "Use this option if you want to convert a Steam installation to the 2004 version with the Mod Loader.$\r$\n\
No mods will be installed in this mode.$\r$\n\
If you also want to install mods, select a different profile."
LangString DESC_REDIST_ALL 1036 "Use this option to install DirectX, Visual C++ runtimes and .NET Framework.$\r$\n\
Your SADX installation will not be affected.$\r$\n\
Nothing else will be installed in this mode. If you also want to install mods, select a different profile."

;Strings added or renamed in the 2024 update
LangString DE_E_NETF 1036 "Installing .NET Framework..."
LangString D_NETF 1036 "Downloading .NET Framework Setup..."
LangString DE_NETFPRESENT 1036 ".NET Framework is already installed ($NetFrameworkVersion)."
LangString PROFILENAME_STEAMCONV 1036 "Steam to 2004"
LangString PROFILENAME_REDIST 1036 "Redists"
LangString DESC_OPTION_MANAGERCLASSIC 1036 "Use the alternative SADX Mod Manager instead of the modern SA Manager. Required on Windows XP."
LangString DE_B_ONION 1036 "Backing up Onion Blur settings..."
LangString DE_R_ONION 1036 "Restoring Onion Blur settings..."
LangString DESC_NETF 1036 "Install or update .NET Framework, which is required for conversion tools to work properly. $\r$\n\
$\r$\n\
The installer checks if .NET Framework is already installed before downloading it."
LangString D_MODMANAGER 1036 "Downloading SADX Mod Manager..."
LangString DE_E_MANAGER 1036 "Extracting the Mod Manager..."