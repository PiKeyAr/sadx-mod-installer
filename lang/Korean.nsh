﻿!insertmacro LANGFILE "Korean" = "한국어" =

;General strings for NSIS MUI
${LangFileString} MUI_TEXT_LICENSE_TITLE "사용권 계약"
${LangFileString} MUI_TEXT_LICENSE_SUBTITLE "다음 사용권 계약을 자세히 읽어 주십시오."
${LangFileString} MUI_INNERTEXT_LICENSE_TOP "나머지 계약 내용을 보려면 스크롤을 내리거나 <PAGE DOWN> 키를 누르십시오."
${LangFileString} MUI_INNERTEXT_LICENSE_BOTTOM "인스톨러 사용 전에 소닉 어드벤처 DX가 설치된 폴더를 백업해 주십시오. '동의' 버튼을 눌러 다음으로 진행합니다."
${LangFileString} MUI_INNERTEXT_LICENSE_BOTTOM_CHECKBOX " "
${LangFileString} MUI_TEXT_COMPONENTS_TITLE "구성 요소 선택"
${LangFileString} MUI_TEXT_COMPONENTS_SUBTITLE "설치하고자 하는 모드와 기능을 선택해 주세요."
${LangFileString} MUI_INNERTEXT_COMPONENTS_DESCRIPTION_TITLE "설명"
${LangFileString} MUI_INNERTEXT_COMPONENTS_DESCRIPTION_INFO "자세한 설명을 보려면 마우스 포인터를 구성 요소 위로 올려주세요."
${LangFileString} MUI_TEXT_DIRECTORY_TITLE "설치 위치 선택"
${LangFileString} MUI_TEXT_DIRECTORY_SUBTITLE "소닉 어드벤처 DX가 설치된 폴더를 선택해 주세요."
${LangFileString} MUI_TEXT_INSTALLING_TITLE "설치하는 중"
${LangFileString} MUI_TEXT_INSTALLING_SUBTITLE "SADX 모드 인스톨러가 작업하는 동안 기다려 주십시오."
${LangFileString} MUI_BUTTONTEXT_FINISH "&완료"

;Install type names
LangString INSTTYPE_DC 1042 "드림캐스트 모드"
LangString INSTTYPE_SADX 1042 "소닉 어드벤처 DX + 향상된 기능"
LangString INSTTYPE_MIN 1042 "최소 설치/순정"
LangString INSTTYPE_STEAMCONV 1042 "Steam 버전을 2004 버전으로 변환"
LangString INSTTYPE_REDIST 1042 "런타임만 설치"

;Install profile names
LangString PROFILENAME_DC 1042 "드림캐스트"
LangString PROFILENAME_SADX 1042 "소닉 어드벤처 DX 향상"
LangString PROFILENAME_MIN 1042 "최소 설치"
LangString PROFILENAME_CUSTOM 1042 "사용자 지정"
LangString PROFILENAME_STEAMCONV 1042 "Steam 버전을 2004 버전으로 변환"
LangString PROFILENAME_REDIST 1042 "런타임만 설치"

;Section names
LangString SECTIONNAME_REMOVEMODS 1042 "현재 설치된 모드를 전부 삭제"
LangString SECTIONNAME_PERMISSIONS 1042 "게임이 설치된 폴더의 사용 권한 확인 및 수정"
LangString SECTIONNAME_MODLOADER 1042 "SADX 모드로더 (MainMemory, x-hax 제작)"
LangString SECTIONNAME_LAUNCHER 1042 "SADX 런처 (PkR 제작)"
LangString SECTIONNAME_DEPENDENCIES 1042 "모드로더 필수 프로그램"
LangString SECTIONNAME_VCC 1042 "Visual C++ 런타임"
LangString SECTIONNAME_DX9 1042 "DirectX 9.0c"
LangString SECTIONNAME_BUGFIXES 1042 "버그 수정 및 조작/카메라 관련 모드"
LangString SECTIONNAME_SADXFE 1042 "SADX: Fixed Edition (SonicFreak94 제작)"
LangString SECTIONNAME_FRAMELIMIT 1042 "프레임 제한 모드 (SonicFreak94 제작)"
LangString SECTIONNAME_DCMODS 1042 "드림캐스트 모드"
LangString SECTIONNAME_DCCONV 1042 "드림캐스트 컨버전 모드 (PkR 제작)"
LangString SECTIONNAME_SA1CHARS 1042 "드림캐스트 캐릭터 팩 (ItsEasyActually 제작)"
LangString SECTIONNAME_LANTERN 1042 "랜턴 엔진 모드 (SonicFreak94 제작)"
LangString SECTIONNAME_DLCS 1042 "소닉 어드벤처 DLC 모드 (PkR 제작)"
LangString SECTIONNAME_ENH 1042 "향상된 게임플레이 기능을 제공하는 모드"
LangString SECTIONNAME_SMOOTH 1042 "부드러운 카메라 모드 (SonicFreak94 제작)"
LangString SECTIONNAME_ONION 1042 "어니언 스킨 블러 모드 (SonicFreak94 제작)"
LangString SECTIONNAME_IDLE 1042 "수다쟁이 잡담 모드 (SonicFreak94 제작)"
LangString SECTIONNAME_PAUSE 1042 "일시정지 메뉴 숨기기 모드 (SonicFreak94 제작)"
LangString SECTIONNAME_STEAM 1042 "Steam 도전과제 지원 모드 (MainMemory 제작)"
LangString SECTIONNAME_SUPER 1042 "슈퍼 소닉 모드 (Kell, SonicFreak94 제작)"
LangString SECTIONNAME_TIME 1042 "시간의 흐름 모드 (PkR 제작)"
LangString SECTIONNAME_ADX 1042 "ADX 음성 및 음악 (2004 버전용)"
LangString SECTIONNAME_SND 1042 "사운드 오버홀 3 (PkR 제작)"
LangString SECTIONNAME_HDGUI 1042 "HD GUI 2 (PkR 외 다수 제작)"
LangString SECTIONNAME_SADXWTR 1042 "SADX 스타일 물 텍스처 (PkR, SteG 제작)"
LangString SECTIONNAME_MANAGERCLASSIC 1042 "SADX 모드 매니저 클래식 (PkR 제작)"

;Guide Mode descriptions and checkboxes
LangString GUIDE_INFO_LANTERN 1042 "랜턴 엔진 모드는 소닉 어드벤처 DX PC판에서 드림캐스트판의 광원 시스템을 구현합니다. 랜턴 엔진은 캐릭터 모델의 과도한 물광 표현을 제거하고 스테이지, 오브젝트, 캐릭터의 조명을 더욱 생동감 있게 바꾸어 줍니다."
LangString GUIDE_INFO_DCCONV 1042 "드림캐스트 컨버전 모드는 게임을 전체적으로 개편해서 오리지널 드림캐스트판처럼 바꾸어 줍니다. 많은 버그들을 수정하고 드림캐스트판의 스테이지, 텍스처, 오브젝트 모델, 특수효과, 브랜딩을 복원합니다."
LangString GUIDE_INFO_DXWTR 1042 "드림캐스트 컨버전 모드는 소닉 어드벤처 DX와 유사하되 품질과 애니메이션 효과가 개선된 별도의 물 텍스처를 제공합니다."
LangString GUIDE_INFO_SA1CHARS 1042 "드림캐스트 캐릭터 팩은 PC판에서 드림캐스트판의 캐릭터 모델을 사용할 수 있는 모드입니다. 소닉 어드벤처 DX의 캐릭터 모델링이 퀄리티가 더 좋지만, 옛날 드림캐스트판의 오리지널 느낌이나 감성을 살리려고 적용하는 사람들도 꽤 있습니다."
LangString GUIDE_INFO_ONION 1042 "어니언 스킨 블러 모드는 소닉의 발과 테일즈의 꼬리에 '모션 블러'효과를 재현해 줍니다. 이 효과는 원래 드림캐스트 일본판에 있었던 효과였지만, 이후에 발매된 버전에서는 삭제되었습니다."
LangString GUIDE_INFO_HDGUI 1042 "HD GUI 2 모드는 대부분의 그래픽 UI 텍스처들(메뉴, HUD, 아이템 캡슐 아이콘 등)을 고해상도 커스텀 텍스처로 변경합니다."
LangString GUIDE_INST_LANTERN 1042 "랜턴 엔진 모드 설치 (SonicFreak94 제작)"
LangString GUIDE_INST_DCCONV 1042 "드림캐스트 컨버전 모드 설치 (PkR 제작)"
LangString GUIDE_INST_DXWTR 1042 "SADX 스타일 물 텍스처 적용 (SteG 제작)"
LangString GUIDE_INST_SA1CHARS 1042 "드림캐스트 캐릭터 팩 설치 (ItsEasyActually 제작)"
LangString GUIDE_INST_ONION 1042 "어니언 스킨 블러 모드 설치 (SonicFreak94 제작)"
LangString GUIDE_INST_HDGUI 1042 "HD GUI 2 모드 설치 (PkR, Dark Sonic, Sonikko, SPEEPSHighway 제작)"

;Download strings
LangString D_DLCS 1042 "드림캐스트 DLC 콘텐츠 다운로드 중..."
LangString D_DCCONV 1042 "드림캐스트 컨버전 모드 다운로드 중..."
LangString D_STEAM 1042 "Steam 도전과제 지원 모드 다운로드 중..."
LangString D_RESOURCES 1042 "커스텀 리소스 다운로드 중..."
LangString D_DX9 1042 "DirectX 설치 프로그램 다운로드 중..."
LangString D_VC2019 1042 "Visual C++ 런타임 설치 프로그램 다운로드 중..."
LangString D_LAUNCHER 1042 "SADX 런처 다운로드 중..."
LangString D_NET 1042 ".NET 런타임 설치 프로그램 다운로드 중..."
LangString D_NETF 1042 ".NET Framework 설치 프로그램 다운로드 중..."
LangString D_STEAMTOOLS 1042 "SADX Steam용 도구 다운로드 중..."
LangString D_MODLOADER 1042 "SADX 모드로더 다운로드 중..."
LangString D_MODMANAGER 1042 "SADX 모드 매니저 다운로드 중..."
LangString D_FILELIST 1042 "파일 리스트 다운로드 중..."
LangString D_INSTCHK 1042 "인스톨러 업데이트 확인 중..."
LangString D_GENERIC 1042 "$Modname 다운로드 중..."
LangString D_UPDATE 1042 "$UpdateFilename 체크 중..."
LangString D_INSTALLER 1042 "인스톨러 다운로드 중..."
LangString D_INSTALLER_R 1042 "새로운 버전을 실행하는 중..."

;Headers
LangString MISC_INSTALLER 1042 "SADX 모드 인스톨러"
LangString HEADER_GUIDE_TITLE 1042 "모드 선택 가이드"
LangString HEADER_GUIDE_TEXT 1042 "이 모드를 설치하려면 체크박스에 체크해 주세요. 스크린샷을 클릭하면 자세한 비교를 볼 수 있습니다."
LangString HEADER_ADD_TITLE 1042 "부가적인 모드"
LangString HEADER_ADD_TEXT 1042 "설치하고 싶은 모드의 체크박스에 체크해 주십시오."
LangString HEADER_UPDATES_TITLE 1042 "업데이트 확인"
LangString HEADER_UPDATES_TEXT 1042 "모드를 최신 상태로 유지하는 편이 좋습니다."
LangString HEADER_ICON_TITLE 1042 "게임 아이콘 선택"
LangString HEADER_ICON_TEXT 1042 "게임 창에 표시할 아이콘을 선택해 주십시오."
LangString HEADER_TYPE_TITLE 1042 "설치 유형"
LangString HEADER_TYPE_TEXT 1042 "설치 유형을 선택해 주십시오."

;Options
LangString OPTIONNAME_INSTMODE 1042 "설치 유형"
LangString OPTIONNAME_GUIDE 1042 "가이드 모드 - 잘 모르겠으면 이 유형 선택"
LangString OPTIONNAME_PRESET 1042 "프리셋 모드"
LangString OPTIONNAME_PRESETS 1042 "프리셋"
LangString OPTIONNAME_ADVANCED 1042 "고급 옵션 보기"
LangString OPTIONNAME_PRESERVE 1042 "모드 개별 설정 유지(설치된 모드가 있으면)"
LangString OPTIONNAME_OPTIMAL 1042 "모드로더 최적 설정 사용"
LangString OPTIONNAME_FAILSAFE 1042 "모드로더 안전 모드 사용"
LangString OPTIONNAME_ICONSEL 1042 "소닉 어드벤처 DX exe 파일에 적용할 아이콘을 선택해 주세요."
LangString OPTIONNAME_ICON_DX 1042 "게임큐브판 소닉 어드벤처 DX의 저장 데이터 아이콘"
LangString OPTIONNAME_ICON_HD 1042 "소닉 어드벤처 DX HD 아이콘 (Lester LJSTAR 제작)"
LangString OPTIONNAME_ICON_SA1 1042 "소닉 어드벤처 박스아트 아이콘 (PkR 제작)"
LangString OPTIONNAME_ICON_VM 1042 "소닉 어드벤처 VMU 커스텀 아이콘 (McAleeCh 제작)"
LangString OPTIONNAME_UPDATES 1042 "업데이트를 확인합니다"
LangString OPTIONNAME_NOUPDATES 1042 "업데이트를 확인하지 않습니다: 필요한 파일만 다운로드합니다"
LangString OPTIONNAME_WTITLE 1042 "창 제목을 $\"Sonic Adventure$\"로 변경"
LangString OPTIONNAME_ICON 1042 "sonic.exe 커스텀 아이콘 사용"
LangString OPTIONNAME_MANAGERCLASSIC 1042 "클래식 모드 매니저 사용"

;Messages
LangString MSG_UPDATES 1042 "업데이트를 확인하시겠습니까?$\r$\n$\r$\n$\r$\n$\r$\n$\r$\n\
이 인스톨러를 포함해 SADX 모드로더와 나머지 모드들은 아직 개발 중입니다. 업데이트하면 버그 수정이나 새로운 기능이 추가되는 효과가 있습니다.$\r$\n\
$\r$\n모드들을 최신 상태로 유지하여 새 콘텐츠를 수신하고 문제를 방지하십시오."
LangString MSG_OFFLINE 1042 "오프라인으로 설치 중인 경우, 두 번째 옵션을 선택해 주십시오."
LangString MSG_FOLDER_FOUND 1042 "다음 폴더가 소닉 어드벤처 DX가 설치된 위치로 검색되었습니다.$\r$\n\
$\r$\n다른 폴더에 설치하고 싶으시면 '찾아보기' 버튼을 눌러서 다른 폴더를 선택해 주세요."
LangString MSG_FOLDER_NOTFOUND 1042 "소닉 어드벤처 DX가 설치된 위치를 찾지 못하였습니다. 설치된 폴더를 수동으로 선택해 주십시오."
LangString MSG_DEFAULT 1042 "모드로더 설정이 안전 모드 기본값으로 재설정됩니다:$\r$\n\
$\r$\n해상도: 640x480$\r$\n\
전체 화면: 활성화$\r$\n\
테두리 없는 전체 화면: 비활성화$\r$\n\
수직 동기화 (VSync): 비활성화$\r$\n\
$\r$\n\
계속하시겠습니까?"
LangString MSG_INSTALLERUPDATE 1042 "인스톨러의 업데이트 데이터가 발견되었습니다. 자동 업데이트를 실행하시겠습니까?$\r$\n\
$\r$\n새로운 버전이 자동으로 다시 시작될 것입니다."
LangString MSG_FOUNDUPD 1042 "$UpdatesFound 패키지의 업데이트 데이터가 발견되었습니다!$\r$\n\
$\r$\n$WhichUpdates$\r$\n\
instdata 폴더가 정리되었으며 위 패키지의 최신 버전이 대신 다운로드됩니다."
LangString MSG_CHECKUPDATES 1042 "모드로더 업데이트 체크 중..."
LangString MSG_PROFILE 1042 "원하는 종류의 설정을 선택해 주세요. 프리셋을 사용할 수도 있고, 모드를 수동으로 선택할 수도 있습니다."
LangString MSG_START 1042 "SADX 모드 인스톨러에 오신 것을 환영합니다."
LangString MSG_WELCOME 1042 "이 프로그램은 MainMemory의 SADX 모드로더를 포함해 여러 가지 모드들을 설치합니다.$\r$\n$\r$\n모드들을 전부 설치하려면 소닉 어드벤처 DX가 설치된 드라이브에 약 1.3GB의 여유 공간이 필요합니다.$\r$\n$\r$\n웹 버전의 인스톨러를 사용 중인 경우 sadx_setup.exe가 있는 드라이브에 적어도 1GB의 여유 공간이 있는지 확인해 주십시오.$\r$\n$\r$\n'다음' 버튼을 눌러 다음으로 진행합니다."
LangString MSG_COMPLETE 1042 "설치가 완료되었습니다."
LangString MSG_FINISH 1042 "이제 설치한 모드로 소닉 어드벤처 DX를 즐길 수 있습니다.$\r$\n\
$\r$\n이 인스톨러가 사용한 파일은 'instdata' 폴더에 저장되었습니다. 나중에 인스톨러를 오프라인에서 사용할 수 있도록 보관하거나, 삭제하여 공간을 절약할 수 있습니다."

;Errors
LangString ERR_REQUIREDOS 1042 "지원하지 않는 운영 체제입니다. 이 프로그램은 Windows 7 SP1 이상에서만 정상 작동합니다.$\r$\n계속하시겠습니까?"
LangString ERR_NET_MISSING 1042 "Steam 변환 도구를 실행하기 위해서는 .NET Framework 4.8 이상이 필요합니다.$\r$\n수동으로 .NET Framework를 설치하고 인스톨러를 다시 실행해 주십시오."
LangString ERR_D_RESOURCES 1042 "리소스 데이터 다운로드 실패: $DownloadErrorCode. 계속하시겠습니까?"
LangString ERR_D_STEAM 1042 "Steam 도전과제 지원 모드 다운로드 실패: $DownloadErrorCode. 계속해서 다른 모드 설치를 진행하시겠습니까?"
LangString ERR_D_DLCS 1042 "SA1 DLC 콘텐츠 다운로드 실패: $DownloadErrorCode. 계속해서 다른 모드 설치를 진행하시겠습니까?"
LangString ERR_D_DCCONV 1042 "드림캐스트 컨버전 모드 다운로드 실패: $DownloadErrorCode. 계속해서 다른 모드 설치를 진행하시겠습니까?"
LangString ERR_D_DX9 1042 "다운로드 실패: $DownloadErrorCode. DirectX 설치 프로그램을 수동으로 실행해 주십시오."
LangString ERR_D_VC2019 1042 "다운로드 실패: $DownloadErrorCode. Visual C++ 런타임을 수동으로 설치해 주십시오."
LangString ERR_D_LAUNCHER 1042 "다운로드 실패: $DownloadErrorCode. 수동으로 런처를 다운로드하거나 인스톨러를 다시 실행해 주십시오.$\r$\n\
$\r$\n\
이 문제가 계속 발생할 경우 오프라인 버전의 인스톨러를 다운로드해 주십시오."
LangString ERR_D_NETFR 1042 "다운로드 실패: $DownloadErrorCode. .NET Framework를 수동으로 설치해 주십시오."
LangString ERR_D_NET 1042 "다운로드 실패: $DownloadErrorCode. .NET 8.0 데스크톱 런타임을 수동으로 설치해 주십시오."
LangString ERR_SIZE 1042 "$UpdateFilename 의 크기를 찾을 수 없습니다."
LangString ERR_FOLDER 1042 "현재 설치되어 있는 소닉 어드벤처 DX에서 일부 파일이 누락되었습니다.$\r$\n\
$\r$\n\
모드로더를 소닉 어드벤처 DX 메인 폴더(sonic.exe 또는 Sonic Adventure DX.exe가 있는 폴더)에 설치하고 있는지 확인해 주십시오."
LangString ERR_2004CHECK 1042 "설치 프로그램에서 인스톨러와 호환되지 않는 sonic.exe를 탐지했습니다.$\r$\n\
인스톨러와 호환되지 않는 버전:$\r$\n\
Sold Out Software 유통 버전 (SafeDisc DRM 적용)$\r$\n\
Dreamcast Collection 2010 유통 버전 (StarForce DRM 적용)$\r$\n\
일본 버전 (SafeDisc DRM 적용)$\r$\n\
크랙이 적용된 유럽판$\r$\n\
변조된 EXE를 포함한 유럽판$\r$\n\
인스톨러와 호환되는 버전의 게임을 설치한 뒤 인스톨러를 다시 실행해 주십시오."
LangString ERR_MODLOADER 1042 "모드로더가 올바르게 다운로드되거나 설치되지 않았습니다. $\r$\n\
인터넷 연결 상태를 확인하고 인스톨러를 다시 실행해 주십시오.$\r$\n\
$\r$\n\
이 문제가 계속 발생할 경우 오프라인 버전의 인스톨러를 다운로드해 주십시오."
LangString ERR_DOWNLOAD_TOOLS 1042 "일부 도구를 다운로드하지 못했습니다. $\r$\n\
인터넷 연결 상태를 확인하고 인스톨러를 다시 실행해 주십시오.$\r$\n\
$\r$\n\
이 문제가 계속 발생할 경우 오프라인 버전의 인스톨러를 다운로드해 주십시오."
LangString ERR_MODDOWNLOAD 1042 "$ModName 다운로드 실패: $DownloadErrorCode. 계속해서 다른 모드 설치를 진행하시겠습니까?"
LangString ERR_DOWNLOAD_FILE 1042 "파일을 다운로드하는 동안 오류가 발생했습니다. 다시 시도하시겠습니까?"
LangString ERR_MODDATE 1042 "$ModFilename 모드에 대해 mod.manifest 의 수정한 날짜를 확인하지 못했습니다. 모드가 올바르게 설치되었는지 확인해 주십시오."
LangString ERR_MISSINGFILES 1042 "현재 설치되어 있는 소닉 어드벤처 DX에서 일부 파일이 누락되었습니다.$\r$\n\
$\r$\n\
모드로더를 소닉 어드벤처 DX 메인 폴더(sonic.exe 또는 Sonic Adventure DX.exe가 있는 폴더)에 설치하고 있는지 확인해 주십시오.$\r$\n\
$\r$\n\
서로 다른 버전의 소닉 어드벤처 DX 파일이 있는 경우 인스톨러가 버전을 올바르게 감지하지 못할 수 있습니다. 이 문제를 해결하려면 순정 상태에서 시작하십시오."
LangString ERR_DOWNLOAD_CANCEL_Q 1042 "다운로드를 취소하시겠습니까?"
LangString ERR_DOWNLOAD_CANCEL_A 1042 "다운로드를 취소합니다."
LangString ERR_DOWNLOAD_FATAL 1042 "다운로드 실패: $DownloadErrorCode. 인스톨러를 다시 실행해 주십시오.$\r$\n\
$\r$\n\
이 문제가 계속 발생할 경우 오프라인 버전의 인스톨러를 다운로드해 주십시오."
LangString ERR_DOWNLOAD_RETRY 1042 "다운로드에 실패했습니다. 다시 시도하시겠습니까?"
LangString ERR_PERMISSION 1042 "소닉 어드벤처 DX 폴더의 권한을 설정하는 도중 오류가 발생했습니다.$\r$\n\
$\r$\n\
소닉 어드벤처 DX 폴더의 소유권을 가져온 다음 액세스 권한을 수동으로 설정해 주십시오."

;Mod descriptions
LangString DESC_ADD_ALL 1042 "이 모드들은 그래픽적인 변화는 없고 게임플레이에 변화를 줍니다.$\r$\n\
$\r$\n설치할 모드를 선택하세요."
LangString DESC_ADD_SND 1042 "음질과 볼륨을 개선하고, 소닉 어드벤처 DX의 사운드 버그를 수정하고, 누락된 소리를 복원하는 사운드 오버홀 모드"
LangString DESC_ADD_DLCS 1042 "드림캐스트판에 있었던 소닉 어드벤처의 기념일 이벤트와 챌린지들을 구현하는 소닉 어드벤처 DLC 모드"
LangString DESC_ADD_SUPER 1042 "스토리를 클리어하면 액션 스테이지에서 슈퍼 소닉으로 변신할 수 있는 슈퍼 소닉 모드"
LangString DESC_REMOVEMODS 1042 "진행하기 전에 mods 폴더에 있는 모든 모드들을 삭제합니다.$\r$\n\
$\r$\n\
경고! 모든 모드가 삭제되기 때문에 위험한 옵션입니다.$\r$\n\
현재 사용하는 모드에 문제가 있어 새로 시작하고 싶을 때만 사용해 주세요."
LangString DESC_MODLOADER 1042 "SADX 모드로더를 설치하거나 업데이트합니다. (필수)$\r$\n\
$\r$\n\
현재 설치된 소닉 어드벤처 DX가 스팀 버전일 경우 설치 전 2004 버전으로 변환합니다."
LangString DESC_LAUNCHER 1042 "SADX 런처를 설치합니다. (필수)$\r$\n\
$\r$\n\
조작 설정을 변경할 수 있는 도구입니다."
LangString DESC_NETF 1042 "변환 도구가 제대로 작동하는 데 필요한 .NET Framework를 설치하거나 업데이트합니다. $\r$\n\
$\r$\n\
다운로드 전 .NET Framework가 이미 설치되어 있는지 확인합니다."
LangString DESC_NET 1042 "SA 모드 매니저가 제대로 작동하는 데 필요한 .NET 데스크톱 런타임을 설치하거나 업데이트합니다."
LangString DESC_RUNTIME 1042 "DLL 기반 모드가 제대로 작동하는 데 필요한 Visual C++ 2010, 2012, 2013 및 2015/2017/2019 런타임을 설치하거나 업데이트합니다."
LangString DESC_DIRECTX 1042 "소닉 어드벤처 DX 및 랜턴 엔진 모드에 필요한 DirectX 런타임을 업데이트합니다. $\r$\n\
$\r$\n\
다운로드 전 DirectX 9.0c가 이미 설치되어 있는지 확인합니다."
LangString DESC_PERMISSIONS 1042 "소닉 어드벤처 DX 폴더의 소유권을 가져오고 하위 폴더까지 권한을 설정합니다. 이렇게 하면 관리자 권한 없이 모드로더를 활성화하거나 비활성화할 때 권한 오류가 발생하지 않습니다.$\r$\n\
$\r$\n\
권한 수정은 소닉 어드벤처 DX가 Program Files 폴더에 설치된 경우에만 실행됩니다."
LangString DESC_ADXAUDIO 1042 "드림캐스트판에 있던 고음질의 ADX 음악과 음성입니다. $\r$\n\
$\r$\n\
스팀 버전에는 이미 ADX 음성 및 음악이 포함되어 있기 때문에 설치할 필요가 없습니다."
LangString DESC_SADXFE 1042 "오리지널 PC판 소닉 어드벤처 DX의 다양한 버그 수정 및 개선 사항이 포함된 모드입니다.$\r$\n\
$\r$\n\
권장 사항입니다!"
LangString DESC_SMOOTHCAM 1042 "1인칭 시점에서 카메라를 부드럽게 움직입니다."
LangString DESC_FRAMELIMIT 1042 "더 정확하게 프레임을 제한할 수 있는 모드입니다.$\r$\n\
$\r$\n\
일부 컴퓨터에서 60 FPS가 아닌 63 FPS로 실행되는 등의 프레임레이트 문제를 해결합니다.$\r$\n\
$\r$\n\
게임플레이를 더 부드럽게 만들 수 있지만, 저사양 시스템에서 약간의 퍼포먼스 이슈가 있을 수 있습니다."
LangString DESC_PAUSEHIDE 1042 "드림캐스트판처럼 X+Y를 누르면 일시정지 메뉴를 숨깁니다. 드림캐스트 컨버전 모드에 이미 포함되어 있습니다."
LangString DESC_ONIONBLUR 1042 "드림캐스트 일본판에 있었던 소닉의 발과 테일즈의 꼬리의 '모션 블러'효과를 재현해 줍니다."
LangString DESC_DLCS 1042 "드림캐스트 전용 다운로드 콘텐츠(DLC)가 소닉 어드벤처 DX의 모드로 재탄생하였습니다. $\r$\n\
$\r$\n\
모드의 설정에서 자세한 내용을 볼 수 있습니다."
LangString DESC_STEAM 1042 "2004 버전에서 Steam 도전 과제를 지원하게 해 주는 모드입니다. $\r$\n\
$\r$\n\
Steam 라이브러리에 소닉 어드벤처 DX가 있을 경우에만 작동합니다."
LangString DESC_LANTERN 1042 "소닉 어드벤처 DX PC판에서 드림캐스트판의 광원 시스템을 구현해 주는 모드입니다. $\r$\n\
$\r$\n\
권장 사항입니다!"
LangString DESC_DCMODS 1042 "드림캐스트판의 스테이지, 오브젝트 모델, 이펙트, 보스, 브랜딩 등을 복원합니다."
LangString DESC_SNDOVERHAUL 1042 "몇몇 사운드 문제를 해결하고 대부분의 효과음을 드림캐스트판에서 뽑은 고퀄리티 사운드로 교체합니다."
LangString DESC_HDGUI 1042 "HUD, 버튼, 아이템 캡슐 아이콘, 메뉴 등과 같은 대부분의 그래픽 UI 요소들을 고해상도 커스텀 텍스처로 변경합니다."
LangString DESC_TIMEOFDAY 1042 "스테이션 스퀘어와 미스틱 루인 사이의 기차를 타면 시간이 흘러 배경이 바뀝니다."
LangString DESC_DCCONV 1042 "게임을 전체적으로 개편해서 소닉 어드벤처 DX를 오리지널 드림캐스트판 SA1처럼 바꾸어 줍니다."
LangString DESC_BUGFIXES 1042 "게임플레이에는 큰 변화 없이 문제를 해결하고 기술적 향상 효과가 있는 모드입니다."
LangString DESC_SA1CHARS 1042 "드림캐스트판의 캐릭터 모델을 사용할 수 있는 모드입니다."
LangString DESC_MODLOADERSTUFF 1042 "모드로더 및 모드들이 제대로 작동하려면 다양한 프로그램들이 필요합니다."
LangString DESC_SUPERSONIC 1042 "스토리 클리어 후 슈퍼 소닉으로 변신할 수 있습니다."
LangString DESC_ENHANCEMENTS 1042 "새로운 게임플레이 기능을 추가하거나 순정 소닉 어드벤처 DX의 그래픽을 개선한 모드입니다."
LangString DESC_IDLECHATTER 1042 "Z를 누르면 캐릭터가 스테이지에 대해 이것저것 말합니다."
LangString DESC_SADXWTR 1042 "일부 스테이지에 별도의 물 텍스처를 추가하고 에메랄드 코스트의 파도 효과를 다시 활성화합니다."
LangString DESC_MANAGERCLASSIC 1042 "Windows XP에서 동작하는 SADX 모드 매니저의 다른 버전입니다."

;Other descriptions
LangString DESC_DESC 1042 "설명"
LangString DESC_PRESERVE 1042 "체크해두면 모드 업데이트 및 재설치 시 config.ini 파일이 유지됩니다."
LangString DESC_ICON 1042 ".exe 실행 파일이 커스텀 아이콘 중 하나로 변경됩니다."
LangString DESC_DCMODS_ALL 1042 "해당 모드들은 소닉 어드벤처 DX를 드림캐스트판 오리지널 소닉 어드벤처처럼 만들어 줍니다.$\r$\n\
포함된 모드: 어니언 스킨 블러, 드림캐스트 컨버전, 드림캐스트 캐릭터 팩, DLC 콘텐츠, 사운드 오버홀, HD GUI, 랜턴 엔진, 시간의 흐름, 수다쟁이 잡담, 부드러운 카메라, 프레임 제한, 슈퍼 소닉"
LangString DESC_SADX_ALL 1042 "소닉 어드벤처 DX를 더 좋은 퀄리티로 즐길 수 있습니다.$\r$\n\
포함된 모드: SADX: Fixed Edition, 어니언 스킨 블러, 에메랄드 코스트 수정, 사운드 오버홀, HD GUI, 시간의 흐름, 수다쟁이 잡담, 일시정지 메뉴 숨기기, 부드러운 카메라, 프레임 제한, 슈퍼 소닉"
LangString DESC_MIN_ALL 1042 "최소한의 필수적인 모드들만 설치됩니다(스피드런용).$\r$\n\
포함된 모드: 프레임 제한"
LangString DESC_STEAMCONV_ALL 1042 "Steam 버전을 2004 버전으로 변환하고 모드로더를 설치하려면 이 옵션을 선택하세요.$\r$\n\
이 옵션은 모드를 설치하지 않습니다.$\r$\n\
모드를 설치할 경우 다른 옵션을 선택하세요."
LangString DESC_REDIST_ALL 1042 "DirectX, Visual C++ 런타임 및 .NET Framework를 설치하려면 이 옵션을 선택하세요.$\r$\n\
소닉 어드벤처 DX의 게임 파일에는 영향을 미치지 않습니다.$\r$\n\
이 옵션은 모드로더를 포함한 나머지 요소를 설치하지 않습니다. 모드를 설치할 경우 다른 옵션을 선택하세요."
LangString DESC_CUSTOM_ALL 1042 "사용자 지정 유형입니다.$\r$\n\
어떤 모드를 설치할지 선택할 수 있고, 모드로더 설정을 직접 변경할 수 있습니다."
LangString DESC_GUIDE_ALL 1042 "가이드 모드 유형입니다.$\r$\n\
각 모드별로 적용 시의 비교 스크린샷을 보면서 설치 여부를 결정할 수 있습니다."
LangString DESC_OPTIMAL 1042 "인스톨러가 화면의 해상도를 감지해서 최적의 그래픽 품질을 구성해 모드로더를 설정해 줍니다."
LangString DESC_FAILSAFE 1042 "실행 시 게임이 충돌할 경우 사용해 주십시오.$\r$\n\
해상도가 640x480 전체 화면으로 설정되고 테두리 없는 전체 화면과 같은 모든 향상 기능들이 비활성화됩니다."
LangString DESC_WINDOWTITLE 1042 "게임의 창 제목이 $\"SonicAdventureDXPC$\" 대신 $\"Sonic Adventure$\" 로 설정됩니다.$\r$\n\
$\r$\n\
경고: FUSION의 챠오 에디터를 사용하려면 이 기능을 활성화하지 마세요."
LangString DESC_MOREMODS 1042 "더 많은 소닉 어드벤처 DX 모드 찾기"
LangString DESC_SHORTCUTS 1042 "바탕 화면에 바로 가기 만들기"
LangString DESC_RUNSADX 1042 "소닉 어드벤처 DX 실행"
LangString DESC_RUNLAUNCHER 1042 "SADX 런처로 조작 및 설정 변경"
;The resources below are only available in English. Please indicate that in your translation
LangString DESC_DREAMCASTIFY 1042 "Dreamcastify - 소닉 어드벤처 DX에서 다운그레이드된 요소를 정리한 웹 사이트(영어)"
LangString DESC_DISCORD 1042 "SADX 모딩 디스코드 서버(영어)"
LangString DESC_OPTION_MANAGERCLASSIC 1042 "최신 SA 매니저 대신 다른 SADX 모드 매니저 사용(영어, Windows XP에서 실행 시 필수)"

;Detail output
LangString DE_SND 1042 "데이터를 소닉 어드벤처 DX 2004 버전 포맷으로 변환하는 중 (시간이 좀 걸릴 수 있습니다)..."
LangString DE_INSTDATA 1042 "instdata 폴더 만드는 중..."
LangString DE_7Z 1042 "7Z 복사하는 중..."
LangString DE_OWNER 1042 "소닉 어드벤처 DX 폴더의 소유권을 가져오는 중: $Permission1"
LangString DE_PERM 1042 "소닉 어드벤처 DX 폴더의 사용 권한: $Permission2"
LangString DE_REALL 1042 "모든 모드를 제거하는 중..."
LangString DE_PRGFILES 1042 "소닉 어드벤처 DX가 Program Files 폴더에 설치되어 있는지 확인 중..."
LangString DE_RECU 1042 "소닉 어드벤처 DX가 Program Files 폴더에 설치되어 있습니다. 하위 폴더까지 권한 설정 중..."
LangString DE_2004FOUND 1042 "2004 버전이 감지되었습니다 (sonic.exe)"
LangString DE_MANIFEST 1042 "$ModFilename의 수정한 날짜: $2-$1-$0T$4:$5:$6Z"
LangString DE_TAKEOWN 1042 "$R9의 소유권을 가져오는 중: $0"
LangString DE_PERMSET1 1042 "$R9의 사용 권한을 설정하는 중 (1): $0"
LangString DE_PERMSET2 1042 "$R9의 사용 권한을 설정하는 중 (2): $0"
LangString DE_PERMSET3 1042 "$R9의 사용 권한을 설정하는 중 (3): $0"
LangString DE_E_GENERIC 1042 "$Modname의 압축을 해제하는 중..."
LangString DE_C_EXE 1042 "sonic.exe의 MD5 해시를 확인 중..."
LangString DE_E_ML 1042 "SADX 모드로더의 압축을 해제하는 중..."
LangString DE_E_MANAGER 1042 "모드 매니저의 압축을 해제하는 중..."
LangString DE_I_ML 1042 "SADX 모드로더를 설치/업데이트 중..."
LangString DE_EXEFOUND 1042 "사용 가능한 sonic.exe가 발견되었습니다. 모드로더 설치를 시작합니다."
LangString DE_EXEUNK 1042 "호환되지 않는 sonic.exe가 발견되었습니다."
LangString DE_DETECT 1042 "설치된 소닉 어드벤처 DX의 유형을 분석하는 중..."
LangString DE_2010FOUND 1042 "2010 버전이 감지되었습니다 (Sonic Adventure DX.exe)"
LangString DE_C_2010 1042 "소닉 어드벤처 DX 2010 버전의 무결성을 확인하는 중..."
LangString DE_E_TOOLS 1042 "도구의 압축을 해제하는 중..."
LangString DE_C_TOOLS 1042 "도구를 확인하는 중..."
LangString DE_CLEANUP 1042 "정리하는 중..."
LangString DE_E_LAUNCHER 1042 "SADX 런처의 압축을 해제하는 중..."
LangString DE_CHECKNET 1042 ".NET Framework 버전 확인 중..."
LangString DE_E_NETF 1042 ".NET Framework를 설치하는 중..."
LangString DE_E_NET 1042 ".NET 데스크톱 런타임을 설치하는 중..."
LangString DE_NETFPRESENT 1042 ".NET Framework가 이미 설치되어 있습니다 ($NetFrameworkVersion)."
LangString DE_C_VCC 1042 "Visual C++ 런타임 설치 여부를 확인 중..."
LangString DE_E_VC2019 1042 "Visual C++ 런타임의 압축을 해제하는 중..."
LangString DE_I_VC2019 1042 "Visual C++ 런타임을 설치하는 중..."
LangString DE_C_DX9 1042 "DirectX 9.0c 설치 여부를 확인 중..."
LangString DE_I_DX9_1 1042 "DirectX 설치 프로그램 실행 중 (1)..."
LangString DE_I_DX9_2 1042 "DirectX 설치 프로그램 실행 중 (2)..."
LangString DE_B_SADXFE 1042 "SADXFE 설정을 백업하는 중..."
LangString DE_R_SADXFE 1042 "SADXFE 구성을 복원하는 중..."
LangString DE_B_GAMEC 1042 "게임의 컨트롤러 데이터베이스를 백업하는 중..."
LangString DE_R_GAMEC 1042 "게임의 컨트롤러 데이터베이스를 복원하는 중..."
LangString DE_B_DCCONV 1042 "드림캐스트 컨버전 모드의 설정을 백업하는 중..."
LangString DE_E_DCCONV 1042 "드림캐스트 컨버전 모드의 압축을 해제하는 중..."
LangString DE_R_DCCONV 1042 "드림캐스트 컨버전 모드의 설정을 복원하는 중..."
LangString DE_DCTITLE_ON 1042 "드림캐스트 컨버전 모드의 창 제목을 활성화하는 중..."
LangString DE_DCTITLE_OFF 1042 "드림캐스트 컨버전 모드의 창 제목을 비활성화하는 중..."
LangString DE_WTR_ON 1042 "SADX 스타일 물 텍스처를 활성화하는 중..."
LangString DE_WTR_OFF 1042 "SADX 스타일 물 텍스처를 비활성화하는 중..."
LangString DE_B_SA1CHARS 1042 "드림캐스트 캐릭터 구성을 백업하는 중..."
LangString DE_R_SA1CHARS 1042 "드림캐스트 캐릭터 구성을 복원하는 중..."
LangString DE_B_DLCS 1042 "드림캐스트 DLC 구성을 백업하는 중..."
LangString DE_E_DLCS 1042 "SA1 DLC 콘텐츠를 압축 푸는 중..."
LangString DE_R_DLCS 1042 "드림캐스트 DLC 구성을 복원하는 중..."
LangString DE_E_STEAM 1042 "Steam 도전 과제 지원 모드를 압축 푸는 중..."
LangString DE_MODORDER 1042 "모드 적용 순서를 구성하는 중..."
LangString DE_B_MLINI 1042 "이전 모드로더 설정을 백업하는 중..."
LangString DE_I_MLINI 1042 "모드로더 설정을 작성하는 중..."
LangString DE_B_SADXINI 1042 "게임 설정을 백업하는 중..."
LangString DE_I_SADXINI 1042 "게임 설정을 작성하는 중..."
LangString DE_E_RESOURCES 1042 "아이콘의 압축을 해제하는 중..."
LangString DE_I_RESOURCES 1042 "아이콘 복사 중..."
LangString DE_B_SADXWTR 1042 "SADX 스타일 물 텍스처 모드의 설정을 백업하는 중..."
LangString DE_R_SADXWTR 1042 "SADX 스타일 물 텍스처 모드의 설정을 복원하는 중..."
LangString DE_B_ONION 1042 "어니언 스킨 블러 모드의 설정을 백업하는 중..."
LangString DE_R_ONION 1042 "어니언 스킨 블러 모드의 설정을 복원하는 중..."

;Mod names (generic installer)
LangString MOD_SADXFE 1042 "SADXFE"
LangString MOD_FRAME 1042 "프레임 제한 모드"
LangString MOD_SA1CHARS 1042 "드림캐스트 캐릭터 팩"
LangString MOD_LANTERN 1042 "랜턴 엔진 모드"
LangString MOD_SMOOTHCAM 1042 "부드러운 카메라 모드"
LangString MOD_ONION 1042 "어니언 스킨 블러 모드"
LangString MOD_IDLE 1042 "수다쟁이 잡담 모드"
LangString MOD_PAUSE 1042 "일시정지 숨기기 모드"
LangString MOD_SUPER 1042 "슈퍼 소닉 모드"
LangString MOD_TIME 1042 "시간의 흐름 모드"
LangString MOD_ADX 1042 "ADX 오디오"
LangString MOD_SND 1042 "사운드 오버홀"
LangString MOD_HDGUI 1042 "HD GUI"
LangString MOD_SADXWTR 1042 "SADX 스타일 물 텍스처"

;inetc stuff
LangString INETC_DOWNLOADING 1042 "%s을(를) 다운로드 중" 
LangString INETC_CONNECT 1042 "연결하는 중..."
LangString INETC_SECOND 1042 "초"
LangString INETC_MINUTE 1042 "분"
LangString INETC_HOUR 1042 "시간"
LangString INETC_PLURAL 1042 " 1042 "
LangString INETC_PROGRESS 1042 "%dkB (%d%%) / %dkB 속도: %d.%01dkB/s"
;Make sure there's an empty space between the first quotation mark and (%d in the string below
LangString INETC_REMAINING 1042 "(%d %s%s 남음)"

;Unused strings
${LangFileString} MUI_TEXT_WELCOME_INFO_TITLE " "
${LangFileString} MUI_TEXT_WELCOME_INFO_TEXT " "
${LangFileString} MUI_TEXT_FINISH_TITLE "  "
${LangFileString} MUI_TEXT_FINISH_SUBTITLE "  "
${LangFileString} MUI_TEXT_ABORT_TITLE " "
${LangFileString} MUI_TEXT_ABORT_SUBTITLE " "
${LangFileString} MUI_TEXT_FINISH_INFO_TITLE " "
${LangFileString} MUI_TEXT_FINISH_INFO_TEXT " "
${LangFileString} MUI_TEXT_FINISH_INFO_REBOOT " "
${LangFileString} MUI_TEXT_FINISH_REBOOTNOW " "
${LangFileString} MUI_TEXT_FINISH_REBOOTLATER " "
${LangFileString} MUI_TEXT_FINISH_RUN " "
${LangFileString} MUI_TEXT_FINISH_SHOWREADME " "
${LangFileString} MUI_INNERTEXT_LICENSE_BOTTOM_CHECKBOX " "
${LangFileString} MUI_INNERTEXT_LICENSE_BOTTOM_RADIOBUTTONS " "