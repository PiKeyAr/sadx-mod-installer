!insertmacro LANGFILE "Spanish" = "Español" "Espanol"

;General strings
${LangFileString} MUI_TEXT_LICENSE_TITLE "Información"
${LangFileString} MUI_TEXT_LICENSE_SUBTITLE "Favor de leer las siguientes notas antes de usar este instalador."
${LangFileString} MUI_INNERTEXT_LICENSE_TOP "Desplázate hacia abajo o presiona AvPág para ver el resto de las notas."
${LangFileString} MUI_INNERTEXT_LICENSE_BOTTOM "Por favor, crea un respaldo de tu fólder de SADX antes de usar este instalador. Da clic en Continuar para proceder."
${LangFileString} MUI_TEXT_COMPONENTS_TITLE "Elige los componentes."
${LangFileString} MUI_TEXT_COMPONENTS_SUBTITLE "Elige los mods y características que deseas instalar."
${LangFileString} MUI_INNERTEXT_COMPONENTS_DESCRIPTION_TITLE "Descripción"
${LangFileString} MUI_INNERTEXT_COMPONENTS_DESCRIPTION_INFO "Mueve el cursor del mouse sobre un componente para ver su descripción."
${LangFileString} MUI_TEXT_DIRECTORY_TITLE "Elige la ubicación de la instalación."
${LangFileString} MUI_TEXT_DIRECTORY_SUBTITLE "Elige el fólder donde está instalado SADX."
${LangFileString} MUI_TEXT_INSTALLING_TITLE "Instalando"
${LangFileString} MUI_TEXT_INSTALLING_SUBTITLE "Por favor espera mientras el Instalador de Mods de SADX trabaja en el juego."
${LangFileString} MUI_BUTTONTEXT_FINISH "&Finalizar"

;Install type names
LangString INSTTYPE_DC 1034 "Mods de Dreamcast"
LangString INSTTYPE_SADX 1034 "SADX + mejoras"
LangString INSTTYPE_MIN 1034 "Mínima/Vanilla"
LangString INSTTYPE_STEAMCONV 1034 "Steam a 2004"
LangString INSTTYPE_REDIST 1034 "Redistrubuciones"

;Install profile names
LangString PROFILENAME_DC 1034 "Dreamcast"
LangString PROFILENAME_SADX 1034 "SADX mejorado"
LangString PROFILENAME_MIN 1034 "Mínimo"
LangString PROFILENAME_CUSTOM 1034 "Personalizado"
LangString PROFILENAME_STEAMCONV 1034 "Steam a 2004"
LangString PROFILENAME_REDIST 1034 "Redistrubuciones"

;Section names
LangString SECTIONNAME_REMOVEMODS 1034 "Elimina todos los mods actuales."
LangString SECTIONNAME_PERMISSIONS 1034 "Revisa y arregla los permisos del folder de SADX."
LangString SECTIONNAME_MODLOADER 1034 "Cargador de mods de SADX por MainMemory y SonicFreak94"
LangString SECTIONNAME_LAUNCHER 1034 "Iniciador de SADX por PkR"
LangString SECTIONNAME_DEPENDENCIES 1034 "Dependencias del Mod Loader."
LangString SECTIONNAME_VCC 1034 "Runtimes de Visual C++."
LangString SECTIONNAME_DX9 1034 "DirectX 9.0c"
LangString SECTIONNAME_BUGFIXES 1034 "Corrección de errores y mods para controles y cámara."
LangString SECTIONNAME_SADXFE 1034 "SADX: Fixed Edition por SonicFreak94"
LangString SECTIONNAME_FRAMELIMIT 1034 "Frame Limiter por SonicFreak94"
LangString SECTIONNAME_DCMODS 1034 "Mods de Dreamcast"
LangString SECTIONNAME_DCCONV 1034 "Dreamcast Conversion por PkR"
LangString SECTIONNAME_SA1CHARS 1034 "Dreamcast Characters Pack por ItsEasyActually"
LangString SECTIONNAME_LANTERN 1034 "Lantern Engine por SonicFreak94"
LangString SECTIONNAME_DLCS 1034 "DLCs de Sonic Adventure por PkR"
LangString SECTIONNAME_ENH 1034 "Mods para mejoras del juego."
LangString SECTIONNAME_SMOOTH 1034 "Smooth Camera por SonicFreak94"
LangString SECTIONNAME_ONION 1034 "Onion Skin Blur por SonicFreak94"
LangString SECTIONNAME_IDLE 1034 "Idle Chatter por SonicFreak94"
LangString SECTIONNAME_PAUSE 1034 "Hide Pause por SonicFreak94"
LangString SECTIONNAME_STEAM 1034 "Steam Achievements por MainMemory"
LangString SECTIONNAME_SUPER 1034 "Super Sonic por Kell & SonicFreak94"
LangString SECTIONNAME_TIME 1034 "Time of Day por PkR"
LangString SECTIONNAME_ADX 1034 "Voces y música en ADX (para el port de 2004)."
LangString SECTIONNAME_SND 1034 "Sound Overhaul 3 por PkR"
LangString SECTIONNAME_HDGUI 1034 "HD GUI 2 por PkR y otros"
LangString SECTIONNAME_SADXWTR 1034 "SADX Style Water por PkR y SteG"
LangString SECTIONNAME_MANAGERCLASSIC 1034 "Gestor de mods de SADX Clásico por PkR"

;Guide Mode descriptions and checkboxes
LangString GUIDE_INFO_LANTERN 1034 "El mod Lantern Engine recrea la iluminación de la versión de Dreamcast en SADX PC. Lantern Engine remueve el brillo excesivo en los modelos de los personajes y hace la iluminación en niveles, objetos y personajes más vibrante."
LangString GUIDE_INFO_DCCONV 1034 "Dreamcast Conversion es una revisión completa del juego que lo hace verse y sentirse más como la versión de Dreamcast de SA1. Corrige errores y restaura los niveles de Dreamcast, texturas, modelos de los objetos, efectos especiales y logotipos."
LangString GUIDE_INFO_DXWTR 1034 "Dreamcast Conversion incluye la opción para habilitar distintas texturas del agua, similares a la del SADX original, pero con mejor calidad y animación. Algunos prefieren usar estas texturas en los niveles de Dreamcast."
LangString GUIDE_INFO_SA1CHARS 1034 "Dreamcast Characters Pack es un mod que restaura los modelos de los personajes de Dreamcast en la versión de PC. Aunque los personajes de SADX tienen más polígonos, algunos prefieren los modelos originales del Dreamcast por sus estética."
LangString GUIDE_INFO_ONION 1034 "El mod Onion Skin Blur recrea el 'movimiento desenfocado' en los pies de Sonic y las colas de Tails. Este efecto estaba originalmente en la versión japonesa de Dreamcast de SA1, pero fue retirado en revisiones posteriores."
LangString GUIDE_INFO_HDGUI 1034 "HD GUI 2 remplaza muchas texturas de la interfaz (menús, HUD, íconos de las cápsulas, etc.) con personalizaciones de más alta resolución."
LangString GUIDE_INST_LANTERN 1034 "Instalar Lantern Engine por SonicFreak94"
LangString GUIDE_INST_DCCONV 1034 "Instalar Dreamcast Conversion por PkR"
LangString GUIDE_INST_DXWTR 1034 "Habilitar texturas de agua estilo SADX por SteG"
LangString GUIDE_INST_SA1CHARS 1034 "Instalar Dreamcast Characters Pack por ItsEasyActually"
LangString GUIDE_INST_ONION 1034 "Instalar Onion Skin Blur por SonicFreak94"
LangString GUIDE_INST_HDGUI 1034 "Instalar HD GUI 2 por PkR, Dark Sonic, Sonikko y SPEEPSHighway"

;Download strings
LangString D_DLCS 1034 "Descargando DLCs de Dreamcast..."
LangString D_DCCONV 1034 "Descargando Dreamcast Conversion..."
LangString D_STEAM 1034 "Descargando Steam Achievements..."
LangString D_RESOURCES 1034 "Descargando íconos..."
LangString D_DX9 1034 "Descargando Instalador de DirectX..."
LangString D_VC2019 1034 "Descargando instalador de runtimes de Visual C++..."
LangString D_LAUNCHER 1034 "Descargando Iniciador de SADX..."
LangString D_NET 1034 "Descargando Instalador de .NET Runtime..."
LangString D_NETF 1034 "Descargando Instalador de .NET Framework..."
LangString D_STEAMTOOLS 1034 "Descargando herramientas..."
LangString D_MODLOADER 1034 "Descargando Cargador de Mods de SADX..."
LangString D_MODMANAGER 1034 "Descargando Gestor de Mods de SADX..."
LangString D_FILELIST 1034 "Descargando lista de archivos..."
LangString D_INSTCHK 1034 "Revisando actualizaciones del instalador..."
LangString D_GENERIC 1034 "Descargando $Modname..."
LangString D_UPDATE 1034 "Revisando $UpdateFilename ..."
LangString D_INSTALLER 1034 "Descargando el instalador..."
LangString D_INSTALLER_R 1034 "Ejecutando la nueva versión..."

;Headers
LangString MISC_INSTALLER 1034 "Instalador de mods de SADX"
LangString HEADER_GUIDE_TITLE 1034 "Guía de selección de mods"
LangString HEADER_GUIDE_TEXT 1034 "Marca la casilla si quieres instalar este mod. Da clic en la captura para una comparación detallada."
LangString HEADER_ADD_TITLE 1034 "Mods adicionales"
LangString HEADER_ADD_TEXT 1034 "Marca las casillas de los mods que quieras instalar."
LangString HEADER_UPDATES_TITLE 1034 "Revisar actualizaciones"
LangString HEADER_UPDATES_TEXT 1034 "Se recomienda mantener los mods y el Mod Loader actualizados."
LangString HEADER_ICON_TITLE 1034 "Elige tu ícono para el EXE."
LangString HEADER_ICON_TEXT 1034 "Elige un ícono personalizado para el archivo EXE de SADX EXE."
LangString HEADER_TYPE_TITLE 1034 "Tipo de instalación"
LangString HEADER_TYPE_TEXT 1034 "Elige el tipo de instalación."

;Options
LangString OPTIONNAME_INSTMODE 1034 "Modo del instalador"
LangString OPTIONNAME_GUIDE 1034 "Modo guiado - Elige esta si no estas seguro"
LangString OPTIONNAME_PRESET 1034 "Modo predeterminado"
LangString OPTIONNAME_PRESETS 1034 "Predeterminado"
LangString OPTIONNAME_ADVANCED 1034 "Mostrar opciones avanzadas"
LangString OPTIONNAME_PRESERVE 1034 "Conservar ajustes individuales de los mods (de haberlos)"
LangString OPTIONNAME_OPTIMAL 1034 "Usar los ajustes óptimos del Cargador de mods"
LangString OPTIONNAME_FAILSAFE 1034 "Usar ajustes a prueba de fallos del Cargador de mods"
LangString OPTIONNAME_ICONSEL 1034 "Elige tu ícono preferido para tu ejecutable de SADX."
LangString OPTIONNAME_ICON_DX 1034 "Ícono de guardado de SADX en Gamecube"
LangString OPTIONNAME_ICON_HD 1034 "Ícono HD por Lester LJSTAR"
LangString OPTIONNAME_ICON_SA1 1034 "Ícono box art de SA1 por PkR"
LangString OPTIONNAME_ICON_VM 1034 "Ícono del VMU de SA1 recreado por McAleeCh"
LangString OPTIONNAME_UPDATES 1034 "Buscar actualizaciones"
LangString OPTIONNAME_NOUPDATES 1034 "No buscar actualizaciones: sólo descargar los archivos necesarios (de haberlos)"
LangString OPTIONNAME_WTITLE 1034 "Cambiar título de la ventana a $\"Sonic Adventure$\""
LangString OPTIONNAME_ICON 1034 "Elegir un ícono personalizado para sonic.exe"
LangString OPTIONNAME_MANAGERCLASSIC 1034 "Use the Classic Mod Manager"

;Messages
LangString MSG_UPDATES 1034 "¿Quieres revisar si hay actualizaciones?$\r$\n$\r$\n$\r$\n$\r$\n$\r$\n\
Este instalador, el Cargador de mods de SADX y los mods son trabajos en progreso. Las actualizaciones corrigen errores y añaden nuevas características.$\r$\n\
$\r$\nMantén tus mods actualizados para recibir nuevo contenido y evitar errores."
LangString MSG_OFFLINE 1034 "Para instalar sin conexión, elige la segunda opción."
LangString MSG_FOLDER_FOUND 1034 "El instalador detectó el siguiente folder como el lugar donde se instaló SADX.$\r$\n\
$\r$\nPara instalar en otro folder, da clic en Explorar y elige otro folder."
LangString MSG_FOLDER_NOTFOUND 1034 "El instalador no pudo detectar una instalación de SADX. Elige manualmente el lugar donde instalaste el juego."
LangString MSG_DEFAULT 1034 "Los ajustes del Mod Loader se restaurarán a aquellos a prueba de fallos:$\r$\n\
$\r$\nResolución: 640x480$\r$\n\
Pantalla completa: sí$\r$\n\
Pantalla sin bordes: no$\r$\n\
VSync: no$\r$\n\
$\r$\n\
¿Continuar?"
LangString MSG_INSTALLERUPDATE 1034 "¡Se encontraron actualizaciones para el instalador! ¿Realizar la actualización?$\r$\n\
$\r$\nLa nueva versión se lanzará automáticamente."
LangString MSG_FOUNDUPD 1034 "¡Se encontraron actualizaciones para el/los paquete(s) de $UpdatesFound!$\r$\n\
$\r$\n$WhichUpdates$\r$\n\
El folder instdata fue limpiado y las últimas versiones de los paquetes arriba seleccionados se descargarán."
LangString MSG_CHECKUPDATES 1034 "Buscando actualizaciones del Cargador de mods..."
LangString MSG_PROFILE 1034 "Elige tu tipo de instalación de SADX preferida. Puedes usar un ajuste predeterminado de mods o elegirlos manualmente."
LangString MSG_START 1034 "¡Bienvenido/a al Instalador de mods de SADX!"
LangString MSG_WELCOME 1034 "Este programa instalará el Cargador de mods de SADX de MainMemory y una selección de mods de varios creadores.$\r$\n$\r$\nUna instalación por defecto con todos los mods ocupará aproximadamente 1.3GB dentro de tu folder de SADX.$\r$\n$\r$\nSi usas el instalador web, asegúrate de teneral menos 1GB disponible en el disco donde se encuentre sadx_setup.exe$\r$\n$\r$\nDa clic para continuar."
LangString MSG_COMPLETE 1034 "¡Instalación completada!"
LangString MSG_FINISH 1034 "Ahora puedes jugar SADX con los mods que instalaste.$\r$\n\
$\r$\nLos archivos usados por este instalador se guardaron en el folder 'instdata'. Puedes conservarlo para usar el instalador sin conexión luego o borrarlo para ahorrar espacio."

;Errors
LangString ERR_REQUIREDOS 1034 "Este programa podría no funcionar en sistemas operativos anteriores a Windows XP SP3.$\r$\n¿Deseas continuar de cualquier modo?"
LangString ERR_NET_MISSING 1034 "La herramienta de conversión requiere que .NET Framework 4.0 esté instalado.$\r$\nPor favor, instala .NET Framework y corre de nuevo el instalador."
LangString ERR_D_RESOURCES 1034 "Falló la descarga del ícono: $DownloadErrorCode. ¿Continuar?"
LangString ERR_D_STEAM 1034 "Falló la descarga de Steam Achievements: $DownloadErrorCode. ¿Continuar instalando otros mods?"
LangString ERR_D_DLCS 1034 "Falló la descarga de DLCs de Dreamcast: $DownloadErrorCode. ¿Continuar instalando otros mods?"
LangString ERR_D_DCCONV 1034 "Falló la descarga de DC Conversion: $DownloadErrorCode. ¿Continuar instalando otros mods?"
LangString ERR_D_DX9 1034 "Falló la descarga: $DownloadErrorCode. Por favor instala DirectX manualmente."
LangString ERR_D_VC2019 1034 "Falló la descarga: $DownloadErrorCode. Por favor instala los runtimes de Visual C++ manualmente."
LangString ERR_D_LAUNCHER 1034 "Falló la descarga: $DownloadErrorCode. Por favor descarga el Iniciador manualmente o ejecuta el instalador de nuevo.$\r$\n\
$\r$\n\
Si ocurre este error constantemente, descarga el instalador sin conexión."
LangString ERR_D_NETFR 1034 "Falló la descarga: $DownloadErrorCode. Por favor, instala .NET Framework manualmente."
LangString ERR_D_NET 1034 "Falló la descarga: $DownloadErrorCode. Por favor, instala el runtime de .NET 8.0 Desktop manualmente."
LangString ERR_SIZE 1034 "¡No se encontró el tamaño de $UpdateFilename!"
LangString ERR_FOLDER 1034 "El instalador detectó que faltan algunos archivos en tu instalación de SADX.$\r$\n\
$\r$\n\
Asegurate de instalar el Mod Loader en el folder principal de SADX (donde sonic.exe o Sonic Adventure DX.exe esté)."
LangString ERR_2004CHECK 1034 "El instalador detectó que el sonic.exe que posees es incompatible con el Instalador de mods.$\r$\n\
Las siguientes versiones no cuentan con soporte:$\r$\n\
Dreamcast Collection 2010 (StarForce DRM)$\r$\n\
Sold Out Software (SafeDisc DRM)$\r$\n\
Versión japonésa (SafeDisc DRM)$\r$\n\
Copias crackeadas de la versión europea$\r$\n\
EXEs hackeados (traducciones, etc.) de la versión europea$\r$\n\
Por favor, obtén una versión compatible del juego y corre de nuevo el instalador."
LangString ERR_MODLOADER 1034 "El instalador detectó que el Mod Loader no se ha descargado o instalado correctamente. $\r$\n\
Revisa tu conexión a Internet y ejecuta el instalador nuevamente.$\r$\n\
$\r$\n\
Si ocurre este error constantemente, descarga el instalador sin conexión."
LangString ERR_DOWNLOAD_TOOLS 1034 "El instalador detectó que algunas herramientas no se han podido descargar. $\r$\n\
Revisa tu conexión a Internet y ejecuta el instalador nuevamente.$\r$\n\
$\r$\n\
Si ocurre este error constantemente, descarga el instalador sin conexión."
LangString ERR_MODDOWNLOAD 1034 "Falló la descarga de $ModName: $DownloadErrorCode. ¿Continuar instalando otros mods?"
LangString ERR_DOWNLOAD_FILE 1034 "Error al descargar el archivo. ¿Intentar otra vez?"
LangString ERR_MODDATE 1034 "No se pudo verificar la fecha de última modificación de mod.manifest del mod $ModFilename. Asegúrate de que el mod se instaló correctamente."
LangString ERR_MISSINGFILES 1034 "El instalador detectó que faltan algunos archivos en tu instalación de SADX.$\r$\n\
$\r$\n\
Asegurate de instalar el Cargador de mods en el folder principal de SADX (donde sonic.exe o Sonic Adventure DX.exe esté).$\r$\n\
$\r$\n\
Si tienes archivos mezclados de diferentes versiones de SADX, el instalador podría no poder detectar correctamente tu versión. Para resolverlo, haz una instalación limpia del juego."
LangString ERR_DOWNLOAD_CANCEL_Q 1034 "¿Cancelar descarga?"
LangString ERR_DOWNLOAD_CANCEL_A 1034 "Descarga cancelada"
LangString ERR_DOWNLOAD_FATAL 1034 "Error en la descarga: $DownloadErrorCode. Ejecuta el programa de nuevo.$\r$\n\
$\r$\n\
Si ocurre este error constantemente, descarga el instalador sin conexión."
LangString ERR_DOWNLOAD_RETRY 1034 "Falló la descarga. ¿Quieres intentarlo otra vez?"
LangString ERR_PERMISSION 1034 "¡Error asignando permisos al folder de SADX!$\r$\n\
$\r$\n\
Ve a tu folder de SADX y fija los permisos de acceso manualmente."

;Mod descriptions
LangString DESC_ADD_ALL 1034 "Estos mods introducen cambios al modo de juego y mejoras no visuales.$\r$\n\
$\r$\nElige qué mods quieres instalar."
LangString DESC_ADD_SND 1034 "Instala Sound Overhaul para mejorar la calidad del sonido, arreglar errores en los sonidos y restaurar sonidos faltantes"
LangString DESC_ADD_DLCS 1034 "Instala el mod de DLCs para añadir eventos de temporada y retos del Dreamcast a Sonic Adventure"
LangString DESC_ADD_SUPER 1034 "Instala el mod Super Sonic para poder transformarte en Súper Sonic tras completar la historia"
LangString DESC_REMOVEMODS 1034 "Borrar todos los mods en el folder de mods antes de continuar.$\r$\n\
$\r$\n\
¡ADVERTENCIA! Esta es una opción peligrosa, borrará todos tus mods.$\r$\n\
Úsala sólo si tienes problemas con tus mods actuales y quieres comenzar de cero."
LangString DESC_MODLOADER 1034 "Instala o actualiza SADX Mod Loader (necesario).$\r$\n\
$\r$\n\
La versión de Steam de SADX se convertirá en la de 2004 antes de la instalación."
LangString DESC_LAUNCHER 1034 "Instala el Iniciador de SADX (necesario).$\r$\n\
$\r$\n\
El Iniciador de SADX es una herramienta para configurar controles."
LangString DESC_NETF 1034 "Instala o actualiza .NET Framework, que es necesario para que las herramientas de conversión funcionen adecuadamente. $\r$\n\
$\r$\n\
El instalador revisa si .NET Framework ya está instalado antes de descargarlo."
LangString DESC_NET 1034 "Instala o actualiza el runtime .NET 8.0 Desktop, que es necesario para que el Gestor de mods de SADX funcione adecuadamente."
LangString DESC_RUNTIME 1034 "Instala o actualiza los runtimes de Visual C++ 2010, 2012, 2013 y 2015/2017/2019, necesarios para que los mods basados en DLL funcionen adecuadamente."
LangString DESC_DIRECTX 1034 "Actualizar los runtimes de DirectX, que son necesarios para SADX y el mod Lantern Engine. $\r$\n\
$\r$\n\
El instalador revisa si está instalado DirectX 9.0c antes de descargarlo."
LangString DESC_PERMISSIONS 1034 "Toma propiedad del fólder de SADX y fija permisos recursivos. Esto previene errores con los permisos al habilitar o deshabilitar el Cargador de mods sin permisos de administrador.$\r$\n\
$\r$\n\
La corrección de permisos sólo funciona cuando SADX está instalado en la carpeta Archivos de programa (Program Files)."
LangString DESC_ADXAUDIO 1034 "Música y voces de mayor calidad en ADX, de la versión de Dreamcast. $\r$\n\
$\r$\n\
No es necesario si se instala en la versión de Steam, al contar ya con las voces y música en ADX."
LangString DESC_SADXFE 1034 "Un mod que contiene varias correcciones y mejoras para la versión de PC original de SADX.$\r$\n\
$\r$\n\
¡Recomendado!"
LangString DESC_SMOOTHCAM 1034 "Movimiento fluido de la cámara en primera persona."
LangString DESC_FRAMELIMIT 1034 "Un limitador de fotogramas más preciso..$\r$\n\
$\r$\n\
Corrige errores con los fotogramas y las intermitencias en algunas computadoras.$\r$\n\
$\r$\n\
Se puede jugar de manera más fluida, pero puede afectar el desempeño, especialmente en sistemas antiguos."
LangString DESC_PAUSEHIDE 1034 "Oculta el menú Pausa al presionar X+Y, como en el Dreamcast. DC Conversion ya lo incluye."
LangString DESC_ONIONBLUR 1034 "Añade un 'desenfoque de movimiento' a los pies de Sonic y las colas de Tails (como en la primera versión japonesa de Dreamcast)."
LangString DESC_DLCS 1034 "Contenido descargable exclusivo de Dreamcast recreado como un mod para SADX. $\r$\n\
$\r$\n\
Revisa la configuración del mod para más detalles."
LangString DESC_STEAM 1034 "Soporte para los logros de Steam en el port de 2004. $\r$\n\
$\r$\n\
Debes comprar la versión de Steam de SADX para que funcione este mod."
LangString DESC_LANTERN 1034 "Un mod que reimplementa el motor de iluminación de la versión de Sonic Adventure del Dreamcast en SADX. $\r$\n\
$\r$\n\
¡Recomendado!"
LangString DESC_DCMODS 1034 "Niveles, modelos de objetos, efectos, jefes, logos y más del Sonic Adventure para Dreamcast."
LangString DESC_SNDOVERHAUL 1034 "Corrige varios problemas de sonido y reemplaza la mayoría de los efectos con sonidos de más alta calidad extraídos de la versión de Dreamcast."
LangString DESC_HDGUI 1034 "Reemplaza la interfaz, como la HUD, botones, íconos de vidas, menús y más con versiones de mayor resolución."
LangString DESC_TIMEOFDAY 1034 "Puedes cambiar la hora del día al tomar el tren entre Station Square y Mystic Ruins."
LangString DESC_DCCONV 1034 "Mods que restauran/reemplazan varios recursos para que SADX parezca más la versión original de SA1."
LangString DESC_BUGFIXES 1034 "Mods que corrigen problemas o añaden mejores técnicas sin cambios mayores a los recursos o al modo de juego."
LangString DESC_SA1CHARS 1034 "Modelos de personajes de la versión de Dreamcast."
LangString DESC_MODLOADERSTUFF 1034 "Varios requisitos para que el Cargador y los mods funcionen correctamente."
LangString DESC_SUPERSONIC 1034 "Permite transformarse en Súper Sonic tras completar la Historia Final."
LangString DESC_ENHANCEMENTS 1034 "Mods que añaden nuevas características o mejoran la apariencia del SADX puro."
LangString DESC_IDLECHATTER 1034 "¡Presiona Z para oír lo que tu personaje tiene qué decir del nivel!"
LangString DESC_SADXWTR 1034 "Añade texturas alternativas para el agua en algunos niveles y rehabilita el efecto de las olas en Emerald Coast."
LangString DESC_MANAGERCLASSIC 1034 "Una versión alternativa del Gestor de mods de SADX que funciona en Windows XP."

;Other descriptions
LangString DESC_DESC 1034 "Descripción"
LangString DESC_PRESERVE 1034 "Habilitar esto preservará config.ini al actualizar y reinstalar mods."
LangString DESC_ICON 1034 "Puedes utilizar uno de los íconos personalizados disponibles en este instalador para la ventana del juego."
LangString DESC_DCMODS_ALL 1034 "Estos mods hacen que SADX se vea como la versión original de SA1 de Dreamcast.$\r$\n\
Mods incluídos: Onion Blur, Dreamcast Conversion, DC Characters, DLCs, Sound Overhaul, HD GUI, Lantern Engine, Time of Day, Idle Chatter, Smooth Camera, Frame Limit, Super Sonic."
LangString DESC_SADX_ALL 1034 "Una experiencia de SADX puro mejorada.$\r$\n\
Mods incluídos: SADX: Fixed Edition, Onion Blur, Enhanced Emerald Coast, Sound Overhaul, HD GUI, Time of Day, Idle Chatter, Pause Hide, Smooth Camera, Frame Limit, Super Sonic."
LangString DESC_MIN_ALL 1034 "Sólo mods esenciales o amigables con speedruns se instalarán.$\r$\n\
Mods incluídos: Frame Limit."
LangString DESC_STEAMCONV_ALL 1034 "Utiliza esta opción si deseas convertir la versión de Steam a la versión de 2004 con el Cargador de mods.$\r$\n\
No se instalarán mods en este modo.$\r$\n\
Si deseas instalar mods, selecciona un perfil diferente."
LangString DESC_REDIST_ALL 1034 "Utiliza esta opción para instalar DirectX, runtimes de Visual C++ y .NET Framework.$\r$\n\
Tu instalación de SADX no será afectada.$\r$\n\
No se instalará nada más en este modo. Si deseas instalar mods, selecciona un perfil diferente."
LangString DESC_CUSTOM_ALL 1034 "Instalación personalizada.$\r$\n\
Podrás seleccionar los mods a instalar y configurar los ajustes del Mod Loader."
LangString DESC_GUIDE_ALL 1034 "Modo guía.$\r$\n\
El instalador mostrará capturas comparativas para ayudarte a decidir qué mods deseas instalar."
LangString DESC_OPTIMAL 1034 "El instalador detectará la resolución de tu pantalla y configurará el juego y el Cargador de mods para la mejor calidad visual."
LangString DESC_FAILSAFE 1034 "Usa este si el juego deja de responder al iniciarlo.$\r$\n\
La resolución se fijará a 640x480 a pantalla completa, y todas las mejoras, como la pantalla completa sin bordes, se deshabilitarán."
LangString DESC_WINDOWTITLE 1034 "El título de la ventana del juego dirá $\"Sonic Adventure$\" en lugar de $\"SonicAdventureDXPC$\".$\r$\n\
$\r$\n\
ADVERTENCIA: El Editor de Chao de Fusion no detectará el juego al usar esta función."
LangString DESC_MOREMODS 1034 "Más mods de SADX"
LangString DESC_SHORTCUTS 1034 "Crear atajos en el escritorio"
LangString DESC_RUNSADX 1034 "Ejecutar Sonic Adventure DX"
LangString DESC_RUNLAUNCHER 1034 "Cambiar controles y ajustes con el Iniciador de SADX"
;Los siguientes recursos sólo se encuentran disponibles en inglés.
LangString DESC_DREAMCASTIFY 1034 "Dreamcastify - una página sobre las degradaciones de SADX (en inglés)"
LangString DESC_DISCORD 1034 "Únete al servidor de Discord de modding para SADX (primordialmente en inglés; hay usuarios capaces de brindar asistencia en español)"
LangString DESC_OPTION_MANAGERCLASSIC 1034 "Utiliza el Gestor de mods de SADX alternativo en lugar del Gestor de mods de SA moderno. Necesario en Windows XP."

;Detail output
LangString DE_SND 1034 "Convirtiendo bancos de sonido al formto de SADX 2004 (podría demorarse un poco)..."
LangString DE_INSTDATA 1034 "Creando folder instdata..."
LangString DE_7Z 1034 "Copiando 7Z..."
LangString DE_OWNER 1034 "Tomando propiedad del folder de SADX: $Permission1"
LangString DE_PERM 1034 "Permisos del folder de SADX: $Permission2"
LangString DE_REALL 1034 "Quitando todos los mods..."
LangString DE_PRGFILES 1034 "Revisando si SADX está en Archivos de programa..."
LangString DE_RECU 1034 "SADX está en Archivos de programa. Fijando permisos recursivos a la carpeta..."
LangString DE_2004FOUND 1034 "Versión de 2004 detectada (sonic.exe)"
LangString DE_MANIFEST 1034 "Fecha de manifiesto de $ModFilename: $2-$1-$0T$4:$5:$6Z"
LangString DE_TAKEOWN 1034 "Tomando propiedad de $R9: $0"
LangString DE_PERMSET1 1034 "Fijando permisos para $R9 (1): $0"
LangString DE_PERMSET2 1034 "Fijando permisos para $R9 (2): $0"
LangString DE_PERMSET3 1034 "SFijando permisos para $R9 (3): $0"
LangString DE_E_GENERIC 1034 "Extrayendo $Modname..."
LangString DE_C_EXE 1034 "Revisando MD5 de sonic.exe..."
LangString DE_E_ML 1034 "Extrayendo Cargador de mods de SADX..."
LangString DE_E_MANAGER 1034 "Extrayendo el Cargador de mods..."
LangString DE_I_ML 1034 "Instalando/Actualizando el Cargador de mods de SADX..."
LangString DE_EXEFOUND 1034 "Se encontró un sonic.exe útil, continuando la instalación del Mod Loader."
LangString DE_EXEUNK 1034 "Se detectó un sonic.exe incompatible."
LangString DE_DETECT 1034 "Detectando tipo de instalación de SADX..."
LangString DE_2010FOUND 1034 "Versión de 2010 detectada (Sonic Adventure DX.exe)"
LangString DE_C_2010 1034 "Revisando integridad de la instalación de SADX 2010..."
LangString DE_E_TOOLS 1034 "Extrayendo herramientas..."
LangString DE_C_TOOLS 1034 "Verificando herramientas..."
LangString DE_CLEANUP 1034 "Limpiando..."
LangString DE_E_LAUNCHER 1034 "Extrayendo Iniciador de SADX..."
LangString DE_CHECKNET 1034 "Revisando versión de .NET Framework..."
LangString DE_E_NETF 1034 "Instalando .NET Framework..."
LangString DE_E_NET 1034 "Instalando runtime de .NET Desktop..."
LangString DE_NETFPRESENT 1034 "Ya se encuentra instalado .NET Framework ($NetFrameworkVersion)."
LangString DE_C_VCC 1034 "Revisando runtimes de Visual C++ e instalando, de ser necesario..."
LangString DE_E_VC2019 1034 "Extrayendo runtime de Visual C++ 2019..."
LangString DE_I_VC2019 1034 "Instalando runtime de Visual C++ 2019..."
LangString DE_C_DX9 1034 "Revisando DirectX 9.0c e instalando, de ser necesario..."
LangString DE_I_DX9_1 1034 "Ejecutando instalador de DirectX (1)..."
LangString DE_I_DX9_2 1034 "Ejecutando instalador de DirectX (2)..."
LangString DE_B_SADXFE 1034 "Respaldando ajustes de SADXFE..."
LangString DE_R_SADXFE 1034 "Restaurando ajustes de SADXFE..."
LangString DE_B_GAMEC 1034 "Respaldando base de datos de controles de juego..."
LangString DE_R_GAMEC 1034 "Restaurando base de datos de controles de juego..."
LangString DE_B_DCCONV 1034 "Respaldando ajustes de Dreamcast Conversion..."
LangString DE_E_DCCONV 1034 "Extrayendo Dreamcast Conversion..."
LangString DE_R_DCCONV 1034 "Restaurando ajustes de Dreamcast Conversion..."
LangString DE_DCTITLE_ON 1034 "Habilitando ventana de título de DC Conversion..."
LangString DE_DCTITLE_OFF 1034 "Deshabilitando ventana de título de DC Conversion..."
LangString DE_WTR_ON 1034 "Habilitando SADX Style Water..."
LangString DE_WTR_OFF 1034 "Deshabilitando SADX Style Water..."
LangString DE_B_SA1CHARS 1034 "Respaldando ajustes de Dreamcast Characters..."
LangString DE_R_SA1CHARS 1034 "Restaurando ajustes de Dreamcast Characters..."
LangString DE_B_DLCS 1034 "Respaldando ajustes de Dreamcast DLCs..."
LangString DE_E_DLCS 1034 "Extrayendo contenido DLC de SA1..."
LangString DE_R_DLCS 1034 "Restaurando ajustes de Dreamcast DLCs..."
LangString DE_E_STEAM 1034 "Extrayendo Steam Achievements..."
LangString DE_MODORDER 1034 "Configurando orden de los mods..."
LangString DE_B_MLINI 1034 "Respaldando la configuración del viejo Cargador de mods..."
LangString DE_I_MLINI 1034 "Escribiendo la configuración del Cargador de mods..."
LangString DE_B_SADXINI 1034 "Respaldando ajustes del juego..."
LangString DE_I_SADXINI 1034 "Escribiendo los ajustes del juego..."
LangString DE_E_RESOURCES 1034 "Extrayendo íconos..."
LangString DE_I_RESOURCES 1034 "Copiando íconos..."
LangString DE_B_SADXWTR 1034 "Respaldando ajustes de SADX Style Water..."
LangString DE_R_SADXWTR 1034 "Restaurando ajustes de SADX Style Water..."
LangString DE_B_ONION 1034 "Respaldando ajustes de Onion Blur..."
LangString DE_R_ONION 1034 "Restaurando ajustes de Onion Blur..."

;Mod names (generic installer)
LangString MOD_SADXFE 1034 "SADXFE"
LangString MOD_FRAME 1034 "Frame Limiter"
LangString MOD_SA1CHARS 1034 "Dreamcast Characters"
LangString MOD_LANTERN 1034 "Lantern Engine"
LangString MOD_SMOOTHCAM 1034 "Smooth Camera"
LangString MOD_ONION 1034 "Onion Skin Blur"
LangString MOD_IDLE 1034 "Idle Chatter"
LangString MOD_PAUSE 1034 "Pause Hide"
LangString MOD_SUPER 1034 "Super Sonic"
LangString MOD_TIME 1034 "Time Of Day"
LangString MOD_ADX 1034 "ADX Audio"
LangString MOD_SND 1034 "Sound Overhaul"
LangString MOD_HDGUI 1034 "HD GUI"
LangString MOD_SADXWTR 1034 "SADX Style Water"

;inetc stuff
LangString INETC_DOWNLOADING 1034 "Descargando %s" 
LangString INETC_CONNECT 1034 "Conectando..."
LangString INETC_SECOND 1034 "segundo"
LangString INETC_MINUTE 1034 "minuto"
LangString INETC_HOUR 1034 "hora"
LangString INETC_PLURAL 1034 "s" 
LangString INETC_PROGRESS 1034 "%dkB (%d%%) de %dkB a %d.%01dkB/s" 
LangString INETC_REMAINING 1034 "(%d %s%s restantes)"

;Unused strings
${LangFileString} MUI_TEXT_WELCOME_INFO_TITLE " "
${LangFileString} MUI_TEXT_WELCOME_INFO_TEXT " "
${LangFileString} MUI_TEXT_FINISH_TITLE "  "
${LangFileString} MUI_TEXT_FINISH_SUBTITLE "  "
${LangFileString} MUI_TEXT_ABORT_TITLE " "
${LangFileString} MUI_TEXT_ABORT_SUBTITLE " "
${LangFileString} MUI_TEXT_FINISH_INFO_TITLE " "
${LangFileString} MUI_TEXT_FINISH_INFO_TEXT " "
${LangFileString} MUI_TEXT_FINISH_INFO_REBOOT " "
${LangFileString} MUI_TEXT_FINISH_REBOOTNOW " "
${LangFileString} MUI_TEXT_FINISH_REBOOTLATER " "
${LangFileString} MUI_TEXT_FINISH_RUN " "
${LangFileString} MUI_TEXT_FINISH_SHOWREADME " "
${LangFileString} MUI_INNERTEXT_LICENSE_BOTTOM_CHECKBOX " "
${LangFileString} MUI_INNERTEXT_LICENSE_BOTTOM_RADIOBUTTONS " "