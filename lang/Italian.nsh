!insertmacro LANGFILE "Italian" = "Italiano" =

;General strings for NSIS MUI
${LangFileString} MUI_TEXT_LICENSE_TITLE "Informazioni"
${LangFileString} MUI_TEXT_LICENSE_SUBTITLE "Leggi le seguenti note prima di proseguire con l'installazione"
${LangFileString} MUI_INNERTEXT_LICENSE_TOP "Scorri il testo o premi Pag Giù per vedere il resto delle note."
${LangFileString} MUI_INNERTEXT_LICENSE_BOTTOM "Esegui un backup della cartella dove SADX è installato. Clicca su continua per procedere."
${LangFileString} MUI_TEXT_COMPONENTS_TITLE "Scegli i componenti"
${LangFileString} MUI_TEXT_COMPONENTS_SUBTITLE "Scegli quali mod installare."
${LangFileString} MUI_INNERTEXT_COMPONENTS_DESCRIPTION_TITLE "Descrizione"
${LangFileString} MUI_INNERTEXT_COMPONENTS_DESCRIPTION_INFO "Muovi un cursore del mouse su un componente per leggere la descrizione."
${LangFileString} MUI_TEXT_DIRECTORY_TITLE "Scegli cartella d'installazione"
${LangFileString} MUI_TEXT_DIRECTORY_SUBTITLE "Seleziona la cartella d'installazione di SADX da usare con l'installer."
${LangFileString} MUI_TEXT_INSTALLING_TITLE "Installazione"
${LangFileString} MUI_TEXT_INSTALLING_SUBTITLE "Aspetta mentre SADX Mod Installer modifica i file di gioco."
${LangFileString} MUI_BUTTONTEXT_FINISH "&Fine"

;Install type names
LangString INSTTYPE_DC 1040 "Dreamcast mods"
LangString INSTTYPE_SADX 1040 "SADX + miglioramenti"
LangString INSTTYPE_MIN 1040 "Minimale/Vanilla"
LangString INSTTYPE_STEAMCONV 1040 "Da Steam al port 2004"
LangString INSTTYPE_REDIST 1040 "Redists"

;Install profile names
LangString PROFILENAME_DC 1040 "Dreamcast"
LangString PROFILENAME_SADX 1040 "Enhanced SADX"
LangString PROFILENAME_MIN 1040 "Minimale"
LangString PROFILENAME_CUSTOM 1040 "Personalizzata"
LangString PROFILENAME_STEAMCONV 1040 "Da Steam al port 2004"
LangString PROFILENAME_REDIST 1040 "Redists"

;Section names
LangString SECTIONNAME_REMOVEMODS 1040 "Rimuovi tutte le mod correnti"
LangString SECTIONNAME_PERMISSIONS 1040 "Controlla e modifica i permessi della cartella di SADX"
LangString SECTIONNAME_MODLOADER 1040 "SADX Mod Loader di MainMemory & SonicFreak94"
LangString SECTIONNAME_LAUNCHER 1040 "SADX Launcher di PkR"
LangString SECTIONNAME_DEPENDENCIES 1040 "Dipendenze del Mod Loader"
LangString SECTIONNAME_VCC 1040 "Visual C++ runtimes"
LangString SECTIONNAME_DX9 1040 "DirectX 9.0c"
LangString SECTIONNAME_BUGFIXES 1040 "Mod correzione di bug e per controlli/telecamere mods"
LangString SECTIONNAME_SADXFE 1040 "SADX: Fixed Edition di SonicFreak94"
LangString SECTIONNAME_FRAMELIMIT 1040 "Frame Limit di SonicFreak94"
LangString SECTIONNAME_DCMODS 1040 "Mod Dreamcast"
LangString SECTIONNAME_DCCONV 1040 "Dreamcast Conversion di PkR"
LangString SECTIONNAME_SA1CHARS 1040 "Dreamcast Characters Pack di ItsEasyActually"
LangString SECTIONNAME_LANTERN 1040 "Lantern Engine di SonicFreak94"
LangString SECTIONNAME_DLCS 1040 "Dreamcast DLC di PkR"
LangString SECTIONNAME_ENH 1040 "Miglioramenti e mod per gameplay"
LangString SECTIONNAME_SMOOTH 1040 "Smooth Camera di SonicFreak94"
LangString SECTIONNAME_ONION 1040 "Onion Skin Blur di SonicFreak94"
LangString SECTIONNAME_IDLE 1040 "Idle Chatter di SonicFreak94"
LangString SECTIONNAME_PAUSE 1040 "Pause Hide di SonicFreak94"
LangString SECTIONNAME_STEAM 1040 "Steam Achievements di MainMemory"
LangString SECTIONNAME_SUPER 1040 "Super Sonic di Kell & SonicFreak94"
LangString SECTIONNAME_TIME 1040 "Time of Day di PkR"
LangString SECTIONNAME_ADX 1040 "ADX voices and music (per il port 2004)"
LangString SECTIONNAME_SND 1040 "Sound Overhaul 3 di PkR"
LangString SECTIONNAME_HDGUI 1040 "HD GUI 2 di PkR & others"
LangString SECTIONNAME_SADXWTR 1040 "SADX Style Water di PkR & SteG"
LangString SECTIONNAME_MANAGERCLASSIC 1040 "SADX Mod Manager Classic di PkR"

;Guide Mode descriptions and checkboxes
LangString GUIDE_INFO_LANTERN 1040 "La mod Lantern Engine ricrea il sistema di illuminazione della versione Dreamcast in SADX per PC. Lantern Engine rimuove la lucentezza eccessiva sui personaggi e rende l'lluminazione di livelli, oggetti e personaggi più vivace."
LangString GUIDE_INFO_DCCONV 1040 "La mod DC Conversion è una modifica estensiva del gioco che lo rende identico alla versione per Dreamcast di SA1. Corregge molti bug e ripristina i livelli, le texture, i modelli, gli effetti speciali e i menù della versione Dreamcast."
LangString GUIDE_INFO_DXWTR 1040 "La mod Dreamcast Conversion ha un'opzione per abilitare delle texture alternative per l'acqua, che sono simili a quelle dell'acqua vanilla di SADX ma di migliore qualità e animate meglio. Alcune persone le preferiscono usarle nei livelli Dreamcast."
LangString GUIDE_INFO_SA1CHARS 1040 "Il Dreamcast Characters Pack è una mod che ripristina i modelli dei personaggi Dreamcast nella versione PC. Anche se i modelli di SADX hanno più poligoni, alcune persone preferiscono i modelli Dreamcast per le loro proporzioni ed estetica."
LangString GUIDE_INFO_ONION 1040 "La mod Onion Skin Blur ricrea l'effetto 'motion blur' sui piedi di Sonic e le code di Tails. Questo effetto era presente nella versione Dreamcast Giapponese di SA1, ma è stato rimosso nelle versioni successive."
LangString GUIDE_INFO_HDGUI 1040 "HD GUI 2 rimpiazza la maggior parte delle texture dell'interfaccia grafica (menù, HUD, icone delle capsule oggetto ecc.) con texture custom ad alta risoluzione."
LangString GUIDE_INST_LANTERN 1040 "Installa la mod Lantern Engine di SonicFreak94"
LangString GUIDE_INST_DCCONV 1040 "Installa la mod Dreamcast Conversion di PkR"
LangString GUIDE_INST_DXWTR 1040 "Abilita le texture SADX Style Water di SteG"
LangString GUIDE_INST_SA1CHARS 1040 "Installa la mod Dreamcast Characters Pack di ItsEasyActually"
LangString GUIDE_INST_ONION 1040 "Installa la mod Onion Skin Blur di SonicFreak94"
LangString GUIDE_INST_HDGUI 1040 "Installa la mod HD GUI 2 di PkR, Dark Sonic, Sonikko e SPEEPSHighway"

;Download strings
LangString D_DLCS 1040 "Scaricando Dreamcast DLC..."
LangString D_DCCONV 1040 "Scaricando Dreamcast Conversion..."
LangString D_STEAM 1040 "Scaricando Steam Achievements..."
LangString D_RESOURCES 1040 "Scaricando icone..."
LangString D_DX9 1040 "Scaricando DirectX Setup..."
LangString D_VC2019 1040 "Scaricando l'installer Visual C++ runtime..."
LangString D_LAUNCHER 1040 "Scaricando SADX Launcher..."
LangString D_NET 1040 "Scaricando .NET Runtime Setup..."
LangString D_NETF 1040 "Scaricando .NET Framework Setup..."
LangString D_STEAMTOOLS 1040 "Scaricando strumenti..."
LangString D_MODLOADER 1040 "Scaricando SADX Mod Loader..."
LangString D_MODMANAGER 1040 "Scaricando SADX Mod Manager..."
LangString D_FILELIST 1040 "Scaricando lista file..."
LangString D_INSTCHK 1040 "Controllo aggiornamenti installer..."
LangString D_GENERIC 1040 "Scaricando $Modname..."
LangString D_UPDATE 1040 "Controllando $UpdateFilename ..."
LangString D_INSTALLER 1040 "Scaricando l'installer..."
LangString D_INSTALLER_R 1040 "Esecuzione della nuova versione..."

;Headers
LangString MISC_INSTALLER 1040 "SADX Mod Installer"
LangString HEADER_GUIDE_TITLE 1040 "Guida alla selezione delle mod"
LangString HEADER_GUIDE_TEXT 1040 "Spunta la casella se vuoi installare questa mod. Clicca sulla foto per maggiori informazioni."
LangString HEADER_ADD_TITLE 1040 "Mod aggiuntive"
LangString HEADER_ADD_TEXT 1040 "Spunta la casella delle mod che vuoi installare."
LangString HEADER_UPDATES_TITLE 1040 "Controlla aggiornamenti"
LangString HEADER_UPDATES_TEXT 1040 "Si raccomanda di tenere aggiornate le mod e il Mod Loader."
LangString HEADER_ICON_TITLE 1040 "Seleziona icona per l'eseguibile"
LangString HEADER_ICON_TEXT 1040 "Seleziona un'icona personalizzata per l'eseguibile di SADX."
LangString HEADER_TYPE_TITLE 1040 "Tipo di installazione"
LangString HEADER_TYPE_TEXT 1040 "Seleziona tipo di installazione."

;Options
LangString OPTIONNAME_INSTMODE 1040 "Tipo di installazione"
LangString OPTIONNAME_GUIDE 1040 "Installazione guidata - seleziona questo se sei inesperto"
LangString OPTIONNAME_PRESET 1040 "Installazione con preset"
LangString OPTIONNAME_PRESETS 1040 "Preset"
LangString OPTIONNAME_ADVANCED 1040 "Mostra opzioni avanzate"
LangString OPTIONNAME_PRESERVE 1040 "Mantieni impostazioni delle mod individuali (se presenti)"
LangString OPTIONNAME_OPTIMAL 1040 "Usa impostazioni ottimali per il Mod Loader"
LangString OPTIONNAME_FAILSAFE 1040 "Usa impostazioni di base per il Mod Loader"
LangString OPTIONNAME_ICONSEL 1040 "Seleziona l'icona che preferisci per l'eseguibile di SADX."
LangString OPTIONNAME_ICON_DX 1040 "Icona salvataggio SADX del Gamecube"
LangString OPTIONNAME_ICON_HD 1040 "Icona HD di Lester LJSTAR"
LangString OPTIONNAME_ICON_SA1 1040 "Icona Copertina SA1 DC di PkR"
LangString OPTIONNAME_ICON_VM 1040 "Icona VMU di SA1 ricreata da McAleeCh"
LangString OPTIONNAME_UPDATES 1040 "Controlla aggiornamenti"
LangString OPTIONNAME_NOUPDATES 1040 "Non controllare aggiornamenti: scarica solo i file richiesti (se presenti)"
LangString OPTIONNAME_WTITLE 1040 "Cambia il titolo della finestra in $\"Sonic Adventure$\""
LangString OPTIONNAME_ICON 1040 "Scegli un'icona personalizzata per il gioco"
LangString OPTIONNAME_MANAGERCLASSIC 1040 "Usa il Classic Mod Manager"

;Messages
LangString MSG_UPDATES 1040 "Vuoi controllare se ci sono aggiornamenti?$\r$\n$\r$\n$\r$\n$\r$\n$\r$\n\
Questo installer, il SADX Mod Loader e le mod sono tutti in lavorazione. Gli aggiornamenti aggiustano bug e introducono nuove funzioni.$\r$\n\
$\r$\nMantieni le tue mod aggiornate per ricevere nuovi contenuti ed evitare problemi."
LangString MSG_OFFLINE 1040 "Per l'installazione offline, seleziona la seconda opzione."
LangString MSG_FOLDER_FOUND 1040 "Il setup ha riconosciuto la seguente cartella come cartella d'installazione di SADX.$\r$\n\
$\r$\nPer installare in un'altra cartella, clicca su Sfoglia e seleziona la cartella desiderata."
LangString MSG_FOLDER_NOTFOUND 1040 "Il setup non ha trovato la cartella d'installazione di SADX. Selezionala manualmente."
LangString MSG_DEFAULT 1040 "Il modloader sarà ripristinato alle impostazioni di base:$\r$\n\
$\r$\nRisoluzione: 640x480$\r$\n\
Schermo Intero: on$\r$\n\
Schermo Intero Senza Bordi: off$\r$\n\
VSync: off$\r$\n\
Filtraggio Texture e Mipmap: off$\r$\n\
Ridimensiona alla risoluzione nativa: off$\r$\n\
Adatta dimensione HUD: off$\r$\n\
Adatta dimensione sfondi e video: stretch$\r$\n\
$\r$\n\
Continuare?"
LangString MSG_INSTALLERUPDATE 1040 "Trovati aggiornamenti per l'installer! Proseguire con l'aggiornamento?$\r$\n\
$\r$\nLa nuova versione sarà lanciata automaticamente."
LangString MSG_FOUNDUPD 1040 "Trovati aggiornamenti per $UpdatesFound !$\r$\n\
$\r$\n$WhichUpdates$\r$\n\
La cartella instdata è stata svuotata e le ultime versioni dei pacchetti saranno installate."
LangString MSG_CHECKUPDATES 1040 "Controllo aggiornamenti Mod Loader..."
LangString MSG_PROFILE 1040 "Seleziona il tipo di installazione preferito. Puoi usare un preset di mod predefinito oppure scegliere quali installare."
LangString MSG_START 1040 "Benvenuto nel SADX Mod Installer!"
LangString MSG_WELCOME 1040 "Questo programma installerà il SADX Mod Loader di MainMemory e una selezione di mod di vari autori.$\r$\n$\r$\nL'installazione standard con tutte le mod aumenterà la grandezza della tua cartella di SADX di approssimativamente 1.3GB.$\r$\n$\r$\nSe usi la versione web di questo installer, assicurati di avere almeno 1GB di spazio disponibile sul disco con sadx_setup.exe$\r$\n$\r$\nClicca su avanti per continuare."
LangString MSG_COMPLETE 1040 "Installazione completata!"
LangString MSG_FINISH 1040 "Ora puoi giocare a SADX con le mod che hai installato.$\r$\n\
$\r$\nI file utilizzati da questo installer sono stati salvati nella cartella 'instdata'. Puoi conservarla per utilizzare l'installer in modalità offline in futuro, o cancellarla per recuperare spazio."

;Errors
LangString ERR_REQUIREDOS 1040 "Sistema Operativo incompatibile rilevato. Questo programma richiede Windows 7 SP1 o successivo per funzionare correttamente.$\r$\nContinuare?"
LangString ERR_NET_MISSING 1040 "Lo strumento Steam conversion richiede .NET Framework installato.$\r$\nInstallare .NET Framework manualmente ed eseguire l'installer nuovamente."
LangString ERR_D_RESOURCES 1040 "Download delle risorse fallito: $DownloadErrorCode. Continuare?"
LangString ERR_D_STEAM 1040 "Download Steam Achievements fallito: $DownloadErrorCode. Continuare con l'istallazione delle altre mod?"
LangString ERR_D_DLCS 1040 "Download contenuti DLC SA1 fallito: $DownloadErrorCode. Continuare con l'istallazione delle altre mod?"
LangString ERR_D_DCCONV 1040 "Download DC Conversion fallito: $DownloadErrorCode. Continuare con l'istallazione delle altre mod?"
LangString ERR_D_DX9 1040 "Download fallito: $DownloadErrorCode. Installa DirectX manualmente."
LangString ERR_D_VC2019 1040 "Download fallito: $DownloadErrorCode. Installa Visual C++ 2019 runtimes manualmente."
LangString ERR_D_LAUNCHER 1040 "Download fallito: $DownloadErrorCode. Scarica il launcher manualmente o reinstalla il programma.$\r$\n\
$\r$\n\
Se continui ad incappare in questo errore, scarica una versione offline dell'installer."
LangString ERR_D_NETFR 1040 "Download fallito: $DownloadErrorCode. Installre .NET Framework manualmente."
LangString ERR_D_NET 1040 "Download fallito: $DownloadErrorCode. Installare .NET 8.0 Desktop runtime manualmente."
LangString ERR_SIZE 1040 "Dimensione di $UpdateFilename non trovata!"
LangString ERR_FOLDER 1040 "Il setup ha trovato dei file mancanti nella tua installazione di SADX.$\r$\n\
$\r$\n\
Assicurati di installare il Mod Loader nella cartella principale di SADX (dove ci sono i file sonic.exe o Sonic Adventure DX.exe)."
LangString ERR_2004CHECK 1040 "Il setup ha rilevato che sonic.exe è incompatibile con il Mod Installer.$\r$\n\
Le seguenti versioni non sono supportate:$\r$\n\
Sold Out Software (SafeDisc DRM)$\r$\n\
Dreamcast Collection (StarForce DRM)$\r$\n\
Versione Giapponese (SafeDisc DRM)$\r$\n\
Copie piratate della versione Europea$\r$\n\
EXE modificati (traduzioni etc.) della versione Europea$\r$\n\
Ottenre una versione compatibile del gioco ed eseguire l'installer nuovamente."
LangString ERR_MODLOADER 1040 "Il setup ha rilevato che il Mod Loader non è stato scaricato o installato correttamente. $\r$\n\
Controllare la propria connessione Internet ed eseguire l'installer nuovamente.$\r$\n\
$\r$\n\
Se continui ad incappare in questo errore, scarica una versione offline dell'installer."
LangString ERR_DOWNLOAD_TOOLS 1040 "Il setup ha fallito il download di alcuni strumenti. $\r$\n\
Controlla la tua connessione ed eseguire l'installer nuovamente.$\r$\n\
$\r$\n\
Se continui ad incappare in questo errore, scarica una versione offline dell'installer."
LangString ERR_MODDOWNLOAD 1040 "Download di $ModName fallito: $DownloadErrorCode. Continuare con l'istallazione delle altre mod?"
LangString ERR_DOWNLOAD_FILE 1040 "Errore nel download del file. Riprovare?"
LangString ERR_MODDATE 1040 "Controllo dell'ultima modifica del mod.manifest per la mod $ModFilename fallito. Assicurati che la mod sia installata correttamente."
LangString ERR_MISSINGFILES 1040 "Il setup ha trovato dei file mancanti nella tua installazione di SADX.$\r$\n\
$\r$\n\
Assicurati di installare il Mod Loader nella cartella principale di SADX (dove ci sono i file sonic.exe o Sonic Adventure DX.exe).$\r$\n\
$\r$\n\
Se hai dei file di varie versioni di SADX, l'installer può fallire nel riconoscere i file. Per risolvere, inizia con un'istallazione pulita del gioco."
LangString ERR_DOWNLOAD_CANCEL_Q 1040 "Annullare il download?"
LangString ERR_DOWNLOAD_CANCEL_A 1040 "Annulla il download"
LangString ERR_DOWNLOAD_FATAL 1040 "Download fallito: $DownloadErrorCode. Ripeti l'istallazione.$\r$\n\
$\r$\n\
Se continui ad incappare in questo errore, scarica una versione offline dell'installer."
LangString ERR_DOWNLOAD_RETRY 1040 "Download fallito. Riprovare?"
LangString ERR_PERMISSION 1040 "Errore nell'impostare i permessi della cartella di SADX!$\r$\n\
$\r$\n\
Diventa il proprietario della cartella di SADX e imposta i permessi di accesso manualmente."

;Mod descriptions
LangString DESC_ADD_ALL 1040 "Queste mod cambiano il gameplay e introducono dei cambiamenti non relativi alla grafca.$\r$\n\
$\r$\nSeleziona quali mod installare."
LangString DESC_ADD_SND 1040 "Installa Sound Overhaul per migliorare la qualità dei suoni, risolvere dei bug dei suoni di SADX e ripristina suoni mancanti"
LangString DESC_ADD_DLCS 1040 "Installa la mod DLC per aggiungere gli eventi stagionali e le challenge di Sonic Adventure dalla Dreamcast"
LangString DESC_ADD_SUPER 1040 "Installa la mod Super Sonic per poterti trasformare in Super Sonic nei livelli dopo aver completato la storia principale"
LangString DESC_REMOVEMODS 1040 "Cancella tutte le mod nella cartella delle mod prima di procedere.$\r$\n\
$\r$\n\
ATTENZIONE! Quest'operazione non può essere annullata.$\r$\n\
Usala solo se hai dei problemi con le tue mod correnti."
LangString DESC_MODLOADER 1040 "Installa o aggiorna il SADX Mod Loader (obbligatorio).$\r$\n\
$\r$\n\
La versione Steam di SADX sarà trasformata nella versione SADX 2004 prima di procedere con l'installazione."
LangString DESC_LAUNCHER 1040 "Installa il launcher di SADX (obbligatorio).$\r$\n\
$\r$\n\
Il Launcher di SADX è uno strumento che serve a configurare i controlli."
LangString DESC_NETF 1040 "Installa o aggiorna .NET Framework, che è richiesto per far funzionare gli strumenti di conversione correttamente. $\r$\n\
$\r$\n\
L'installer controllerà se .NET Framework sia già installato prima di scaricarlo."
LangString DESC_NET 1040 "Installa o aggiorna .NET Framework, che serve al SADX Mod Manager per funzionare correttamente. $\r$\n\
$\r$\n\
L'installer controlla se la versione .NET Framework è già installata prima di scaricarla."
LangString DESC_RUNTIME 1040 "Installa/aggiorna le Visual C++ 2010, 2012, 2013 e 2015/2017/2019 runtimes, che servono alle mod DLL per funzionare correttamente. $\r$\n\
$\r$\n\
L'installer controlla se le Visual C++ runtime sono già installate prima di scaricarle."
LangString DESC_DIRECTX 1040 "Aggiorna le DirectX runtimes, che servono per SADX e la mod Lantern Engine. $\r$\n\
$\r$\n\
L'installer controlla se DirectX 9.0c sia già installato prima di scaricarle."
LangString DESC_PERMISSIONS 1040 "Diventa il proprietario della cartella di SADX e imposta i permessi di accesso anche alle sottocartelle. Questo previene errori provocati dai permessi di accesso quando si abilita o disabilita il Mod Loader senza permessi di amministratore.$\r$\n\
$\r$\n\
Il fix dei permessi funziona solo se SADX è installato nella cartella Program Files."
LangString DESC_ADXAUDIO 1040 "Musica di qualità più alta in formato ADX e voci della verisone Dreamcast. $\r$\n\
$\r$\n\
Non serve se si installa sulla versione Steam, perché già ha suoni e musica in formato ADX."
LangString DESC_SADXFE 1040 "Una mod che contiene vari bug fix e miglioramenti per la versione originale PC di SADX.$\r$\n\
$\r$\n\
Raccomandata!"
LangString DESC_SMOOTHCAM 1040 "Telecamera fluida in prima persona.$\r$\n\
$\r$\n\
Può essere usata insieme alla Input Mod per una telecamera più fluida."
LangString DESC_FRAMELIMIT 1040 "Un limitatore del framerate più accurato.$\r$\n\
$\r$\n\
Risolve problemi di framerate su alcuni computer.$\r$\n\
$\r$\n\
Rende il gioco più fluido, ma potrebbe avere un piccolo impatto sulla performance su computer più vecchi."
LangString DESC_PAUSEHIDE 1040 "Nasconde il menù di pausa quando si premono i tasti X+Y come sulla versione Dreamcast. La DC Conversion include già questa funzione."
LangString DESC_ONIONBLUR 1040 "Aggiunge un effetto 'motion blur (sfocatura di movimento)' ai piedi di Sonic (come nella versione Dreamcast Giapponese di SA1) e alle code di Tails."
LangString DESC_DLCS 1040 "Contenuti scaricabili esclusivi per Dreamcast ricreati sotto forma di mod per SADX. $\r$\n\
$\r$\n\
Controlla la configurazione della mod per ulteriori informazioni."
LangString DESC_STEAM 1040 "Supporto per gli obbiettivi di Steam nella versione 2004. $\r$\n\
$\r$\n\
Devi avere SADX su Steam perché questa mod funzioni."
LangString DESC_LANTERN 1040 "Una mod per SADX che reimplementa il motore di illuminazione della versione Dreamcast di Sonic Adventure. $\r$\n\
$\r$\n\
Raccomandata!"
LangString DESC_DCMODS 1040 "Livelli, modelli, effetti, boss, menù etc della versione Dreamcast di Sonic Adventure."
LangString DESC_SNDOVERHAUL 1040 "Risolve molti problemi riguardanti i suoni e rimpiazza la maggior parte degli effetti sonori con altri di qualità migliore della versione Dreamcast."
LangString DESC_HDGUI 1040 "Rimpiazza la maggior parte degli elementi GUI (Interfaccia Grafica), come l'HUD, i bottoni, le icone della vita, menù etc. con versioni ad alta risoluzione."
LangString DESC_TIMEOFDAY 1040 "Puoi cambiare momento della giornata prendendo il treno tra Station Square e Mystic Ruins."
LangString DESC_DCCONV 1040 "Delle mod che rimpiazzano/ristorano vari asset e rendono SADX simile alla versione Dreamcast di SA1."
LangString DESC_BUGFIXES 1040 "Delle mod che risolvono problemi o aggiungono miglioramenti senza cambiare asset o il gameplay."
LangString DESC_SA1CHARS 1040 "Modelli dei personaggi della verisone Dreamcast."
LangString DESC_MODLOADERSTUFF 1040 "Varie dipendenze obbligatorie per il corretto funzionamento del Mod Loader e delle mod."
LangString DESC_SUPERSONIC 1040 "Ti permette di trasformarti in Super Sonic nei livelli dopo il completamento della storia."
LangString DESC_ENHANCEMENTS 1040 "Delle mod che aggiungono nuove funzionalità di gameplay o migliorano l'aspetto della versione vanilla di SADX."
LangString DESC_IDLECHATTER 1040 "Premi Z per sentire cosa i personaggi hanno da dire riguardo il livello!"
LangString DESC_SADXWTR 1040 "Aggiunge delle texture alternative per l'acqua in alcuni livelli e ripristina l'effetto onda in Emerald Coast."
LangString DESC_MANAGERCLASSIC 1040 "Versione alternativa del SADX Mod Manager che funziona su Windows XP."

;Other descriptions
LangString DESC_DESC 1040 "Descrizione"
LangString DESC_PRESERVE 1040 "Abilitando questa opzione il file config.ini verrà conservato quando si aggiornano o reinstallano le mod."
LangString DESC_ICON 1040 "Puoi usare una delle icone personalizzate disponibili in questo installer per la finestra di gioco."
LangString DESC_DCMODS_ALL 1040 "Queste mod rendono SADX più simile alla versione Dreamcast.$\r$\n\
Mod incluse: Onion Blur, Dreamcast Conversion, DC Characters, DLCs, Sound Overhaul, HD GUI, Lantern Engine, Time of Day, Input Mod, Idle Chatter, Smooth Camera, Frame Limit, Super Sonic."
LangString DESC_SADX_ALL 1040 "Migliora l'esperienza vanilla di SADX.$\r$\n\
Mod incluse: SADX: Fixed Edition, Onion Blur, Enhanced Emerald Coast, Sound Overhaul, HD GUI, Time of Day, Input Mod, Idle Chatter, Pause Hide, Smooth Camera, Frame Limit, Super Sonic."
LangString DESC_MIN_ALL 1040 "Saranno installate solo le mod essenziali e speedrun friendly.$\r$\n\
Mod incluse: Frame Limit."
LangString DESC_STEAMCONV_ALL 1040 "Selezionare questa opzione se si vuole convertire un'installazione Steam ad una versione 2004 con il Mod Loader.$\r$\n\
Nessuna mod verrà installata con questa opzione.$\r$\n\
Se vuoi installare mod, seleziona un preset diverso."
LangString DESC_REDIST_ALL 1040 "Usa questa opzione per installare DirectX, Visual C++ runtimes e .NET Framework.$\r$\n\
La tua installazione di SADX non sarà toccata.$\r$\n\
Null'altro verrà installato con questa opzione. Se vuoi installare mod, seleziona un preset diverso."
LangString DESC_CUSTOM_ALL 1040 "Installazione personalizzata.$\r$\n\
Potrai selezionare quali mod installare e configurare il Mod Loader."
LangString DESC_GUIDE_ALL 1040 "Modalità Guidata.$\r$\n\
L'installer mostrerà degli screenshot per aiutarti a decidere quali mod installare."
LangString DESC_OPTIMAL 1040 "L'installer riconoscerà la tua risoluzione e configurerà il gioco e il Mod Loader per avere la miglior qualità grafica."
LangString DESC_FAILSAFE 1040 "Usa questa opzione se il gioco crasha al lancio.$\r$\n\
La risoluzione sarà reimpostata a 640x480 a schermo intero, e tutti i miglioramenti come Schermo Intero senza Bordi saranno annullati."
LangString DESC_WINDOWTITLE 1040 "Il titolo dela finestra di gioco sarà cambiato in $\"Sonic Adventure$\" invece di $\"SonicAdventureDXPC$\".$\r$\n\
$\r$\n\
Funziona solo con Dreamcast Conversion.$\r$\n\
ATTENZIONE: Fusion's Chao Editor non riuscirà a trovare il gioco con quest'opzione."
LangString DESC_MOREMODS 1040 "Altre mod per SADX (Solo in Inglese)"
LangString DESC_SHORTCUTS 1040 "Crea collegamenti sul desktop"
LangString DESC_RUNSADX 1040 "Apri Sonic Adventure DX"
LangString DESC_RUNLAUNCHER 1040 "Cambia i controlli e le impostazioni con il SADX Launcher"
;Le seguenti risorse sono disponibili solo in Inglese.
LangString DESC_DREAMCASTIFY 1040 "Dreamcastify - un sito sui peggioramenti di SADX (Solo in Inglese)"
LangString DESC_DISCORD 1040 "Unisciti al server Discord di SADX modding (Solo in Inglese)"
LangString DESC_OPTION_MANAGERCLASSIC 1040 "Usa il SADX Mod Manager alternativo invece del SA Manager moderno. Richiesto su Windows XP."

;Detail output
LangString DE_SND 1040 "Conversione delle soundbank al formato di SADX 2004 (potrebbe volerci un po')..."
LangString DE_INSTDATA 1040 "Creazione cartella instdata..."
LangString DE_7Z 1040 "Copia 7Z..."
LangString DE_OWNER 1040 "Cambiamento proprietario della cartella di SADX: $Permission1"
LangString DE_PERM 1040 "Cambiamento dei permessi della cartella di SADX: $Permission2"
LangString DE_REALL 1040 "Rimozione di tutte le mod..."
LangString DE_PRGFILES 1040 "Controllo se SADX è nella cartella Program Files..."
LangString DE_RECU 1040 "SADX è nella cartella Program Files. Impostazione dei permessi alle sottocartelle..."
LangString DE_2004FOUND 1040 "Rilevata versione 2004 (sonic.exe)"
LangString DE_MANIFEST 1040 "Data manifest per $ModFilename: $2-$1-$0T$4:$5:$6Z"
LangString DE_TAKEOWN 1040 "Cambiamento proprietario di $R9: $0"
LangString DE_PERMSET1 1040 "Impostazione permessi per $R9 (1): $0"
LangString DE_PERMSET2 1040 "Impostazione permessi per $R9 (2): $0"
LangString DE_PERMSET3 1040 "Impostazione permessi per $R9 (3): $0"
LangString DE_E_GENERIC 1040 "Estraendo $Modname..."
LangString DE_C_EXE 1040 "Controllo MD5 di sonic.exe..."
LangString DE_E_ML 1040 "Estrazione SADX Mod Loader..."
LangString DE_E_MANAGER 1040 "Estraendo il Mod Manager..."
LangString DE_I_ML 1040 "Installazione/aggiornamento di SADX Mod Loader..."
LangString DE_EXEFOUND 1040 "Trovato sonic.exe utilizzabile, installazione del Mod Loader."
LangString DE_EXEUNK 1040 "Sonic.exe incompatibile rilevato."
LangString DE_DETECT 1040 "Controllando il tipo di installazione di SADX..."
LangString DE_2010FOUND 1040 "Rilevata versione 2010 (Sonic Adventure DX.exe)"
LangString DE_C_2010 1040 "Controllo integrità dell'installazione di SADX 2010..."
LangString DE_E_TOOLS 1040 "Estrazione strumenti..."
LangString DE_C_TOOLS 1040 "Verifica strumenti..."
LangString DE_CLEANUP 1040 "Pulizia..."
LangString DE_E_LAUNCHER 1040 "Estrazione del SADX Launcher..."
LangString DE_CHECKNET 1040 "Controllo versione .NET Framework..."
LangString DE_E_NETF 1040 "Installazione .NET Framework..."
LangString DE_E_NET 1040 "Installazione .NET Framework..."
LangString DE_NETFPRESENT 1040 ".NET Framework è gia installato ($NetFrameworkVersion)."
LangString DE_C_VCC 1040 "Controllo Visual C++ runtimes..."
LangString DE_E_VC2019 1040 "Estrazione Visual C++ 2019 runtime..."
LangString DE_I_VC2019 1040 "Installazione Visual C++ 2019 runtime..."
LangString DE_C_DX9 1040 "Controllo DirectX 9.0c e installazione se necessaria..."
LangString DE_I_DX9_1 1040 "Esecuzione DirectX Setup (1)..."
LangString DE_I_DX9_2 1040 "Esecuzione DirectX Setup (2)..."
LangString DE_B_SADXFE 1040 "Creando un backup delle impostazioni SADXFE..."
LangString DE_R_SADXFE 1040 "Ripristino impostazioni SADXFE..."
LangString DE_B_GAMEC 1040 "Creando un backup del database dei controllers..."
LangString DE_R_GAMEC 1040 "Ripristino database dei controllers..."
LangString DE_B_DCCONV 1040 "Creando un backup delle impostazioni Dreamcast Conversion..."
LangString DE_E_DCCONV 1040 "Estrazione Dreamcast Conversion..."
LangString DE_R_DCCONV 1040 "Ripristino impostazioni Dreamcast Conversion..."
LangString DE_DCTITLE_ON 1040 "Abilitando il nome finestra DC Conversion..."
LangString DE_DCTITLE_OFF 1040 "Disabilitando il nome finestra DC Conversion..."
LangString DE_WTR_ON 1040 "Abilitando SADX Style Water..."
LangString DE_WTR_OFF 1040 "Disabilitando SADX Style Water..."
LangString DE_B_SA1CHARS 1040 "Creando un backup di impostazioni Dreamcast Characters..."
LangString DE_R_SA1CHARS 1040 "Ripristino impostazioni Dreamcast Characters..."
LangString DE_B_DLCS 1040 "Creando un backup delle impostazioni Dreamcast DLC..."
LangString DE_E_DLCS 1040 "Estrazione Dreamcast DLC..."
LangString DE_R_DLCS 1040 "Ripristino impostazioni Dreamcast DLCs..."
LangString DE_E_STEAM 1040 "Estrazione Steam Achievements..."
LangString DE_MODORDER 1040 "Configurazione Mod Loader..."
LangString DE_B_MLINI 1040 "Creando un backup delle impostazioni del Mod Loader..."
LangString DE_I_MLINI 1040 "Configurando il Mod Loader..."
LangString DE_B_SADXINI 1040 "Creando un backup delle impostazioni di gioco..."
LangString DE_I_SADXINI 1040 "Configurando impostazioni di gioco..."
LangString DE_E_RESOURCES 1040 "Estrazione icone..."
LangString DE_I_RESOURCES 1040 "Copiando icone..."
LangString DE_B_SADXWTR 1040 "Creando un backup delle impostazioni di SADX Style Water..."
LangString DE_R_SADXWTR 1040 "Ripristino delle impostazioni di SADX Style Water..."
LangString DE_B_ONION 1040 "Creando un backup delle impostazioni Onion Blur..."
LangString DE_R_ONION 1040 "Ripristino delle impostazioni di Onion Blur..."

;Mod names (generic installer)
LangString MOD_SADXFE 1040 "SADXFE"
LangString MOD_FRAME 1040 "Frame Limit"
LangString MOD_SA1CHARS 1040 "Dreamcast Characters"
LangString MOD_LANTERN 1040 "Lantern Engine"
LangString MOD_SMOOTHCAM 1040 "Smooth Camera"
LangString MOD_ONION 1040 "Onion Skin Blur"
LangString MOD_IDLE 1040 "Idle Chatter"
LangString MOD_PAUSE 1040 "Pause Hide"
LangString MOD_SUPER 1040 "Super Sonic"
LangString MOD_TIME 1040 "Time Of Day"
LangString MOD_ADX 1040 "ADX Audio"
LangString MOD_SND 1040 "Sound Overhaul"
LangString MOD_HDGUI 1040 "HD GUI"
LangString MOD_SADXWTR 1040 "SADX Style Water"

;inetc stuff
LangString INETC_DOWNLOADING 1040 "Scaricando %s"
LangString INETC_CONNECT 1040 "Connessione..."
LangString INETC_SECOND 1040 "secondo"
LangString INETC_MINUTE 1040 "minuto"
LangString INETC_HOUR 1040 "ora"
LangString INETC_PLURAL 1040 "/i/e"
LangString INETC_PROGRESS 1040 "%dkB (%d%%) di %dkB @ %d.%01dkB/s"
;Make sure there's an empty space between the first quotation mark and (%d in the string below
LangString INETC_REMAINING 1040 " (%d %s%s rimanenti)"

;Unused strings
${LangFileString} MUI_TEXT_WELCOME_INFO_TITLE " "
${LangFileString} MUI_TEXT_WELCOME_INFO_TEXT " "
${LangFileString} MUI_TEXT_FINISH_TITLE "  "
${LangFileString} MUI_TEXT_FINISH_SUBTITLE "  "
${LangFileString} MUI_TEXT_ABORT_TITLE " "
${LangFileString} MUI_TEXT_ABORT_SUBTITLE " "
${LangFileString} MUI_TEXT_FINISH_INFO_TITLE " "
${LangFileString} MUI_TEXT_FINISH_INFO_TEXT " "
${LangFileString} MUI_TEXT_FINISH_INFO_REBOOT " "
${LangFileString} MUI_TEXT_FINISH_REBOOTNOW " "
${LangFileString} MUI_TEXT_FINISH_REBOOTLATER " "
${LangFileString} MUI_TEXT_FINISH_RUN " "
${LangFileString} MUI_TEXT_FINISH_SHOWREADME " "
${LangFileString} MUI_INNERTEXT_LICENSE_BOTTOM_CHECKBOX " "
${LangFileString} MUI_INNERTEXT_LICENSE_BOTTOM_RADIOBUTTONS " "
