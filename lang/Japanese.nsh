!insertmacro LANGFILE "Japanese" = "日本語" =

;General strings
LangString MISC_INSTALLER 1041 "SADX Mod Installer"
${LangFileString} MUI_TEXT_LICENSE_TITLE "情報"
${LangFileString} MUI_TEXT_LICENSE_SUBTITLE "インストーラーを実行する前に以下の注意事項をお読みください。"
${LangFileString} MUI_INNERTEXT_LICENSE_TOP "PageDown キーを使うか下にスクロールして、残りの注意事項を確認してください。"
${LangFileString} MUI_INNERTEXT_LICENSE_BOTTOM "インストーラーを実行する前に、ソニックアドベンチャー DX フォルダをバックアップしてください。次に進むには「続行」をクリックします。"
${LangFileString} MUI_INNERTEXT_LICENSE_BOTTOM_CHECKBOX " "
${LangFileString} MUI_TEXT_COMPONENTS_TITLE "コンポーネントの選択"
${LangFileString} MUI_TEXT_COMPONENTS_SUBTITLE "インストールしたい Mod や機能を選択します。"
${LangFileString} MUI_INNERTEXT_COMPONENTS_DESCRIPTION_TITLE "説明"
${LangFileString} MUI_INNERTEXT_COMPONENTS_DESCRIPTION_INFO "マウスカーソルをコンポーネントにあわせると、説明が表示されます。"
${LangFileString} MUI_TEXT_DIRECTORY_TITLE "インストール先の選択"
${LangFileString} MUI_TEXT_DIRECTORY_SUBTITLE "インストーラーが使用するソニックアドベンチャー DX フォルダを選択します。"
${LangFileString} MUI_TEXT_INSTALLING_TITLE "インストール中"
${LangFileString} MUI_TEXT_INSTALLING_SUBTITLE "SADX Mod Installer が動作している間は、しばらくお待ちください。"
${LangFileString} MUI_BUTTONTEXT_FINISH "&終了"

;Install types and profile names
LangString INSTTYPE_DC 1041 "ドリームキャスト Mod"
LangString INSTTYPE_SADX 1041 "ソニックアドベンチャー DX + 機能強化"
LangString INSTTYPE_MIN 1041 "最小 / バニラ(オリジナル)版"
LangString PROFILENAME_DC 1041 "ドリームキャスト"
LangString PROFILENAME_SADX 1041 "エンハンスド ソニックアドベンチャー DX"
LangString PROFILENAME_MIN 1041 "最小"
LangString PROFILENAME_CUSTOM 1041 "カスタムモード"

;Section names
LangString SECTIONNAME_REMOVEMODS 1041 "すべての Mod を削除する"
LangString SECTIONNAME_PERMISSIONS 1041 "ソニックアドベンチャー DX フォルダ権限の確認と修正"
LangString SECTIONNAME_MODLOADER 1041 "SADX Mod Loader 作者 MainMemory & SonicFreak94"
LangString SECTIONNAME_LAUNCHER 1041 "SADX Launcher 作者 PkR"
LangString SECTIONNAME_DEPENDENCIES 1041 "Mod Loader 依存関係"
LangString SECTIONNAME_VCC 1041 "Visual C++ ランタイム"
LangString SECTIONNAME_DX9 1041 "DirectX 9.0c"
LangString SECTIONNAME_BUGFIXES 1041 "バグ修正と操作 / カメラ Mod"
LangString SECTIONNAME_SADXFE 1041 "SADX: Fixed Edition 作者 SonicFreak94"
LangString SECTIONNAME_INPUTMOD 1041 "Input Mod 作者 SonicFreak94"
LangString SECTIONNAME_FRAMELIMIT 1041 "Frame Limiter 作者 SonicFreak94"
LangString SECTIONNAME_CCEF 1041 "Camera Fix 作者 VeritasDL & SonicFreak94"
LangString SECTIONNAME_DCMODS 1041 "Dreamcast Mod"
LangString SECTIONNAME_DCCONV 1041 "Dreamcast Conversion 作者 PkR"
LangString SECTIONNAME_SA1CHARS 1041 "Dreamcast Characters Pack 作者 ItsEasyActually"
LangString SECTIONNAME_LANTERN 1041 "Lantern Engine 作者 SonicFreak94"
LangString SECTIONNAME_DLCS 1041 "Sonic Adventure DLCs 作者 PkR"
LangString SECTIONNAME_ENH 1041 "機能強化とゲームプレイ Mod"
LangString SECTIONNAME_SMOOTH 1041 "Smooth Camera 作者 SonicFreak94"
LangString SECTIONNAME_ONION 1041 "Onion Skin Blur 作者 SonicFreak94"
LangString SECTIONNAME_IDLE 1041 "Idle Chatter 作者 SonicFreak94"
LangString SECTIONNAME_PAUSE 1041 "Pause Hide 作者 SonicFreak94"
LangString SECTIONNAME_STEAM 1041 "Steam Achievements 作者 MainMemory"
LangString SECTIONNAME_SUPER 1041 "Super Sonic 作者 Kell & SonicFreak94"
LangString SECTIONNAME_TIME 1041 "Time of Day 作者 PkR"
LangString SECTIONNAME_ADX 1041 "ADX ボイスとミュージック (2004年移植版から)"
LangString SECTIONNAME_SND 1041 "Sound Overhaul 3 作者 PkR"
LangString SECTIONNAME_HDGUI 1041 "HD GUI 2 作者 PkR & その他"
LangString SECTIONNAME_SADXWTR 1041 "SADX Style Water by PkR & SteG"
LangString SECTIONNAME_MANAGERCLASSIC 1041 "SADX Mod Manager Classic by PkR"

;Guide Mode descriptions and checkboxes
LangString GUIDE_INFO_LANTERN 1041 "Lantern Engine Mod は、ドリームキャスト版のライティングシステムを PC 版ソニックアドベンチャー DX で再現します。Lantern Engine は、キャラクターモデルの過剰な光沢を取り除き、レベル、オブジェクト、キャラクター上の照明をより鮮やかにします。"
LangString GUIDE_INFO_DCCONV 1041 "Dreamcast Conversion はゲームの完全なオーバーホールで、ドリームキャスト版初代ソニックアドベンチャーのような見た目になります。多くのバグを修正し、ドリームキャストのレベル、テクスチャ、オブジェクトモデル、特殊効果、ブランディングを復元します。"
LangString GUIDE_INFO_DXWTR 1041 "Dreamcast Conversion には、別の水テクスチャを有効にするオプションが付属しています。これはバニラ(オリジナル)版ソニックアドベンチャー DX の水表現に似ていますが、高品質でアニメーション化されています。ユーザーによってはドリームキャストのレベルで、これらのテクスチャを使用することを好むかもしれません。"
LangString GUIDE_INFO_SA1CHARS 1041 "Dreamcast Characters Pack は、PC 版にドリームキャストのキャラクターモデルを復元する Mod です。ソニックアドベンチャー DX のキャラクターは高ポリゴンですが、プロポーションやテクスチャ、美的感覚からオリジナルのドリームキャストモデルを好むユーザーがいるかもしれません。"
LangString GUIDE_INFO_ONION 1041 "Onion Skin Blur Mod はソニックの足とテイルズの尻尾に「モーションブラー」効果を再現します。この効果は元々日本語版ドリームキャストバージョンの初代ソニックアドベンチャーにありましたが、後のリビジョン(改訂)で削除されました。"
LangString GUIDE_INFO_HDGUI 1041 "HD GUI はほとんどのGUIテクスチャ(メニュー、HUD、アイテムカプセルアイコンなど)を、高解像度カスタムテクスチャに入れ替えます。"
LangString GUIDE_INST_LANTERN 1041 "Lantern Engine (作者 SonicFreak94) をインストール"
LangString GUIDE_INST_DCCONV 1041 "Dreamcast Conversion (作者 PkR) をインストール"
LangString GUIDE_INST_DXWTR 1041 "SADX Style Water textures (作者 SteG) を有効化"
LangString GUIDE_INST_SA1CHARS 1041 "Dreamcast Characters Pack (作者 ItsEasyActually) をインストール"
LangString GUIDE_INST_ONION 1041 "Onion Skin Blur (作者 SonicFreak94) をインストール"
LangString GUIDE_INST_HDGUI 1041 "HD GUI 2 (作者 PkR, Dark Sonic, Sonikko & SPEEPSHighway) をインストール"

;Download strings
LangString D_DSOUND 1041 "DirectSound wrapper DLL ダウンロード中..."
LangString D_DLCS 1041 "Dreamcast DLC ダウンロード中..."
LangString D_D3D8 1041 "D3D8to9 DLL ダウンロード中..."
LangString D_DCCONV 1041 "Dreamcast Conversion ダウンロード中..."
LangString D_STEAM 1041 "Steam Achievements ダウンロード中..."
LangString D_RESOURCES 1041 "カスタムリソース ダウンロード中..."
LangString D_DX9 1041 "DirectX セットアップ ダウンロード中..."
LangString D_VC2010 1041 "VS2010 ランタイム ダウンロード中..."
LangString D_VC2012 1041 "VS2012 ランタイム ダウンロード中..."
LangString D_VC2013 1041 "VS2013 ランタイム ダウンロード中..."
LangString D_VC2019 1041 "VS2019 ランタイム ダウンロード中..."
LangString D_LAUNCHER 1041 "SADX Launcher ダウンロード中..."
LangString D_NET 1041 ".NET Web セットアップ ダウンロード中..."
LangString D_CHRMODELS 1041 "CHRMODELS ダウンロード中..."
LangString D_STEAMTOOLS 1041 "ツール ダウンロード中..."
LangString D_2004BIN 1041 "SADX 2004 バイナリ ダウンロード中..."
LangString D_2004EXE 1041 "SADX 2004 EXE ファイル ダウンロード中..."
LangString D_VOICES 1041 "追加ボイス ダウンロード中..."
LangString D_MODLOADER 1041 "SADX Mod Loader ダウンロード中..."
LangString D_FILELIST 1041 "ファイルリスト ダウンロード中..."
LangString D_INSTCHK 1041 "インストーラーアップデート チェック中..."
LangString D_GENERIC 1041 "$Modname ダウンロード中..."
LangString D_UPDATE 1041 "$UpdateFilename チェック中..."
LangString D_INSTALLER 1041 "インストーラー ダウンロード中..."
LangString D_INSTALLER_R 1041 "新バージョン 実行中..."

;Headers
LangString HEADER_GUIDE_TITLE 1041 "Mod 選択ガイド"
LangString HEADER_GUIDE_TEXT 1041 "この Mod をインストールする場合はチェックボックスにチェックを入れてください。スクリーンショットをクリックすると、より詳細な比較ができます。"
LangString HEADER_ADD_TITLE 1041 "追加 Mod"
LangString HEADER_ADD_TEXT 1041 "インストールしたい Mod のチェックボックスにチェックを入れます。"
LangString HEADER_UPDATES_TITLE 1041 "アップデートの確認"
LangString HEADER_UPDATES_TEXT 1041 "Mod や Mod Loader を最新の状態にしておくことをおすすめします。"
LangString HEADER_SET_TITLE 1041 "Mod Loader 構成"
LangString HEADER_SET_TEXT 1041 "SADX/Mod Loader を設定します。後から Mod Manager でいつでも変更することができます。"
LangString HEADER_ICON_TITLE 1041 "EXE アイコンの選択"
LangString HEADER_ICON_TEXT 1041 "ソニックアドベンチャー DX の EXE ファイルのカスタムアイコンを選択してください。"
LangString HEADER_TYPE_TITLE 1041 "インストールの種類"
LangString HEADER_TYPE_TEXT 1041 "インストール方法を選択してください。"

;Mod Loader settings
LangString SET_TRUE 1041 "真"
LangString SET_FALSE 1041 "偽"
LangString SET_DISP 1041 "ディスプレイオプション"
LangString SET_RES 1041 "解像度"
LangString SET_SCREEN 1041 "スクリーンモード"
LangString SET_WINDOWED 1041 "ウィンドウ"
LangString SET_FULLSCREEN 1041 "フルスクリーン"
LangString SET_BORDERLESS 1041 "ボーダーレスフルスクリーン"
LangString SET_SCALE 1041 "画面にあわせる (ダウンサンプリング用)"
LangString SET_VSYNC 1041 "VSync"
LangString SET_DETAIL 1041 "詳細レベル"
LangString SET_HIGH 1041 "高 (ベスト)"
LangString SET_LOW 1041 "低"
LangString SET_LOWEST 1041 "最低"
LangString SET_FRAMERATE 1041 "フレームレート"
LangString SET_OTHER 1041 "その他オプション"
LangString SET_PAUSE 1041 "非アクティブ時に一時停止 (ALT+TAB)"
LangString SET_UI 1041 "UI スケーリング (推奨)"
LangString SET_UPD 1041 "アップデートオプション"
LangString SET_CHECKMODUPD 1041 "Mod のアップデートをチェック"
LangString SET_CHECKUPD 1041 "アップデートの確認"
LangString SET_FREQ 1041 "アップデート頻度"
LangString SET_DAYS 1041 "日"
LangString SET_WEEKS 1041 "週"
LangString SET_HOURS 1041 "時間"
LangString SET_ALWAYS 1041 "常時"

;Options
LangString OPTIONNAME_INSTMODE 1041 "インストーラーモード"
LangString OPTIONNAME_GUIDE 1041 "ガイドモード - 不明な場合はこれを選択"
LangString OPTIONNAME_PRESET 1041 "プリセットモード"
LangString OPTIONNAME_PRESETS 1041 "プリセット"
LangString OPTIONNAME_ADVANCED 1041 "高度なオプションを表示する"
LangString OPTIONNAME_PRESERVE 1041 "各 Mod 設定を保存する"
LangString OPTIONNAME_OPTIMAL 1041 "最適な Mod Loader の設定を使用する"
LangString OPTIONNAME_MANUAL 1041 "手動で Mod Loader 設定を構成する"
LangString OPTIONNAME_FAILSAFE 1041 "Mod Loader の設定にフェイルセーフを使用する"
LangString OPTIONNAME_ICONSEL 1041 "ソニックアドベンチャー DX 実行ファイルのアイコンを選択します。"
LangString OPTIONNAME_ICON_DX 1041 "ソニックアドベンチャー DX ゲームキューブ版セーブアイコン"
LangString OPTIONNAME_ICON_HD 1041 "ソニックアドベンチャー DX HD アイコン 作者 Lester LJSTAR"
LangString OPTIONNAME_ICON_SA1 1041 "ソニックアドベンチャー ドリームキャスト版カバーアートアイコン 作者 PkR"
LangString OPTIONNAME_ICON_VM 1041 "ソニックアドベンチャー ドリームキャスト版セーブアイコン 作者 McAleeCh"
LangString OPTIONNAME_UPDATES 1041 "アップデートを確認する"
LangString OPTIONNAME_NOUPDATES 1041 "アップデートを確認しない: 必要なファイルのみダウンロードする"
LangString OPTIONNAME_WTITLE 1041 "ウィンドウタイトルを $\"Sonic Adventure$\" に変更する"
LangString OPTIONNAME_ICON 1041 "sonic.exe のカスタムアイコンを選択する"
LangString OPTIONNAME_MANAGERCLASSIC 1041 "Use the Classic Mod Manager"

;Messages
LangString MSG_UPDATES 1041 "アップデートを確認してみませんか?$\r$\n$\r$\n$\r$\n$\r$\n$\r$\n\
このインストーラーに含まれる、SADX Mod Loader と Mod はすべて開発中のものです。アップデートすることでバグを修正したり、新機能を追加したりすることがあります。$\r$\n\
$\r$\n新しいコンテンツの入手と問題回避のため、Mod を最新の状態にしておきましょう。"
LangString MSG_OFFLINE 1041 "オフラインインストールの場合は、2番目のオプションを選択します。"
LangString MSG_FOLDER_FOUND 1041 "セットアップ中に以下のフォルダをソニックアドベンチャー DX のインストール場所として検出しました。$\r$\n\
$\r$\n別のフォルダにインストールするには、[参照] をクリックして別のフォルダを選択してください。"
LangString MSG_FOLDER_NOTFOUND 1041 "セットアップ中にソニックアドベンチャー DX のインストール場所を検出できませんでした。手動でインストール場所を選択してください。"
LangString MSG_DEFAULT 1041 "Mod Loader の設定はフェイルセーフの初期設定にリセットします:$\r$\n\
$\r$\n解像度: 640x480$\r$\n\
フルスクリーン: オン$\r$\n\
ボーダーレスフルスクリーン: オフ$\r$\n\
VSync: オフ$\r$\n\
テクスチャフィルタリングとミップマップ: オフ$\r$\n\
画面にあわせる: オフ$\r$\n\
HUD スケーリング: オフ$\r$\n\
背景とムービースケーリング: 伸縮$\r$\n\
$\r$\n\
続けますか?"
LangString MSG_INSTALLERUPDATE 1041 "インストーラーのアップデートが見つかりました! 自動更新を実行しますか?$\r$\n\
$\r$\n新バージョンは自動的に再起動します。"
LangString MSG_FOUNDUPD 1041 "$UpdatesFound パッケージのアップデートが見つかりました!$\r$\n\
$\r$\n$WhichUpdates$\r$\n\
「instdata」フォルダがクリーンアップされ、代わりに上記パッケージの最新版がダウンロードされます。"
LangString MSG_CHECKUPDATES 1041 "Mod Loader のアップデートをチェックしています..."
LangString MSG_PROFILE 1041 "お好みで改造セットアップ種類を選択します。一般的な Mod プリセットを使用するか、手動でModを選択することができます。"
LangString MSG_START 1041 "SADX Mod Installer へようこそ!"
LangString MSG_WELCOME 1041 "このプログラムは、MainMemory 氏の ソニックアドベンチャーDX Mod Loader と、複数のクリエイターによって作成されたModをインストールします。$\r$\n$\r$\nすべての Mod を含む初期設定でのインストールは、ソニックアドベンチャーDX フォルダに約 1.3GB 規模のファイルを追加します。$\r$\n$\r$\nウェブ版インストーラーを使用する場合は、sadx_setup.exe のあるドライブに少なくとも 1GB の空き容量があることを確認してください。$\r$\n$\r$\n続けるには「次へ」をクリックしてください。"
LangString MSG_COMPLETE 1041 "インストールが完了しました!"
LangString MSG_FINISH 1041 "これでインストールした Mod でソニックアドベンチャー DX をプレイできるようになりました。$\r$\n\
$\r$\nこのインストーラーで使用したファイルは「instdata」フォルダに保存しています。後でオフラインでインストーラーを使用するために保管しておくこともできますし、容量節約のため削除することもできます。"

;Errors
LangString ERR_REQUIREDOS 1041 "Incompatible OS detected. This program requires Windows 7 SP1 or later to work properly.$\r$\nContinue?"
LangString ERR_NET_MISSING 1041 "The Steam conversion tool requires .NET Framework to be installed.$\r$\nPlease install .NET Framework manually and run the installer again."
LangString ERR_D_RESOURCES 1041 "リソースデータのダウンロードに失敗しました: $DownloadErrorCode. 続けますか?"
LangString ERR_D_DSOUND 1041 "ダウンロードに失敗しました: $DownloadErrorCode. dsound.dll がソニックアドベンチャー DX のメインフォルダに配置されるまで、Sound Overhaul は正常に機能しません。"
LangString ERR_D_STEAM 1041 "Steam Achievements のダウンロードに失敗しました: $DownloadErrorCode. 別の Mod のインストールを続けますか?"
LangString ERR_D_DLCS 1041 "初代ソニックアドベンチャー DLC ダウンロードに失敗しました: $DownloadErrorCode. 別の Mod のインストールを続けますか?"
LangString ERR_D_D3D8 1041 "ダウンロードに失敗しました: $DownloadErrorCode. d3d8.dll がソニックアドベンチャー DX のメインフォルダに配置されるまで、Lantern Engine は正常に機能しません。"
LangString ERR_D_DCCONV 1041 "Dreamcast Conversion ダウンロードに失敗しました: $DownloadErrorCode. 別の Mod のインストールを続けますか?"
LangString ERR_D_DX9 1041 "ダウンロードに失敗しました: $DownloadErrorCode. 手動で DirectX セットアップを実行してください。"
LangString ERR_D_VC2010 1041 "ダウンロードに失敗しました: $DownloadErrorCode. 手動で Visual C++ 2010 ランタイムをインストールしてください。"
LangString ERR_D_VC2012 1041 "ダウンロードに失敗しました: $DownloadErrorCode. 手動で Visual C++ 2012 ランタイムをインストールしてください。"
LangString ERR_D_VC2013 1041 "ダウンロードに失敗しました: $DownloadErrorCode. 手動で Visual C++ 2013 ランタイムをインストールしてください。"
LangString ERR_D_VC2019 1041 "ダウンロードに失敗しました: $DownloadErrorCode. 手動で Visual C++ 2019 ランタイムをインストールしてください。"
LangString ERR_D_LAUNCHER 1041 "ダウンロードに失敗しました: $DownloadErrorCode. 手動でランチャーをダウンロードするか、再度セットアッププログラムを実行してください。$\r$\n\
$\r$\n\
この問題が常に発生する場合は、オフライン版インストーラーをダウンロードしてください。"
LangString ERR_D_NET 1041 "ダウンロードに失敗しました: $DownloadErrorCode. 手動で .NET Framework をインストールしてください。"
LangString ERR_SIZE 1041 "$UpdateFilename サイズが見つかりません!"
LangString ERR_FOLDER 1041 "セットアップ中にインストール済みソニックアドベンチャー DX にいくつかのファイルが不足していることが検出されました。$\r$\n\
$\r$\n\
Mod Loader がソニックアドベンチャー DX のメインフォルダ(sonic.exe または Sonic Adventure DX.exe があるフォルダ)にインストールしていることを確認してください。"
LangString ERR_2004CHECK 1041 "セットアップ中にソニックアドベンチャー DX 2004年版 EXE のダウンロードに失敗したか、チェックサムが正しくないことが検出されました。 $\r$\n\
インターネットの接続状況を確認して、再度インストーラーを実行してください。$\r$\n\
$\r$\n\
この問題が常に発生する場合は、オフライン版インストーラーをダウンロードしてください。"
LangString ERR_MODLOADER 1041 "セットアップ中に Mod Loader が正しくダウンロードまたはインストールされていないことを検出しました。 $\r$\n\
インターネットの接続状況を確認して、再度インストーラーを実行してください。$\r$\n\
$\r$\n\
この問題が常に発生する場合は、オフライン版インストーラーをダウンロードしてください。"
LangString ERR_DOWNLOAD_TOOLS 1041 "セットアップ中にいくつかのツールのダウンロードに失敗したことが検出されました。 $\r$\n\
インターネットの接続状況を確認して、再度インストーラーを実行してください。$\r$\n\
$\r$\n\
この問題が常に発生する場合は、オフライン版インストーラーをダウンロードしてください。"
LangString ERR_DOWNLOAD_2004BIN 1041 "セットアップ中にいくつかのソニックアドベンチャー DX 2004年版ファイルのダウンロードに失敗したことが検出されました。$\r$\n\
インターネットの接続状況を確認して、再度インストーラーを実行してください。$\r$\n\
$\r$\n\
この問題が常に発生する場合は、オフライン版インストーラーをダウンロードしてください。"
LangString ERR_MODDOWNLOAD 1041 "$ModName のダウンロードに失敗しました: $DownloadErrorCode. 別の Mod のインストールを続けますか?"
LangString ERR_DOWNLOAD_FILE 1041 "ファイルのダウンロードでエラーが発生しました。再試行しますか?"
LangString ERR_MODDATE 1041 "Mod 名 $ModFilename の mod.manifest の最終更新日の確認に失敗しました。Mod が正しくインストールされていることを確認してください。"
LangString ERR_MISSINGFILES 1041 "セットアップ中にインストール済みソニックアドベンチャー DX にいくつかのファイルが不足していることが検出されました。$\r$\n\
$\r$\n\
Mod Loader がソニックアドベンチャー DX のメインフォルダ(sonic.exe または Sonic Adventure DX.exe があるフォルダ)にインストールしていることを確認してください。$\r$\n\
$\r$\n\
ソニックアドベンチャー DX の異なるバージョンのファイルが混在している場合、インストーラーがバージョンを正しく検出できないことがあります。これを修正するには、ゲームをクリーンな状態にしてから始めてください。"
LangString ERR_DOWNLOAD_CANCEL_Q 1041 "ダウンロードを中止しますか?"
LangString ERR_DOWNLOAD_CANCEL_A 1041 "ダウンロード中止"
LangString ERR_DOWNLOAD_FATAL 1041 "ダウンロードに失敗しました: $DownloadErrorCode. 再度セットアッププログラムを実行してください。$\r$\n\
$\r$\n\
この問題が常に発生する場合は、オフライン版インストーラーをダウンロードしてください。"
LangString ERR_DOWNLOAD_RETRY 1041 "ダウンロードに失敗しました. Would you like to try again?"
LangString ERR_PERMISSION 1041 "ソニックアドベンチャー DX フォルダの権限設定でエラーが発生しました!$\r$\n\
$\r$\n\
ソニックアドベンチャー DX フォルダの所有権を取得して、手動でアクセス許可を設定してください。"
LangString ERR_CHRMODELS 1041 "CHRMODELS.dll ダウンロード中にエラーが発生しました: $DownloadErrorCode $\r$\n\
インターネットの接続状況を確認して、再度インストーラーを実行してください。$\r$\n\
$\r$\n\
この問題が常に発生する場合は、オフライン版インストーラーをダウンロードしてください。"
LangString ERR_CHRMODELSBAD 1041 "セットアップ中に改造版 CHRMODELS.DLL が検出されました。 $\r$\n\
$\r$\n\
CHRMODELS.bak としてバックアップされて、標準の CHRMODELS.DLL に置き換えられます。これは Mod Loader が正常に動作するために必要です。$\r$\n\
$\r$\n\
CHRMODELS.DLL を改造した古い Mod を使用している場合は、MOD Loader ではサポートされていないので注意してください。"
LangString ERR_EXEMD5 1041 "ダウンロードした sonic.exe の MD5 ハッシュ値が正しくありません。セットアッププログラムを再度実行してください。$\r$\n\
$\r$\n\
この問題が常に発生する場合は、オフライン版インストーラーをダウンロードしてください。"

;Mod descriptions
LangString DESC_ADD_ALL 1041 "ゲームプレイの変更や非ビジュアルな点を改善する Mod を紹介します。$\r$\n\
$\r$\nインストールしたい Mod を選択してください。"
LangString DESC_ADD_SND 1041 "Sound Overhaul をインストールすることで音質と音量を改善し、サウンドバグを修正、失われているサウンドを復元します"
LangString DESC_ADD_DLCS 1041 "ドリームキャスト版からソニックアドベンチャーのホリデーイベントやチャレンジを追加するために DLC Mod をインストールします"
LangString DESC_ADD_SUPER 1041 "スーパーソニック Mod をインストールすると、ストーリーをクリアした後スーパーソニックに変身できるようになります"
LangString DESC_REMOVEMODS 1041 "処理を進める前に、mods フォルダ内のすべての Mod を削除してください。$\r$\n\
$\r$\n\
警告! この危険なオプションは全ての Mod を削除します。$\r$\n\
現在の Mod に問題があり、新たに始めたい場合にのみ使用してください。"
LangString DESC_MODLOADER 1041 "(必須)SADX Mod Loader のインストールまたはアップデートします。$\r$\n\
$\r$\n\
Steam 版ソニックアドベンチャー DX は 2004年バージョンに変換してからインストールします。"
LangString DESC_LAUNCHER 1041 "(必須)SADX Launcher のインストールします。$\r$\n\
$\r$\n\
SADX Launcher は Mod Loader の設定を変更したり、Input Mod による操作制御するためのツールです。"
LangString DESC_NET 1041 "SADX Mod Manager を正常に動作させるのに必要な.NET Framework をインストールまたはアップデートします。 $\r$\n\
$\r$\n\
このインストーラーでは .NET Framework がすでにインストールされているかどうかをダウンロードする前に確認します。"
LangString DESC_RUNTIME 1041 "DLL ベースの Mod が正常に動作するために必要な Visual C++ 2010 / 2012 / 2013 / 2015 / 2017 / 2019 ランタイムをインストール / アップデートします。 $\r$\n\
$\r$\n\
このインストーラーでは Visual C++ ランタイムがすでにインストールされているかどうかをダウンロードする前に確認します。"
LangString DESC_DIRECTX 1041 "ソニックアドベンチャー DX と Lantern Engine  Mod に必要な DirectX ランタイムを更新します。 $\r$\n\
$\r$\n\
このインストーラーでは DirectX 9.0c がすでにインストールされているかどうかをダウンロードする前に確認します。"
LangString DESC_PERMISSIONS 1041 "ソニックアドベンチャー DX フォルダの所有権を取得し、再帰的な権限を設定します。これにより管理者権限がなくても Mod Loader を有効にしたり無効にしたりする際の権限エラーを防ぐことができます。$\r$\n\
$\r$\n\
この権限修正は、ソニックアドベンチャー DX が Program Files フォルダにインストールされている場合にのみ実行されます。"
LangString DESC_ADXAUDIO 1041 "ドリームキャスト版の高音質 ADX 音楽とボイス。 $\r$\n\
$\r$\n\
Steam 版にはすでに ADX ボイスと音楽が収録されているため、Steam 版にインストールする際には必要ありません。"
LangString DESC_SADXFE 1041 "オリジナル PC 版ソニックアドベンチャー DX の様々なバグフィックスと改善を含む Mod です。$\r$\n\
$\r$\n\
推奨!"
LangString DESC_INPUTMOD 1041 "コントローラーボタン再割り当て機能を備えるソニックアドベンチャー DX 操作システムに全面的に刷新します。XInput と一部のDInput(DirectInput)コントローラーの問題を修正します。 $\r$\n\
$\r$\n\
推奨!"
LangString DESC_SMOOTHCAM 1041 "一人称視点でのスムーズなカメラ移動。$\r$\n\
$\r$\n\
Input Mod と併用することで、さらにスムーズな動きが可能です。"
LangString DESC_FRAMELIMIT 1041 "より正確なフレームレート制限。$\r$\n\
$\r$\n\
一部の PC 環境でのフレームレート問題(60fps ではなく 63fps ゲームで動作するなど)を修正します。$\r$\n\
$\r$\n\
ゲームプレイをスムーズにしますが、古いシステムではパフォーマンスに若干の影響があるかもしれません。"
LangString DESC_PAUSEHIDE 1041 "ドリームキャスト版のように X+Y を押すと一時停止メニューが非表示になります。Dreamcast Conversion にはすでに含まれています。"
LangString DESC_ONIONBLUR 1041 "日本語バージョンのドリームキャス版初代ソニックアドベンチャーのように、ソニックの足とテイルズの尻尾に「モーションブラー」効果を追加します。"
LangString DESC_DLCS 1041 "ドリームキャスト版限定ダウンロードコンテンツをソニックアドベンチャー DX に Mod として再現します。 $\r$\n\
$\r$\n\
詳しくは Mod 設定を参照してください。"
LangString DESC_STEAM 1041 "2004年移植版で Steam 実績をサポートします。 $\r$\n\
$\r$\n\
この Mod を動作させるには Steam 版ソニックアドベンチャー DX を所有していなければなりません。"
LangString DESC_LANTERN 1041 "ドリームキャスト版『ソニックアドベンチャー』に搭載されているライティングエンジンを再実装したソニックアドベンチャー DX 用の Mod です。 $\r$\n\
$\r$\n\
推奨!"
LangString DESC_DCMODS 1041 "ドリームキャスト版ソニックアドベンチャーのレベル、オブジェクトモデル、エフェクト、ボス、ブランディングなど。"
LangString DESC_SNDOVERHAUL 1041 "いくつかあるサウンド問題を修正し、ドリームキャスト版からリッピングされた高品質サウンドで効果音の大部分を入れ替えます。"
LangString DESC_HDGUI 1041 "HUD、ボタン、ライフアイコン、メニューなどほとんどの GUI 部分を高解像度のデータに差し替えます。"
LangString DESC_TIMEOFDAY 1041 "ステーションスクウェアとミスティックルーインの間を電車で移動することで、時間帯を変えることができます。"
LangString DESC_DCCONV 1041 "インゲーム内に様々なコンテンツを追加 / 入れ替えて、ソニックアドベンチャー DX をドリームキャスト版初代ソニックアドベンチャーのようにする Mod です。"
LangString DESC_BUGFIXES 1041 "コンテンツやゲームプレイの根幹に大きな変更を加えることなく、問題を修正したり技術的な強化を加えたりする Mod です。"
LangString DESC_SA1CHARS 1041 "ドリームキャスト版キャラクターモデル。"
LangString DESC_MODLOADERSTUFF 1041 "Mod Loader と Mod が正常に動作するためには様々な依存関係が必要です。"
LangString DESC_SUPERSONIC 1041 "最終ストーリーをクリアするとスーパーソニックに変身できます。"
LangString DESC_CCEF 1041 "レベルの再読み込み時に時にカメラが自動的に戻るのを防止します。SADXFE に含まれています。 $\r$\n\
$\r$\n\
競技などで SADXFE を使わない、やり込みプレイやタイムアタック走者に有効です。"
LangString DESC_ENHANCEMENTS 1041 "新しいゲームプレイ機能を追加したり、バニラ(オリジナル)版ソニックアドベンチャー DX の外観を改善する Mod です。"
LangString DESC_IDLECHATTER 1041 "Z キーを押すと、キャラクターがステージについて語っているのが聞けます!"
LangString DESC_SADXWTR 1041 "Adds alternative water textures to some levels and re-enables the ocean wave effect in Emerald Coast."
LangString DESC_MANAGERCLASSIC 1041 "Alternative version of the SADX Mod Manager that works on Windows XP."

;Other descriptions
LangString DESC_DESC 1041 "説明"
LangString DESC_PRESERVE 1041 "これを有効にすると、Mod を更新したり再インストールしたりする際に config.ini が保持されます。"
LangString DESC_ICON 1041 "実行ファイルは、このインストーラーで利用可能ないずれかのカスタムアイコンでパッチされます。"
LangString DESC_DCMODS_ALL 1041 "これらの Mod は、SADX はオリジナルドリームキャスト版初代ソニックアドベンチャーのように見えるようになります。$\r$\n\
Mod 内容 : Onion Blur, Dreamcast Conversion, DC Characters, DLCs, Sound Overhaul, HD GUI, Lantern Engine, Time of Day, Input Mod, Idle Chatter, Smooth Camera, Frame Limit, Super Sonic."
LangString DESC_SADX_ALL 1041 "強化されたバニラ(オリジナル)版ソニックアドンチャー DX を体験.$\r$\n\
Mod 内容: SADX: Fixed Edition, Onion Blur, Enhanced Emerald Coast, Sound Overhaul, HD GUI, Time of Day, Input Mod, Idle Chatter, Pause Hide, Smooth Camera, Frame Limit, Super Sonic."
LangString DESC_MIN_ALL 1041 "必須 / speedrun（やり込み）に適した Mod のみがインストールされます。$\r$\n\
Mod 内容: Frame Limit."
LangString DESC_CUSTOM_ALL 1041 "カスタムインストール。$\r$\n\
インストールする Mod を選択し、Mod Loader の設定ができます。"
LangString DESC_GUIDE_ALL 1041 "ガイドモード。$\r$\n\
インストーラーには比較用スクリーンショットが表示され、それぞれの Mod をインストールするかどうかを決めるのに役立ちます。"
LangString DESC_MANUAL 1041 "解像度、フレームレート、クリッピング、VSync などのオプションを手動で選択することができます。"
LangString DESC_OPTIMAL 1041 "インストーラーは画面解像度を検出し、ベストのビジュアルクオリティをゲームと Mod ローダーに設定します。"
LangString DESC_FAILSAFE 1041 "起動時にゲームがクラッシュする場合に使用します。$\r$\n\
解像度は 640×480 のフルスクリーンにリセットされ、ボーダーレスフルスクリーンなどの拡張機能はすべて無効になります。"
LangString DESC_WINDOWTITLE 1041 "ゲームのウィンドウタイトルは「$\"SonicAdventureDXPC$\"」ではなく「$\"Sonic Adventure$\"」にセットされます。.$\r$\n\
$\r$\n\
警告! Fusion's Chao Edito を使いたい場合は、この機能を有効にしないでください!"
LangString DESC_MOREMODS 1041 "他のソニックアドベンチャー DX Mod についてはこちら　（英語のみ）"
LangString DESC_SHORTCUTS 1041 "デスクトップにショートカットを作成する"
LangString DESC_RUNSADX 1041 "ソニックアドベンチャー DX を実行する"
LangString DESC_RUNLAUNCHER 1041 "SADX Launcher で操作方法と設定を変更する"
;以下のリソースは英語でのみ利用可能です。翻訳の際には、その旨を明記してください
LangString DESC_DREAMCASTIFY 1041 "Dreamcastify - ソニックアドベンチャー DX ダウングレードに関するサイト　（英語のみ）"
LangString DESC_DISCORD 1041 "SADX modding Discord サーバーに参加する　（英語のみ）"

;Detail output
LangString DE_SND 1041 "サウンドバンクをソニックアドベンチャー DX 2004 形式に変換中 (時間がかかるかもしれません)..."
LangString DE_MOV 1041 "SFD ムービーを MPG に変換中 (これもしばらくかかるかもしれません)..."
LangString DE_VOICES 1041 "追加ボイス ダウンロード中..."
LangString DE_INSTDATA 1041 "instdata フォルダ 作成中..."
LangString DE_7Z 1041 "7Z コピー中..."
LangString DE_OWNER 1041 "ソニックアドベンチャー DX フォルダの所有権を取得中: $Permission1"
LangString DE_PERM 1041 "ソニックアドベンチャー DX フォルダの権限: $Permission2"
LangString DE_REALL 1041 "すべての Mod を削除中..."
LangString DE_PRGFILES 1041 "ソニックアドベンチャー DX が Program Files にあるかどうかチェック中..."
LangString DE_RECU 1041 "ソニックアドベンチャー DX は Program Files にあります。再帰的なフォルダの権限を設定中..."
LangString DE_2004FOUND 1041 "2004年バージョンを検出しました (sonic.exe)"
LangString DE_CHRM 1041 "CHRMODELS.DLL が見つかりました。MD5 ハッシュ値チェック中..."
LangString DE_MANIFEST 1041 "$ModFilename の Manifest 日付: $2-$1-$0T$4:$5:$6Z"
LangString DE_PREVIOUS_DX 1041 "以前のソニックアドベンチャー DX 設定を読み込み中..."
LangString DE_PREVIOUS_ML 1041 "以前の Mod Loader 設定を読み込み中..."
LangString DE_TAKEOWN 1041 "$R9 の所有権を取得中: $0"
LangString DE_PERMSET1 1041 "$R9 (1) へ権限を設定中: $0"
LangString DE_PERMSET2 1041 "$R9 (2) へ権限を設定中: $0"
LangString DE_PERMSET3 1041 "$R9 (3) へ権限を設定中: $0"
LangString DE_E_GENERIC 1041 "$Modname 展開中..."
LangString DE_B_CHR 1041 "古い CHRMODELS バックアップ中..."
LangString DE_D_CHR 1041 "CHRMODELS ダウンロード中... (～1.7MB)"
LangString DE_E_CHR 1041 "CHRMODELS 展開中..."
LangString DE_C_CHR 1041 "CHRMODELS 整合性を検証中..."
LangString DE_B_EXE 1041 "sonic.exe バックアップ中..."
LangString DE_E_EXE 1041 "ソニックアドベンチャー DX 2004 EXE ファイル 展開中..."
LangString DE_C_EXE 1041 "sonic.exe の MD5 ハッシュ値 チェック中..."
LangString DE_E_BIN 1041 "ソニックアドベンチャー DX 2004 バイナリ 展開中..."
LangString DE_C_BIN 1041 "ソニックアドベンチャー DX 2004 バイナリ 検証中..."
LangString DE_E_ML 1041 "SADX Mod Loader 展開中..."
LangString DE_I_ML 1041 "SADX Mod Loader インストール / アップデート中..."
LangString DE_EXEFOUND 1041 "Mod Loader インストール進行中に使用可能な sonic.exe が見つかりました。"
LangString DE_EXEUNK 1041 "不明な sonic.exe です、US 版 EXE + Mod Loader インストールを続行します。"
LangString DE_DETECT 1041 "ソニックアドベンチャー DX のインストール種類を検出中..."
LangString DE_2010FOUND 1041 "2010年バージョンを検出しました (Sonic Adventure DX.exe)"
LangString DE_C_2010 1041 "ソニックアドベンチャー DX 2010年版 インストールの整合性をチェック中..."
LangString DE_E_TOOLS 1041 "ツール 展開中..."
LangString DE_C_TOOLS 1041 "ツール 検証中..."
LangString DE_E_SCR 1041 "スクリプト 展開中..."
LangString DE_E_VOICES 1041 "追加ボイス インストール中..."
LangString DE_SAVE 1041 "2010年版ソニックアドベンチャー DX のセーブデータを 2004年版ソニックアドベンチャー DX のセーブデータフォルダーにコピー中..."
LangString DE_DPI 1041 "sonic.exe へ高 DPI / フルスクリーン最適化の例外を追加中..."
LangString DE_FSOPT 1041 "sonic.exe へフルスクリーン最適化と DPI の例外を追加中e..."
LangString DE_FSOPTNOTFOUND 1041 "フルスクリーン最適化の切り替えが存在しませ、DPI の例外を追加中..."
LangString DE_CLEANUP 1041 "クリーンアップ中..."
LangString DE_E_LAUNCHER 1041 "SADX Launcher 展開中..."
LangString DE_CHECKNET 1041 ".NET Framework バージョン チェック中..."
LangString DE_E_NET 1041 ".NET Framework インストール中..."
LangString DE_NETPRESENT 1041 ".NET Framework がすでにインストールされています ($NetFrameworkVersion)。"
LangString DE_C_VCC 1041 "Visual C++ ランタイム チェック中、必要に応じてインストールします..."
LangString DE_E_VC2010 1041 "Visual C++ 2010 ランタイム 展開中..."
LangString DE_I_VC2010 1041 "Visual C++ 2010 ランタイム インストール中..."
LangString DE_E_VC2012 1041 "Visual C++ 2012 ランタイム 展開中..."
LangString DE_I_VC2012 1041 "Visual C++ 2012 ランタイム インストール中..."
LangString DE_E_VC2013 1041 "Visual C++ 2013 ランタイム 展開中..."
LangString DE_I_VC2013 1041 "Visual C++ 2013 ランタイム インストール中..."
LangString DE_E_VC2019 1041 "Visual C++ 2019 ランタイム 展開中..."
LangString DE_I_VC2019 1041 "Visual C++ 2019 ランタイム インストール中..."
LangString DE_C_DX9 1041 "DirectX 9.0c チェック中、必要に応じてインストールします..."
LangString DE_I_DX9_1 1041 "DirectX セットアップ (1) 実行中..."
LangString DE_I_DX9_2 1041 "DirectX セットアップ (2) 実行中..."
LangString DE_B_SADXFE 1041 "SADXFE 設定 バックアップ中..."
LangString DE_R_SADXFE 1041 "SADXFE 構成 復元中..."
LangString DE_B_INPUT 1041 "Input Mod 設定 バックアップ中..."
LangString DE_R_INPUT 1041 "Input Mod 構成 復元中..."
LangString DE_B_GAMEC 1041 "ゲームコントローラー データーベース バックアップ中..."
LangString DE_R_GAMEC 1041 "ゲームコントローラー データベース 復元中..."
LangString DE_B_DCCONV 1041 "Dreamcast Conversion 設定 バックアップ中..."
LangString DE_E_DCCONV 1041 "Dreamcast Conversion 展開中..."
LangString DE_R_DCCONV 1041 "Dreamcast Conversion 設定 復元中..."
LangString DE_DCTITLE_ON 1041 "Dreamcast Conversion ウィンドウタイトル 有効化中..."
LangString DE_DCTITLE_OFF 1041 "Dreamcast Conversion ウィンドウタイトル 無効化中..."
LangString DE_WTR_ON 1041 "SADX Style Water 有効化中..."
LangString DE_WTR_OFF 1041 "SADX Style Water 無効化中..."
LangString DE_B_SA1CHARS 1041 "Dreamcast Characters 構成 バックアップ中..."
LangString DE_R_SA1CHARS 1041 "Dreamcast Characters 構成 復元中..."
LangString DE_B_DLCS 1041 "ドリームキャスト DLC 構成 バックアップ中..."
LangString DE_E_DLCS 1041 "初代ソニックアドベンチャー DLC 展開中..."
LangString DE_R_DLCS 1041 "ドリームキャスト DLC 構成 復元中..."
LangString DE_E_STEAM 1041 "Steam Achievements 展開中..."
LangString DE_E_DSOUND 1041 "DirectSound DLL wrapper 追加中..."
LangString DE_I_DSOUNDINI 1041 "dsound.ini 追加中..."
LangString DE_MODORDER 1041 "Mod 順序 構成中..."
LangString DE_B_MLINI 1041 "SADXModLoader.ini バックアップ中..."
LangString DE_I_MLINI 1041 "SADXModLoader.ini 追加中..."
LangString DE_B_SADXINI 1041 "sonicDX.ini バックアップ中..."
LangString DE_I_SADXINI 1041 "sonicDX.ini 追加中..."
LangString DE_E_RESOURCES 1041 "リソースデータ 展開中..."
LangString DE_I_RESOURCES 1041 "カスタムリソースで sonic.exe にパッチ適用中..."
LangString DE_B_SADXWTR 1041 "Backing up SADX Style Water settings..."
LangString DE_R_SADXWTR 1041 "Restoring SADX Style Water settings..."

;Mod names (generic installer)
LangString MOD_SADXFE 1041 "SADXFE"
LangString MOD_INPUT 1041 "Input Mod"
LangString MOD_FRAME 1041 "Frame Limiter"
LangString MOD_CCEF 1041 "Camera Code Error Fix"
LangString MOD_SA1CHARS 1041 "Dreamcast Characters"
LangString MOD_LANTERN 1041 "Lantern Engine"
LangString MOD_SMOOTHCAM 1041 "Smooth Camera"
LangString MOD_ONION 1041 "Onion Skin Blur"
LangString MOD_IDLE 1041 "Idle Chatter"
LangString MOD_PAUSE 1041 "Pause Hide"
LangString MOD_SUPER 1041 "Super Sonic"
LangString MOD_TIME 1041 "Time Of Day"
LangString MOD_ADX 1041 "ADX Audio"
LangString MOD_SND 1041 "Sound Overhaul"
LangString MOD_HDGUI 1041 "HD GUI"
LangString MOD_SADXWTR 1041 "SADX Style Water"

;inetc stuff
LangString INETC_DOWNLOADING 1041 "%s ダウンロード中" 
LangString INETC_CONNECT 1041 "接続中 ..."
LangString INETC_SECOND 1041 "秒"
LangString INETC_MINUTE 1041 "分"
LangString INETC_HOUR 1041 "時間"
LangString INETC_PLURAL 1041 "s" 
LangString INETC_PROGRESS 1041 "%dkB (%d%%) / %dkB @データ転送速度 %d.%01dkB/s" 
LangString INETC_REMAINING 1041 "(残り時間 %d %s%s)"

;Unused strings
${LangFileString} MUI_TEXT_WELCOME_INFO_TITLE " "
${LangFileString} MUI_TEXT_WELCOME_INFO_TEXT " "
${LangFileString} MUI_TEXT_FINISH_TITLE "  "
${LangFileString} MUI_TEXT_FINISH_SUBTITLE "  "
${LangFileString} MUI_TEXT_ABORT_TITLE " "
${LangFileString} MUI_TEXT_ABORT_SUBTITLE " "
${LangFileString} MUI_TEXT_FINISH_INFO_TITLE " "
${LangFileString} MUI_TEXT_FINISH_INFO_TEXT " "
${LangFileString} MUI_TEXT_FINISH_INFO_REBOOT " "
${LangFileString} MUI_TEXT_FINISH_REBOOTNOW " "
${LangFileString} MUI_TEXT_FINISH_REBOOTLATER " "
${LangFileString} MUI_TEXT_FINISH_RUN " "
${LangFileString} MUI_TEXT_FINISH_SHOWREADME " "
${LangFileString} MUI_INNERTEXT_LICENSE_BOTTOM_CHECKBOX " "
${LangFileString} MUI_INNERTEXT_LICENSE_BOTTOM_RADIOBUTTONS " "

;Strings added in the 2022 update
LangString INSTTYPE_STEAMCONV 1041 "Steamから2004へ"
LangString INSTTYPE_REDIST 1041 "ランタイム"
LangString DESC_STEAMCONV_ALL 1041 "このオプションを選択すると、SADX Steam版を2004版へ交換しMod Loaderをインストールします。$\r$\n\
このモードは、Modsはインストールしません。$\r$\n\
Modsをインストールしたい場合は他のオプションを選んでください。"
LangString DESC_REDIST_ALL 1041 "このオプションを選択すると、DirectX, Visual C++, .NET Frameworkなどのランタイムのみをインストールします.$\r$\n\
このモードではゲームファイルを編集しません。$\r$\n\
ランタイム以外のものはインストールしません。Modsもインストールしたい場合は他のオプションを選んでください。"

;Strings added or renamed in the 2024 update
LangString DE_E_NETF 1041 "Installing .NET Framework..."
LangString D_NETF 1041 "Downloading .NET Framework Setup..."
LangString DE_NETFPRESENT 1041 ".NET Framework is already installed ($NetFrameworkVersion)."
LangString PROFILENAME_STEAMCONV 1041 "Steam to 2004"
LangString PROFILENAME_REDIST 1041 "Redists"
LangString DESC_OPTION_MANAGERCLASSIC 1041 "Use the alternative SADX Mod Manager instead of the modern SA Manager. Required on Windows XP."
LangString DE_B_ONION 1041 "Backing up Onion Blur settings..."
LangString DE_R_ONION 1041 "Restoring Onion Blur settings..."
LangString DESC_NETF 1041 "Install or update .NET Framework, which is required for conversion tools to work properly. $\r$\n\
$\r$\n\
The installer checks if .NET Framework is already installed before downloading it."
LangString D_MODMANAGER 1041 "Downloading SADX Mod Manager..."
LangString DE_E_MANAGER 1041 "Extracting the Mod Manager..."