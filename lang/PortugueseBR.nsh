!insertmacro LANGFILE "PortugueseBR" = "Português (Brasil)" "Portugues (Brasil)"
;Examples of other languages with translated strings
;!insertmacro LANGFILE "French" = "Français" "Francais"
;!insertmacro LANGFILE "Spanish" = "Español" "Espanol"
;!insertmacro LANGFILE "German" = "Deutsch" =

;General strings for NSIS MUI
${LangFileString} MUI_TEXT_LICENSE_TITLE "Informação"
${LangFileString} MUI_TEXT_LICENSE_SUBTITLE "Por favor, leia as seguintes notas antes de usar este instalador."
${LangFileString} MUI_INNERTEXT_LICENSE_TOP "Role para baixo ou aperte Page Down para ver o resto das notas."
${LangFileString} MUI_INNERTEXT_LICENSE_BOTTOM "Por favor, faça um backup da pasta de seu SADX antes de usar este instalador. Clique em Eu Concordo para prosseguir."
${LangFileString} MUI_TEXT_COMPONENTS_TITLE "Escolha os componentes"
${LangFileString} MUI_TEXT_COMPONENTS_SUBTITLE "Escolha quais mods e recursos deseja instalar."
${LangFileString} MUI_INNERTEXT_COMPONENTS_DESCRIPTION_TITLE "Descrição"
${LangFileString} MUI_INNERTEXT_COMPONENTS_DESCRIPTION_INFO "Mova o cursor do mouse sobre um componente para ver sua descrição."
${LangFileString} MUI_TEXT_DIRECTORY_TITLE "Escolha o local de instalação"
${LangFileString} MUI_TEXT_DIRECTORY_SUBTITLE "Selecione a pasta de jogo de seu SADX."
${LangFileString} MUI_TEXT_INSTALLING_TITLE "Instalando"
${LangFileString} MUI_TEXT_INSTALLING_SUBTITLE "Por favor, aguarde enquanto o SADX Mod Installer está trabalhando no jogo."
${LangFileString} MUI_BUTTONTEXT_FINISH "&Finalizado"

;Install type names
LangString INSTTYPE_DC 1046 "Mods de Dreamcast"
LangString INSTTYPE_SADX 1046 "SADX + melhorias"
LangString INSTTYPE_MIN 1046 "Mínima/Padrão"
LangString INSTTYPE_STEAMCONV 1046 "Steam para 2004"
LangString INSTTYPE_REDIST 1046 "Redists"

;Install profile names
LangString PROFILENAME_DC 1046 "Dreamcast"
LangString PROFILENAME_SADX 1046 "SADX melhorado"
LangString PROFILENAME_MIN 1046 "Mínima"
LangString PROFILENAME_CUSTOM 1046 "Modo customizado"
LangString PROFILENAME_STEAMCONV 1046 "Steam para 2004"
LangString PROFILENAME_REDIST 1046 "Redists"

;Section names
LangString SECTIONNAME_REMOVEMODS 1046 "Remover todos os mods atuais"
LangString SECTIONNAME_PERMISSIONS 1046 "Checar e corrigir permissões da pasta do jogo"
LangString SECTIONNAME_MODLOADER 1046 "SADX Mod Loader por MainMemory & x-hax"
LangString SECTIONNAME_LAUNCHER 1046 "SADX Launcher por PkR"
LangString SECTIONNAME_DEPENDENCIES 1046 "Dependências do Mod Loader"
LangString SECTIONNAME_VCC 1046 "Runtimes do Visual C++"
LangString SECTIONNAME_DX9 1046 "DirectX 9.0c"
LangString SECTIONNAME_BUGFIXES 1046 "Mods de correção de bugs e controle/câmera"
LangString SECTIONNAME_SADXFE 1046 "SADX: Fixed Edition por SonicFreak94"
LangString SECTIONNAME_FRAMELIMIT 1046 "Frame Limit por SonicFreak94"
LangString SECTIONNAME_DCMODS 1046 "Mods de Dreamcast"
LangString SECTIONNAME_DCCONV 1046 "Dreamcast Conversion por PkR"
LangString SECTIONNAME_SA1CHARS 1046 "Dreamcast Characters Pack por ItsEasyActually"
LangString SECTIONNAME_LANTERN 1046 "Lantern Engine por SonicFreak94"
LangString SECTIONNAME_DLCS 1046 "Dreamcast DLC por PkR"
LangString SECTIONNAME_ENH 1046 "Mods de melhorias e jogabilidade"
LangString SECTIONNAME_SMOOTH 1046 "Smooth Camera por SonicFreak94"
LangString SECTIONNAME_ONION 1046 "Onion Skin Blur por SonicFreak94"
LangString SECTIONNAME_IDLE 1046 "Idle Chatter por SonicFreak94"
LangString SECTIONNAME_PAUSE 1046 "Pause Hide por SonicFreak94"
LangString SECTIONNAME_STEAM 1046 "Steam Achievements por MainMemory"
LangString SECTIONNAME_SUPER 1046 "Super Sonic por Kell & SonicFreak94"
LangString SECTIONNAME_TIME 1046 "Time of Day por PkR"
LangString SECTIONNAME_ADX 1046 "Vozes e música em ADX (para o port de 2004)"
LangString SECTIONNAME_SND 1046 "Sound Overhaul 3 por PkR"
LangString SECTIONNAME_HDGUI 1046 "HD GUI 2 por PkR & outros"
LangString SECTIONNAME_SADXWTR 1046 "SADX Style Water por PkR & SteG"
LangString SECTIONNAME_MANAGERCLASSIC 1046 "SADX Mod Manager Clássico por PkR"

;Guide Mode descriptions and checkboxes
LangString GUIDE_INFO_LANTERN 1046 "O mod Lantern Engine recria o sistema de iluminação da versão de Dreamcast no SADX de PC. Lantern Engine remove o brilho excessivo dos modelos de personagens e torna a iluminação das fases, objetos e personagens mais vibrante."
LangString GUIDE_INFO_DCCONV 1046 "Dreamcast Conversion é uma revisão completa do jogo que faz com que ele se pareça mais com a versão de Dreamcast do SA1. Ele corrige muitos bugs e restaura as fases, texturas, modelos de objetos, efeitos especiais e marcas do Dreamcast."
LangString GUIDE_INFO_DXWTR 1046 "Dreamcast Conversion vem com uma opção para habilitar texturas alternativas de água, que são semelhantes à água padrão do SADX, mas de maior qualidade e melhor animadas. Algumas pessoas podem preferir usar essas texturas nas fases de Dreamcast."
LangString GUIDE_INFO_SA1CHARS 1046 "Dreamcast Characters Pack é um mod que restaura os modelos de personagens do Dreamcast na versão de PC. Embora os personagens de SADX tenham mais polígonos, algumas pessoas podem preferir os modelos originais do Dreamcast por suas proporções ou estética."
LangString GUIDE_INFO_ONION 1046 "O mod Onion Skin Blur recria o efeito de 'desfoque de movimento' nos pés de Sonic e nas caudas de Tails. Este efeito estava originalmente na versão japonesa de Dreamcast do SA1, mas foi removido em revisões posteriores."
LangString GUIDE_INFO_HDGUI 1046 "O HD GUI 2 substitui a maioria das texturas de GUI (menus, HUD, ícones de cápsulas de itens, etc.) por texturas personalizadas de alta resolução."
LangString GUIDE_INST_LANTERN 1046 "Instalar Lantern Engine por SonicFreak94"
LangString GUIDE_INST_DCCONV 1046 "Instalar Dreamcast Conversion por PkR"
LangString GUIDE_INST_DXWTR 1046 "Habilitar texturas de SADX Style Water por SteG"
LangString GUIDE_INST_SA1CHARS 1046 "Instalar Dreamcast Characters Pack por ItsEasyActually"
LangString GUIDE_INST_ONION 1046 "Instalar Onion Skin Blur por SonicFreak94"
LangString GUIDE_INST_HDGUI 1046 "Instalar HD GUI 2 por PkR, Dark Sonic, Sonikko e SPEEPSHighway"

;Download strings
LangString D_DLCS 1046 "Baixando Dreamcast DLC..."
LangString D_DCCONV 1046 "Baixando Dreamcast Conversion..."
LangString D_STEAM 1046 "Baixando Steam Achievements..."
LangString D_RESOURCES 1046 "Baixando ícones..."
LangString D_DX9 1046 "Baixando Instalador do DirectX..."
LangString D_VC2019 1046 "Baixando Instalador de Runtime Visual C++..."
LangString D_LAUNCHER 1046 "Baixando SADX Launcher..."
LangString D_NET 1046 "Baixando Instalador de Runtime .NET..."
LangString D_NETF 1046 "Baixando Intalador de Framework .NET..."
LangString D_STEAMTOOLS 1046 "Baixando ferramentas..."
LangString D_MODLOADER 1046 "Baixando SADX Mod Loader..."
LangString D_MODMANAGER 1046 "Baixando SADX Mod Manager..."
LangString D_FILELIST 1046 "Baixando lista de arquivos..."
LangString D_INSTCHK 1046 "Checando atualizações de instalador..."
LangString D_GENERIC 1046 "Baixando $Modname..."
LangString D_UPDATE 1046 "Checando $UpdateFilename ..."
LangString D_INSTALLER 1046 "Baixando o instalador..."
LangString D_INSTALLER_R 1046 "Executando a nova versão..."

;Headers
LangString MISC_INSTALLER 1046 "SADX Mod Installer"
LangString HEADER_GUIDE_TITLE 1046 "Guia de seleção de mods"
LangString HEADER_GUIDE_TEXT 1046 "Marque a caixa de seleção se quiser instalar este mod. Clique na captura de tela para uma comparação mais detalhada."
LangString HEADER_ADD_TITLE 1046 "Mods adicionais"
LangString HEADER_ADD_TEXT 1046 "Marque as caixas de seleção para os mods que deseja instalar."
LangString HEADER_UPDATES_TITLE 1046 "Checar por atualizações"
LangString HEADER_UPDATES_TEXT 1046 "É recomendado manter os mods e o Mod Loader atualizados."
LangString HEADER_ICON_TITLE 1046 "Selecione o ícone do seu jogo"
LangString HEADER_ICON_TEXT 1046 "Selecione um ícone personalizado para a janela do jogo."
LangString HEADER_TYPE_TITLE 1046 "Tipo de instalação"
LangString HEADER_TYPE_TEXT 1046 "Escolha o tipo de instalação."

;Options
LangString OPTIONNAME_INSTMODE 1046 "Modo do Instalador"
LangString OPTIONNAME_GUIDE 1046 "Modo guia - selecione isto se estiver incerto"
LangString OPTIONNAME_PRESET 1046 "Modo predefinido"
LangString OPTIONNAME_PRESETS 1046 "Predefinições"
LangString OPTIONNAME_ADVANCED 1046 "Mostrar opções avançadas"
LangString OPTIONNAME_PRESERVE 1046 "Preservar configurações individuais de mods (se houverem)"
LangString OPTIONNAME_OPTIMAL 1046 "Usar configurações ideais do Mod Loader"
LangString OPTIONNAME_FAILSAFE 1046 "Usar configurações à prova de falhas do Mod Loader"
LangString OPTIONNAME_ICONSEL 1046 "Escolha o ícone preferido para a janela do jogo."
LangString OPTIONNAME_ICON_DX 1046 "Ícone de salvamento do SADX de Gamecube"
LangString OPTIONNAME_ICON_HD 1046 "Ícone Customizado em HD por Lester LJSTAR"
LangString OPTIONNAME_ICON_SA1 1046 "Ícone da arte de caixa do SA1 por PkR"
LangString OPTIONNAME_ICON_VM 1046 "Ícone do VMU do SA1 recriado por McAleeCh"
LangString OPTIONNAME_UPDATES 1046 "Checar por atualizações"
LangString OPTIONNAME_NOUPDATES 1046 "Não checar por atualizações: apenas baixar arquivos necessários (se houverem)"
LangString OPTIONNAME_WTITLE 1046 "Mudar título da janela para $\"Sonic Adventure$\""
LangString OPTIONNAME_ICON 1046 "Escolher um ícone de jogo customizado"
LangString OPTIONNAME_MANAGERCLASSIC 1046 "Usar o Mod Manager Clássico"

;Messages
LangString MSG_UPDATES 1046 "Gostaria de checar por atualizações?$\r$\n$\r$\n$\r$\n$\r$\n$\r$\n\
Este instalador, SADX Mod Loader e os mods são todos trabalhos em andamento. Atualizações corrigem bugs e adicionam novos recursos.$\r$\n\
$\r$\nMantenha seus mods atualizados para receber novos conteúdos e evitar problemas."
LangString MSG_OFFLINE 1046 "Para a instalação offline, escolha a segunda opção."
LangString MSG_FOLDER_FOUND 1046 "O Instalador detectou a seguinte pasta como seu local de instalação do SADX.$\r$\n\
$\r$\nPara instalar numa pasta diferente, clique em Procurar e selecione outra pasta."
LangString MSG_FOLDER_NOTFOUND 1046 "O Instalador não pôde detectar o local de instalação do SADX. Por favor, escolha o local do jogo manualmente."
LangString MSG_DEFAULT 1046 "Suas configurações do Mod Loader serão redefinidas para os padrões à prova de falhas:$\r$\n\
$\r$\nResolução: 640x480$\r$\n\
Tela cheia: ligada$\r$\n\
Tela cheia sem bordas: desligada$\r$\n\
Sincronização Vertical: desligada$\r$\n\
$\r$\n\
Continuar?"
LangString MSG_INSTALLERUPDATE 1046 "Atualizações encontradas para o instalador! Executar uma atualização automática?$\r$\n\
$\r$\nA nova versão será reiniciada automaticamente."
LangString MSG_FOUNDUPD 1046 "Encontradas atualizações para $UpdatesFound pacote(s)!$\r$\n\
$\r$\n$WhichUpdates$\r$\n\
A pasta instdata foi limpa e as versões mais recentes dos pacotes acima serão baixadas."
LangString MSG_CHECKUPDATES 1046 "Checando atualizações para o Mod Loader..."
LangString MSG_PROFILE 1046 "Escolha o tipo favorito de instalar o SADX com mods. Você pode usar uma predefinição de mods comum ou selecionar os mods manualmente."
LangString MSG_START 1046 "Bem-vindo(a) ao SADX Mod Installer!"
LangString MSG_WELCOME 1046 "Este programa instalará o SADX Mod Loader da MainMemory e uma seleção de mods de vários criadores.$\r$\n$\r$\nUma instalação padrão com todos os mods adicionará aproximadamente 1.3 GB ao tamanho da sua pasta do SADX.$\r$\n$\r$\nSe estiver usando a versão web deste instalador, certifique-se de que também haja pelo menos 1 GB disponível na unidade com sadx_setup.exe$\r$\n$\r$\nClique em Próximo para continuar."
LangString MSG_COMPLETE 1046 "Instalação completa!"
LangString MSG_FINISH 1046 "Agora você pode jogar SADX com os mods que instalou.$\r$\n\
$\r$\nOs arquivos usados por este instalador foram salvos na pasta 'instdata'. Você pode mantê-la para usar o instalador offline depois ou excluí-la para economizar espaço."

;Errors
LangString ERR_REQUIREDOS 1046 "Este programa pode não funcionar em sistemas anteriores ao Windows XP. SP3.$\r$\nContinuar mesmo assim?"
LangString ERR_NET_MISSING 1046 "A ferramenta de conversão requer que o .NET Framework 4.0 seja instalado.$\r$\nPor favor, instale o .NET Framework manualmente e execute o instalador novamente."
LangString ERR_D_RESOURCES 1046 "Download de ícone falhou: $DownloadErrorCode. Continuar?"
LangString ERR_D_STEAM 1046 "Download de Steam Achievements falhou: $DownloadErrorCode. Continuar instalando outros mods?"
LangString ERR_D_DLCS 1046 "Download de Dreamcast DLC falhou: $DownloadErrorCode. Continuar instalando outros mods?"
LangString ERR_D_DCCONV 1046 "Download de DC Conversion falhou: $DownloadErrorCode. Continuar instalando outros mods?"
LangString ERR_D_DX9 1046 "Download falhou: $DownloadErrorCode. Por favor, execute o instalador do DirectX manualmente."
LangString ERR_D_VC2019 1046 "Download falhou: $DownloadErrorCode. Por favor, instale os runtimes do Visual C++ manualmente."
LangString ERR_D_LAUNCHER 1046 "Download falhou: $DownloadErrorCode. Por favor, baixe o launcher manualmente ou execute o programa de instalação novamente.$\r$\n\
$\r$\n\
Se estiver enfrentando esse problema constantemente, por favor, baixe uma versão offline do instalador."
LangString ERR_D_NETFR 1046 "Download falhou: $DownloadErrorCode. Por favor, instale o .NET Framework manualmente."
LangString ERR_D_NET 1046 "Download falhou: $DownloadErrorCode. Por favor, instale o runtime do .NET 8.0 Desktop manualmente."
LangString ERR_SIZE 1046 "Tamanho do $UpdateFilename não encontrado!"
LangString ERR_FOLDER 1046 "O Instalador detectou que alguns arquivos estão ausentes na instalação do SADX.$\r$\n\
$\r$\n\
Certifique-se de instalar o Mod Loader na pasta principal do SADX (onde sonic.exe ou Sonic Adventure DX.exe está)."
LangString ERR_2004CHECK 1046 "O Instalador detectou que sonic.exe é incompatível com o Mod Installer.$\r$\n\
As seguintes versões não são suportadas:$\r$\n\
Dreamcast Collection 2010 (StarForce DRM)$\r$\n\
Sold Out Software (SafeDisc DRM)$\r$\n\
Lançamento Japonês (SafeDisc DRM)$\r$\n\
Cópias crackeadas da versão Européia$\r$\n\
EXEs Hackeados (traduções, etc.) da versão Européia$\r$\n\
Por favor, obtenha uma versão compatível do jogo e execute o instalador novamente."
LangString ERR_MODLOADER 1046 "O Instalador detectou que o Mod Loader não foi baixado ou instalado corretamente. $\r$\n\
Por favor, verifique sua conexão com a Internet e execute o instalador novamente.$\r$\n\
$\r$\n\
Se estiver enfrentando esse problema constantemente, por favor, baixe uma versão offline do instalador."
LangString ERR_DOWNLOAD_TOOLS 1046 "Setup has detected that some tools have failed to download. $\r$\n\
Por favor, verifique sua conexão com a Internet e execute o instalador novamente.$\r$\n\
$\r$\n\
Se estiver enfrentando esse problema constantemente, por favor, baixe uma versão offline do instalador."
LangString ERR_MODDOWNLOAD 1046 "O download de $ModName falhou: $DownloadErrorCode. Continuar instalando outros mods?"
LangString ERR_DOWNLOAD_FILE 1046 "Erro baixando o arquivo. Tentar novamente?"
LangString ERR_MODDATE 1046 "Falha ao verificar a data da última modificação do mod.manifest para o mod $ModFilename. Certifique-se de que o mod foi instalado corretamente."
LangString ERR_MISSINGFILES 1046 "O Instalador detectou que alguns arquivos estão ausentes em sua instalação do SADX.$\r$\n\
$\r$\n\
Certifique-se de instalar o Mod Loader na pasta principal do SADX (onde sonic.exe ou Sonic Adventure DX.exe está).$\r$\n\
$\r$\n\
Se tiver arquivos misturados de diferentes versões do SADX, o instalador pode não conseguir detectar sua versão corretamente. Para corrigir isso, reinstale uma versão limpa do jogo."
LangString ERR_DOWNLOAD_CANCEL_Q 1046 "Cancelar download?"
LangString ERR_DOWNLOAD_CANCEL_A 1046 "Cancelar download"
LangString ERR_DOWNLOAD_FATAL 1046 "Download falhou: $DownloadErrorCode. Por favor, execute o instalador novamente.$\r$\n\
$\r$\n\
Se estiver enfrentando esse problema constantemente, por favor, baixe uma versão offline do instalador."
LangString ERR_DOWNLOAD_RETRY 1046 "Download falhou. Gostaria de tentar novamente?"
LangString ERR_PERMISSION 1046 "Erro ao definir permissões da pasta SADX!$\r$\n\
$\r$\n\
Por favor, assuma a propriedade de sua pasta SADX e defina as permissões de acesso manualmente."

;Mod descriptions
LangString DESC_ADD_ALL 1046 "Esses mods introduzem mudanças na jogabilidade e melhorias não visuais.$\r$\n\
$\r$\nEscolha quais mods deseja intalar."
LangString DESC_ADD_SND 1046 "Instalar o Sound Overhaul para melhorar a qualidade e o volume do som, corrigir bugs de som do SADX e restaurar sons ausentes"
LangString DESC_ADD_DLCS 1046 "Instalar o mod de DLCs para adicionar eventos festivos de Sonic Adventure e desafios do Dreamcast"
LangString DESC_ADD_SUPER 1046 "Instalar o mod Super Sonic para poder se transformar no Super Sonic em Fases de Ação depois de terminar a história"
LangString DESC_REMOVEMODS 1046 "Exclui todos os mods da pasta mods antes de prosseguir.$\r$\n\
$\r$\n\
AVISO! Isso não pode ser desfeito.$\r$\n\
Use esta opção apenas se estiver tendo problemas com os mods atuais."
LangString DESC_MODLOADER 1046 "Instala ou atualiza o SADX Mod Loader (necessário).$\r$\n\
$\r$\n\
A versão de Steam do SADX será convertida para a versão de 2004 antes da instalação."
LangString DESC_LAUNCHER 1046 "Instala o SADX Launcher (necessário).$\r$\n\
$\r$\n\
SADX Launcher é uma ferramenta para configurar controles."
LangString DESC_NETF 1046 "Instalar ou atualizar o .NET Framework, que é necessário para que as ferramentas de conversão funcionem corretamente. $\r$\n\
$\r$\n\
O instalador verifica se o .NET Framework já está instalado antes de baixá-lo."
LangString DESC_NET 1046 "Instala ou atualiza o runtime do .NET 8.0 Desktop, que é necessário para que o SA Mod Manager funcione corretamente."
LangString DESC_RUNTIME 1046 "Instala/atualiza os runtimes do Visual C++ de 2010, 2012, 2013 e 2015/2017/2019, que são necessários para que os mods baseados em DLL funcionem corretamente."
LangString DESC_DIRECTX 1046 "Atualiza os runtimes do DirectX, que são necessários para o SADX e o mod Lantern Engine. $\r$\n\
$\r$\n\
O instalador checa se o DirectX 9.0c já está instalado antes de baixá-lo."
LangString DESC_PERMISSIONS 1046 "Apropria-se da pasta SADX e define permissões recursivas. Isso previne erros de permissão ao ativar ou desativar o Mod Loader sem direitos de administrador.$\r$\n\
$\r$\n\
A correção de permissão só é executada quando o SADX é instalado na pasta Arquivos de Programas."
LangString DESC_ADXAUDIO 1046 "Música e vozes de alta qualidade em ADX da versão de Dreamcast. $\r$\n\
$\r$\n\
Não é necessário instalar na versão de Steam, pois ela já possui vozes e músicas em ADX."
LangString DESC_SADXFE 1046 "Um mod contendo várias correções de bugs e melhorias para a versão original de PC do SADX.$\r$\n\
$\r$\n\
Recomendado!"
LangString DESC_SMOOTHCAM 1046 "Movimento de câmera mais suave em primeira pessoa."
LangString DESC_FRAMELIMIT 1046 "Um limitador de taxa de quadros mais preciso.$\r$\n\
$\r$\n\
Corrige a taxa de quadros incorreta e a gagueira em alguns computadores.$\r$\n\
$\r$\n\
Torna a jogabilidade mais suave, mas pode afetar o desempenho, especialmente em sistemas mais antigos."
LangString DESC_PAUSEHIDE 1046 "Oculta o menu de Pausa quando X+Y é pressionado como no Dreamcast. DC Conversion já inclui isso."
LangString DESC_ONIONBLUR 1046 "Adiciona um efeito de 'desfoque de movimento' aos pés de Sonic (como na versão japonesa de Dreamcast do SA1) e às caudas de Tails."
LangString DESC_DLCS 1046 "Conteúdo para download exclusivo de Dreamcast recriado como um mod para SADX. $\r$\n\
$\r$\n\
Veja a configuração do mod para mais detalhes."
LangString DESC_STEAM 1046 "Suporte para conquistas da Steam na versão de 2004. $\r$\n\
$\r$\n\
Você deve possuir SADX na Steam para que este mod funcione."
LangString DESC_LANTERN 1046 "Um mod para SADX que reimplementa o motor de iluminação da versão de Dreamcast do Sonic Adventure. $\r$\n\
$\r$\n\
Recomendado!"
LangString DESC_DCMODS 1046 "Fases, modelos de objetos, efeitos, chefes, marcas, etc. do Sonic Adventure de Dreamcast."
LangString DESC_SNDOVERHAUL 1046 "Corrige vários problemas de som e substitui a maioria dos efeitos sonoros por sons de qualidade superior extraídos da versão de Dreamcast."
LangString DESC_HDGUI 1046 "Substitui a maioria dos elementos da GUI, como HUD, botões, ícones de vida, menus, etc., por versões de resolução maior."
LangString DESC_TIMEOFDAY 1046 "Você pode mudar a hora do dia pegando o trem entre a Station Square e as Mystic Ruins."
LangString DESC_DCCONV 1046 "Mods que adicionam de volta/substituem vários recursos do jogo e tornam o SADX mais parecido com a versão de Dreamcast do SA1."
LangString DESC_BUGFIXES 1046 "Mods que corrigem problemas ou adicionam melhorias técnicas sem grandes alterações nos recursos ou na jogabilidade principal."
LangString DESC_SA1CHARS 1046 "Modelos de personagens da versão de Dreamcast."
LangString DESC_MODLOADERSTUFF 1046 "Várias dependências necessárias para que o Mod Loader e os mods funcionem corretamente."
LangString DESC_SUPERSONIC 1046 "Permite se transformar em Super Sonic após completar a história final."
LangString DESC_ENHANCEMENTS 1046 "Mods que adicionam novos recursos de jogabilidade ou melhoram a aparência do SADX padrão."
LangString DESC_IDLECHATTER 1046 "Aperte Z para ouvir o que seu personagem tem a dizer sobre a fase!"
LangString DESC_SADXWTR 1046 "Adiciona texturas de água alternativas à algumas fases e reativa o efeito de onda do oceano na Emerald Coast."
LangString DESC_MANAGERCLASSIC 1046 "Versão alternativa do SADX Mod Manager que funciona no Windows XP."

;Other descriptions
LangString DESC_DESC 1046 "Descrição"
LangString DESC_PRESERVE 1046 "Habilitar isso preservará o config.ini ao atualizar e reinstalar alguns mods."
LangString DESC_ICON 1046 "Você pode usar um dos ícones personalizados disponíveis neste instalador para a janela do jogo."
LangString DESC_DCMODS_ALL 1046 "Esses mods fazem o SADX parecer mais com a versão original de Dreamcast do SA1.$\r$\n\
Mods incluídos: Onion Blur, Dreamcast Conversion, DC Characters, DLC, Sound Overhaul, HD GUI, Lantern Engine, Time of Day, Idle Chatter, Smooth Camera, Frame Limit, Super Sonic."
LangString DESC_SADX_ALL 1046 "Experiência melhorada do SADX padrão.$\r$\n\
Mods incluídos: SADX: Fixed Edition, Onion Blur, Enhanced Emerald Coast, Sound Overhaul, HD GUI, Time of Day, Idle Chatter, Pause Hide, Smooth Camera, Frame Limit, Super Sonic."
LangString DESC_MIN_ALL 1046 "Apenas mods essenciais/amigáveis para speedrun serão instalados.$\r$\n\
Mods incluídos: Frame Limit."
LangString DESC_STEAMCONV_ALL 1046 "Use esta opção se quiser converter uma instalação da Steam para a versão 2004 com o Mod Loader.$\r$\n\
Nenhum mod será instalado neste modo.$\r$\n\
Se também deseja instalar mods, selecione um perfil diferente."
LangString DESC_REDIST_ALL 1046 "Use essa opção para instalar o DirectX, os runtimes do Visual C++ e o .NET Framework.$\r$\n\
Sua instalação do SADX não será afetada.$\r$\n\
Nada mais será instalado neste modo. Se também deseja instalar mods, selecione um perfil diferente."
LangString DESC_CUSTOM_ALL 1046 "Instalação customizada.$\r$\n\
Você poderá escolher quais mods instalar."
LangString DESC_GUIDE_ALL 1046 "Modo guia.$\r$\n\
O instalador mostrará capturas de tela de comparação para ajudá-lo a decidir se deseja instalar cada mod."
LangString DESC_OPTIMAL 1046 "O instalador detectará sua resolução de tela e configurará o jogo e o Mod Loader para a melhor qualidade visual."
LangString DESC_FAILSAFE 1046 "Use isso se o jogo travar na inicialização.$\r$\n\
A resolução será redefinida para tela cheia de 640x480 e todos os aprimoramentos, como tela cheia sem bordas, serão desativados."
LangString DESC_WINDOWTITLE 1046 "O título da janela do jogo será definido como $\"Sonic Adventure$\" em vez de $\"SonicAdventureDXPC$\".$\r$\n\
Só funciona com o Dreamcast Conversion.$\r$\n\
AVISO: O Chao Editor do Fusion não detectará o jogo com esta opção."
LangString DESC_MOREMODS 1046 "Mais mods de SADX"
LangString DESC_SHORTCUTS 1046 "Criar atalhos para a área de trabalho"
LangString DESC_RUNSADX 1046 "Executar Sonic Adventure DX"
LangString DESC_RUNLAUNCHER 1046 "Alterar controles e configurações com o SADX Launcher"
;The tools and web resources below are only available in English. Please indicate that in your translation
LangString DESC_DREAMCASTIFY 1046 "Dreamcastify - um site sobre piorias do SADX (disponível somente em Inglês)"
LangString DESC_DISCORD 1046 "Juntar-se ao servidor do Discord de modding de SADX (disponível somente em Inglês)"
LangString DESC_OPTION_MANAGERCLASSIC 1046 "Usar o SADX Mod Manager alternativo em vez do SA Manager moderno. Necessário no Windows XP. (disponível somente em Inglês)"

;Detail output
LangString DE_SND 1046 "Convertendo dados para o formato do SADX 2004 (isso pode demorar um pouco)..."
LangString DE_INSTDATA 1046 "Criando pasta instdata..."
LangString DE_7Z 1046 "Copiando 7Z..."
LangString DE_OWNER 1046 "Assumindo a propriedade da pasta SADX: $Permission1"
LangString DE_PERM 1046 "Permissões da pasta do SADX: $Permission2"
LangString DE_REALL 1046 "Removendo todos os mods..."
LangString DE_PRGFILES 1046 "Checando se o SADX está em Arquivos de Programas..."
LangString DE_RECU 1046 "SADX está em Arquivos de Programas. Definindo permissões de pasta recursivas..."
LangString DE_2004FOUND 1046 "Versão de 2004 detectada (sonic.exe)"
LangString DE_MANIFEST 1046 "Data de manifesto para $ModFilename: $2-$1-$0T$4:$5:$6Z"
LangString DE_TAKEOWN 1046 "Tomando propriedade de $R9: $0"
LangString DE_PERMSET1 1046 "Definindo permissões para $R9 (1): $0"
LangString DE_PERMSET2 1046 "Definindo permissões para $R9 (2): $0"
LangString DE_PERMSET3 1046 "Definindo permissões para $R9 (3): $0"
LangString DE_E_GENERIC 1046 "Extraindo $Modname..."
LangString DE_C_EXE 1046 "Checando MD5 de sonic.exe..."
LangString DE_E_ML 1046 "Extraindo SADX Mod Loader..."
LangString DE_E_MANAGER 1046 "Extraindo o Mod Manager..."
LangString DE_I_ML 1046 "Instalando/atualizando SADX Mod Loader..."
LangString DE_EXEFOUND 1046 "Encontrado sonic.exe utilizável, prosseguindo com a instalação do Mod Loader."
LangString DE_EXEUNK 1046 "Sonic.exe incompatível detectado."
LangString DE_DETECT 1046 "Detectando o tipo de instalação do SADX..."
LangString DE_2010FOUND 1046 "Versão de 2010 detectada (Sonic Adventure DX.exe)"
LangString DE_C_2010 1046 "Checando integridade da instalação de SADX 2010..."
LangString DE_E_TOOLS 1046 "Extraindo ferramentas..."
LangString DE_C_TOOLS 1046 "Verificando ferramentas..."
LangString DE_CLEANUP 1046 "Limpando..."
LangString DE_E_LAUNCHER 1046 "Extraindo SADX Launcher..."
LangString DE_CHECKNET 1046 "Checando a versão do .NET Framework..."
LangString DE_E_NETF 1046 "Instalando .NET Framework..."
LangString DE_E_NET 1046 "Instalando .NET Desktop Runtime..."
LangString DE_NETFPRESENT 1046 ".NET Framework já está instalado ($NetFrameworkVersion)."
LangString DE_C_VCC 1046 "Checando runtimes do Visual C++..."
LangString DE_E_VC2019 1046 "Extraindo runtimes do Visual C++..."
LangString DE_I_VC2019 1046 "Instalando runtimes do Visual C++..."
LangString DE_C_DX9 1046 "Checando DirectX 9.0c e instalando se necessário..."
LangString DE_I_DX9_1 1046 "Executando o Instalador do DirectX (1)..."
LangString DE_I_DX9_2 1046 "Executando o Instalador do DirectX (2)..."
LangString DE_B_SADXFE 1046 "Fazendo backup das configurações do SADXFE..."
LangString DE_R_SADXFE 1046 "Restaurando a configuração do SADXFE..."
LangString DE_B_GAMEC 1046 "Fazendo backup do banco de dados do controlador do jogo..."
LangString DE_R_GAMEC 1046 "Restaurando o banco de dados do controlador do jogo..."
LangString DE_B_DCCONV 1046 "Fazendo backup das configurações do Dreamcast Conversion..."
LangString DE_E_DCCONV 1046 "Extraindo Dreamcast Conversion..."
LangString DE_R_DCCONV 1046 "Restaurando configurações do Dreamcast Conversion..."
LangString DE_DCTITLE_ON 1046 "Habilitando título de janela do DC Conversion..."
LangString DE_DCTITLE_OFF 1046 "Desabilitando título de janela do DC Conversion..."
LangString DE_WTR_ON 1046 "Habilitando SADX Style Water..."
LangString DE_WTR_OFF 1046 "Desabilitando SADX Style Water..."
LangString DE_B_SA1CHARS 1046 "Fazendo backup da configuração do Dreamcast Characters..."
LangString DE_R_SA1CHARS 1046 "Restaurando configuração do Dreamcast Characters..."
LangString DE_B_DLCS 1046 "Fazendo backup da configuração do Dreamcast DLC..."
LangString DE_E_DLCS 1046 "Extraindo Dreamcast DLC..."
LangString DE_R_DLCS 1046 "Restaurando a configuração do Dreamcast DLC..."
LangString DE_E_STEAM 1046 "Extraindo Steam Achievements..."
LangString DE_MODORDER 1046 "Preparando a configuração do Mod Loader..."
LangString DE_B_MLINI 1046 "Fazendo backup da configuração do velho Mod Loader..."
LangString DE_I_MLINI 1046 "Escrevendo a configuração do Mod Loader..."
LangString DE_B_SADXINI 1046 "Fazendo backup da configuração do jogo..."
LangString DE_I_SADXINI 1046 "Escrevendo a configuração do jogo..."
LangString DE_E_RESOURCES 1046 "Extraindo ícones..."
LangString DE_I_RESOURCES 1046 "Copiando ícone..."
LangString DE_B_SADXWTR 1046 "Fazendo backup das configurações do SADX Style Water..."
LangString DE_R_SADXWTR 1046 "Restaurando configurações do SADX Style Water..."
LangString DE_B_ONION 1046 "Fazendo backup das configurações do Onion Blur..."
LangString DE_R_ONION 1046 "Restaurando configurações do Onion Blur..."

;Mod names (generic installer)
LangString MOD_SADXFE 1046 "SADXFE"
LangString MOD_FRAME 1046 "Frame Limit"
LangString MOD_SA1CHARS 1046 "Dreamcast Characters"
LangString MOD_LANTERN 1046 "Lantern Engine"
LangString MOD_SMOOTHCAM 1046 "Smooth Camera"
LangString MOD_ONION 1046 "Onion Skin Blur"
LangString MOD_IDLE 1046 "Idle Chatter"
LangString MOD_PAUSE 1046 "Pause Hide"
LangString MOD_SUPER 1046 "Super Sonic"
LangString MOD_TIME 1046 "Time Of Day"
LangString MOD_ADX 1046 "ADX Audio"
LangString MOD_SND 1046 "Sound Overhaul"
LangString MOD_HDGUI 1046 "HD GUI"
LangString MOD_SADXWTR 1046 "SADX Style Water"

;inetc stuff
LangString INETC_DOWNLOADING 1046 "Baixando %s"
LangString INETC_CONNECT 1046 "Conectando..."
LangString INETC_SECOND 1046 "segundo"
LangString INETC_MINUTE 1046 "minuto"
LangString INETC_HOUR 1046 "hora"
LangString INETC_PLURAL 1046 "s"
LangString INETC_PROGRESS 1046 "%dkB (%d%%) de %dkB @ %d.%01dkB/s"
;Make sure there's an empty space between the first quotation mark and (%d in the string below
LangString INETC_REMAINING 1046 " (%d %s%s restante)"

;Unused strings
${LangFileString} MUI_TEXT_WELCOME_INFO_TITLE " "
${LangFileString} MUI_TEXT_WELCOME_INFO_TEXT " "
${LangFileString} MUI_TEXT_FINISH_TITLE "  "
${LangFileString} MUI_TEXT_FINISH_SUBTITLE "  "
${LangFileString} MUI_TEXT_ABORT_TITLE " "
${LangFileString} MUI_TEXT_ABORT_SUBTITLE " "
${LangFileString} MUI_TEXT_FINISH_INFO_TITLE " "
${LangFileString} MUI_TEXT_FINISH_INFO_TEXT " "
${LangFileString} MUI_TEXT_FINISH_INFO_REBOOT " "
${LangFileString} MUI_TEXT_FINISH_REBOOTNOW " "
${LangFileString} MUI_TEXT_FINISH_REBOOTLATER " "
${LangFileString} MUI_TEXT_FINISH_RUN " "
${LangFileString} MUI_TEXT_FINISH_SHOWREADME " "
${LangFileString} MUI_INNERTEXT_LICENSE_BOTTOM_CHECKBOX " "
${LangFileString} MUI_INNERTEXT_LICENSE_BOTTOM_RADIOBUTTONS " "