﻿!insertmacro LANGFILE "Russian" = "Русский" =

;General strings
${LangFileString} MUI_TEXT_LICENSE_TITLE "Информация"
${LangFileString} MUI_TEXT_LICENSE_SUBTITLE "Пожалуйста, ознакомьтесь с приведённой ниже информацией перед тем, как продолжить."
${LangFileString} MUI_INNERTEXT_LICENSE_TOP "Пролистайте вниз, чтобы увидеть весь текст."
${LangFileString} MUI_INNERTEXT_LICENSE_BOTTOM "Создайте резервную копию папки с SADX прежде чем использовать этот инсталлятор, затем нажмите $\"Продолжить$\"."
${LangFileString} MUI_INNERTEXT_LICENSE_BOTTOM_CHECKBOX " "
${LangFileString} MUI_TEXT_COMPONENTS_TITLE "Выбор компонентов"
${LangFileString} MUI_TEXT_COMPONENTS_SUBTITLE "Выберите моды и компоненты, которые желаете установить. Нажмите $\"Установить$\", чтобы запустить процесс."
${LangFileString} MUI_INNERTEXT_COMPONENTS_DESCRIPTION_TITLE "Описание"
${LangFileString} MUI_INNERTEXT_COMPONENTS_DESCRIPTION_INFO "Наведите курсор на необходимый компонент, чтобы получить его описание."
${LangFileString} MUI_TEXT_DIRECTORY_TITLE "Выбор каталога установки"
${LangFileString} MUI_TEXT_DIRECTORY_SUBTITLE "Укажите путь к папке с SADX."
${LangFileString} MUI_TEXT_INSTALLING_TITLE "Установка"
${LangFileString} MUI_TEXT_INSTALLING_SUBTITLE "Подождите, пока инсталлятор модов SADX завершит свою работу."
${LangFileString} MUI_BUTTONTEXT_FINISH "&Завершить"

;Install types and profile names
LangString INSTTYPE_DC 1049 "Dreamcast-моды"
LangString INSTTYPE_SADX 1049 "SADX + улучшения"
LangString INSTTYPE_MIN 1049 "Минимальный/оригинальный"
LangString PROFILENAME_DC 1049 "Dreamcast"
LangString PROFILENAME_SADX 1049 "Улучшенная SADX"
LangString PROFILENAME_MIN 1049 "Минимальный"
LangString PROFILENAME_CUSTOM 1049 "Выборочная установка"

;Section names
LangString SECTIONNAME_REMOVEMODS 1049 "Удалить имеющиеся моды"
LangString SECTIONNAME_PERMISSIONS 1049 "Проверить разрешения для папки SADX"
LangString SECTIONNAME_MODLOADER 1049 "SADX Mod Loader от MainMemory & SonicFreak94"
LangString SECTIONNAME_LAUNCHER 1049 "SADX Launcher от PkR"
LangString SECTIONNAME_DEPENDENCIES 1049 "Компоненты, необходимые для Mod Loader"
LangString SECTIONNAME_VCC 1049 "Visual C++ runtimes"
LangString SECTIONNAME_DX9 1049 "DirectX 9.0c"
LangString SECTIONNAME_BUGFIXES 1049 "Исправление багов и моды для камеры и управления"
LangString SECTIONNAME_SADXFE 1049 "SADX: Fixed Edition от SonicFreak94"
LangString SECTIONNAME_INPUTMOD 1049 "Input Mod от SonicFreak94"
LangString SECTIONNAME_FRAMELIMIT 1049 "Frame Limiter от SonicFreak94"
LangString SECTIONNAME_CCEF 1049 "Camera Fix от VeritasDL & SonicFreak94"
LangString SECTIONNAME_DCMODS 1049 "Dreamcast-моды"
LangString SECTIONNAME_DCCONV 1049 "Dreamcast Conversion от PkR"
LangString SECTIONNAME_SA1CHARS 1049 "Dreamcast Characters Pack от ItsEasyActually"
LangString SECTIONNAME_LANTERN 1049 "Lantern Engine от SonicFreak94"
LangString SECTIONNAME_DLCS 1049 "Sonic Adventure DLCs от PkR"
LangString SECTIONNAME_ENH 1049 "Геймплейные моды и улучшения"
LangString SECTIONNAME_SMOOTH 1049 "Smooth Camera от SonicFreak94"
LangString SECTIONNAME_ONION 1049 "Onion Skin Blur от SonicFreak94"
LangString SECTIONNAME_IDLE 1049 "Idle Chatter от SonicFreak94"
LangString SECTIONNAME_PAUSE 1049 "Pause Hide от SonicFreak94"
LangString SECTIONNAME_STEAM 1049 "Steam Achievements от MainMemory"
LangString SECTIONNAME_SUPER 1049 "Super Sonic от Kell & SonicFreak94"
LangString SECTIONNAME_TIME 1049 "Time of Day от PkR"
LangString SECTIONNAME_ADX 1049 "ADX voices and music (для порта 2004 года)"
LangString SECTIONNAME_SND 1049 "Sound Overhaul 3 от PkR"
LangString SECTIONNAME_HDGUI 1049 "HD GUI 2 от PkR & других"
LangString SECTIONNAME_SADXWTR 1049 "SADX Style Water от PkR и SteG"
LangString SECTIONNAME_MANAGERCLASSIC 1049 "SADX Mod Manager Classic by PkR"

;Guide Mode descriptions and checkboxes
LangString GUIDE_INFO_LANTERN 1049 "Мод Lantern Engine воссоздаёт систему освещения из версии игры для Dreamcast на ПК. Этот мод делает блеск на моделях персонажей более плавным, а освещение на уровнях и объектах становится более $\"живым$\"."
LangString GUIDE_INFO_DCCONV 1049 "Dreamcast Conversion полностью преображает игру, делая её максимально похожей на Dreamcast-версию. Мод исправляет различные баги, а также восстанавливает уровни, текстуры, объекты, эффекты и логотипы с Dreamcast."
LangString GUIDE_INFO_DXWTR 1049 "Надстройка для DC Conversion, включающая альтернативную текстуру воды, похожую на оригинальную из SADX, но в высоком разрешении и с лучшей анимацией. Эта опция для тех, кто предпочитает текстуры воды SADX на уровнях из Dreamcast-версии."
LangString GUIDE_INFO_SA1CHARS 1049 "Мод Dreamcast Characters Pack восстанавливает оригинальные модели персонажей с Dreamcast в ПК-версии игры. Хотя модели персонажей в SADX используют больше полигонов, некоторые игроки предпочитают оригинальные модели персонажей из версии Dreamcast."
LangString GUIDE_INFO_ONION 1049 "Мод Onion Skin Blur восстанавливает эффект размытия у ног Соника и хвостов Тейлза. Этот эффект присутствовал в японской версии на Dreamcast, но был убран в последующих."
LangString GUIDE_INFO_HDGUI 1049 "HD GUI 2 заменяет текстуры интерфейса (меню, HUD, иконки капсул и др.) на текстуры в высоком разрешением."
LangString GUIDE_INST_LANTERN 1049 "Установить Lantern Engine от SonicFreak94"
LangString GUIDE_INST_DCCONV 1049 "Установить Dreamcast Conversion от PkR"
LangString GUIDE_INST_DXWTR 1049 "Включить SADX Style Water textures от SteG"
LangString GUIDE_INST_SA1CHARS 1049 "Установить Dreamcast Characters Pack от ItsEasyActually"
LangString GUIDE_INST_ONION 1049 "Установить Onion Skin Blur от SonicFreak94"
LangString GUIDE_INST_HDGUI 1049 "Установить HD GUI 2 от PkR, Dark Sonic, Sonikko & SPEEPSHighway"

;Download strings
LangString D_DSOUND 1049 "Скачивание библиотеки DirectSound wrapper..."
LangString D_DLCS 1049 "Скачивание Dreamcast DLCs..."
LangString D_D3D8 1049 "Скачивание библиотеки D3D8to9..."
LangString D_DCCONV 1049 "Скачивание Dreamcast Conversion..."
LangString D_STEAM 1049 "Скачивание Steam Achievements..."
LangString D_RESOURCES 1049 "Скачивание доп. компонентов..."
LangString D_DX9 1049 "Скачивание DirectX Setup..."
LangString D_VC2010 1049 "Скачивание VS2010 runtimes..."
LangString D_VC2012 1049 "Скачивание VS2012 runtimes..."
LangString D_VC2013 1049 "Скачивание VS2013 runtimes..."
LangString D_VC2019 1049 "Скачивание VS2019 runtimes..."
LangString D_LAUNCHER 1049 "Скачивание SADX Launcher..."
LangString D_NET 1049 "Скачивание веб-версии программы установки .NET..."
LangString D_CHRMODELS 1049 "Скачивание CHRMODELS..."
LangString D_STEAMTOOLS 1049 "Скачивание tools..."
LangString D_2004BIN 1049 "Скачивание файлов SADX 2004..."
LangString D_2004EXE 1049 "Скачивание EXE-файла SADX 2004..."
LangString D_VOICES 1049 "Скачивание дополнительных голосов..."
LangString D_MODLOADER 1049 "Скачивание SADX Mod Loader..."
LangString D_FILELIST 1049 "Скачивание списка файлов..."
LangString D_INSTCHK 1049 "Проверка на наличие обновлений для инсталлятора..."
LangString D_GENERIC 1049 "Скачивание $Modname..."
LangString D_UPDATE 1049 "Проверка $UpdateFilename ..."
LangString D_INSTALLER 1049 "Скачивание инсталлятора..."
LangString D_INSTALLER_R 1049 "Запуск новой версии..."

;Headers
LangString MISC_INSTALLER 1049 "Инсталлятор модов для SADX"
LangString HEADER_GUIDE_TITLE 1049 "Помощник по выбору модов"
LangString HEADER_GUIDE_TEXT 1049 "Выберите моды, которые желаете установить. Нажмите на скриншот для более детального сравнения."
LangString HEADER_ADD_TITLE 1049 "Дополнительные моды"
LangString HEADER_ADD_TEXT 1049 "Выберите моды, которые желаете установить."
LangString HEADER_UPDATES_TITLE 1049 "Проверка обновлений"
LangString HEADER_UPDATES_TEXT 1049 "Рекомендуется регулярно проверять обновления модов и Mod Loader."
LangString HEADER_SET_TITLE 1049 "Настройка Mod Loader"
LangString HEADER_SET_TEXT 1049 "Настройки SADX/Mod Loader. Позже их можно будет изменить в Mod Manager."
LangString HEADER_ICON_TITLE 1049 "Выбор иконки EXE-файла"
LangString HEADER_ICON_TEXT 1049 "Выберите иконку для EXE-файла SADX."
LangString HEADER_TYPE_TITLE 1049 "Тип установки"
LangString HEADER_TYPE_TEXT 1049 "Выберите тип установки."

;Mod Loader settings
LangString SET_TRUE 1049 "Да"
LangString SET_FALSE 1049 "Нет"
LangString SET_DISP 1049 "Настройки экрана"
LangString SET_RES 1049 "Разрешение"
LangString SET_SCREEN 1049 "Режим окна"
LangString SET_WINDOWED 1049 "Оконный"
LangString SET_FULLSCREEN 1049 "Полноэкранный"
LangString SET_BORDERLESS 1049 "Безрамочный полноэкранный режим"
LangString SET_SCALE 1049 "Масштабирование экрана (для даунсэмплинга)"
LangString SET_VSYNC 1049 "Вертикальная синхронизация"
LangString SET_DETAIL 1049 "Уровень детализации"
LangString SET_HIGH 1049 "Высокий (рекомендуется)"
LangString SET_LOW 1049 "Низкий"
LangString SET_LOWEST 1049 "Очень низкий"
LangString SET_FRAMERATE 1049 "Частота кадров"
LangString SET_OTHER 1049 "Прочие настройки"
LangString SET_PAUSE 1049 "Пауза, когда окно неактивно (ALT+TAB)"
LangString SET_UI 1049 "Масштабирование интерфейса (рекомендуется)"
LangString SET_UPD 1049 "Настройки обновлений"
LangString SET_CHECKMODUPD 1049 "Проверка обновлений модов"
LangString SET_CHECKUPD 1049 "Проверка обновлений"
LangString SET_FREQ 1049 "Частота обновлений"
LangString SET_DAYS 1049 "Дни"
LangString SET_WEEKS 1049 "Недели"
LangString SET_HOURS 1049 "Часы"
LangString SET_ALWAYS 1049 "Всегда"

;Options
LangString OPTIONNAME_INSTMODE 1049 "Режим установки модов"
LangString OPTIONNAME_GUIDE 1049 "Режим подсказки - рекомендуется начинающим"
LangString OPTIONNAME_PRESET 1049 "Набор модов"
LangString OPTIONNAME_PRESETS 1049 "Наборы модов"
LangString OPTIONNAME_ADVANCED 1049 "Показать дополнительные параметры"
LangString OPTIONNAME_PRESERVE 1049 "Сохранить индивидуальные настройки модов (если есть)"
LangString OPTIONNAME_OPTIMAL 1049 "Использовать оптимальные настройки Mod Loader"
LangString OPTIONNAME_MANUAL 1049 "Настроить Mod Loader вручную"
LangString OPTIONNAME_FAILSAFE 1049 "Использовать минимальные настройки Mod Loader"
LangString OPTIONNAME_ICONSEL 1049 "Выбрать иконку для EXE-файла SADX"
LangString OPTIONNAME_ICON_DX 1049 "Иконка сохранения SADX с Gamecube"
LangString OPTIONNAME_ICON_HD 1049 "HD-иконка от Lester LJSTAR"
LangString OPTIONNAME_ICON_SA1 1049 "Обложка SA1 от PkR"
LangString OPTIONNAME_ICON_VM 1049 "Обновлённая HD иконка сохранения SA1 от McAleeCh"
LangString OPTIONNAME_UPDATES 1049 "Проверить наличие обновлений"
LangString OPTIONNAME_NOUPDATES 1049 "Не проверять обновления: загрузить только необходимые файлы (при их отсутствии)"
LangString OPTIONNAME_WTITLE 1049 "Изменить название окна на $\"Sonic Adventure$\""
LangString OPTIONNAME_ICON 1049 "Выбрать иконку для sonic.exe"
LangString OPTIONNAME_MANAGERCLASSIC 1049 "Use the Classic Mod Manager"

;Messages
LangString MSG_UPDATES 1049 "Проверить наличие обновлений?$\r$\n$\r$\n$\r$\n$\r$\n$\r$\n\
Этот инсталлятор, SADX Mod Loader и моды постоянно обновляются. Исправляются ошибки, добавляются новые функции.$\r$\n\
$\r$\nСвоевременное обновление обеспечивает поддержку нового контента и позволит избежать проблем."
LangString MSG_OFFLINE 1049 "Для автономной (оффлайн) установки выберите второй вариант."
LangString MSG_FOLDER_FOUND 1049 "Программа установки обнаружила уже существующую папку с установленной SADX.$\r$\n\
$\r$\nЧтобы задать другую папку, нажмите $\"Обзор$\" и выберите другую папку."
LangString MSG_FOLDER_NOTFOUND 1049 "Программа установки не обнаружила SADX. Выберите папку для установки вручную."
LangString MSG_DEFAULT 1049 "Настройки Mod Loader будут сброшены на минимальные:$\r$\n\
$\r$\nРазрешение: 640x480$\r$\n\
Полноэкранный режим: вкл$\r$\n\
Безрамочный полноэкранный режим: выкл$\r$\n\
Вертикальная синхронизация: выкл$\r$\n\
Фильтрация текстур и мипмапы: выкл$\r$\n\
Масштабирование экрана: выкл$\r$\n\
Масштабирование интерфейса: выкл$\r$\n\
Масштабирование фоновых изображений и роликов: растянуть$\r$\n\
$\r$\n\
Продолжить?"
LangString MSG_INSTALLERUPDATE 1049 "Найдено обновление для инсталлятора! Выполнить автообновление?$\r$\n\
$\r$\nНовая версия запустится автоматически."
LangString MSG_FOUNDUPD 1049 "Найдены обновления: $UpdatesFound!$\r$\n\
$\r$\n$WhichUpdates$\r$\n\
Папка instdata была очищена, взамен удалённых файлов будут загружены последние версии модов."
LangString MSG_CHECKUPDATES 1049 "Проверка обновлений Mod Loader..."
LangString MSG_PROFILE 1049 "Выберите режим установки модов. Вы можете установить набор модов или выбрать моды вручную."
LangString MSG_START 1049 "Добро пожаловать в инсталлятор модов для Sonic Adventure DX!"
LangString MSG_WELCOME 1049 "Эта программа установит SADX Mod Loader от MainMemory и моды от различных авторов.$\r$\n$\r$\nПо умолчанию установка всех модов добавит примерно 1.3Гб к общему размеру вашей папки с SADX.$\r$\n$\r$\nЕсли вы используете веб-версию этого инсталлятора, убедитесь, что на диске с sadx_setup.exe доступно как минимум 1Гб свободного места.$\r$\n$\r$\nНажмите $\"Далее$\", чтобы продолжить."
LangString MSG_COMPLETE 1049 "Установка завершена!"
LangString MSG_FINISH 1049 "Теперь вы можете играть в SADX с выбранными модами.$\r$\n\
$\r$\nФайлы, использованные этим инсталлятором, были сохранены в папке 'instdata'. Вы можете оставить их для повторной установки оффлайн или удалить их, чтобы сэкономить место на диске."

;Errors
LangString ERR_REQUIREDOS 1049 "Обнаружена несовместимая версия ОС. Для работы программы требуется Windows 7 SP1 или новее.$\r$\nПродолжить?"
LangString ERR_NET_MISSING 1049 "Для работы конвертера версии Steam требуется .NET Framework.$\r$\nПожалуйста, установите .NET Framework вручную и перезапустите инсталлятор."
LangString ERR_D_RESOURCES 1049 "Ошибка загрузки данных: $DownloadErrorCode. Продолжить?"
LangString ERR_D_DSOUND 1049 "Ошибка загрузки: $DownloadErrorCode. Sound Overhaul не будет работать корректно, пока файл dsound.dll не будет помещён в корневую папку SADX."
LangString ERR_D_STEAM 1049 "Ошибка загрузки Steam Achievements: $DownloadErrorCode. Продолжить установку других модов?"
LangString ERR_D_DLCS 1049 "Ошибка загрузки SA1 DLC: $DownloadErrorCode. Продолжить установку других модов?"
LangString ERR_D_D3D8 1049 "Ошибка загрузки: $DownloadErrorCode. Lantern Engine не будет работать корректно, пока файл d3d8.dll не будет помещён в корневую папку SADX."
LangString ERR_D_DCCONV 1049 "Ошибка загрузки DC Conversion: $DownloadErrorCode. Продолжить установку других модов?"
LangString ERR_D_DX9 1049 "Ошибка загрузки: $DownloadErrorCode. Пожалуйста, запустите программу установки DirectX вручную."
LangString ERR_D_VC2010 1049 "Ошибка загрузки: $DownloadErrorCode. Пожалуйста, запустите программу установки Visual C++ 2010 runtimes вручную."
LangString ERR_D_VC2012 1049 "Ошибка загрузки: $DownloadErrorCode. Пожалуйста, запустите программу установки C++ 2012 runtimes вручную."
LangString ERR_D_VC2013 1049 "Ошибка загрузки: $DownloadErrorCode. Пожалуйста, запустите программу установки C++ 2013 runtimes вручную."
LangString ERR_D_VC2019 1049 "Ошибка загрузки: $DownloadErrorCode. Пожалуйста, запустите программу установки C++ 2019 runtimes вручную."
LangString ERR_D_LAUNCHER 1049 "Ошибка загрузки: $DownloadErrorCode. Пожалуйста, скачайте SADX Launcher вручную или перезапустите инсталлятор.$\r$\n\
$\r$\n\
Если вы постоянно сталкиваетесь с этой проблемой, пожалуйста, скачайте оффлайн версию инсталлятора."
LangString ERR_D_NET 1049 "Ошибка загрузки: $DownloadErrorCode. Пожалуйста, установите .NET Framework вручную."
LangString ERR_SIZE 1049 "Размер $UpdateFilename не найден!"
LangString ERR_FOLDER 1049 "Программа установки обнаружила недостающие файлы в вашей папке с SADX.$\r$\n\
$\r$\n\
Убедитесь, что вы устанавливаете Mod Loader в корневую папку SADX (где находятся sonic.exe или Sonic Adventure DX.exe)."
LangString ERR_2004CHECK 1049 "Программа установки обнаружила, что EXE-файл из версии 2004 невозможно загрузить или он имеет неверную контрольную сумму. $\r$\n\
Проверьте ваше интернет-соединение и запустите инсталлятор заново.$\r$\n\
$\r$\n\
Если вы постоянно сталкиваетесь с этой проблемой, пожалуйста, скачайте оффлайн версию инсталлятора."
LangString ERR_MODLOADER 1049 "Программа установки обнаружила, что Mod Loader не был установлен корректно. $\r$\n\
Проверьте ваше интернет-соединение и запустите инсталлятор заново.$\r$\n\
$\r$\n\
Если вы постоянно сталкиваетесь с этой проблемой, пожалуйста, скачайте оффлайн версию инсталлятора."
LangString ERR_DOWNLOAD_TOOLS 1049 "Программа установки обнаружила, что некоторые моды не были загружены. $\r$\n\
Проверьте ваше интернет-соединение и запустите инсталлятор заново.$\r$\n\
$\r$\n\
Если вы постоянно сталкиваетесь с этой проблемой, пожалуйста, скачайте оффлайн версию инсталлятора."
LangString ERR_DOWNLOAD_2004BIN 1049 "Программа установки обнаружила, что некоторые файлы SADX 2004 не были загружены.$\r$\n\
Проверьте ваше интернет-соединение и запустите инсталлятор заново.$\r$\n\
$\r$\n\
Если вы постоянно сталкиваетесь с этой проблемой, пожалуйста, скачайте оффлайн версию инсталлятора."
LangString ERR_MODDOWNLOAD 1049 "Ошибка загрузки $ModName: $DownloadErrorCode. Продолжить установку других модов?"
LangString ERR_DOWNLOAD_FILE 1049 "Ошибка загрузки файла. Повторить попытку загрузить файл?"
LangString ERR_MODDATE 1049 "Ошибка проверки последней даты изменения mod.manifest для $ModFilename. Убедитесь, что мод был установлен корректно."
LangString ERR_MISSINGFILES 1049 "Программа установки обнаружила недостающие файлы в вашей папке с SADX.$\r$\n\
$\r$\n\
Убедитесь, что вы устанавливаете Mod Loader в корневую папку SADX (где находятся sonic.exe или Sonic Adventure DX.exe).$\r$\n\
$\r$\n\
Если у вас смешаны файлы из различных версий SADX, инсталлятор может некорректно определить вашу версию. Чтобы исправить это, установите чистую версию игры."
LangString ERR_DOWNLOAD_CANCEL_Q 1049 "Отменить загрузку?"
LangString ERR_DOWNLOAD_CANCEL_A 1049 "Отмена загрузки"
LangString ERR_DOWNLOAD_FATAL 1049 "Ошибка загрузки: $DownloadErrorCode. Пожалуйста, запустите инсталлятор заново.$\r$\n\
$\r$\n\
Если вы постоянно сталкиваетесь с этой проблемой, пожалуйста, скачайте оффлайн версию инсталлятора."
LangString ERR_DOWNLOAD_RETRY 1049 "Ошибка загрузки. Повторить попытку загрузить файл?"
LangString ERR_PERMISSION 1049 "Ошибка назначения прав для папки SADX!$\r$\n\
$\r$\n\
Получите доступ к правам папки SADX и назначьте разрешения вручную."
LangString ERR_CHRMODELS 1049 "Ошибка загрузки CHRMODELS.dll: $DownloadErrorCode $\r$\n\
Проверьте ваше интернет-соединение и запустите инсталлятор заново.$\r$\n\
$\r$\n\
Если вы постоянно сталкиваетесь с этой проблемой, пожалуйста, скачайте оффлайн версию инсталлятора."
LangString ERR_CHRMODELSBAD 1049 "Программа установки обнаружила модифицированный файл CHRMODELS.DLL. $\r$\n\
$\r$\n\
Он будет сохранён как CHRMODELS.bak и заменён обычным CHRMODELS.DLL. Это требуется для корректной работы Mod Loader.$\r$\n\
$\r$\n\
Если вы используете старые моды с модифицированной CHRMODELS.DLL, учитывайте то, что они не поддерживаются Mod Loader."
LangString ERR_EXEMD5 1049 "MD5-хэш загруженного sonic.exe не совпадает с оригинальным. Пожалуйста, запустите инсталлятор заново.$\r$\n\
$\r$\n\
Если вы постоянно сталкиваетесь с этой проблемой, пожалуйста, скачайте оффлайн версию инсталлятора."

;Mod descriptions
LangString DESC_ADD_ALL 1049 "Данные моды содержат невизуальные улучшения либо вносят изменения в игровой процесс.$\r$\n\
$\r$\nВыберите моды, которые хотите установить."
LangString DESC_ADD_SND 1049 "Установить Sound Overhaul для улучшения качества звука и исправления ошибок, связанных со звуком"
LangString DESC_ADD_DLCS 1049 "Установить Dreamcast DLCs, чтобы добавить DLC-контент из Dreamcast-версии Sonic Adventure"
LangString DESC_ADD_SUPER 1049 "Установить мод Super Sonic, чтобы получить возможность превращаться в Супер Соника после прохождения игры"
LangString DESC_REMOVEMODS 1049 "Удалить все моды в папке mods перед тем, как продолжить.$\r$\n\
$\r$\n\
ВНИМАНИЕ! Это КРАЙНЕ ОПАСНОЕ действие, так как будут удалены все ваши моды.$\r$\n\
Используйте только для переустановки с нуля."
LangString DESC_MODLOADER 1049 "Установить или обновить SADX Mod Loader (ОБЯЗАТЕЛЬНО).$\r$\n\
$\r$\n\
Steam-версия SADX будет сконвертирована в версию 2004 года перед установкой."
LangString DESC_LAUNCHER 1049 "Установить SADX Launcher (ОБЯЗАТЕЛЬНО).$\r$\n\
$\r$\n\
SADX Launcher — программа для изменения настроек Mod Loader и настройки управления в Input Mod."
LangString DESC_NET 1049 "Установить или обновить .NET Framework, который требуется для корректной работы SADX Mod Manager. $\r$\n\
$\r$\n\
Программа установки проверяет наличие .NET Framework перед загрузкой."
LangString DESC_RUNTIME 1049 "Установить или обновить Visual C++ 2010, 2012, 2013 и 2015/2017/2019 runtimes, которые требуются для корректной работы модов на базе DLL. $\r$\n\
$\r$\n\
Программа установки проверяет наличие Visual C++ runtimes перед загрузкой."
LangString DESC_DIRECTX 1049 "Установить DirectX 9.0c, который требуется для SADX и мода Lantern Engine. $\r$\n\
$\r$\n\
Программа установки проверит наличие DirectX 9.0c перед загрузкой."
LangString DESC_PERMISSIONS 1049 "Получить права доступа к папке SADX. Это предотвратит ошибки, которые могут возникнуть при включении/выключении Mod Loader без прав Администратора.$\r$\n\
$\r$\n\
Данная опция включается только когда SADX установлена в папку Program Files."
LangString DESC_ADXAUDIO 1049 "Музыка лучшего качества и озвучка из версии Dreamcast в формате ADX. $\r$\n\
$\r$\n\
Не требуется, если установлена Steam-версия, поскольку она уже содержит музыку и озвучку в формате ADX."
LangString DESC_SADXFE 1049 "Этот мод исправляет различные ошибки в оригинальной ПК-версии SADX и добавляет некоторые улучшения.$\r$\n\
$\r$\n\
РЕКОМЕНДУЕТСЯ!"
LangString DESC_INPUTMOD 1049 "Полная модернизация системы управления SADX с переназначением клавиш/кнопок. Устраняет проблемы с геймпадами XInput и некоторыми геймпадами DInput. $\r$\n\
$\r$\n\
РЕКОМЕНДУЕТСЯ!"
LangString DESC_SMOOTHCAM 1049 "Плавное перемещение камеры в режиме от первого лица.$\r$\n\
$\r$\n\
Может использоваться вместе с Input Mod для ещё более плавного движения."
LangString DESC_FRAMELIMIT 1049 "Более точный ограничитель частоты кадров.$\r$\n\
$\r$\n\
Исправляет проблемы с частотой кадров (например, когда игра работает с частотой 63 кадра вместо 60) на некоторых компьютерах.$\r$\n\
$\r$\n\
Делает игровой процесс плавнее, однако производительность на старых системах может снизиться."
LangString DESC_PAUSEHIDE 1049 "Скрывает меню паузы при нажатии X+Y как на Dreamcast. Включено в состав DC Conversion."
LangString DESC_ONIONBLUR 1049 "Восстанавливает 'эффект размытия' у ног Соника (как в японской версии SA1 для Dreamcast) и на хвостах Тейлза."
LangString DESC_DLCS 1049 "Эксклюзивные DLC для версии Dreamcast теперь доступны в качестве мода для SADX. $\r$\n\
$\r$\n\
Более подробная информация содержится в настройках мода."
LangString DESC_STEAM 1049 "Поддержка достижений Steam в порте 2004-го года. $\r$\n\
$\r$\n\
Вы должны приобрести SADX в Steam для работы этого мода."
LangString DESC_LANTERN 1049 "Мод, воссоздающий в SADX движок освещения из Dreamcast-версии Sonic Adventure. $\r$\n\
$\r$\n\
РЕКОМЕНДУЕТСЯ!"
LangString DESC_DCMODS 1049 "Уровни, модели объектов, эффекты, боссы, логотипы и прочее из Dreamcast-версии Sonic Adventure."
LangString DESC_SNDOVERHAUL 1049 "Исправляет различные проблемы со звуком и заменяет большинство звуковых эффектов их аналогами из Dreamcast-версии."
LangString DESC_HDGUI 1049 "Добавляет HD-версии большинства элементов интерфейса, таких как HUD, кнопки, иконки жизней, меню и прочее."
LangString DESC_TIMEOFDAY 1049 "Этот мод изменяет время суток, когда персонаж садится на поезд на Привокзальной Площади или в Таинственных Руинах."
LangString DESC_DCCONV 1049 "Эти моды возвращают или заменяют различные ресурсы игры и делают SADX более похожей на Dreamcast-версию SA1."
LangString DESC_BUGFIXES 1049 "Эти моды исправляют проблемы и вносят различные технические улучшения без глобальных изменений ресурсов игры."
LangString DESC_SA1CHARS 1049 "Модели персонажей из Dreamcast-версии."
LangString DESC_MODLOADERSTUFF 1049 "Различные библиотеки, которые нужны для корректной работы Mod Loader и модов."
LangString DESC_SUPERSONIC 1049 "Позволяет превращаться в Супер Соника после прохождения основной игры."
LangString DESC_CCEF 1049 "Исправляет ошибку, из-за которой камера возвращается в автоматический режим при перезапуске уровня. Входит в состав SADXFE. $\r$\n\
$\r$\n\
Полезно для спидраннеров, которые не используют SADXFE."
LangString DESC_ENHANCEMENTS 1049 "Моды, которые добавляют новые игровые возможности и улучшают внешний вид SADX."
LangString DESC_IDLECHATTER 1049 "Нажмите кнопку Z, чтобы персонаж сказал что-нибудь!"
LangString DESC_SADXWTR 1049 "Adds alternative water textures to some levels and re-enables the ocean wave effect in Emerald Coast."
LangString DESC_MANAGERCLASSIC 1049 "Alternative version of the SADX Mod Manager that works on Windows XP."

;Other descriptions
LangString DESC_DESC 1049 "Описание"
LangString DESC_PRESERVE 1049 "Сохранит config.ini при обновлении или переустановке мода."
LangString DESC_ICON 1049 "Инсталлятор изменит иконку sonic.exe на выбранную вами."
LangString DESC_DCMODS_ALL 1049 "Эти моды делают SADX более похожей на Dreamcast-версию Sonic Adventure.$\r$\n\
В набор входят: Onion Blur, Dreamcast Conversion, DC Characters, DLCs, Sound Overhaul, HD GUI, Lantern Engine, Time of Day, Input Mod, Idle Chatter, Smooth Camera, Frame Limit, Super Sonic."
LangString DESC_SADX_ALL 1049 "Улучшенная версия оригинального PC порта SADX.$\r$\n\
В набор входят: SADX: Fixed Edition, Onion Blur, Enhanced Emerald Coast, Sound Overhaul, HD GUI, Time of Day, Input Mod, Idle Chatter, Pause Hide, Smooth Camera, Frame Limit, Super Sonic."
LangString DESC_MIN_ALL 1049 "Только необходимые/разрешённые для спидранов моды.$\r$\n\
В набор входят: Frame Limit."
LangString DESC_CUSTOM_ALL 1049 "Выборочная установка.$\r$\n\
Вы можете выбрать моды и настройки Mod Loader вручную."
LangString DESC_GUIDE_ALL 1049 "Помощь в установке модов.$\r$\n\
Инсталлятор покажет скриншоты, которые помогут вам выбрать моды для установки."
LangString DESC_MANUAL 1049 "Вы сможете вручную выбрать разрешение, включить вертикальную синхронизацию и прочее."
LangString DESC_OPTIMAL 1049 "Инсталлятор установит оптимальное разрешение экрана и настроит игру и Mod Loader для лучшего качества изображения."
LangString DESC_FAILSAFE 1049 "Используйте, если игра вылетает при запуске.$\r$\n\
Игра запустится в полноэкранном режиме с разрешением 640x480, а остальные улучшения (безрамочный режим на весь экран и т.д.) будут отключены."
LangString DESC_WINDOWTITLE 1049 "Заголовок окна при игре в оконном режиме будет показывать $\"Sonic Adventure$\" вместо оригинального $\"SonicAdventureDXPC$\".$\r$\n\
$\r$\n\
ВНИМАНИЕ! НЕ ВКЛЮЧАЙТЕ, ЕСЛИ ХОТИТЕ ИСПОЛЬЗОВАТЬ FUSION'S CHAO EDITOR!"
LangString DESC_MOREMODS 1049 "Другие моды для SADX"
LangString DESC_SHORTCUTS 1049 "Создать ярлыки на рабочем столе"
LangString DESC_RUNSADX 1049 "Запустить Sonic Adventure DX"
LangString DESC_RUNLAUNCHER 1049 "Настроить игру и управление в SADX Launcher"
;The resources below are only available in English. Please indicate that in your translation
LangString DESC_DREAMCASTIFY 1049 "Dreamcastify — вебсайт об ухудшениях в SADX (на английском)"
LangString DESC_DISCORD 1049 "Discord-сервер по моддингу SADX (на английском)"

;Detail output
LangString DE_SND 1049 "Превращение звуковых банков в формат SADX 2004 (может занять некоторое время)..."
LangString DE_MOV 1049 "Превращение SFD-роликов в MPG (может занять некоторое время)..."
LangString DE_VOICES 1049 "Загрузка дополнительных файлов озвучки..."
LangString DE_INSTDATA 1049 "Создание папки instdata..."
LangString DE_7Z 1049 "Копирование 7Z..."
LangString DE_OWNER 1049 "Получение прав доступа к папке SADX: $Permission1"
LangString DE_PERM 1049 "Разрешения папки SADX: $Permission2"
LangString DE_REALL 1049 "Удаление всех модов..."
LangString DE_PRGFILES 1049 "Проверка на наличие папки SADX в Program Files..."
LangString DE_RECU 1049 "SADX в Program Files. Установка особых прав..."
LangString DE_2004FOUND 1049 "Обнаружена версия 2004-го года (sonic.exe)"
LangString DE_CHRM 1049 "Обнаружен CHRMODELS.DLL. Проверка MD5..."
LangString DE_MANIFEST 1049 "Устаревший манифест мода $ModFilename: $2-$1-$0T$4:$5:$6Z"
LangString DE_PREVIOUS_DX 1049 "Загрузка предыдущих настроек SADX..."
LangString DE_PREVIOUS_ML 1049 "Загрузка предыдущих настроек Mod Loader..."
LangString DE_TAKEOWN 1049 "Получение прав доступа к папке $R9: $0"
LangString DE_PERMSET1 1049 "Разрешения $R9 (1): $0"
LangString DE_PERMSET2 1049 "Разрешения $R9 (2): $0"
LangString DE_PERMSET3 1049 "Разрешения $R9 (3): $0"
LangString DE_E_GENERIC 1049 "Извлечение $Modname..."
LangString DE_B_CHR 1049 "Сохранение старого CHRMODELS..."
LangString DE_D_CHR 1049 "Загрузка CHRMODELS... (~1.7Мб)"
LangString DE_E_CHR 1049 "Извлечение CHRMODELS..."
LangString DE_C_CHR 1049 "Проверка CHRMODELS..."
LangString DE_B_EXE 1049 "Сохранение старого sonic.exe..."
LangString DE_E_EXE 1049 "Извлечение SADX 2004 EXE..."
LangString DE_C_EXE 1049 "Проверка MD5 у sonic.exe..."
LangString DE_E_BIN 1049 "Извлечение файлов SADX 2004..."
LangString DE_C_BIN 1049 "Проверка файлов SADX 2004..."
LangString DE_E_ML 1049 "Извлечение SADX Mod Loader..."
LangString DE_I_ML 1049 "Установка/обновление SADX Mod Loader..."
LangString DE_EXEFOUND 1049 "Найден требуемый sonic.exe, переход к установке Mod Loader."
LangString DE_EXEUNK 1049 "Найден неизвестный sonic.exe, переход к загрузке US EXE + установке Mod Loader."
LangString DE_DETECT 1049 "Определение типа установки SADX..."
LangString DE_2010FOUND 1049 "Обнаружена версия 2010-го года (Sonic Adventure DX.exe)"
LangString DE_C_2010 1049 "Проверка установки SADX 2010..."
LangString DE_E_TOOLS 1049 "Извлекаются программы..."
LangString DE_C_TOOLS 1049 "Проверяются программы..."
LangString DE_E_SCR 1049 "Извлечение скриптов..."
LangString DE_E_VOICES 1049 "Установка дополнительной озвучки..."
LangString DE_SAVE 1049 "Копирование сохранений SADX 2010 в папку сохранений SADX 2004..."
LangString DE_DPI 1049 "Добавление исключений High DPI/оптимизации полного экрана для sonic.exe..."
LangString DE_FSOPT 1049 "Добавление исключений оптимизации полного экрана и DPI для sonic.exe..."
LangString DE_FSOPTNOTFOUND 1049 "Оптимизация полного экрана не включена, добавление исключений DPI..."
LangString DE_CLEANUP 1049 "Очистка..."
LangString DE_E_LAUNCHER 1049 "Извлечение SADX Launcher..."
LangString DE_CHECKNET 1049 "Проверка версии .NET Framework..."
LangString DE_E_NET 1049 "Установка .NET Framework..."
LangString DE_NETPRESENT 1049 ".NET Framework уже установлен ($NetFrameworkVersion)."
LangString DE_C_VCC 1049 "Проверка Visual C++ runtimes и установка, если требуется..."
LangString DE_E_VC2010 1049 "Извлечение Visual C++ 2010 runtime..."
LangString DE_I_VC2010 1049 "Установка Visual C++ 2010 runtime..."
LangString DE_E_VC2012 1049 "Извлечение Visual C++ 2012 runtime..."
LangString DE_I_VC2012 1049 "Установка Visual C++ 2012 runtime..."
LangString DE_E_VC2013 1049 "Извлечение Visual C++ 2013 runtime..."
LangString DE_I_VC2013 1049 "Установка Visual C++ 2013 runtime..."
LangString DE_E_VC2019 1049 "Извлечение Visual C++ 2019 runtime..."
LangString DE_I_VC2019 1049 "Установка Visual C++ 2019 runtime..."
LangString DE_C_DX9 1049 "Проверка DirectX 9.0c и установка, если требуется..."
LangString DE_I_DX9_1 1049 "Запуск программы установки DirectX (1)..."
LangString DE_I_DX9_2 1049 "Запуск программы установки DirectX (2)..."
LangString DE_B_SADXFE 1049 "Сохранение настроек SADXFE..."
LangString DE_R_SADXFE 1049 "Восстановление настроек SADXFE.."
LangString DE_B_INPUT 1049 "Сохранение настроек Input Mod..."
LangString DE_R_INPUT 1049 "Восстановление настроек Input Mod..."
LangString DE_B_GAMEC 1049 "Сохранение базы данных контроллеров..."
LangString DE_R_GAMEC 1049 "Восстановление базы данных контроллеров..."
LangString DE_B_DCCONV 1049 "Сохранение настроек Dreamcast Conversion..."
LangString DE_E_DCCONV 1049 "Извлечение Dreamcast Conversion..."
LangString DE_R_DCCONV 1049 "Восстановление настроек Dreamcast Conversion..."
LangString DE_DCTITLE_ON 1049 "Включение названия окна из DC Conversion..."
LangString DE_DCTITLE_OFF 1049 "Отключение названия окна из DC Conversion..."
LangString DE_WTR_ON 1049 "Включение SADX Style Water..."
LangString DE_WTR_OFF 1049 "Отключение SADX Style Water..."
LangString DE_B_SA1CHARS 1049 "Сохранение настроек Dreamcast Characters..."
LangString DE_R_SA1CHARS 1049 "Восстановление настроек Dreamcast Characters..."
LangString DE_B_DLCS 1049 "Сохранение настроек Dreamcast DLCs..."
LangString DE_E_DLCS 1049 "Извлечение SA1 DLCs..."
LangString DE_R_DLCS 1049 "Восстановление настроек Dreamcast DLCs..."
LangString DE_E_STEAM 1049 "Извлечение Steam Achievements..."
LangString DE_E_DSOUND 1049 "Добавление DirectSound DLL wrapper..."
LangString DE_I_DSOUNDINI 1049 "Добавление dsound.ini..."
LangString DE_MODORDER 1049 "Настройка порядка модов..."
LangString DE_B_MLINI 1049 "Сохранение SADXModLoader.ini..."
LangString DE_I_MLINI 1049 "Добавление SADXModLoader.ini..."
LangString DE_B_SADXINI 1049 "Сохранение sonicDX.ini..."
LangString DE_I_SADXINI 1049 "Добавление sonicDX.ini..."
LangString DE_E_RESOURCES 1049 "Распаковка данных..."
LangString DE_I_RESOURCES 1049 "Модификация sonic.exe..."
LangString DE_B_SADXWTR 1049 "Backing up SADX Style Water settings..."
LangString DE_R_SADXWTR 1049 "Restoring SADX Style Water settings..."

;Mod names (generic installer)
LangString MOD_SADXFE 1049 "SADXFE"
LangString MOD_INPUT 1049 "Input Mod"
LangString MOD_FRAME 1049 "Frame Limiter"
LangString MOD_CCEF 1049 "Camera Code Error Fix"
LangString MOD_SA1CHARS 1049 "Dreamcast Characters"
LangString MOD_LANTERN 1049 "Lantern Engine"
LangString MOD_SMOOTHCAM 1049 "Smooth Camera"
LangString MOD_ONION 1049 "Onion Skin Blur"
LangString MOD_IDLE 1049 "Idle Chatter"
LangString MOD_PAUSE 1049 "Pause Hide"
LangString MOD_SUPER 1049 "Super Sonic"
LangString MOD_TIME 1049 "Time Of Day"
LangString MOD_ADX 1049 "ADX Audio"
LangString MOD_SND 1049 "Sound Overhaul"
LangString MOD_HDGUI 1049 "HD GUI"
LangString MOD_SADXWTR 1049 "SADX Style Water"

;inetc stuff
LangString INETC_DOWNLOADING 1049 "Загрузка: %s" 
LangString INETC_CONNECT 1049 "Подключение ..."
LangString INETC_SECOND 1049 "сек."
LangString INETC_MINUTE 1049 "мин."
LangString INETC_HOUR 1049 "час."
LangString INETC_PLURAL 1049 " 1049 " 
LangString INETC_PROGRESS 1049 "%dкБ (%d%%) из %dкБ @ %d.%01dкБ/ов" 
LangString INETC_REMAINING 1049 "(Осталось: %d %s%s)"

;Unused strings
${LangFileString} MUI_TEXT_WELCOME_INFO_TITLE " "
${LangFileString} MUI_TEXT_WELCOME_INFO_TEXT " "
${LangFileString} MUI_TEXT_FINISH_TITLE "  "
${LangFileString} MUI_TEXT_FINISH_SUBTITLE "  "
${LangFileString} MUI_TEXT_ABORT_TITLE " "
${LangFileString} MUI_TEXT_ABORT_SUBTITLE " "
${LangFileString} MUI_TEXT_FINISH_INFO_TITLE " "
${LangFileString} MUI_TEXT_FINISH_INFO_TEXT " "
${LangFileString} MUI_TEXT_FINISH_INFO_REBOOT " "
${LangFileString} MUI_TEXT_FINISH_REBOOTNOW " "
${LangFileString} MUI_TEXT_FINISH_REBOOTLATER " "
${LangFileString} MUI_TEXT_FINISH_RUN " "
${LangFileString} MUI_TEXT_FINISH_SHOWREADME " "
${LangFileString} MUI_INNERTEXT_LICENSE_BOTTOM_CHECKBOX " "
${LangFileString} MUI_INNERTEXT_LICENSE_BOTTOM_RADIOBUTTONS " "

;Strings added in the 2022 update
LangString INSTTYPE_STEAMCONV 1049 "Steam в 2004"
LangString INSTTYPE_REDIST 1049 "Дистрибутивы"
LangString DESC_STEAMCONV_ALL 1049 "Эта опция позволяет конвертировать Steam версию игры в версию 2004 и установить Mod Loader.$\r$\n\
В этом режиме моды не устанавливаются.$\r$\n\
Для установки модов выберите другую опцию."
LangString DESC_REDIST_ALL 1049 "Эта опция позволяет установить DirectX, Visual C++ runtimes и .NET Framework.$\r$\n\
Файлы игры останутся без изменений.$\r$\n\
В этом режиме устанавливаются только библиотеки, необходимые для работы игры. Для установки модов выберите другую опцию."

;Strings added or renamed in the 2024 update
LangString DE_E_NETF 1049 "Installing .NET Framework..."
LangString D_NETF 1049 "Downloading .NET Framework Setup..."
LangString DE_NETFPRESENT 1049 ".NET Framework is already installed ($NetFrameworkVersion)."
LangString PROFILENAME_STEAMCONV 1049 "Steam to 2004"
LangString PROFILENAME_REDIST 1049 "Redists"
LangString DESC_OPTION_MANAGERCLASSIC 1049 "Use the alternative SADX Mod Manager instead of the modern SA Manager. Required on Windows XP."
LangString DE_B_ONION 1049 "Backing up Onion Blur settings..."
LangString DE_R_ONION 1049 "Restoring Onion Blur settings..."
LangString DESC_NETF 1049 "Install or update .NET Framework, which is required for conversion tools to work properly. $\r$\n\
$\r$\n\
The installer checks if .NET Framework is already installed before downloading it."
LangString D_MODMANAGER 1049 "Downloading SADX Mod Manager..."
LangString DE_E_MANAGER 1049 "Extracting the Mod Manager..."