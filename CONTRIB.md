# SADX Mod Installer contributions and derivative works

There is no license for SADX Mod Installer, and I cannot prevent anyone from doing whatever they want with its source code and assets. Therefore, the guidelines below are merely a request for cooperation that I put out in good faith that people will follow them.

## Using SADX Mod Installer code in your own projects

* SADX Mod Installer is no longer in development. If you want to make another installer for SADX based on the code from this repository, please feel free to go ahead. Credit is appreciated.
* If you would like to make a similar installer for another game, please create your own fork. Credit is appreciated.
* Please do not use the code on this repo to create malware or fake/scam installers.

Thank you for your help.