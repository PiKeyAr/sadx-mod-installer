; This file contains code for all sections

Section -SetupTools
SectionIn 1 2 3 4
DetailPrint $(DE_INSTDATA)
DetailPrint "Language: $LANGUAGE"
CreateDirectory "$EXEDIR\instdata"
SetOutPath "$INSTDIR"
DetailPrint $(DE_7Z)
SetOutPath "$TEMP"
File "resources\7za.exe"
AccessControl::SetFileOwner "$INSTDIR" "(BU)"
Pop $Permission1
DetailPrint $(DE_OWNER)
AccessControl::GrantOnFile "$INSTDIR" "(BU)" "FullAccess"
Pop $Permission2
DetailPrint $(DE_PERM)
SectionEnd

Section /o $(SECTIONNAME_REMOVEMODS) SECTION_REMOVEMODS
DetailPrint $(DE_REALL)
RmDir /r "$INSTDIR\mods"
SectionEnd

Section /o $(SECTIONNAME_PERMISSIONS) SECTION_PERMISSIONS
SectionIn 1 2 3 4
DetailPrint $(DE_PRGFILES)
StrCpy $0 $PROGRAMFILES
StrLen $2 $0
StrCpy $1 $INSTDIR $2
${If} $0 == $1
	DetailPrint $(DE_RECU)
	${Locate} "$INSTDIR" "/L=FD /M=*.*" "LocateCallback"
	IfErrors 0 +2
	MessageBox MB_OK $(ERR_PERMISSION)
${EndIf}
SectionEnd

SectionGroup $(SECTIONNAME_DEPENDENCIES) SECTIONGROUP_MODLOADER
Section ".NET Framework" SECTION_NETF
SectionIn 1 2 3 5
DetailPrint $(DE_CHECKNET)
${If} ${RunningX64}
	SetRegView 64
${Else}
	SetRegView 32
${EndIf}
; Framework 4.0 (old Manager and Steam conversion tool)
ReadRegStr $NetFrameworkVersion HKLM "SOFTWARE\Microsoft\NET Framework Setup\NDP\v4\Client" "Version"
${If} $NetFrameworkVersion = "4.0.30319"
	goto skipnet
${EndIf}
IfFileExists "$EXEDIR\instdata\dotNetFx40_Full_x86_x64.exe" installnet_xp
DetailPrint $(D_NETF)
inetc::get /WEAKSECURITY /RESUME $(ERR_DOWNLOAD_RETRY) /CAPTION $(D_NET) /CANCELTEXT $(ERR_DOWNLOAD_CANCEL_A) /QUESTION $(ERR_DOWNLOAD_CANCEL_Q) /TRANSLATE $(INETC_DOWNLOADING) $(INETC_CONNECT) $(INETC_SECOND) $(INETC_MINUTE) $(INETC_HOUR) $(INETC_PLURAL) $(INETC_PROGRESS) $(INETC_REMAINING) "https://dcmods.unreliable.network/owncloud/data/PiKeyAr/files/Setup/data/dotNetFx40_Full_x86_x64.exe" "$EXEDIR\instdata\dotNetFx40_Full_x86_x64.exe" /END
Pop $DownloadErrorCode
StrCmp $DownloadErrorCode "OK" installnet_xp 0
MessageBox MB_OK $(ERR_D_NET)
Goto netend

installnet_xp:
	DetailPrint $(DE_E_NETF)
	ExecWait '"$EXEDIR\instdata\dotNetFx40_Full_x86_x64.exe" /q /norestart'
	goto netend
skipnet:
	DetailPrint $(DE_NETFPRESENT)
	goto netend
netend:
SectionEnd

Section ".NET 8.0 Desktop Runtime" SECTION_NET
SectionIn 1 2 3 5
${If} ${IsWinXP}
	DetailPrint "Windows XP"
	goto netend70
${Else}
	IfFileExists "$EXEDIR\instdata\$DotNetExecutable" installnet70
	DetailPrint $(D_NET)
	inetc::get /WEAKSECURITY /RESUME $(ERR_DOWNLOAD_RETRY) /CAPTION $(D_NET) /CANCELTEXT $(ERR_DOWNLOAD_CANCEL_A) /QUESTION $(ERR_DOWNLOAD_CANCEL_Q) /TRANSLATE $(INETC_DOWNLOADING) $(INETC_CONNECT) $(INETC_SECOND) $(INETC_MINUTE) $(INETC_HOUR) $(INETC_PLURAL) $(INETC_PROGRESS) $(INETC_REMAINING) "https://dcmods.unreliable.network/owncloud/data/PiKeyAr/files/Setup/data/$DotNetExecutable" "$EXEDIR\instdata\$DotNetExecutable" /END
	Pop $DownloadErrorCode
	StrCmp $DownloadErrorCode "OK" installnet70 0
	MessageBox MB_OK $(ERR_D_NET)
	Goto netend70
	installnet70:
		DetailPrint $(DE_E_NET)
		ExecWait '"$EXEDIR\instdata\$DotNetExecutable /install /quiet /norestart' "$DirectXSetupError"
		goto netend70
${EndIf}
netend70:
SectionEnd

Section $(SECTIONNAME_VCC) SECTION_VCC
SectionIn 1 2 3 5
DetailPrint $(DE_C_VCC)
SetOutPath "$INSTDIR"
IfFileExists "$EXEDIR\instdata\VisualCppRedist_AIO_x86_x64.exe" extract2019 download2019

download2019:
	DetailPrint $(D_VC2019)
	inetc::get /WEAKSECURITY /RESUME $(ERR_DOWNLOAD_RETRY) /CAPTION $(D_VC2019) /CANCELTEXT $(ERR_DOWNLOAD_CANCEL_A) /QUESTION $(ERR_DOWNLOAD_CANCEL_Q) /TRANSLATE $(INETC_DOWNLOADING) $(INETC_CONNECT) $(INETC_SECOND) $(INETC_MINUTE) $(INETC_HOUR) $(INETC_PLURAL) $(INETC_PROGRESS) $(INETC_REMAINING) "https://dcmods.unreliable.network/owncloud/data/PiKeyAr/files/Setup/data/VisualCppRedist_AIO_x86_x64.exe" "$EXEDIR\instdata\VisualCppRedist_AIO_x86_x64.exe" /END
	Pop $DownloadErrorCode
	StrCmp $DownloadErrorCode "OK" extract2019 0
	MessageBox MB_OK $(ERR_D_VC2019)
	Goto finishedvs

extract2019:
	DetailPrint $(DE_I_VC2019)
	ExecWait '"$EXEDIR\instdata\VisualCppRedist_AIO_x86_x64.exe" /ai /gm2' "$DirectXSetupError"
	goto finishedvs

finishedvs:
SectionEnd

Section $(SECTIONNAME_DX9) SECTION_DIRECTX
SectionIn 1 2 3 5
DetailPrint $(DE_C_DX9)
IfFileExists $WINDIR\system32\D3DX9_43.dll donedx installdx

installdx:
	IfFileExists "$EXEDIR\instdata\directx_Jun2010_redist.exe" rundxsetup downloaddx

downloaddx:
	DetailPrint $(D_DX9)
	inetc::get /WEAKSECURITY /RESUME $(ERR_DOWNLOAD_RETRY) /CAPTION $(D_DX9) /CANCELTEXT $(ERR_DOWNLOAD_CANCEL_A) /QUESTION $(ERR_DOWNLOAD_CANCEL_Q) /TRANSLATE $(INETC_DOWNLOADING) $(INETC_CONNECT) $(INETC_SECOND) $(INETC_MINUTE) $(INETC_HOUR) $(INETC_PLURAL) $(INETC_PROGRESS) $(INETC_REMAINING) "https://dcmods.unreliable.network/owncloud/data/PiKeyAr/files/Setup/data/directx_Jun2010_redist.exe" "$EXEDIR\instdata\directx_Jun2010_redist.exe" /END
	Pop $DownloadErrorCode
	StrCmp $DownloadErrorCode "OK" rundxsetup 0
	MessageBox MB_OK $(ERR_D_DX9)
	Goto donedx

rundxsetup:
	DetailPrint $(DE_I_DX9_1)
	RmDir /r "$EXEDIR\instdata\DX"
	CreateDirectory "$EXEDIR\instdata\DX"
	ExecWait '"$EXEDIR\instdata\directx_Jun2010_redist.exe" /Q /T:"$EXEDIR\instdata\DX"' $DirectXSetupError
	DetailPrint $(DE_I_DX9_2)
	ExecWait '"$EXEDIR\instdata\DX\dxsetup.exe" /silent' $DirectXSetupError
	RmDir /r "$EXEDIR\instdata\DX"
	goto donedx
donedx:
SectionEnd

SectionGroupEnd

Section $(SECTIONNAME_MANAGERCLASSIC) SECTION_MANAGERCLASSIC
	StrCpy $ModManagerArchive "SADXModManager.exe"
SectionEnd

Section $(SECTIONNAME_MODLOADER) SECTION_MODLOADER
SectionIn 1 2 3 4
StrCpy $ConvToolsArchive "steam_tools"
${nsProcess::KillProcess} "sonic.exe" $R0
${nsProcess::KillProcess} "sadx.exe" $R0
${nsProcess::KillProcess} "Sonic Adventure DX.exe" $R0
${nsProcess::KillProcess} "SADXModManager.exe" $R0
${nsProcess::KillProcess} "SAModManager.exe" $R0
${nsProcess::KillProcess} "AppLauncher.exe" $R0
goto install

sadx2004check:
DetailPrint $(DE_2004FOUND)
IfFileExists "$INSTDIR\system\CHRMODELS_orig.dll" sonicexecheck 0
IfFileExists "$INSTDIR\system\CHRMODELS.dll" 0 missingfiles
goto sonicexecheck
 
sonicexecheck:
	DetailPrint $(DE_C_EXE)
	md5dll::GetMD5File "$INSTDIR\sonic.exe"
	Pop $sonicexemd5
	DetailPrint $sonicexemd5
	StrCmp $sonicexemd5 "1b65b196137b5a853d781ba93a3046a2" sonicexebad 0 ;Sold Out Software
	StrCmp $sonicexemd5 "6e2e64ebf62787af47ed813221040898" sonicexebad 0 ;JP
	StrCpy $ConvToolsArchive "2004_tools"
	goto checkdownload1
	
sonicexebad:
	DetailPrint $(DE_EXEUNK)
	goto exeerror
	
missingfiles:
	MessageBox MB_OK $(ERR_MISSINGFILES)
	Quit

install:
	SetOutPath "$INSTDIR"
	DetailPrint $(DE_DETECT)	
	IfFileExists "$INSTDIR\Sonic Adventure DX.exe" steamcheck 0
	IfFileExists "$INSTDIR\sonic.exe" sadx2004check_pre missingfiles
	
; Check for an incomplete conversion (sonic.exe but Steam data)
sadx2004check_pre:
	; Check SFD/MPG
	IfFileExists "$INSTDIR\system\SA1.SFD" 0 +2
	IfFileExists "$INSTDIR\system\SA1.MPG" 0 checkdownload1
	; Check SFD/MPG (intro)
	IfFileExists "$INSTDIR\DLC\re-us2.sfd" 0 +2
	IfFileExists "$INSTDIR\system\RE-US.mpg" 0 checkdownload1
goto sadx2004check

steamcheck:
	DetailPrint $(DE_2010FOUND)
	md5dll::GetMD5File "$INSTDIR\Sonic Adventure DX.exe"
	Pop $sonicexemd5
	DetailPrint $sonicexemd5
	StrCmp $sonicexemd5 "4aef25b17613a0455e1606db8bbf8025" sonicexebad 0 ;Dreamcast Collection with DRM
	DetailPrint $(DE_C_2010)
	; Check if certain files from either 2004 or Steam exist. If the file from either version isn't there, it's a fatal error.
	; Check SFD/MPG
	IfFileExists "$INSTDIR\system\SA1.SFD" +2 0
	IfFileExists "$INSTDIR\system\SA1.MPG" 0 missingfiles
	; Check SFD/MPG (intro)
	IfFileExists "$INSTDIR\DLC\re-us2.sfd" +2 0
	IfFileExists "$INSTDIR\system\RE-US.mpg" 0 missingfiles
	; Check DAT soundbanks
	IfFileExists "$INSTDIR\SoundData\SE\WINDY_VALLEY_BANK05.dat" +2 0
	IfFileExists "$INSTDIR\system\SoundData\SE\WINDY_VALLEY_BANK05.dat" 0 missingfiles
	; Check ADX/WMA music
	IfFileExists "$INSTDIR\SoundData\bgm\wma\option.adx" +3 0
	IfFileExists "$INSTDIR\system\SoundData\bgm\wma\option.wma" +2 0
	IfFileExists "$INSTDIR\system\SoundData\bgm\wma\option.adx" 0 missingfiles
	; Check ADX/WMA voices
	IfFileExists "$INSTDIR\SoundData\voice_us\wma\0000.adx" +3 0
	IfFileExists "$INSTDIR\system\SoundData\voice_us\wma\0000.wma" +2 0
	IfFileExists "$INSTDIR\system\SoundData\voice_us\wma\0000.adx" 0 missingfiles
	goto checkdownload1
	
checkdownload1:
	IfFileExists "$EXEDIR\instdata\$ConvToolsArchive.7z" extracttools downloadtools

downloadtools:
	DetailPrint $(D_STEAMTOOLS)
	DetailPrint 'https://dcmods.unreliable.network/owncloud/data/PiKeyAr/files/Setup/data/$ConvToolsArchive.7z'
	inetc::get /WEAKSECURITY /RESUME $(ERR_DOWNLOAD_RETRY) /CAPTION $(D_STEAMTOOLS) /CANCELTEXT $(ERR_DOWNLOAD_CANCEL_A) /QUESTION $(ERR_DOWNLOAD_CANCEL_Q) /TRANSLATE $(INETC_DOWNLOADING) $(INETC_CONNECT) $(INETC_SECOND) $(INETC_MINUTE) $(INETC_HOUR) $(INETC_PLURAL) $(INETC_PROGRESS) $(INETC_REMAINING) "https://dcmods.unreliable.network/owncloud/data/PiKeyAr/files/Setup/data/$ConvToolsArchive.7z" "$EXEDIR\instdata\$ConvToolsArchive.7z" /END
	Pop $DownloadErrorCode
	StrCmp $DownloadErrorCode "OK" extracttools 0
	MessageBox MB_OK $(ERR_DOWNLOAD_FATAL)
	Quit

extracttools:
	DetailPrint $(DE_E_TOOLS)
	nsexec::ExecToStack '"$TEMP\7za.exe" x "$EXEDIR\instdata\$ConvToolsArchive.7z" -o"$INSTDIR" -aoa'
	DetailPrint $(DE_C_TOOLS)
	IfFileExists "$INSTDIR\SteamHelper.exe" 0 toolerror
	IfFileExists "$INSTDIR\hpatchz.exe" 0 toolerror
	goto install_steam

toolerror:
	MessageBox MB_OK $(ERR_DOWNLOAD_TOOLS)
	Quit

install_steam:
	DetailPrint $(DE_SND)
	${If} ${RunningX64}
		SetRegView 64
	${Else}
		SetRegView 32
	${EndIf}

	${If} ${IsWinXP}
		ReadRegStr $NetFrameworkVersion HKLM "SOFTWARE\Microsoft\NET Framework Setup\NDP\v4\Client" "Version"
		${If} $NetFrameworkVersion != "4.0.30319"
		MessageBox MB_OK $(ERR_NET_MISSING)
		Quit
		${EndIf}
	SetOutPath "$INSTDIR"
	nsexec::ExecToLog '"$INSTDIR\SteamHelper.exe"'	
	goto modloader
	${EndIf}

	ReadRegDWORD $NetFrameworkVersion HKLM "SOFTWARE\Microsoft\NET Framework Setup\NDP\v4\Full" "Release"
	${If} $NetFrameworkVersion < "378389"
		MessageBox MB_OK $(ERR_NET_MISSING)
		Quit
	${EndIf}
	SetOutPath "$INSTDIR"
	nsexec::ExecToLog '"$INSTDIR\SteamHelper.exe"'	
	goto modloader

modloader:
	; Cleanup Steam Helper stuff
	Delete "$INSTDIR\SteamHelper.*"
	Delete "$INSTDIR\patch*.dat"
	Delete "$INSTDIR\hpatchz.exe"
	Delete "$INSTDIR\ffmpeg.exe"
	IfFileExists "$EXEDIR\instdata\SADXModLoader.7z" +5 0
	DetailPrint $(D_MODLOADER)
	inetc::get /WEAKSECURITY /RESUME $(ERR_DOWNLOAD_RETRY) /CAPTION $(D_MODLOADER) /CANCELTEXT $(ERR_DOWNLOAD_CANCEL_A) /QUESTION $(ERR_DOWNLOAD_CANCEL_Q) /TRANSLATE $(INETC_DOWNLOADING) $(INETC_CONNECT) $(INETC_SECOND) $(INETC_MINUTE) $(INETC_HOUR) $(INETC_PLURAL) $(INETC_PROGRESS) $(INETC_REMAINING) "https://dcmods.unreliable.network/owncloud/data/PiKeyAr/files/Setup/data/SADXModLoader.7z" "$EXEDIR\instdata\SADXModLoader.7z" /END
	Pop $DownloadErrorCode
	StrCmp $DownloadErrorCode "OK" 0 +5
	IfFileExists "$EXEDIR\instdata\$ModManagerArchive" extractmodloader 0
	DetailPrint $(D_MODMANAGER)
	inetc::get /WEAKSECURITY /RESUME $(ERR_DOWNLOAD_RETRY) /CAPTION $(D_MODMANAGER) /CANCELTEXT $(ERR_DOWNLOAD_CANCEL_A) /QUESTION $(ERR_DOWNLOAD_CANCEL_Q) /TRANSLATE $(INETC_DOWNLOADING) $(INETC_CONNECT) $(INETC_SECOND) $(INETC_MINUTE) $(INETC_HOUR) $(INETC_PLURAL) $(INETC_PROGRESS) $(INETC_REMAINING) "https://dcmods.unreliable.network/owncloud/data/PiKeyAr/files/Setup/data/$ModManagerArchive" "$EXEDIR\instdata\$ModManagerArchive" /END
	Pop $DownloadErrorCode
	StrCmp $DownloadErrorCode "OK" extractmodloader 0
	MessageBox MB_OK $(ERR_DOWNLOAD_FATAL)
	Quit

extractmodloader:
	; Set Loader path (mods\.modloader)
	StrCpy $ModLoaderPath "$INSTDIR\mods\.modloader"
	; Set Manager data path (AppData\Local\SAManager)
	StrCpy $ManagerAppDataPath "$LocalAppdata\SAManager"
	; Delete old data - remove the local folder entirely
	RmDir /r "$INSTDIR\SAManager"
	; Delete old data - remove SADX stuff only in AppData
	RmDir /r "$LocalAppdata\SAManager\SADX"
	; Install the Loader
	DetailPrint $(DE_E_ML)
	CreateDirectory "$ModLoaderPath"
	nsexec::ExecToStack '"$TEMP\7za.exe" x "$EXEDIR\instdata\SADXModLoader.7z" -o"$ModLoaderPath\" -aoa'
	IfFileExists "$ModLoaderPath\SADXModLoader.dll" 0 modloadererror
	IfFileExists "$ModLoaderPath\extlib\BASS\bass.dll" 0 modloadererror
	DetailPrint $(DE_I_ML)
	; Install Loader DLL
	IfFileExists "$INSTDIR\system\CHRMODELS_orig.dll" +2 0
	CopyFiles /SILENT "$INSTDIR\system\CHRMODELS.dll" "$INSTDIR\system\CHRMODELS_orig.dll"
	Delete "$INSTDIR\system\CHRMODELS.dll"
	CopyFiles /SILENT "$ModLoaderPath\SADXModLoader.dll" "$INSTDIR\system\CHRMODELS.dll"
	; Install the Manager - Classic
	${If} $ModManagerArchive == "SADXModManager.exe"
		Delete "$INSTDIR\SADXModManager.exe"
		DetailPrint $(DE_E_MANAGER)
		CopyFiles /SILENT "$EXEDIR\instdata\SADXModManager.exe" "$INSTDIR\SADXModManager.exe"
		IfFileExists "$INSTDIR\SADXModManager.exe" finished modloadererror
	; Install the Manager - Modern
	${Else}
	    Delete "$INSTDIR\SAModManager.exe"
	    Delete "$INSTDIR\SADXModManager.exe"
		DetailPrint $(DE_E_MANAGER)
		nsexec::ExecToStack '"$TEMP\7za.exe" x "$EXEDIR\instdata\$ModManagerArchive" -o"$INSTDIR" -aoa'
		IfFileExists "$INSTDIR\SAModManager.exe" finished modloadererror
	${EndIf}
	goto finished

exeerror:
	MessageBox MB_OK $(ERR_2004CHECK)
	Quit

modloadererror:
	MessageBox MB_OK $(ERR_MODLOADER)
	Quit

finished:
	DetailPrint $(DE_CLEANUP)
	SetOutPath "$INSTDIR"
	Delete "$INSTDIR\system\AL_STG_KINDER_AD_TEX_R.pvm"
	Delete "$INSTDIR\system\AL_TEX_ODEKAKE_MENU_EN_R.pvm"
	Delete "$INSTDIR\system\AL_TEX_ODEKAKE_MENU_JP_R.pvm"
	Delete "$INSTDIR\system\AVA_DLG_F.pvm AVA_DLG_G.pvm"
	Delete "$INSTDIR\system\AVA_DLG_I.pvm AVA_DLG_S.pvm"
	Delete "$INSTDIR\system\AVA_FILESEL_F.pvm"
	Delete "$INSTDIR\system\AVA_FILESEL_G.pvm"
	Delete "$INSTDIR\system\AVA_FILESEL_I.pvm"
	Delete "$INSTDIR\system\AVA_FILESEL_S.pvm"
	Delete "$INSTDIR\system\AVA_GTITLE0_DC.pvm"
	Delete "$INSTDIR\system\AVA_GTITLE0_F.pvm"
	Delete "$INSTDIR\system\AVA_GTITLE0_G.pvm"
	Delete "$INSTDIR\system\AVA_GTITLE0_I.pvm"
	Delete "$INSTDIR\system\AVA_GTITLE0_S.pvm"
	Delete "$INSTDIR\system\ava_help_options.pvm"
	Delete "$INSTDIR\system\ava_how2play.pvm"
	Delete "$INSTDIR\system\ava_LB_MENU.pvm"
	Delete "$INSTDIR\system\AVA_SNDTEST_E_R.pvm"
	Delete "$INSTDIR\system\AVA_SNDTEST_R.pvm"
	Delete "$INSTDIR\system\AVA_TITLE_CMN2.pvm"
	Delete "$INSTDIR\system\AVA_TITLE_F.pvm"
	Delete "$INSTDIR\system\AVA_TITLE_G.pvm"
	Delete "$INSTDIR\system\AVA_TITLE_I.pvm"
	Delete "$INSTDIR\system\AVA_TITLE_S.pvm"
	Delete "$INSTDIR\system\ava_tool_tips.pvm"
	Delete "$INSTDIR\system\AVA_VMSSEL_F.pvm"
	Delete "$INSTDIR\system\AVA_VMSSEL_G.pvm"
	Delete "$INSTDIR\system\AVA_VMSSEL_I.pvm"
	Delete "$INSTDIR\system\AVA_VMSSEL_S.pvm"
	Delete "$INSTDIR\system\CON_REGULAR_F.pvm"
	Delete "$INSTDIR\system\CON_REGULAR_G.pvm"
	Delete "$INSTDIR\system\CON_REGULAR_I.pvm"
	Delete "$INSTDIR\system\CON_REGULAR_S.pvm"
	Delete "$INSTDIR\system\DC_HELP_OPTIONS_F.pvm"
	Delete "$INSTDIR\system\DC_HELP_OPTIONS_G.pvm"
	Delete "$INSTDIR\system\DC_HELP_OPTIONS_I.pvm"
	Delete "$INSTDIR\system\DC_HELP_OPTIONS_J.pvm"
	Delete "$INSTDIR\system\DC_HELP_OPTIONS_S.pvm"
	Delete "$INSTDIR\system\DC_HOW_PLAY_F.pvm"
	Delete "$INSTDIR\system\DC_HOW_PLAY_G.pvm"
	Delete "$INSTDIR\system\DC_HOW_PLAY_I.pvm"
	Delete "$INSTDIR\system\DC_HOW_PLAY_J.pvm"
	Delete "$INSTDIR\system\DC_HOW_PLAY_S.pvm"
	Delete "$INSTDIR\system\DC_LEADER_MENU.pvm"
	Delete "$INSTDIR\system\DC_LEADER_MENU_F.pvm"
	Delete "$INSTDIR\system\DC_LEADER_MENU_G.pvm"
	Delete "$INSTDIR\system\DC_LEADER_MENU_I.pvm"
	Delete "$INSTDIR\system\DC_LEADER_MENU_J.pvm"
	Delete "$INSTDIR\system\DC_LEADER_MENU_S.pvm"
	Delete "$INSTDIR\system\DC_MAIN_MENU.pvm"
	Delete "$INSTDIR\system\DC_MAIN_MENU_F.pvm"
	Delete "$INSTDIR\system\DC_MAIN_MENU_G.pvm"
	Delete "$INSTDIR\system\DC_MAIN_MENU_I.pvm"
	Delete "$INSTDIR\system\DC_MAIN_MENU_J.pvm"
	Delete "$INSTDIR\system\DC_MAIN_MENU_S.pvm"
	Delete "$INSTDIR\system\GAMEOVER_F.pvm"
	Delete "$INSTDIR\system\GAMEOVER_G.pvm"
	Delete "$INSTDIR\system\GAMEOVER_I.pvm"
	Delete "$INSTDIR\system\GAMEOVER_S.pvm"
	Delete "$INSTDIR\system\MAP_EC_A_R.pvm"
	Delete "$INSTDIR\system\MAP_EC_B_R.pvm"
	Delete "$INSTDIR\system\MAP_EC_H_R.pvm"
	Delete "$INSTDIR\system\MAP_MR_A_R.pvm"
	Delete "$INSTDIR\system\MAP_MR_J_R.pvm"
	Delete "$INSTDIR\system\MAP_MR_S_R.pvm"
	Delete "$INSTDIR\system\MAP_PAST_E_R.pvm"
	Delete "$INSTDIR\system\MAP_PAST_S_R.pvm"
	Delete "$INSTDIR\system\MAP_SS_R.pvm"
	Delete "$INSTDIR\system\MIS_C_EN_R.pvm"
	Delete "$INSTDIR\system\MIS_C_J_R.pvm"
	Delete "$INSTDIR\system\PRESSSTART_DC.pvm"
	Delete "$INSTDIR\system\PRESSSTART_DC_F.pvm"
	Delete "$INSTDIR\system\PRESSSTART_DC_G.pvm"
	Delete "$INSTDIR\system\PRESSSTART_DC_I.pvm"
	Delete "$INSTDIR\system\PRESSSTART_DC_J.pvm"
	Delete "$INSTDIR\system\PRESSSTART_DC_S.pvm"
	Delete "$INSTDIR\system\PRESSSTART_F.pvm"
	Delete "$INSTDIR\system\PRESSSTART_G.pvm"
	Delete "$INSTDIR\system\PRESSSTART_I.pvm"
	Delete "$INSTDIR\system\PRESSSTART_J.pvm"
	Delete "$INSTDIR\system\PRESSSTART_S.pvm"
	Delete "$INSTDIR\system\SOC_fontdata0.bin"
	Delete "$INSTDIR\system\SOC_fontdata1.bin"
	Delete "$INSTDIR\system\SonicADV_Socstaff.bin"
	Delete "$INSTDIR\system\TUTO_CMN_E_R.pvm"
	Delete "$INSTDIR\system\TUTO_CMN_F.pvm"
	Delete "$INSTDIR\system\TUTO_CMN_F_R.pvm"
	Delete "$INSTDIR\system\TUTO_CMN_G.pvm"
	Delete "$INSTDIR\system\TUTO_CMN_G_R.pvm"
	Delete "$INSTDIR\system\TUTO_CMN_I.pvm"
	Delete "$INSTDIR\system\TUTO_CMN_I_R.pvm"
	Delete "$INSTDIR\system\TUTO_CMN_R.pvm"
	Delete "$INSTDIR\system\TUTO_CMN_S.pvm"
	Delete "$INSTDIR\system\TUTO_CMN_S_R.pvm"
	Delete "$INSTDIR\install.vdf"
	RmDir /r "$INSTDIR\system\BackGround"
	RmDir /r "$INSTDIR\system\BigEndian"
	RmDir /r "$INSTDIR\system\CreateNewFile"
	RmDir /r "$INSTDIR\system\DC"
	RmDir /r "$INSTDIR\system\Leaderboards"
	RmDir /r "$INSTDIR\system\LittleEndian"
	RmDir /r "$INSTDIR\system\Logo"
	RmDir /r "$INSTDIR\system\NowLoading"
	RmDir /r "$INSTDIR\system\Option"
	RmDir /r "$INSTDIR\system\Tips_DDS"
	RmDir /r "$INSTDIR\system\tips_exit"
	RmDir /r "$INSTDIR\system\texture_replace"
	RmDir /r "$INSTDIR\system\texture_repalce"
	RmDir /r "$INSTDIR\system\TUTO"
	RmDir /r "$INSTDIR\system\TUTO_Texture"
	RmDir /r "$INSTDIR\system\Water"
	RmDir /r "$INSTDIR\linux32"
	RmDir /r "$INSTDIR\linux64"
	RmDir /r "$INSTDIR\osx32"
	RmDir /r "$INSTDIR\win64"
	RmDir /r "$INSTDIR\tools"
	RmDir /r "$INSTDIR\Font"
	RmDir /r "$INSTDIR\Shader"
	Delete "$INSTDIR\Sonic Adventure DX.exe"
	Delete "$INSTDIR\SEGA*.*"
	Delete "$INSTDIR\sadx_snd.cmd"
	Delete "$INSTDIR\sadx_bgm.bat"
	Delete "$INSTDIR\sadx_saves.bat"
	Delete "$INSTDIR\modloader.bat"
	Delete "$INSTDIR\sadxconvert_eu.bat"
	Delete "$INSTDIR\sadxconvert_min.bat"
	Delete "$INSTDIR\D3DX9_43.dll"
	Delete "$INSTDIR\xinput9_1_0.dll"
	IfFileExists "$INSTDIR\system\RE-US.mpg" 0 +2
	RmDir /r "$INSTDIR\DLC"
	IfFileExists "$INSTDIR\system\SoundData\bgm\wma\option.adx" 0 +2
	RmDir /r "$INSTDIR\SoundData"
	IfFileExists "$INSTDIR\system\SA1.mpg" 0 +2
	Delete "$INSTDIR\system\SA*.sfd"
	; Legacy Loader/Manager
	Delete "$INSTDIR\7z.exe"
	Delete "$INSTDIR\7z.dll"
	Delete "$INSTDIR\avcodec*.dll"
	Delete "$INSTDIR\avformat*.dll"
	Delete "$INSTDIR\avutil*.dll"
	Delete "$INSTDIR\bass.dll"
	Delete "$INSTDIR\bass_vgmstream.dll"
	Delete "$INSTDIR\COPYING*"
	Delete "$INSTDIR\d3d8m.dll"
	Delete "$INSTDIR\d3d8.dll"
	Delete "$INSTDIR\jansson.dll"
	Delete "$INSTDIR\libatrac*.dll"
	Delete "$INSTDIR\libcelt*.dll"
	Delete "$INSTDIR\libg*.dll"
	Delete "$INSTDIR\libmpg*.dll"
	Delete "$INSTDIR\libogg.dll"
	Delete "$INSTDIR\libspeex*.dll"
	Delete "$INSTDIR\libvorbis*.dll"
	Delete "$INSTDIR\loader.manifest"
	Delete "$INSTDIR\ModManagerCommon.*"
	Delete "$INSTDIR\NewtonSoft.Json.dll"
	Delete "$INSTDIR\sadxmlver.dll"
	Delete "$INSTDIR\SADXModLoader.dll"
	Delete "$INSTDIR\SADXModManager.pdb"
	Delete "$INSTDIR\SharpDX.*"
	Delete "$INSTDIR\SharpDX.DirectInput.*"
	Delete "$INSTDIR\swresample*.dll"
SectionEnd

Section $(SECTIONNAME_LAUNCHER) SECTION_LAUNCHER
SectionIn 1 2 3 4
IfFileExists "$EXEDIR\instdata\AppLauncher.7z" extractlauncher downloadlauncher

downloadlauncher:
	DetailPrint $(D_LAUNCHER)
	inetc::get /WEAKSECURITY /RESUME $(ERR_DOWNLOAD_RETRY) /CAPTION $(D_LAUNCHER) /CANCELTEXT $(ERR_DOWNLOAD_CANCEL_A) /QUESTION $(ERR_DOWNLOAD_CANCEL_Q) /TRANSLATE $(INETC_DOWNLOADING) $(INETC_CONNECT) $(INETC_SECOND) $(INETC_MINUTE) $(INETC_HOUR) $(INETC_PLURAL) $(INETC_PROGRESS) $(INETC_REMAINING) "https://dcmods.unreliable.network/owncloud/data/PiKeyAr/files/Setup/data/AppLauncher.7z" "$EXEDIR\instdata\AppLauncher.7z" /END
	Pop $DownloadErrorCode
	StrCmp $DownloadErrorCode "OK" extractlauncher 0
	MessageBox MB_OK $(ERR_D_LAUNCHER)
	Goto finished

extractlauncher:
	DetailPrint $(DE_E_LAUNCHER)
	RmDir /r "$INSTDIR\AppLauncher"
	nsexec::ExecToStack '"$TEMP\7za.exe" x "$EXEDIR\instdata\AppLauncher.7z" -o"$INSTDIR" -aoa'
	goto finished
finished:
SectionEnd

SectionGroup $(SECTIONNAME_BUGFIXES) SECTIONGROUP_BUGFIXES

Section $(SECTIONNAME_SADXFE) SECTION_SADXFE
SectionIn 2
StrCpy $Modname $(MOD_SADXFE)
StrCpy $ModFilename "SADXFE"
IntOp $ModInstallNoSubFolderMode 0 + 1
Call ModInstall
${If} $ModInstallSuccess == "1"
	IntOp $INST_SADXFE 0 + 1
	RmDir /r "$INSTDIR\mods\ECGardenOceanFix"
	RmDir /r "$INSTDIR\mods\MR_FinalEggFix"
${EndIf}
IntOp $ModInstallNoSubFolderMode 0 + 0
SectionEnd

Section $(SECTIONNAME_FRAMELIMIT) SECTION_FRAMELIMIT
SectionIn 1 2 3
StrCpy $Modname $(MOD_FRAME)
StrCpy $ModFilename "sadx-frame-limit"
Call ModInstall
${If} $ModInstallSuccess == "1"
	IntOp $INST_FRAMELIMIT 0 + 1
	Call GenerateModVersion
	RmDir /r "$INSTDIR\mods\frame-limit"
${EndIf}
SectionEnd

SectionGroupEnd

SectionGroup $(SECTIONNAME_DCMODS) SECTIONGROUP_DCCONV

Section $(SECTIONNAME_DCCONV) SECTION_DCMODS
SectionIn 1
${If} $PreserveModSettings == "1"
	IfFileExists "$INSTDIR\mods\DreamcastConversion\config.ini" backupconfig nobackup
${Else}
	goto nobackup
${EndIf}

backupconfig:
	DetailPrint $(DE_B_DCCONV)
	CopyFiles /SILENT "$INSTDIR\mods\DreamcastConversion\config.ini" "$EXEDIR\instdata\DreamcastConversion.ini"
	goto nobackup

nobackup:
	IfFileExists "$EXEDIR\instdata\DreamcastConversion.7z" extract download

download:
	DetailPrint $(D_DCCONV)
	inetc::get /WEAKSECURITY /RESUME $(ERR_DOWNLOAD_RETRY) /CAPTION $(D_DCCONV) /CANCELTEXT $(ERR_DOWNLOAD_CANCEL_A) /QUESTION $(ERR_DOWNLOAD_CANCEL_Q) /TRANSLATE $(INETC_DOWNLOADING) $(INETC_CONNECT) $(INETC_SECOND) $(INETC_MINUTE) $(INETC_HOUR) $(INETC_PLURAL) $(INETC_PROGRESS) $(INETC_REMAINING) "https://dcmods.unreliable.network/owncloud/data/PiKeyAr/files/Setup/data/DreamcastConversion.7z" "$EXEDIR\instdata\DreamcastConversion.7z" /END
	Pop $DownloadErrorCode
	StrCmp $DownloadErrorCode "OK" extract 0
	MessageBox MB_YESNO $(ERR_D_DCCONV) IDYES finish IDNO 0
	Quit

extract:
	RmDir /r "$INSTDIR\mods\MR_FinalEggFix"
	RmDir /r "$INSTDIR\mods\DreamcastConversion"
	RmDir /r "$INSTDIR\mods\DC Conversion"
	RmDir /r "$INSTDIR\mods\DC_Mods"
	RmDir /r "$INSTDIR\mods\DC_Edition"
	RmDir /r "$INSTDIR\mods\DC_Camera"
	RmDir /r "$INSTDIR\mods\DC_EnvMaps"
	RmDir /r "$INSTDIR\mods\waterfix"
	RmDir /r "$INSTDIR\mods\DC_Bosses"
	RmDir /r "$INSTDIR\mods\DC_Casinopolis"
	RmDir /r "$INSTDIR\mods\DC_ChaoGardens"
	RmDir /r "$INSTDIR\mods\DC_EggCarrier"
	RmDir /r "$INSTDIR\mods\DC_EmeraldCoast"
	RmDir /r "$INSTDIR\mods\DC_FinalEgg"
	RmDir /r "$INSTDIR\mods\DC_General"
	RmDir /r "$INSTDIR\mods\DC_HotShelter"
	RmDir /r "$INSTDIR\mods\DC_IceCap"
	RmDir /r "$INSTDIR\mods\DC_LostWorld"
	RmDir /r "$INSTDIR\mods\DC_MysticRuins"
	RmDir /r "$INSTDIR\mods\DC_Past"
	RmDir /r "$INSTDIR\mods\DC_RedMountain"
	RmDir /r "$INSTDIR\mods\DC_SkyDeck"
	RmDir /r "$INSTDIR\mods\DC_SpeedHighway"
	RmDir /r "$INSTDIR\mods\DC_StationSquare"
	RmDir /r "$INSTDIR\mods\DC_SubGames"
	RmDir /r "$INSTDIR\mods\DC_TwinklePark"
	RmDir /r "$INSTDIR\mods\DC_WindyValley"
	RmDir /r "$INSTDIR\mods\EggCarrierOceanMusic"
	RmDir /r "$INSTDIR\mods\OptionalMods\SADXStyleWater"
	RmDir /r "$INSTDIR\mods\OptionalMods\RevertECDrawDistance"
	DetailPrint $(DE_E_DCCONV)
	nsexec::ExecToStack '"$TEMP\7za.exe" x "$EXEDIR\instdata\DreamcastConversion.7z" -o"$INSTDIR\mods\DreamcastConversion" -aoa'
	IntOp $INST_DCMODS 0 + 1
	IfFileExists "$EXEDIR\instdata\DreamcastConversion.ini" restoreconfig finish

restoreconfig:
	DetailPrint $(DE_R_DCCONV)
	CopyFiles /SILENT "$EXEDIR\instdata\DreamcastConversion.ini" "$INSTDIR\mods\DreamcastConversion\config.ini"

finish:
	Delete "$EXEDIR\instdata\DreamcastConversion.ini"

; Enable/Disable custom window title
${If} $EnableWindowTitle == "1"
	DetailPrint $(DE_DCTITLE_ON)
	WriteINIStr "$INSTDIR\mods\DreamcastConversion\config.ini" "General" "EnableWindowTitle" "True"
${Else}
	DetailPrint $(DE_DCTITLE_OFF)
	WriteINIStr "$INSTDIR\mods\DreamcastConversion\config.ini" "General" "EnableWindowTitle" "False"
${EndIf}
SectionEnd

Section $(SECTIONNAME_SA1CHARS) SECTION_SA1CHARS
SectionIn 1
StrCpy $Modname $(MOD_SA1CHARS)
StrCpy $ModFilename "SA1_Chars"
Call ModInstall
${If} $ModInstallSuccess == "1"
	IntOp $INST_SA1CHARS 0 + 1
	Call GenerateModVersion
${EndIf}
SectionEnd

Section $(SECTIONNAME_LANTERN) SECTION_LANTERN
SectionIn 1
StrCpy $Modname $(MOD_LANTERN)
StrCpy $ModFilename "sadx-dc-lighting"
Call ModInstall
${If} $ModInstallSuccess == "1"
	IntOp $INST_LANTERN 0 + 1
	Call GenerateModVersion
${EndIf}
SectionEnd

Section $(SECTIONNAME_DLCS) SECTION_DLCS
SectionIn 1
${If} $PreserveModSettings == "1"
	IfFileExists "$INSTDIR\mods\DLC\config.ini" backupconfig nobackup
${Else}
	goto nobackup
${EndIf}

backupconfig:
	DetailPrint $(DE_B_DLCS)
	CopyFiles /SILENT "$INSTDIR\mods\DLC\config.ini" "$EXEDIR\instdata\dlc.ini"
	goto nobackup

nobackup:
	IfFileExists "$EXEDIR\instdata\DLCs.7z" extract download

download:
	DetailPrint $(D_DLCS)
	inetc::get /WEAKSECURITY /RESUME $(ERR_DOWNLOAD_RETRY) /CAPTION $(D_DLCS) /CANCELTEXT $(ERR_DOWNLOAD_CANCEL_A) /QUESTION $(ERR_DOWNLOAD_CANCEL_Q) /TRANSLATE $(INETC_DOWNLOADING) $(INETC_CONNECT) $(INETC_SECOND) $(INETC_MINUTE) $(INETC_HOUR) $(INETC_PLURAL) $(INETC_PROGRESS) $(INETC_REMAINING) "https://dcmods.unreliable.network/owncloud/data/PiKeyAr/files/Setup/data/DLCs.7z" "$EXEDIR\instdata\DLCs.7z" /END
	Pop $DownloadErrorCode
	StrCmp $DownloadErrorCode "OK" extract 0
	MessageBox MB_YESNO $(ERR_D_DLCS) IDYES finish IDNO 0
	Quit

extract:
	DetailPrint $(DE_E_DLCS)
	RmDir /r "$INSTDIR\mods\DLCs"
	RmDir /r "$INSTDIR\mods\DLC"
	RmDir /r "$INSTDIR\mods\DLC_ATT1"
	RmDir /r "$INSTDIR\mods\DLC_ATT2"
	RmDir /r "$INSTDIR\mods\DLC_ATT3"
	RmDir /r "$INSTDIR\mods\DLC_ATT"
	RmDir /r "$INSTDIR\mods\DLC_Famitsu"
	RmDir /r "$INSTDIR\mods\DLC_QUO"
	RmDir /r "$INSTDIR\mods\DLC_LaunchParty"
	RmDir /r "$INSTDIR\mods\DLC_Reebok"
	RmDir /r "$INSTDIR\mods\DLC_SambaGP"
	RmDir /r "$INSTDIR\mods\DLC_Y2K"
	RmDir /r "$INSTDIR\mods\DLC_Halloween"
	RmDir /r "$INSTDIR\mods\DLC_Christmas98"
	RmDir /r "$INSTDIR\mods\DLC_Christmas99"
	CreateDirectory "$INSTDIR\mods\DLC"
	nsexec::ExecToStack '"$TEMP\7za.exe" x "$EXEDIR\instdata\DLCs.7z" -o"$INSTDIR\mods\DLC" -aoa'
	IntOp $INST_DLCS 0 + 1
	IfFileExists "$EXEDIR\instdata\dlc.ini" restoreconfig finish

restoreconfig:
	DetailPrint $(DE_R_DLCS)
	CopyFiles /SILENT "$EXEDIR\instdata\dlc.ini" "$INSTDIR\mods\DLC\config.ini"
	Delete "$EXEDIR\instdata\dlc.ini"
	goto finish

finish:
SectionEnd

SectionGroupEnd

SectionGroup $(SECTIONNAME_ENH) SECTIONGROUP_ENHANCEMENTS

Section $(SECTIONNAME_SMOOTH) SECTION_SMOOTHCAM
SectionIn 1 2
StrCpy $Modname $(MOD_SMOOTHCAM)
StrCpy $ModFilename "smooth-cam"
Call ModInstall
${If} $ModInstallSuccess == "1"
	IntOp $INST_SMOOTHCAM 0 + 1
${EndIf}
SectionEnd

Section $(SECTIONNAME_ONION) SECTION_ONIONBLUR
SectionIn 1 2
StrCpy $Modname $(MOD_ONION)
StrCpy $ModFilename "sadx-onion-blur"
Call ModInstall
${If} $ModInstallSuccess == "1"
	IntOp $INST_ONIONBLUR 0 + 1
	Call GenerateModVersion
${EndIf}
SectionEnd

Section $(SECTIONNAME_IDLE) SECTION_IDLECHATTER
SectionIn 1 2
StrCpy $Modname $(MOD_IDLE)
StrCpy $ModFilename "idle-chatter"
Call ModInstall
${If} $ModInstallSuccess == "1"
	IntOp $INST_IDLECHATTER 0 + 1
	Call GenerateModVersion
${EndIf}
SectionEnd

Section $(SECTIONNAME_PAUSE) SECTION_PAUSEHIDE
SectionIn 2
StrCpy $Modname $(MOD_PAUSE)
StrCpy $ModFilename "pause-hide"
Call ModInstall
${If} $ModInstallSuccess == "1"
	IntOp $INST_PAUSEHIDE 0 + 1
${EndIf}
SectionEnd

Section $(SECTIONNAME_SADXWTR) SECTION_SADXWTR
SectionIn 2
StrCpy $Modname $(MOD_SADXWTR)
StrCpy $ModFilename "sadx-style-water"
Call ModInstall
${If} $ModInstallSuccess == "1"
	IntOp $INST_SADXWTR 0 + 1
	Call GenerateModVersion
${EndIf}
SectionEnd

Section /o $(SECTIONNAME_STEAM) SECTION_STEAM
${If} $InstallType >= "4"
	goto finish
${EndIf}
IfFileExists "$EXEDIR\instdata\SteamAchievements.7z" extract download

download:
	DetailPrint $(D_STEAM)
	inetc::get /WEAKSECURITY /RESUME $(ERR_DOWNLOAD_RETRY) /CAPTION $(D_STEAM) /CANCELTEXT $(ERR_DOWNLOAD_CANCEL_A) /QUESTION $(ERR_DOWNLOAD_CANCEL_Q) /TRANSLATE $(INETC_DOWNLOADING) $(INETC_CONNECT) $(INETC_SECOND) $(INETC_MINUTE) $(INETC_HOUR) $(INETC_PLURAL) $(INETC_PROGRESS) $(INETC_REMAINING) "https://mm.reimuhakurei.net/sadxmods/SteamAchievements.7z" "$EXEDIR\instdata\SteamAchievements.7z" /END
	Pop $DownloadErrorCode
	StrCmp $DownloadErrorCode "OK" extract 0
	MessageBox MB_YESNO $(ERR_D_STEAM) IDYES finish IDNO 0
	Quit

extract:
	RmDir /r "$INSTDIR\mods\SteamAchievements"
	DetailPrint $(DE_E_STEAM)
	nsexec::ExecToStack '"$TEMP\7za.exe" x "$EXEDIR\instdata\SteamAchievements.7z" -o"$INSTDIR" -aoa'
	IntOp $INST_STEAM 0 + 1

finish:
SectionEnd

Section $(SECTIONNAME_SUPER) SECTION_SUPERSONIC
SectionIn 1 2
StrCpy $Modname $(MOD_SUPER)
StrCpy $ModFilename "sadx-super-sonic"
Call ModInstall
${If} $ModInstallSuccess == "1"
	IntOp $INST_SUPERSONIC 0 + 1
	Call GenerateModVersion
${EndIf}
SectionEnd

Section $(SECTIONNAME_TIME) SECTION_TIMEOFDAY
SectionIn 1 2
StrCpy $Modname $(MOD_TIME)
StrCpy $ModFilename "TrainDaytime"
Call ModInstall
${If} $ModInstallSuccess == "1"
	RmDir /r "$INSTDIR\mods\OptionalMods\TrainDaytime"
	RmDir /r "$INSTDIR\mods\DC Conversion\OptionalMods\TrainDaytime"
	IntOp $INST_TIMEOFDAY 0 + 1
	Call GenerateModVersion
${EndIf}
SectionEnd

SectionGroupEnd

Section /o $(SECTIONNAME_ADX) SECTION_ADX
IfFileExists "$INSTDIR\mods\ADXAudio\system\sounddata\voice_us\wma\0000.adx" finishadxaudio 0
IfFileExists "$INSTDIR\mods\ADX Music\system\sounddata\bgm\wma\advamy.adx" 0 getadxaudio
IfFileExists "$INSTDIR\mods\ADX Voices\system\sounddata\voice_us\wma\0000.adx" oldadxaudio getadxaudio

getadxaudio:
	StrCpy $Modname $(MOD_ADX)
	StrCpy $ModFilename "ADXAudio"
	Call ModInstall
	${If} $ModInstallSuccess == "1"
		goto finishadxaudio
	${Else}
	goto endadxaudio
	${EndIf}

finishadxaudio:
	RmDir /r "$INSTDIR\mods\adxmusic"
	RmDir /r "$INSTDIR\mods\ADX Music"
	RmDir /r "$INSTDIR\mods\ADX Voices"
	IntOp $INST_ADX 0 + 1
	goto endadxaudio

oldadxaudio:
	IntOp $INST_ADX 0 + 0
	goto endadxaudio

endadxaudio:
SectionEnd

Section $(SECTIONNAME_SND) SECTION_SNDOVERHAUL
SectionIn 1 2
StrCpy $Modname $(MOD_SND)
StrCpy $ModFilename "SoundOverhaul"
Call ModInstall
${If} $ModInstallSuccess == "1"
		IntOp $INST_SNDOVERHAUL 0 + 1
${EndIf}
SectionEnd

Section $(SECTIONNAME_HDGUI) SECTION_HDGUI
SectionIn 1 2
StrCpy $Modname $(MOD_HDGUI)
StrCpy $ModFilename "HD_DCStyle"
Call ModInstall
${If} $ModInstallSuccess == "1"
	IntOp $INST_HDGUI 0 + 1
	RmDir /r "$INSTDIR\mods\xinput-prompts"
${EndIf}
SectionEnd

Section -ModOrder
SectionIn 1 2 3
DetailPrint $(DE_MODORDER)

; Create files
Call WriteSettings

; Copy files
CreateDirectory "$ModLoaderPath\profiles"
IfFileExists "$ModLoaderPath\profiles\Default.json" 0 +4
Delete "$ModLoaderPath\profiles\Default.json.bak"
DetailPrint $(DE_B_MLINI)
Rename "$ModLoaderPath\profiles\Default.json" "$ModLoaderPath\profiles\Default.json.bak"
DetailPrint $(DE_I_MLINI)
CopyFiles /SILENT "$TEMP\Default.json" "$ModLoaderPath\profiles\Default.json"

IfFileExists "$ModLoaderPath\profiles\Profiles.json" 0 +3
Delete "$ModLoaderPath\profiles\Profiles.json.bak"
Rename "$ModLoaderPath\profiles\Profiles.json" "$ModLoaderPath\profiles\Profiles.json.bak"
CopyFiles /SILENT "$TEMP\Profiles.json" "$ModLoaderPath\profiles\Profiles.json"

IfFileExists "$ManagerAppDataPath\Manager.json" 0 +3
Delete "$ManagerAppDataPath\Manager.json.bak"
Rename "$ManagerAppDataPath\Manager.json" "$ManagerAppDataPath\Manager.json.bak"
CopyFiles /SILENT "$TEMP\Manager.json" "$ManagerAppDataPath\Manager.json"

IfFileExists "$INSTDIR\sonicDX.ini" 0 +4
Delete "$INSTDIR\sonicDX.bak"
DetailPrint $(DE_B_SADXINI)
Rename "$INSTDIR\sonicDX.ini" "$INSTDIR\sonicDX.bak"
DetailPrint $(DE_I_SADXINI)
CopyFiles /SILENT "$TEMP\sonicDX.ini" "$INSTDIR\sonicDX.ini"

Delete "$TEMP\Profiles.json"
Delete "$TEMP\Default.json"
Delete "$TEMP\Manager.json"
Delete "$TEMP\sonicDX.ini"
SectionEnd

Section -SECTION_CUSTOMICON
${If} $InstallType == "5"
	goto finish
${EndIf}
md5dll::GetMD5File "$INSTDIR\sonic.exe"
Pop $sonicexemd5

${If} $EnableCustomIcon == "0"
	StrCpy $CustomIconNumber "0"
${EndIf}

IfFileExists "$EXEDIR\instdata\icondata.7z" extract_icon download_icon
	
download_icon:
	DetailPrint $(D_RESOURCES)
	inetc::get /WEAKSECURITY /RESUME $(ERR_DOWNLOAD_RETRY) /CAPTION $(D_RESOURCES) /CANCELTEXT $(ERR_DOWNLOAD_CANCEL_A) /QUESTION $(ERR_DOWNLOAD_CANCEL_Q) /TRANSLATE $(INETC_DOWNLOADING) $(INETC_CONNECT) $(INETC_SECOND) $(INETC_MINUTE) $(INETC_HOUR) $(INETC_PLURAL) $(INETC_PROGRESS) $(INETC_REMAINING) "https://dcmods.unreliable.network/owncloud/data/PiKeyAr/files/Setup/data/icondata.7z" "$EXEDIR\instdata\icondata.7z" /END
	Pop $DownloadErrorCode
	StrCmp $DownloadErrorCode "OK" extract_icon 0
	MessageBox MB_OK $(ERR_D_RESOURCES)
	goto finish

extract_icon:
	Delete "$INSTDIR\sonic.exe.manifest"
	Delete "$INSTDIR\addmanifest.bat"
	Delete "$INSTDIR\replaceicon*.bat"
	Delete "$INSTDIR\rcedit.exe"
	Delete "$INSTDIR\sa*.ico"
	Delete "$INSTDIR\sonic.ico"
	DetailPrint $(DE_E_RESOURCES)
	nsexec::ExecToStack '"$TEMP\7za.exe" x "$EXEDIR\instdata\icondata.7z" -o"$INSTDIR" -aoa'
	DetailPrint $(DE_I_RESOURCES)
	SetOutPath "$INSTDIR"
	CopyFiles "$INSTDIR\sonic.exe" "$INSTDIR\sonic_original.exe"
	
	${If} $CustomIconNumber == "1"
		CopyFiles "$INSTDIR\sa1.ico" "$INSTDIR\sonic.ico"
	${EndIf}
	
	${If} $CustomIconNumber == "2"
		CopyFiles "$INSTDIR\sa1alt.ico" "$INSTDIR\sonic.ico"
	${EndIf}
	
	${If} $CustomIconNumber == "3"
		CopyFiles "$INSTDIR\sadxicon.ico" "$INSTDIR\sonic.ico"
	${EndIf}
	
	${If} $CustomIconNumber == "4"
		CopyFiles "$INSTDIR\sadxhd.ico" "$INSTDIR\sonic.ico"
	${EndIf}
	
	goto finish

finish:
SectionEnd

Section -Cleanup
SectionIn 1 2 3 4
Delete "$INSTDIR\SaveTransfer.exe"
Delete "$INSTDIR\replaceicon*.bat"
Delete "$INSTDIR\sonic.exe.manifest"
Delete "$INSTDIR\addmanifest.bat"
Delete "$INSTDIR\rcedit.exe"
Delete "$INSTDIR\SteamHelper.*"
Delete "$INSTDIR\SDL2.dll"
Delete "$INSTDIR\input_config.xml"
Delete "$INSTDIR\keycap_config.xml"
Delete "$INSTDIR\system_config.xml"
Delete "$INSTDIR\jsutil.dll"
Delete "$INSTDIR\VGAudio.*"
Delete "$INSTDIR\sfk.exe"
Delete "$INSTDIR\ffmpeg.exe"
Delete "$INSTDIR\hpatchz.exe"
Delete "$INSTDIR\patch*.dat"
Delete "$INSTDIR\lame.exe"
Delete "$INSTDIR\sfd2mpg.exe"
Delete "$INSTDIR\sa*.ico"
Delete "$INSTDIR\*.7z"
Delete "$INSTDIR\mods\DC Conversion\*.7z"
Delete "$INSTDIR\mods\*.7z"
Delete "$INSTDIR\redist\*.*"
Delete "$INSTDIR\mods\Issues and differences from DC.txt"
Delete "$INSTDIR\mods\Changelog.txt"
Delete "$INSTDIR\mods\Credits.txt"
Delete "$INSTDIR\mods\Codes.dat"
Delete "$INSTDIR\mods\Codes.lst"
Delete "$INSTDIR\mods\Patches.dat"
Delete "$INSTDIR\mods\Patches.json"
Delete "$INSTDIR\mods\sadxmlver.txt"
Delete "$INSTDIR\mods\SADXModLoader.dll"
Delete "$INSTDIR\mods\Border_Default.png"
Delete "$INSTDIR\mods\SADXModLoader.7z"
Delete "$INSTDIR\mods\SADXModLoader.log"
Delete "$INSTDIR\mods\*.ini"
Delete "$INSTDIR\config.exe"
Delete "$INSTDIR\config2.exe"
Delete "$INSTDIR\BetterSADX Readme.lnk"
RMDir /r "$INSTDIR\redist"
RMDir /r "$INSTDIR\Data"
;Remove old or redundant mods
Delete "$INSTDIR\mods\mod.ini"
RmDir /r "$INSTDIR\mods\Improved Voice Clips"
RmDir /r "$INSTDIR\mods\BetterSADX"
RmDir /r "$INSTDIR\mods\BetterSADX Core"
RmDir /r "$INSTDIR\mods\BetterSADX_Version"
RmDir /r "$INSTDIR\mods\MetalSonic_Sounds"
RmDir /r "$INSTDIR\mods\Cowgirl"
RmDir /r "$INSTDIR\mods\SADX99Edition"
RmDir /r "$INSTDIR\mods\HDDC_Unscale"
RmDir /r "$INSTDIR\mods\DC_EnvMaps"
RmDir /r "$INSTDIR\mods\KillCream"
RmDir /r "$INSTDIR\mods\DisableSA1TitleScreen"
RmDir /r "$INSTDIR\mods\OptionalMods\DisableSA1TitleScreen"
RmDir /r "$INSTDIR\mods\OptionalMods\KillCream"
RmDir /r "$INSTDIR\mods\OptionalMods\Cowgirl"
RmDir /r "$INSTDIR\mods\OptionalMods\TitlescreenRipples"
RmDir /r "$INSTDIR\mods\DC Conversion\DC_EnvMaps"
RmDir /r "$INSTDIR\mods\DC Conversion\OptionalMods\Cowgirl"
RmDir /r "$INSTDIR\mods\DC Conversion\OptionalMods\TitlescreenRipples"
RmDir /r "$INSTDIR\mods\DC Conversion\OptionalMods\DisableSA1TitleScreen"
Delete "$INSTDIR\redist\*.*"
RMDir /r "$INSTDIR\redist"
Delete "$TEMP\7za.exe"
IfFileExists "$INSTDIR\mods\ADXAudio\system\sounddata\voice_us\wma\0000.adx" 0 +4
RMDir /r "$INSTDIR\mods\ADX Music"
RMDir /r "$INSTDIR\mods\ADX Voices"
RMDir /r "$INSTDIR\mods\adxmusic"
SectionEnd


Function Guide1
${If} $GuideMode == "1"
	StrCpy $ModScreenNumber "0"
	Call fnc_ScreenCompare_Show
${EndIf}
FunctionEnd

Function Guide2
${If} ${SectionIsSelected} ${SECTION_LANTERN}
	${If} $GuideMode == "1"
		StrCpy $ModScreenNumber "1"
		Call fnc_ScreenCompare_Show
	${EndIf}
${EndIf}
FunctionEnd

Function Guide3
${If} ${SectionIsSelected} ${SECTION_DCMODS}
	${If} $GuideMode == "1"
		StrCpy $ModScreenNumber "2"
		Call fnc_ScreenCompare_Show
	${EndIf}
${EndIf}
FunctionEnd

Function Guide4
${If} $GuideMode == "1"
	StrCpy $ModScreenNumber "3"
	Call fnc_ScreenCompare_Show
${EndIf}
FunctionEnd

Function Guide5
${If} $GuideMode == "1"
	StrCpy $ModScreenNumber "4"
	Call fnc_ScreenCompare_Show
${EndIf}
FunctionEnd

Function Guide6
${If} $GuideMode == "1"
	StrCpy $ModScreenNumber "5"
	Call fnc_ScreenCompare_Show
${EndIf}
FunctionEnd

Function ReloadModImage
${If} $GuideMode == "1"
	; Set image ID
	${NSD_GetState} $hCtl_ScreenCompare_CheckBox1 $Whatev
	${If} $Whatev == ${BST_CHECKED}
		IntOp $ModScreenOff 0 + 0
	${Else}
		IntOp $ModScreenOff 0 + 1
	${EndIf}
	; Lantern Engine
	${If} $ModScreenNumber == "0"
		${NSD_SetText} $hCtl_ScreenCompare_ModDescription $(GUIDE_INFO_LANTERN)
		${NSD_SetText} $hCtl_ScreenCompare_CheckBox1 $(GUIDE_INST_LANTERN)
		; Check install
		${NSD_GetState} $hCtl_ScreenCompare_CheckBox1 $Whatev
		${If} $Whatev == ${BST_CHECKED}
			!insertmacro SelectSection ${SECTION_LANTERN}
			!insertmacro SelectSection ${SECTION_DCMODS}
		${Else}
			!insertmacro UnselectSection ${SECTION_LANTERN}
			!insertmacro UnselectSection ${SECTION_DCMODS}
		${EndIf}
	; End
	${EndIf}

; Dreamcast Conversion
${If} $ModScreenNumber == "1"
	${NSD_SetText} $hCtl_ScreenCompare_ModDescription $(GUIDE_INFO_DCCONV)
	${NSD_SetText} $hCtl_ScreenCompare_CheckBox1 $(GUIDE_INST_DCCONV)
	; Check install
	${NSD_GetState} $hCtl_ScreenCompare_CheckBox1 $Whatev
	${If} $Whatev == ${BST_CHECKED}
		!insertmacro SelectSection ${SECTION_DCMODS}
	${Else}
		!insertmacro UnselectSection ${SECTION_DCMODS}
	${EndIf}
	; End
${EndIf}

; SADX Style Water
${If} $ModScreenNumber == "2"
	${NSD_SetText} $hCtl_ScreenCompare_ModDescription $(GUIDE_INFO_DXWTR)
	${NSD_SetText} $hCtl_ScreenCompare_CheckBox1 $(GUIDE_INST_DXWTR)
	; Check install
	${NSD_GetState} $hCtl_ScreenCompare_CheckBox1 $Whatev
	${If} $Whatev == ${BST_CHECKED}
		!insertmacro SelectSection ${SECTION_SADXWTR}
	${Else}
		!insertmacro UnselectSection ${SECTION_SADXWTR}
	${EndIf}
	; End
${EndIf}

; Dreamcast Characters
${If} $ModScreenNumber == "3"
	${NSD_SetText} $hCtl_ScreenCompare_ModDescription $(GUIDE_INFO_SA1CHARS)
	${NSD_SetText} $hCtl_ScreenCompare_CheckBox1 $(GUIDE_INST_SA1CHARS)
	; Check install
	${NSD_GetState} $hCtl_ScreenCompare_CheckBox1 $Whatev
	${If} $Whatev == ${BST_CHECKED}
		!insertmacro SelectSection ${SECTION_SA1CHARS}
	${Else}
		!insertmacro UnselectSection ${SECTION_SA1CHARS}
	${EndIf}
; End
${EndIf}

; Onion Blur
${If} $ModScreenNumber == "4"
	${NSD_SetText} $hCtl_ScreenCompare_ModDescription $(GUIDE_INFO_ONION)
	${NSD_SetText} $hCtl_ScreenCompare_CheckBox1 $(GUIDE_INST_ONION)
	; Check install
	${NSD_GetState} $hCtl_ScreenCompare_CheckBox1 $Whatev
	${If} $Whatev == ${BST_CHECKED}
		!insertmacro SelectSection ${SECTION_ONIONBLUR}
	${Else}
		!insertmacro UnselectSection ${SECTION_ONIONBLUR}
	${EndIf}
	; End
${EndIf}

; HD GUI 2
${If} $ModScreenNumber == "5"
	${NSD_SetText} $hCtl_ScreenCompare_ModDescription $(GUIDE_INFO_HDGUI)
	${NSD_SetText} $hCtl_ScreenCompare_CheckBox1 $(GUIDE_INST_HDGUI)
	; Check install
	${NSD_GetState} $hCtl_ScreenCompare_CheckBox1 $Whatev
	${If} $Whatev == ${BST_CHECKED}
		!insertmacro SelectSection ${SECTION_HDGUI}
	${Else}
		!insertmacro UnselectSection ${SECTION_HDGUI}
	${EndIf}
	; End
${EndIf}

; Set up screen
${NSD_SetStretchedImageOLE} $hCtl_ScreenCompare_Bitmap1 "$PLUGINSDIR\$ModScreenNumber_$ModScreenOff.jpg" $hCtl_ScreenCompare_Bitmap1_hImage
${NSD_FreeImage} $hCtl_ScreenCompare_Bitmap1
${EndIf}
FunctionEnd

Function ShowAdvancedOptions
${NSD_GetState} $hCtl_TypeSel_CheckBox_ShowAdvanced $Whatev
${If} $Whatev == ${BST_CHECKED}
	ShowWindow $hCtl_TypeSel_CheckBox_InstallIcon ${SW_SHOW}
	ShowWindow $hCtl_TypeSel_CheckBox_Preserve ${SW_SHOW}
	ShowWindow $hCtl_TypeSel_CheckBox_WindowTitle ${SW_SHOW}
	ShowWindow $hCtl_TypeSel_RadioButton_SettingsOptimal ${SW_SHOW}
	ShowWindow $hCtl_TypeSel_RadioButton_SettingsFailsafe ${SW_SHOW}
	ShowWindow $hCtl_TypeSel_Checkbox_ManagerClassic ${SW_SHOW}
${Else}
	ShowWindow $hCtl_TypeSel_CheckBox_InstallIcon ${SW_HIDE}
	ShowWindow $hCtl_TypeSel_CheckBox_Preserve ${SW_HIDE}
	ShowWindow $hCtl_TypeSel_CheckBox_WindowTitle ${SW_HIDE}
	ShowWindow $hCtl_TypeSel_RadioButton_SettingsOptimal ${SW_HIDE}
	ShowWindow $hCtl_TypeSel_RadioButton_SettingsFailsafe ${SW_HIDE}
	ShowWindow $hCtl_TypeSel_Checkbox_ManagerClassic ${SW_HIDE}
${EndIf}
FunctionEnd