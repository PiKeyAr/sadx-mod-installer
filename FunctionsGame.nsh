; Function that creates sonicDX.ini, Default.json and Manager.json in the temp folder

Function WriteSettings
	; Set or detect resolution and other settings
	${If} $ModLoaderSettingMode == "2"
		StrCpy $Res_HZ "640"
		StrCpy $Res_V "480"
		StrCpy $ScreenMode "0"
		StrCpy $VSync "false"
	${Else}
		System::Call 'user32::GetSystemMetrics(i 0) i .r0'
		System::Call 'user32::GetSystemMetrics(i 1) i .r1'
		StrCpy $Res_HZ $0
		StrCpy $Res_V $1
		StrCpy $ScreenMode "2"
		StrCpy $VSync "true"
	${EndIf}
	
	; Create sonicDX.ini
	FileOpen $7 "$TEMP\sonicDX.ini" w
	FileWrite $7 '[sonicDX]$\r$\n'
	FileWrite $7 'framerate=1$\r$\n'
	FileWrite $7 'fogemulation=0$\r$\n'
	FileWrite $7 'sound3d=1$\r$\n'
	FileWrite $7 'screensize=0$\r$\n'
	FileWrite $7 'cliplevel=0$\r$\n'
	FileWrite $7 'sevoice=1$\r$\n'
	FileWrite $7 'bgm=1$\r$\n'
	${If} $ScreenMode == 0
		FileWrite $7 'screen=0$\r$\n'
	${Else}
		FileWrite $7 'screen=1$\r$\n'
	${EndIf}
	FileWrite $7 'mousemode=0$\r$\n'
	FileWrite $7 'bgmv=100$\r$\n'
	FileWrite $7 'voicev=100$\r$\n'
	FileClose $7

	; Create Manager.json
	FileOpen $7 "$TEMP\Manager.json" w
	FileWrite $7 '{$\r$\n'
	FileWrite $7 '  "SettingsVersion": 3,$\r$\n'
	FileWrite $7 '  "CurrentSetGame": 1,$\r$\n'
	FileWrite $7 '  "Theme": 2,$\r$\n'

	; Write language settings
	; English
	${If} $LANGUAGE == 1033
		FileWrite $7 '  "Language": 0,$\r$\n'
	${EndIf}

	; French
	${If} $LANGUAGE == 1036
		FileWrite $7 '  "Language": 1,$\r$\n'
	${EndIf}

	; Spanish
	${If} $LANGUAGE == 1034
		FileWrite $7 '  "Language": 2,$\r$\n'
	${EndIf}

	; Portuguese BR
	${If} $LANGUAGE == 1046
		FileWrite $7 '  "Language": 6,$\r$\n'
	${EndIf}

	; Russian
	${If} $LANGUAGE == 1049
		FileWrite $7 '  "Language": 7,$\r$\n'
	${EndIf}

	; Italian
	${If} $LANGUAGE == 1040
		FileWrite $7 '  "Language": 8,$\r$\n'
	${EndIf}

	; Korean
	${If} $LANGUAGE == 1042
		FileWrite $7 '  "Language": 9,$\r$\n'
	${EndIf}
	
	FileWrite $7 '  "ModAuthor": "Unknown",$\r$\n'
	FileWrite $7 '  "EnableDeveloperMode": false,$\r$\n'
	FileWrite $7 '  "KeepManagerOpen": true,$\r$\n'
	FileWrite $7 '  "UpdateSettings": {$\r$\n'
	FileWrite $7 '    "EnableManagerBootCheck": true,$\r$\n'
	FileWrite $7 '    "EnableModsBootCheck": true,$\r$\n'
	FileWrite $7 '    "EnableLoaderBootCheck": true,$\r$\n'
	FileWrite $7 '    "UpdateTimeOutCD": 0,$\r$\n'
	FileWrite $7 '    "UpdateCheckCount": 1$\r$\n'
	FileWrite $7 '  },$\r$\n'
	FileWrite $7 '  "GameEntries": [$\r$\n'
	FileWrite $7 '  {$\r$\n'
	FileWrite $7 '    "Name": "Sonic Adventure DX",$\r$\n'
	; Escape paths because JSON sucks
	${StrRep} $8 "$INSTDIR" "\" "\\"
	FileWrite $7 '    "Directory": "$8",$\r$\n'
	FileWrite $7 '    "Executable": "sonic.exe",$\r$\n'
	FileWrite $7 '    "Type": 1$\r$\n'
	FileWrite $7 '  }$\r$\n'
	FileWrite $7 '  ],$\r$\n'	
	FileWrite $7 '  "KeepModOrder": false$\r$\n'
	FileWrite $7 '}$\r$\n'
	FileClose $7

	; Create Profiles.json
	FileOpen $7 "$TEMP\Profiles.json" w
	FileWrite $7 '{$\r$\n'
	FileWrite $7 '  "ProfileIndex": 0,$\r$\n'
	FileWrite $7 '  "ProfilesList": [$\r$\n'
	FileWrite $7 '    {$\r$\n'
	FileWrite $7 '      "Name": "Default",$\r$\n'
	FileWrite $7 '      "Filename": "Default.json"$\r$\n'
	FileWrite $7 '    }$\r$\n'
	FileWrite $7 '  ]$\r$\n'
	FileWrite $7 '}$\r$\n'
	FileClose $7

	; Create Default.json
	FileOpen $7 "$TEMP\Default.json" w
	FileWrite $7 '{$\r$\n'
	FileWrite $7 '  "SettingsVersion": 3,$\r$\n'
	FileWrite $7 '  "Graphics": {$\r$\n'
	FileWrite $7 '    "SelectedScreen": 0,$\r$\n'
	FileWrite $7 '    "HorizontalResolution": $Res_HZ,$\r$\n'
	FileWrite $7 '    "VerticalResolution": $Res_V,$\r$\n'
	FileWrite $7 '    "Enable43ResolutionRatio": false,$\r$\n'
	FileWrite $7 '    "EnableVsync": $VSync,$\r$\n'
	FileWrite $7 '    "EnablePauseOnInactive": true,$\r$\n'
	FileWrite $7 '    "CustomWindowWidth": $Res_HZ,$\r$\n'
	FileWrite $7 '    "CustomWindowHeight": $Res_V,$\r$\n'
	FileWrite $7 '    "EnableKeepResolutionRatio": false,$\r$\n'
	FileWrite $7 '    "EnableResizableWindow": false,$\r$\n'
	FileWrite $7 '    "FillModeBackground": 2,$\r$\n'
	FileWrite $7 '    "FillModeFMV": 1,$\r$\n'
	FileWrite $7 '    "ModeTextureFiltering": 0,$\r$\n'
	FileWrite $7 '    "ModeUIFiltering": 0,$\r$\n'
	FileWrite $7 '    "EnableUIScaling": true,$\r$\n'
	FileWrite $7 '    "EnableForcedMipmapping": true,$\r$\n'
	FileWrite $7 '    "EnableForcedTextureFilter": true,$\r$\n'
	FileWrite $7 '    "ScreenMode": $ScreenMode,$\r$\n'
	FileWrite $7 '    "ShowMouseInFullscreen": false,$\r$\n'
	FileWrite $7 '    "DisableBorderImage": false,$\r$\n'
	FileWrite $7 '    "Anisotropic": 16,$\r$\n'
	FileWrite $7 '    "RenderBackend": 1$\r$\n'
	FileWrite $7 '  },$\r$\n'
	FileWrite $7 '  "Controller": {$\r$\n'
	FileWrite $7 '    "EnabledInputMod": true$\r$\n'
	FileWrite $7 '  },$\r$\n'
	FileWrite $7 '  "Sound": {$\r$\n'
	FileWrite $7 '    "EnableGameMusic": true,$\r$\n'
	FileWrite $7 '    "EnableGameSound": true,$\r$\n'
	FileWrite $7 '    "EnableGameSound3D": true,$\r$\n'
	FileWrite $7 '    "EnableBassMusic": true,$\r$\n'
	FileWrite $7 '    "EnableBassSFX": true,$\r$\n'
	FileWrite $7 '    "GameMusicVolume": 100,$\r$\n'
	FileWrite $7 '    "GameSoundVolume": 100,$\r$\n'
	FileWrite $7 '    "SEVolume": 100$\r$\n'
	FileWrite $7 '  },$\r$\n'
	FileWrite $7 '  "TestSpawn": {$\r$\n'
	FileWrite $7 '    "UseCharacter": false,$\r$\n'
	FileWrite $7 '    "UseLevel": true,$\r$\n'
	FileWrite $7 '    "UseEvent": false,$\r$\n'
	FileWrite $7 '    "UseGameMode": false,$\r$\n'
	FileWrite $7 '    "UseSave": false,$\r$\n'
	FileWrite $7 '    "LevelIndex": 1,$\r$\n'
	FileWrite $7 '    "ActIndex": 0,$\r$\n'
	FileWrite $7 '    "CharacterIndex": 0,$\r$\n'
	FileWrite $7 '    "EventIndex": -1,$\r$\n'
	FileWrite $7 '    "GameModeIndex": 4,$\r$\n'
	FileWrite $7 '    "SaveIndex": -1,$\r$\n'

	; Write language settings

	; Japanese
	${If} $LANGUAGE == 1041
		FileWrite $7 '    "GameTextLanguage": 0,$\r$\n'
		FileWrite $7 '    "GameVoiceLanguage": 0,$\r$\n'
	${EndIf}

	; English
	${If} $LANGUAGE == 1033
		FileWrite $7 '    "GameTextLanguage": 1,$\r$\n'
		FileWrite $7 '    "GameVoiceLanguage": 1,$\r$\n'
	${EndIf}

	; French
	${If} $LANGUAGE == 1036
		FileWrite $7 '    "GameTextLanguage": 2,$\r$\n'
		FileWrite $7 '    "GameVoiceLanguage": 1,$\r$\n'
	${EndIf}

	; Spanish
	${If} $LANGUAGE == 1034
		FileWrite $7 '    "GameTextLanguage": 3,$\r$\n'
		FileWrite $7 '    "GameVoiceLanguage": 1,$\r$\n'
	${EndIf}

	; German
	${If} $LANGUAGE == 1031
		FileWrite $7 '    "GameTextLanguage": 4,$\r$\n'
		FileWrite $7 '    "GameVoiceLanguage": 1,$\r$\n'
	${EndIf}

	FileWrite $7 '    "UseManual": false,$\r$\n'
	FileWrite $7 '    "UsePosition": false,$\r$\n'
	FileWrite $7 '    "XPosition": 0,$\r$\n'
	FileWrite $7 '    "YPosition": 0,$\r$\n'
	FileWrite $7 '    "ZPosition": 0,$\r$\n'
	FileWrite $7 '    "Rotation": 0$\r$\n'
	FileWrite $7 '  },$\r$\n'
	FileWrite $7 '  "DebugSettings": {$\r$\n'
	FileWrite $7 '    "EnableDebugConsole": false,$\r$\n'
	FileWrite $7 '    "EnableDebugScreen": false,$\r$\n'
	FileWrite $7 '    "EnableDebugFile": false,$\r$\n'
	FileWrite $7 '    "EnableDebugCrashLog": true,$\r$\n'
	FileWrite $7 '    "EnableShowConsole": null$\r$\n'
	FileWrite $7 '  },$\r$\n'
	
	; Installed mods
	IntOp $ModIndex 0 + 0

	FileWrite $7 '  "EnabledMods": [$\r$\n'
	; ADX Audio
	IfFileExists "$INSTDIR\mods\ADXAudio\system\sounddata\bgm\wma\ADVAMY.ADX" 0 +3
	FileWrite $7 '    "ADXAudio"'
	IntOp $ModIndex $ModIndex + 1

	${If} $INST_SADXFE == "1"
		${If} $ModIndex > "0"
			FileWrite $7 ',$\r$\n'
		${EndIf}
		IntOp $ModIndex $ModIndex + 1
		FileWrite $7 '    "SADXFE"'
	${EndIf}

	${If} $INST_SMOOTHCAM == "1"
		${If} $ModIndex > "0"
			FileWrite $7 ',$\r$\n'
		${EndIf}
		FileWrite $7 '    "smooth-cam"'
		IntOp $ModIndex $ModIndex + 1
	${EndIf}

	${If} $INST_PAUSEHIDE == "1"
		${If} $ModIndex > "0"
			FileWrite $7 ',$\r$\n'
		${EndIf}
		FileWrite $7 '    "pause-hide"'
		IntOp $ModIndex $ModIndex + 1
	${EndIf}

	${If} $INST_FRAMELIMIT == "1"
		${If} $ModIndex > "0"
			FileWrite $7 ',$\r$\n'
		${EndIf}
		FileWrite $7 '    "sadx-frame-limit"'
		IntOp $ModIndex $ModIndex + 1
	${EndIf}

	${If} $INST_ONIONBLUR == "1"
		${If} $ModIndex > "0"
			FileWrite $7 ',$\r$\n'
		${EndIf}
		FileWrite $7 '    "sadx-onion-blur"'
		IntOp $ModIndex $ModIndex + 1
	${EndIf}

	${If} $INST_IDLECHATTER == "1"
		${If} $ModIndex > "0"
			FileWrite $7 ',$\r$\n'
		${EndIf}
		FileWrite $7 '    "idle-chatter"'
		IntOp $ModIndex $ModIndex + 1
	${EndIf}

	${If} $INST_STEAM == "1"
		${If} $ModIndex > "0"
			FileWrite $7 ',$\r$\n'
		${EndIf}
		FileWrite $7 '    "SteamAchievements"'
		IntOp $ModIndex $ModIndex + 1
	${EndIf}

	${If} $INST_LANTERN == "1"
		${If} $ModIndex > "0"
			FileWrite $7 ',$\r$\n'
		${EndIf}
		FileWrite $7 '    "sadx-dc-lighting"'
		IntOp $ModIndex $ModIndex + 1
	${EndIf}

	${If} $INST_DCMODS == "1"
		${If} $ModIndex > "0"
			FileWrite $7 ',$\r$\n'
		${EndIf}
		FileWrite $7 '    "DreamcastConversion"'
		IntOp $ModIndex $ModIndex + 1
	${EndIf}

	${If} $INST_DLCs == "1"
		${If} $ModIndex > "0"
			FileWrite $7 ',$\r$\n'
		${EndIf}
		FileWrite $7 '    "DLC"'
		IntOp $ModIndex $ModIndex + 1
	${EndIf}

	${If} $INST_SADXWTR == "1"
		${If} $ModIndex > "0"
			FileWrite $7 ',$\r$\n'
		${EndIf}
		FileWrite $7 '    "sadx-style-water"'
		IntOp $ModIndex $ModIndex + 1
	${EndIf}

	${If} $INST_SNDOVERHAUL == "1"
		${If} $ModIndex > "0"
			FileWrite $7 ',$\r$\n'
		${EndIf}
		FileWrite $7 '    "SoundOverhaul"'
		IntOp $ModIndex $ModIndex + 1
	${EndIf}

	${If} $INST_TIMEOFDAY == "1"
		${If} $ModIndex > "0"
			FileWrite $7 ',$\r$\n'
		${EndIf}
		FileWrite $7 '    "TrainDaytime"'
		IntOp $ModIndex $ModIndex + 1
	${EndIf}

	${If} $INST_SA1CHARS == "1"
		${If} $ModIndex > "0"
			FileWrite $7 ',$\r$\n'
		${EndIf}
		FileWrite $7 '    "SA1_Chars"'
		IntOp $ModIndex $ModIndex + 1
	${EndIf}
	
	${If} $INST_SUPERSONIC == "1"
		${If} $ModIndex > "0"
			FileWrite $7 ',$\r$\n'
		${EndIf}
		FileWrite $7 '    "sadx-super-sonic"'
		IntOp $ModIndex $ModIndex + 1
	${EndIf}	
	
	${If} $INST_HDGUI == "1"
		${If} $ModIndex > "0"
			FileWrite $7 ',$\r$\n'
		${EndIf}
		FileWrite $7 '    "HD_DCStyle"'
		IntOp $ModIndex $ModIndex + 1
	${EndIf}
	FileWrite $7 '  ],$\r$\n'

	; Patches
	FileWrite $7 '"EnabledGamePatches": [$\r$\n'
	FileWrite $7 '    "HRTFSound",$\r$\n'
	FileWrite $7 '    "KeepCamSettings",$\r$\n'
	FileWrite $7 '    "FixVertexColorRendering",$\r$\n'
	FileWrite $7 '    "MaterialColorFix",$\r$\n'
	FileWrite $7 '    "NodeLimit",$\r$\n'
	FileWrite $7 '    "FOVFix",$\r$\n'
	FileWrite $7 '    "SkyChaseResolutionFix",$\r$\n'
	FileWrite $7 '    "Chaos2CrashFix",$\r$\n'
	FileWrite $7 '    "ChunkSpecularFix",$\r$\n'
	FileWrite $7 '    "E102NGonFix",$\r$\n'
	FileWrite $7 '    "ChaoPanelFix",$\r$\n'
	FileWrite $7 '    "PixelOffSetFix",$\r$\n'
	FileWrite $7 '    "LightFix",$\r$\n'
	FileWrite $7 '    "KillGBIX",$\r$\n'
	FileWrite $7 '    "DisableCDCheck",$\r$\n'
	FileWrite $7 '    "ExtendedSaveSupport",$\r$\n'
	FileWrite $7 '    "CrashGuard",$\r$\n'
	FileWrite $7 '    "XInputFix"$\r$\n'
	FileWrite $7 '  ],$\r$\n'
	
	; Codes
	FileWrite $7 '  "EnabledCodes": [$\r$\n'
	FileWrite $7 '    "Can Always Skip Credits",$\r$\n'
	FileWrite $7 '    "Egg Carrier Ocean Music",$\r$\n'
	${If} $INST_SMOOTHCAM == "0"
		FileWrite $7 '    "Use Tornado 2 Health Bar in Sky Chase Act 2",$\r$\n'
		FileWrite $7 '    "Invert Right Stick Y Axis in First Person"$\r$\n'
	${Else}
		FileWrite $7 '    "Use Tornado 2 Health Bar in Sky Chase Act 2"$\r$\n'
	${EndIf}
	FileWrite $7 '  ]$\r$\n'
	FileWrite $7 '}$\r$\n'
	FileClose $7

FunctionEnd