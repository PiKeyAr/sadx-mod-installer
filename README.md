# SADX Mod Installer

This repository contains source code and assets for the [SADX Mod Installer](https://sadxmodinstaller.unreliable.network/), an easy to use tool to install mods on Sonic Adventure DX (PC).

This repository is meant for development purposes. If you just want to use the installer, you can download it from [its homepage](https://sadxmodinstaller.unreliable.network/index.php/download-sadx-mod-installer/).

If you would like to contribute to the installer or use its code in your own projects, please [read this](https://github.com/PiKeyAr/sadx-mod-installer/blob/master/CONTRIB.md).

## Build requirements
[NSIS](https://nsis.sourceforge.io/Download) 3.10 or later with the following plugins (See the [`nsis`](https://github.com/PiKeyAr/sadx-mod-installer/tree/master/nsis) folder in source):

- [NSProcess](https://nsis.sourceforge.io/NsProcess_plugin)

- [NSDialogs_SetImageOLE script](https://nsis.sourceforge.io/NsDialogs_SetImageOLE)

- [Locate plugin](https://nsis.sourceforge.io/Locate_plugin)

- [AccessControl plugin](https://nsis.sourceforge.io/AccessControl_plug-in)

- [Inetc plugin](https://nsis.sourceforge.io/Inetc_plug-in)

- [MD5 plugin](https://nsis.sourceforge.io/MD5_plugin)

- [UserMgr plugin](https://nsis.sourceforge.io/UserMgr_plug-in)