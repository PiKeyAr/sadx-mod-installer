﻿using System;
using System.IO;
using System.Collections.Generic;
using System.Drawing;
using System.Windows.Forms;
using System.Diagnostics;
using System.Linq;
using System.Threading;
using static SDL2.SDL;
using System.Threading.Tasks;

// Controller image from https://www.vecteezy.com/vector-art/29889822-joystick-icon-vector
namespace SteamHelper
{
    public partial class Form_SteamHelper : Form
    {
        private bool EnableAdvanced = false;
        private int CursorVertical = 0;
        private int CursorHorizontal = 0;
        private int CursorHideTimer = 30;
        private Control HighlightedControl;
        private Size WindowSize;
        private float ControlScale;
        private bool CursorHidden;
        List<Tuple<Control, int, int, bool>> TabControls;
        Dictionary<int, int> Grid;
        public Form_SteamHelper()
        {
            InitializeComponent();
            // Set common button parameters
            foreach (Control control in this.Controls)
            {
                if (control is Button button)
                    SetButtonParameters(button);
            }
            // Set top panel color
            panelTopMenu.BackColor = Color.FromArgb(30, 30, 30);
            // Add tabbable controls
            TabControls = new List<Tuple<Control, int, int, bool>> // Control, X index, Y index, toggle or not
            {
                new Tuple<Control, int, int, bool>(buttonInput, 0, 0, true),
                //new Tuple<Control, int, int, bool>(buttonDisplay, 1, 0, true),
                //new Tuple<Control, int, int, bool>(buttonSound, 2, 0, true),
                new Tuple<Control, int, int, bool>(buttonPlay, 1, 0, false),
                new Tuple<Control, int, int, bool>(buttonMods, 2, 0, false),
                new Tuple<Control, int, int, bool>(buttonExit, 3, 0, false),
                new Tuple<Control, int, int, bool>(buttonSwitchDeviceLeft, 0, 1, false),
                new Tuple<Control, int, int, bool>(buttonSwitchDevice, 1, 1, false),
                new Tuple<Control, int, int, bool>(buttonSwitchDeviceRight, 2, 1, false),                
                new Tuple<Control, int, int, bool>(buttonSwitchPlayer, -1, 2, false),
                new Tuple<Control, int, int, bool>(buttonSetControls, -1, 3, false),
                new Tuple<Control, int, int, bool>(buttonReset, -1, 4, false),
                new Tuple<Control, int, int, bool>(buttonAdvanced, -1, 5, false),
                new Tuple<Control, int, int, bool>(buttonDeadzoneLeft, -1, 6, false),
                new Tuple<Control, int, int, bool>(buttonDeadzoneRight, -1, 7, false),
                new Tuple<Control, int, int, bool>(buttonRadialLeft, -1, 8, false),
                new Tuple<Control, int, int, bool>(buttonRadialRight, -1, 9, false),
                new Tuple<Control, int, int, bool>(buttonRumble, -1, 10, false),
            };
            // Set grid
            Grid = new Dictionary<int, int>();
            for (int i = 0; i < TabControls.Max(x => x.Item3) + 1; i++)
            {
                int maxX = 0;
                foreach (var item in TabControls)
                {
                    if (item.Item3 == i && item.Item2 > maxX)
                        maxX = item.Item2;
                }
                Grid.Add(i, maxX);
            }
            // Select the Input button
            ButtonClicked(buttonInput, null);
            // Hide advanced options
            RefreshAdvanced();
        }

        private bool GetGridItem(int x, int y)
        {
            foreach (var item in TabControls)
            {
                if ((item.Item2 == x || item.Item2 == -1) && item.Item3 == y && item.Item1.Enabled) return true;
            }
            return false;
        }

        private void ScaleFonts(List<Control> buttons)
        {
            foreach (Control button in buttons)
            {
                button.Font = new Font(button.Font.FontFamily, button.Font.Size * ControlScale);
            }
        }

        private float GetDpiScaleFactor()
        {
            return (float)(Graphics.FromImage(new Bitmap(1, 1)).DpiX / 96.0f);
        }

        private float GetScreenScale()
        { 
            float vscale = (float)WindowSize.Height / 480.0f / GetDpiScaleFactor();
            float hscale = (float)WindowSize.Width / 640.0f / GetDpiScaleFactor();
            return Math.Max(vscale, hscale);
        }

        private int GetLongestWidth(List<Control> controls)
        {
            int result = 0;
            foreach (Control control in controls)
            {
                if (control.Width > result)
                    result = control.Width;
            }
            return result;
        }

        private int GetLongestHeight(List<Control> controls)
        {
            int result = 0;
            foreach (Control control in controls)
            {
                if (control.Height > result)
                    result = control.Height;
            }
            return result;
        }

        public void SetUpScaling()
        {
            WindowSize = new Size(Screen.FromControl(this).Bounds.Width, Screen.FromControl(this).Bounds.Height);
            Size = WindowSize;
            ControlScale = GetScreenScale();
            List<Control> leftMenuItems = new List<Control>()
            {
                buttonSwitchDevice, buttonSwitchDeviceLeft, buttonSwitchDeviceRight, buttonSwitchPlayer, buttonSetControls, buttonReset, buttonAdvanced,
                buttonDeadzoneLeft, buttonDeadzoneRight, buttonRadialLeft, buttonRadialRight, buttonRumble
            };
            List<Control> controls = new List<Control>()
            {
                // Top buttons
                buttonInput, buttonDisplay, buttonSound, buttonPlay, buttonMods, buttonExit,
                // Labels top and bottom
                labelCurrentDevice, labelShowHint, 
                // Controller
                pictureBoxController,
                // Buttons side
                buttonSwitchDevice, buttonSwitchDeviceLeft, buttonSwitchDeviceRight, buttonSwitchPlayer, buttonSetControls, buttonReset, buttonAdvanced,
                // Buttons side bottom
                buttonDeadzoneLeft, buttonDeadzoneRight, buttonRadialLeft, buttonRadialRight, buttonRumble,
                // Debug
                labelCursor
            };
            // Set button size
            ScaleFonts(controls);
            // Top panel size
            panelTopMenu.Height = buttonInput.Height;
            panelTopMenu.Width = ClientRectangle.Width;
            panelTopMenu.Location = new Point(ClientRectangle.Left, ClientRectangle.Top);
            // Location of top buttons: from left
            buttonInput.Location = new Point(ClientRectangle.Left, ClientRectangle.Top);
            buttonDisplay.Location = new Point(buttonInput.Location.X + buttonInput.Width + (int)(8 * ControlScale), ClientRectangle.Top);
            buttonSound.Location = new Point(buttonDisplay.Location.X + buttonDisplay.Width + (int)(8 * ControlScale), ClientRectangle.Top);
            // Location of top buttons: from right
            buttonExit.Location = new Point(ClientRectangle.Right - buttonExit.Width, ClientRectangle.Top);
            buttonMods.Location = new Point(buttonExit.Location.X - buttonMods.Width - (int)(8 * ControlScale), ClientRectangle.Top);
            buttonPlay.Location = new Point(buttonMods.Location.X - buttonPlay.Width, ClientRectangle.Top);
            // Left menu panel
            panelLeftMenu.Size = new Size(GetLongestWidth(leftMenuItems) + (int)(32 * ControlScale), ClientRectangle.Bottom - panelTopMenu.Bottom);
            panelLeftMenu.Location = new Point(ClientRectangle.Left, panelTopMenu.Bottom);
            // Left menu items
            buttonSwitchDevice.Location = new Point(panelLeftMenu.Right / 2 - buttonSwitchDevice.Width / 2, panelLeftMenu.Top + (int)(16 * ControlScale));
            buttonSwitchDeviceLeft.Location = new Point(buttonSwitchDevice.Left - buttonSwitchDeviceLeft.Width - (int)(16 * ControlScale), buttonSwitchDevice.Top + (buttonSwitchDevice.Height - buttonSwitchDeviceLeft.Height) / 2);
            buttonSwitchDeviceRight.Location = new Point(buttonSwitchDevice.Right + (int)(16 * ControlScale), buttonSwitchDevice.Top + (buttonSwitchDevice.Height - buttonSwitchDeviceRight.Height) / 2);
            buttonSwitchPlayer.Location = new Point(panelLeftMenu.Right / 2 - buttonSwitchPlayer.Width / 2, buttonSwitchDevice.Bottom + (int)(16 * ControlScale));
            buttonSetControls.Location = new Point(panelLeftMenu.Right / 2 - buttonSetControls.Width / 2, buttonSwitchPlayer.Bottom + (int)(16 * ControlScale));
            buttonReset.Location = new Point(panelLeftMenu.Right / 2 - buttonReset.Width / 2, buttonSetControls.Bottom + (int)(16 * ControlScale));
            buttonAdvanced.Location = new Point(panelLeftMenu.Right / 2 - buttonAdvanced.Width / 2, buttonReset.Bottom + (int)(16 * ControlScale));
            // Advanced
            buttonDeadzoneLeft.Location = new Point(panelLeftMenu.Right / 2 - buttonDeadzoneLeft.Width / 2, buttonAdvanced.Bottom + (int)(8 * ControlScale));
            buttonDeadzoneRight.Location = new Point(panelLeftMenu.Right / 2 - buttonDeadzoneRight.Width / 2, buttonDeadzoneLeft.Bottom + (int)(8 * ControlScale));
            buttonRadialLeft.Location = new Point(panelLeftMenu.Right / 2 - buttonRadialLeft.Width / 2, buttonDeadzoneRight.Bottom + (int)(8 * ControlScale));
            buttonRadialRight.Location = new Point(panelLeftMenu.Right / 2 - buttonRadialRight.Width / 2, buttonRadialLeft.Bottom + (int)(8 * ControlScale));
            buttonRumble.Location = new Point(panelLeftMenu.Right / 2 - buttonRumble.Width / 2, buttonRadialRight.Bottom + (int)(8 * ControlScale));
            // Conroller image and title
            int controllerMaxWidth = ClientRectangle.Right - panelLeftMenu.Width - (int)(32 * ControlScale);
            int controllerMaxHeight = ClientRectangle.Bottom - panelTopMenu.Height - labelShowHint.Height - labelCurrentDevice.Height - (int)(64 * ControlScale);
            pictureBoxController.Size = new Size(controllerMaxWidth, Math.Min(controllerMaxHeight, controllerMaxWidth * pictureBoxController.Height/pictureBoxController.Width));
            int controllerCenterX = ClientRectangle.Right - (ClientRectangle.Right - panelLeftMenu.Width) / 2;
            pictureBoxController.Location = new Point(controllerCenterX - controllerMaxWidth / 2, ClientRectangle.Bottom / 2 - pictureBoxController.Height / 2 + (int)(32 * ControlScale));
            int labelcenter = pictureBoxController.Top - (pictureBoxController.Top - panelTopMenu.Bottom) / 2;
            labelCurrentDevice.Location = new Point(pictureBoxController.Right - pictureBoxController.Width / 2 - labelCurrentDevice.Width / 2, labelcenter - labelCurrentDevice.Height / 2);
            int hintcenter = ClientRectangle.Bottom - (ClientRectangle.Bottom - pictureBoxController.Bottom) / 2;
            labelShowHint.Location = new Point(controllerCenterX - labelShowHint.Width / 2, hintcenter - labelShowHint.Height / 2);
            // Debug info
            labelCursor.Location = new Point(0, panelLeftMenu.Bottom - labelCursor.Height);
            // Check for mouse movement for controller highlight
            panelTopMenu.MouseMove += Form_SteamHelper_MouseMove;
            panelLeftMenu.MouseMove += Form_SteamHelper_MouseMove;
            pictureBoxController.MouseMove += Form_SteamHelper_MouseMove;
        }

        private void SetButtonParameters(Button button)
        {
            button.FlatAppearance.BorderSize = 0;
            if (button.Enabled)
            {
                button.FlatAppearance.MouseDownBackColor = Color.FromArgb(60, 60, 60);
                button.FlatAppearance.MouseOverBackColor = Color.FromArgb(96, 96, 96);
                button.Click += ButtonClicked;
                button.MouseMove += Form_SteamHelper_MouseMove;
            }
            else
            {
                button.Visible = false;
            }
        }

        private void ButtonClicked(object sender, EventArgs e)
        {
            Button button = sender as Button;
            bool toggle = false;
            foreach (var item in TabControls)
            {
                Button btn = item.Item1 as Button;
                if (btn == button && item.Item4 == true)
                {
                    toggle = true;
                    btn.BackColor = Color.FromArgb(60, 60, 60);
                }
                if (btn != button && item.Item4 == true && toggle)
                    btn.BackColor = Color.FromArgb(30, 30, 30);
            }
        }

        private void buttonExit_Click(object sender, EventArgs e)
        {
            Close();
        }

        private void Form_SteamHelper_KeyDown(object sender, KeyEventArgs e)
        {
            if (e.KeyCode == Keys.Space)
            {
                PushButton();
            }
        }

        private void PushButton()
        {
            if (HighlightedControl is Button btn)
                btn.PerformClick();
        }

        private void MoveRight()
        {
            int cursorHorizontalMax = Grid[CursorVertical];
            int nextValue = CursorHorizontal + 1;
            if (nextValue > cursorHorizontalMax)
                nextValue = cursorHorizontalMax;
            if (!GetGridItem(nextValue, CursorVertical))
                nextValue = CursorHorizontal;
            CursorHorizontal = nextValue;
        }

        private void MoveLeft()
        {
            int cursorHorizontalMax = Grid[CursorVertical];
            int nextValue = CursorHorizontal - 1;
            if (nextValue < 0)
                nextValue = 0;
            if (!GetGridItem(nextValue, CursorVertical))
                nextValue = CursorHorizontal;
            CursorHorizontal = nextValue;
        }

        private void MoveUp()
        {
            int cursorVerticalMax = Grid.Max(x => x.Key);
            int nextValue = CursorVertical - 1;
            if (nextValue < 0)
                nextValue = 0;
            if (!GetGridItem(CursorHorizontal, nextValue))
                nextValue = CursorVertical;
            CursorVertical = nextValue;
        }

        private void MoveDown()
        {
            int cursorVerticalMax = Grid.Max(x => x.Key);
            int nextValue = CursorVertical + 1;
            if (nextValue > cursorVerticalMax)
                nextValue = cursorVerticalMax;
            if (!GetGridItem(CursorHorizontal, nextValue))
                nextValue = CursorVertical;
            CursorVertical = nextValue;
        }

        protected override bool ProcessCmdKey(ref Message msg, Keys keyData)
        {
            bool result;
            switch (keyData)
            {
                case Keys.Enter:
                    PushButton();
                    result = true;
                    break;
                case Keys.Tab:
                    result = true;
                    break;
                case Keys.Left:
                    MoveLeft();
                    HighlightControl();
                    result = true;
                    break;
                case Keys.Right:
                    MoveRight();
                    HighlightControl();
                    result = true;
                    break;
                case Keys.Up:
                    MoveUp();
                    HighlightControl();
                    result = true;
                    break;
                case Keys.Down:
                    MoveDown();
                    HighlightControl();
                    result = true;
                    break;
                default:
                    result = base.ProcessCmdKey(ref msg, keyData);
                    break;
            }
            labelCursor.Text = "X: " + CursorHorizontal.ToString() + " Y: " + CursorVertical.ToString() + " C: " + CursorHideTimer.ToString();
            return result;
        }


        private void Form_SteamHelper_MouseMove(object sender, MouseEventArgs e)
        {
            if (CursorHidden)
            {
                CursorHideTimer -= 1;
                if (CursorHideTimer <= 0)
                {
                    CursorHidden = false;
                    Cursor.Show();
                }
            }
            labelCursor.Text = "X: " + CursorHorizontal.ToString() + " Y: " + CursorVertical.ToString() + " C: " + CursorHideTimer.ToString();
        }

        private void HighlightControl()
        {
            foreach (var item in TabControls)
            {
                if ((item.Item2 == CursorHorizontal || item.Item2 == -1) && item.Item3 == CursorVertical)
                {
                    Control control = item.Item1;
                    Cursor.Position = new Point(control.Location.X + control.Size.Width / 2, control.Location.Y + control.Size.Height / 2);
                    CursorHideTimer = 10;
                    if (!CursorHidden)
                    {
                        CursorHidden = true;
                        Cursor.Hide();
                    }
                    HighlightedControl = control;
                    return;
                }
            }
           
        }

        private void buttonMods_Click(object sender, EventArgs e)
        {
            return;
            string ManagerPath = Path.Combine(System.Environment.CurrentDirectory, "SAModManager.exe");
            // Classic Manager
            if (!File.Exists(ManagerPath))
                ManagerPath = Path.Combine(System.Environment.CurrentDirectory, "SADXModManager.exe");
            // Run Manager
            if (File.Exists(ManagerPath))
            {
                ExecuteProgram(ManagerPath);
            }
            Close();
        }

        private void buttonPlay_Click(object sender, EventArgs e)
        {
            return;
            string sonicPath = Path.Combine(System.Environment.CurrentDirectory, "sonic.exe");
            ExecuteProgram(sonicPath);
            Close();
        }

        private void ExecuteProgram(string setPath)
        {
            if (File.Exists(setPath))
            {
                ProcessStartInfo info = new ProcessStartInfo { FileName = setPath, UseShellExecute = true, WindowStyle = ProcessWindowStyle.Normal };
                Process.Start(info);
            }
        }

        private void buttonDeadzoneTriggers_Click(object sender, EventArgs e)
        {

        }

        private void buttonDeadzoneRight_Click(object sender, EventArgs e)
        {

        }

        private void buttonDeadzoneLeft_Click(object sender, EventArgs e)
        {

        }

        private void RefreshAdvanced()
        {
            buttonRumble.Enabled = buttonDeadzoneLeft.Enabled = buttonDeadzoneRight.Enabled = buttonRadialLeft.Enabled = buttonRadialRight.Enabled =
            buttonRumble.Visible = buttonDeadzoneLeft.Visible = buttonDeadzoneRight.Visible = buttonRadialLeft.Visible = buttonRadialRight.Visible =
            EnableAdvanced;
            CursorVertical = Math.Min(5, CursorVertical);
        }

        private void buttonAdvanced_Click(object sender, EventArgs e)
        {
            EnableAdvanced = !EnableAdvanced;
            RefreshAdvanced();
        }

        private void InitSDL()
        {
            /*string configpath = Path.Combine(managerAppDataPath, "extlib", "SDL2", "SDLConfig.ini");
            string dbpath = Path.Combine(managerAppDataPath, "extlib", "SDL2", "gamecontrollerdb.txt");
            if (File.Exists(configpath))
                ConfigFile = IniSerializer.Deserialize<SDLConfigIni>(configpath);
            else
                ConfigFile = new SDLConfigIni();
            LoadLibrary(Path.Combine(managerAppDataPath, "extlib", "SDL2", "SDL2.dll"));*/
            int res = SDL_Init(SDL_INIT_JOYSTICK | SDL_INIT_GAMECONTROLLER);
            /*
            if (res != 0)
                throw new Exception("Error initializing SDL2 library: error code " + res.ToString());
            Controllers = new Controller[8];
            for (int i = 0; i < Controllers.Length; i++)
            {
                Controllers[i] = new Controller();
                Controllers[i].Connected = false;
                Controllers[i].ControllerID = -1;
            }
            if (File.Exists(dbpath))
                SDL_GameControllerAddMappingsFromFile(dbpath);
            Task.Factory.StartNew(SDLLoop);*/
        }
    }
}