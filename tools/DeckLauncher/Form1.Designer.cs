﻿
namespace SteamHelper
{
    partial class Form_SteamHelper
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.backgroundWorker1 = new System.ComponentModel.BackgroundWorker();
            this.buttonInput = new System.Windows.Forms.Button();
            this.buttonDisplay = new System.Windows.Forms.Button();
            this.buttonSound = new System.Windows.Forms.Button();
            this.buttonPlay = new System.Windows.Forms.Button();
            this.buttonMods = new System.Windows.Forms.Button();
            this.buttonExit = new System.Windows.Forms.Button();
            this.panelTopMenu = new System.Windows.Forms.Panel();
            this.labelCurrentDevice = new System.Windows.Forms.Label();
            this.labelShowHint = new System.Windows.Forms.Label();
            this.buttonSwitchDevice = new System.Windows.Forms.Button();
            this.buttonSwitchDeviceLeft = new System.Windows.Forms.Button();
            this.buttonSwitchDeviceRight = new System.Windows.Forms.Button();
            this.buttonSwitchPlayer = new System.Windows.Forms.Button();
            this.pictureBoxController = new System.Windows.Forms.PictureBox();
            this.panelLeftMenu = new System.Windows.Forms.Panel();
            this.labelCursor = new System.Windows.Forms.Label();
            this.buttonSetControls = new System.Windows.Forms.Button();
            this.buttonReset = new System.Windows.Forms.Button();
            this.buttonDeadzoneLeft = new System.Windows.Forms.Button();
            this.buttonDeadzoneRight = new System.Windows.Forms.Button();
            this.buttonRumble = new System.Windows.Forms.Button();
            this.buttonRadialLeft = new System.Windows.Forms.Button();
            this.buttonRadialRight = new System.Windows.Forms.Button();
            this.buttonAdvanced = new System.Windows.Forms.Button();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBoxController)).BeginInit();
            this.SuspendLayout();
            // 
            // backgroundWorker1
            // 
            this.backgroundWorker1.WorkerReportsProgress = true;
            // 
            // buttonInput
            // 
            this.buttonInput.AutoSize = true;
            this.buttonInput.AutoSizeMode = System.Windows.Forms.AutoSizeMode.GrowAndShrink;
            this.buttonInput.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(30)))), ((int)(((byte)(30)))), ((int)(((byte)(30)))));
            this.buttonInput.FlatAppearance.BorderSize = 0;
            this.buttonInput.FlatAppearance.MouseDownBackColor = System.Drawing.Color.FromArgb(((int)(((byte)(224)))), ((int)(((byte)(224)))), ((int)(((byte)(224)))));
            this.buttonInput.FlatAppearance.MouseOverBackColor = System.Drawing.Color.Gray;
            this.buttonInput.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.buttonInput.Font = new System.Drawing.Font("Arial Black", 14.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.buttonInput.ForeColor = System.Drawing.Color.White;
            this.buttonInput.Location = new System.Drawing.Point(1, 12);
            this.buttonInput.Name = "buttonInput";
            this.buttonInput.Padding = new System.Windows.Forms.Padding(8, 0, 8, 0);
            this.buttonInput.Size = new System.Drawing.Size(105, 37);
            this.buttonInput.TabIndex = 0;
            this.buttonInput.Text = "INPUT";
            this.buttonInput.UseVisualStyleBackColor = false;
            // 
            // buttonDisplay
            // 
            this.buttonDisplay.AutoSize = true;
            this.buttonDisplay.AutoSizeMode = System.Windows.Forms.AutoSizeMode.GrowAndShrink;
            this.buttonDisplay.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(30)))), ((int)(((byte)(30)))), ((int)(((byte)(30)))));
            this.buttonDisplay.Enabled = false;
            this.buttonDisplay.FlatAppearance.BorderSize = 0;
            this.buttonDisplay.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.buttonDisplay.Font = new System.Drawing.Font("Arial Black", 14.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.buttonDisplay.ForeColor = System.Drawing.Color.White;
            this.buttonDisplay.Location = new System.Drawing.Point(104, 12);
            this.buttonDisplay.Name = "buttonDisplay";
            this.buttonDisplay.Padding = new System.Windows.Forms.Padding(8, 0, 8, 0);
            this.buttonDisplay.Size = new System.Drawing.Size(129, 37);
            this.buttonDisplay.TabIndex = 1;
            this.buttonDisplay.Text = "DISPLAY";
            this.buttonDisplay.UseVisualStyleBackColor = false;
            // 
            // buttonSound
            // 
            this.buttonSound.AutoSize = true;
            this.buttonSound.AutoSizeMode = System.Windows.Forms.AutoSizeMode.GrowAndShrink;
            this.buttonSound.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(30)))), ((int)(((byte)(30)))), ((int)(((byte)(30)))));
            this.buttonSound.Enabled = false;
            this.buttonSound.FlatAppearance.BorderSize = 0;
            this.buttonSound.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.buttonSound.Font = new System.Drawing.Font("Arial Black", 14.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.buttonSound.ForeColor = System.Drawing.Color.White;
            this.buttonSound.Location = new System.Drawing.Point(231, 12);
            this.buttonSound.Name = "buttonSound";
            this.buttonSound.Padding = new System.Windows.Forms.Padding(8, 0, 8, 0);
            this.buttonSound.Size = new System.Drawing.Size(115, 37);
            this.buttonSound.TabIndex = 2;
            this.buttonSound.Text = "SOUND";
            this.buttonSound.UseVisualStyleBackColor = false;
            // 
            // buttonPlay
            // 
            this.buttonPlay.AutoSize = true;
            this.buttonPlay.AutoSizeMode = System.Windows.Forms.AutoSizeMode.GrowAndShrink;
            this.buttonPlay.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(30)))), ((int)(((byte)(30)))), ((int)(((byte)(30)))));
            this.buttonPlay.FlatAppearance.BorderSize = 0;
            this.buttonPlay.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.buttonPlay.Font = new System.Drawing.Font("Arial Black", 14.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.buttonPlay.ForeColor = System.Drawing.Color.White;
            this.buttonPlay.Location = new System.Drawing.Point(354, 12);
            this.buttonPlay.Name = "buttonPlay";
            this.buttonPlay.Padding = new System.Windows.Forms.Padding(8, 0, 8, 0);
            this.buttonPlay.Size = new System.Drawing.Size(93, 37);
            this.buttonPlay.TabIndex = 3;
            this.buttonPlay.Text = "PLAY";
            this.buttonPlay.UseVisualStyleBackColor = false;
            this.buttonPlay.Click += new System.EventHandler(this.buttonPlay_Click);
            // 
            // buttonMods
            // 
            this.buttonMods.AutoSize = true;
            this.buttonMods.AutoSizeMode = System.Windows.Forms.AutoSizeMode.GrowAndShrink;
            this.buttonMods.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(30)))), ((int)(((byte)(30)))), ((int)(((byte)(30)))));
            this.buttonMods.FlatAppearance.BorderSize = 0;
            this.buttonMods.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.buttonMods.Font = new System.Drawing.Font("Arial Black", 14.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.buttonMods.ForeColor = System.Drawing.Color.White;
            this.buttonMods.Location = new System.Drawing.Point(442, 12);
            this.buttonMods.Name = "buttonMods";
            this.buttonMods.Padding = new System.Windows.Forms.Padding(8, 0, 8, 0);
            this.buttonMods.Size = new System.Drawing.Size(101, 37);
            this.buttonMods.TabIndex = 4;
            this.buttonMods.Text = "MODS";
            this.buttonMods.UseVisualStyleBackColor = false;
            this.buttonMods.Click += new System.EventHandler(this.buttonMods_Click);
            // 
            // buttonExit
            // 
            this.buttonExit.AutoSize = true;
            this.buttonExit.AutoSizeMode = System.Windows.Forms.AutoSizeMode.GrowAndShrink;
            this.buttonExit.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(30)))), ((int)(((byte)(30)))), ((int)(((byte)(30)))));
            this.buttonExit.FlatAppearance.BorderSize = 0;
            this.buttonExit.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.buttonExit.Font = new System.Drawing.Font("Arial Black", 14.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.buttonExit.ForeColor = System.Drawing.Color.White;
            this.buttonExit.Location = new System.Drawing.Point(540, 12);
            this.buttonExit.Name = "buttonExit";
            this.buttonExit.Padding = new System.Windows.Forms.Padding(8, 0, 8, 0);
            this.buttonExit.Size = new System.Drawing.Size(88, 37);
            this.buttonExit.TabIndex = 5;
            this.buttonExit.Text = "EXIT";
            this.buttonExit.UseVisualStyleBackColor = false;
            this.buttonExit.Click += new System.EventHandler(this.buttonExit_Click);
            // 
            // panelTopMenu
            // 
            this.panelTopMenu.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(30)))), ((int)(((byte)(30)))), ((int)(((byte)(30)))));
            this.panelTopMenu.Location = new System.Drawing.Point(493, 392);
            this.panelTopMenu.Name = "panelTopMenu";
            this.panelTopMenu.Size = new System.Drawing.Size(50, 33);
            this.panelTopMenu.TabIndex = 7;
            // 
            // labelCurrentDevice
            // 
            this.labelCurrentDevice.AutoSize = true;
            this.labelCurrentDevice.Font = new System.Drawing.Font("Arial Black", 14.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.labelCurrentDevice.ForeColor = System.Drawing.Color.White;
            this.labelCurrentDevice.Location = new System.Drawing.Point(342, 75);
            this.labelCurrentDevice.Margin = new System.Windows.Forms.Padding(5, 8, 5, 8);
            this.labelCurrentDevice.Name = "labelCurrentDevice";
            this.labelCurrentDevice.Size = new System.Drawing.Size(167, 27);
            this.labelCurrentDevice.TabIndex = 8;
            this.labelCurrentDevice.Text = "Current device";
            // 
            // labelShowHint
            // 
            this.labelShowHint.AutoSize = true;
            this.labelShowHint.Font = new System.Drawing.Font("Arial Black", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.labelShowHint.ForeColor = System.Drawing.Color.White;
            this.labelShowHint.Location = new System.Drawing.Point(308, 448);
            this.labelShowHint.Margin = new System.Windows.Forms.Padding(5, 5, 5, 0);
            this.labelShowHint.Name = "labelShowHint";
            this.labelShowHint.Size = new System.Drawing.Size(253, 23);
            this.labelShowHint.TabIndex = 10;
            this.labelShowHint.Text = "Hints will be displayed here";
            // 
            // buttonSwitchDevice
            // 
            this.buttonSwitchDevice.AutoSize = true;
            this.buttonSwitchDevice.AutoSizeMode = System.Windows.Forms.AutoSizeMode.GrowAndShrink;
            this.buttonSwitchDevice.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(20)))), ((int)(((byte)(20)))), ((int)(((byte)(20)))));
            this.buttonSwitchDevice.FlatAppearance.BorderSize = 0;
            this.buttonSwitchDevice.FlatAppearance.MouseDownBackColor = System.Drawing.Color.FromArgb(((int)(((byte)(224)))), ((int)(((byte)(224)))), ((int)(((byte)(224)))));
            this.buttonSwitchDevice.FlatAppearance.MouseOverBackColor = System.Drawing.Color.Gray;
            this.buttonSwitchDevice.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.buttonSwitchDevice.Font = new System.Drawing.Font("Arial Black", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.buttonSwitchDevice.ForeColor = System.Drawing.Color.White;
            this.buttonSwitchDevice.Location = new System.Drawing.Point(39, 75);
            this.buttonSwitchDevice.Margin = new System.Windows.Forms.Padding(4);
            this.buttonSwitchDevice.Name = "buttonSwitchDevice";
            this.buttonSwitchDevice.Size = new System.Drawing.Size(102, 33);
            this.buttonSwitchDevice.TabIndex = 6;
            this.buttonSwitchDevice.Text = "DEVICE 1";
            this.buttonSwitchDevice.UseVisualStyleBackColor = false;
            // 
            // buttonSwitchDeviceLeft
            // 
            this.buttonSwitchDeviceLeft.AutoSize = true;
            this.buttonSwitchDeviceLeft.AutoSizeMode = System.Windows.Forms.AutoSizeMode.GrowAndShrink;
            this.buttonSwitchDeviceLeft.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(20)))), ((int)(((byte)(20)))), ((int)(((byte)(20)))));
            this.buttonSwitchDeviceLeft.FlatAppearance.BorderSize = 0;
            this.buttonSwitchDeviceLeft.FlatAppearance.MouseDownBackColor = System.Drawing.Color.FromArgb(((int)(((byte)(224)))), ((int)(((byte)(224)))), ((int)(((byte)(224)))));
            this.buttonSwitchDeviceLeft.FlatAppearance.MouseOverBackColor = System.Drawing.Color.Gray;
            this.buttonSwitchDeviceLeft.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.buttonSwitchDeviceLeft.Font = new System.Drawing.Font("Webdings", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(2)));
            this.buttonSwitchDeviceLeft.ForeColor = System.Drawing.Color.White;
            this.buttonSwitchDeviceLeft.Location = new System.Drawing.Point(8, 75);
            this.buttonSwitchDeviceLeft.Margin = new System.Windows.Forms.Padding(4);
            this.buttonSwitchDeviceLeft.Name = "buttonSwitchDeviceLeft";
            this.buttonSwitchDeviceLeft.Size = new System.Drawing.Size(36, 29);
            this.buttonSwitchDeviceLeft.TabIndex = 11;
            this.buttonSwitchDeviceLeft.Text = "3";
            this.buttonSwitchDeviceLeft.UseVisualStyleBackColor = false;
            // 
            // buttonSwitchDeviceRight
            // 
            this.buttonSwitchDeviceRight.AutoSize = true;
            this.buttonSwitchDeviceRight.AutoSizeMode = System.Windows.Forms.AutoSizeMode.GrowAndShrink;
            this.buttonSwitchDeviceRight.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(20)))), ((int)(((byte)(20)))), ((int)(((byte)(20)))));
            this.buttonSwitchDeviceRight.FlatAppearance.BorderSize = 0;
            this.buttonSwitchDeviceRight.FlatAppearance.MouseDownBackColor = System.Drawing.Color.FromArgb(((int)(((byte)(224)))), ((int)(((byte)(224)))), ((int)(((byte)(224)))));
            this.buttonSwitchDeviceRight.FlatAppearance.MouseOverBackColor = System.Drawing.Color.Gray;
            this.buttonSwitchDeviceRight.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.buttonSwitchDeviceRight.Font = new System.Drawing.Font("Webdings", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(2)));
            this.buttonSwitchDeviceRight.ForeColor = System.Drawing.Color.White;
            this.buttonSwitchDeviceRight.Location = new System.Drawing.Point(133, 75);
            this.buttonSwitchDeviceRight.Margin = new System.Windows.Forms.Padding(4);
            this.buttonSwitchDeviceRight.Name = "buttonSwitchDeviceRight";
            this.buttonSwitchDeviceRight.Size = new System.Drawing.Size(36, 29);
            this.buttonSwitchDeviceRight.TabIndex = 12;
            this.buttonSwitchDeviceRight.Text = "4";
            this.buttonSwitchDeviceRight.UseVisualStyleBackColor = false;
            // 
            // buttonSwitchPlayer
            // 
            this.buttonSwitchPlayer.AutoSize = true;
            this.buttonSwitchPlayer.AutoSizeMode = System.Windows.Forms.AutoSizeMode.GrowAndShrink;
            this.buttonSwitchPlayer.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(20)))), ((int)(((byte)(20)))), ((int)(((byte)(20)))));
            this.buttonSwitchPlayer.FlatAppearance.BorderSize = 0;
            this.buttonSwitchPlayer.FlatAppearance.MouseDownBackColor = System.Drawing.Color.FromArgb(((int)(((byte)(224)))), ((int)(((byte)(224)))), ((int)(((byte)(224)))));
            this.buttonSwitchPlayer.FlatAppearance.MouseOverBackColor = System.Drawing.Color.Gray;
            this.buttonSwitchPlayer.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.buttonSwitchPlayer.Font = new System.Drawing.Font("Arial Black", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.buttonSwitchPlayer.ForeColor = System.Drawing.Color.White;
            this.buttonSwitchPlayer.Location = new System.Drawing.Point(16, 115);
            this.buttonSwitchPlayer.Name = "buttonSwitchPlayer";
            this.buttonSwitchPlayer.Size = new System.Drawing.Size(148, 33);
            this.buttonSwitchPlayer.TabIndex = 13;
            this.buttonSwitchPlayer.Text = "PLAYER: AUTO";
            this.buttonSwitchPlayer.UseVisualStyleBackColor = false;
            // 
            // pictureBoxController
            // 
            this.pictureBoxController.Image = global::DeckLauncher.Properties.Resources.vecteezy_joystick_icon_vector_298898221;
            this.pictureBoxController.Location = new System.Drawing.Point(226, 113);
            this.pictureBoxController.Name = "pictureBoxController";
            this.pictureBoxController.Size = new System.Drawing.Size(402, 273);
            this.pictureBoxController.SizeMode = System.Windows.Forms.PictureBoxSizeMode.Zoom;
            this.pictureBoxController.TabIndex = 6;
            this.pictureBoxController.TabStop = false;
            // 
            // panelLeftMenu
            // 
            this.panelLeftMenu.AutoSizeMode = System.Windows.Forms.AutoSizeMode.GrowAndShrink;
            this.panelLeftMenu.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(20)))), ((int)(((byte)(20)))), ((int)(((byte)(20)))));
            this.panelLeftMenu.Location = new System.Drawing.Point(559, 392);
            this.panelLeftMenu.Margin = new System.Windows.Forms.Padding(0);
            this.panelLeftMenu.Name = "panelLeftMenu";
            this.panelLeftMenu.Size = new System.Drawing.Size(69, 38);
            this.panelLeftMenu.TabIndex = 8;
            // 
            // labelCursor
            // 
            this.labelCursor.AutoSize = true;
            this.labelCursor.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(20)))), ((int)(((byte)(20)))), ((int)(((byte)(20)))));
            this.labelCursor.ForeColor = System.Drawing.Color.White;
            this.labelCursor.Location = new System.Drawing.Point(12, 458);
            this.labelCursor.Name = "labelCursor";
            this.labelCursor.Size = new System.Drawing.Size(48, 13);
            this.labelCursor.TabIndex = 11;
            this.labelCursor.Text = "X: 0 Y: 0";
            // 
            // buttonSetControls
            // 
            this.buttonSetControls.AutoSize = true;
            this.buttonSetControls.AutoSizeMode = System.Windows.Forms.AutoSizeMode.GrowAndShrink;
            this.buttonSetControls.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(20)))), ((int)(((byte)(20)))), ((int)(((byte)(20)))));
            this.buttonSetControls.FlatAppearance.BorderSize = 0;
            this.buttonSetControls.FlatAppearance.MouseDownBackColor = System.Drawing.Color.FromArgb(((int)(((byte)(224)))), ((int)(((byte)(224)))), ((int)(((byte)(224)))));
            this.buttonSetControls.FlatAppearance.MouseOverBackColor = System.Drawing.Color.Gray;
            this.buttonSetControls.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.buttonSetControls.Font = new System.Drawing.Font("Arial Black", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.buttonSetControls.ForeColor = System.Drawing.Color.White;
            this.buttonSetControls.Location = new System.Drawing.Point(31, 147);
            this.buttonSetControls.Name = "buttonSetControls";
            this.buttonSetControls.Size = new System.Drawing.Size(110, 33);
            this.buttonSetControls.TabIndex = 14;
            this.buttonSetControls.Text = "REDEFINE";
            this.buttonSetControls.UseVisualStyleBackColor = false;
            // 
            // buttonReset
            // 
            this.buttonReset.AutoSize = true;
            this.buttonReset.AutoSizeMode = System.Windows.Forms.AutoSizeMode.GrowAndShrink;
            this.buttonReset.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(20)))), ((int)(((byte)(20)))), ((int)(((byte)(20)))));
            this.buttonReset.FlatAppearance.BorderSize = 0;
            this.buttonReset.FlatAppearance.MouseDownBackColor = System.Drawing.Color.FromArgb(((int)(((byte)(224)))), ((int)(((byte)(224)))), ((int)(((byte)(224)))));
            this.buttonReset.FlatAppearance.MouseOverBackColor = System.Drawing.Color.Gray;
            this.buttonReset.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.buttonReset.Font = new System.Drawing.Font("Arial Black", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.buttonReset.ForeColor = System.Drawing.Color.White;
            this.buttonReset.Location = new System.Drawing.Point(45, 186);
            this.buttonReset.Name = "buttonReset";
            this.buttonReset.Size = new System.Drawing.Size(80, 33);
            this.buttonReset.TabIndex = 15;
            this.buttonReset.Text = "RESET";
            this.buttonReset.UseVisualStyleBackColor = false;
            // 
            // buttonDeadzoneLeft
            // 
            this.buttonDeadzoneLeft.AutoSize = true;
            this.buttonDeadzoneLeft.AutoSizeMode = System.Windows.Forms.AutoSizeMode.GrowAndShrink;
            this.buttonDeadzoneLeft.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(20)))), ((int)(((byte)(20)))), ((int)(((byte)(20)))));
            this.buttonDeadzoneLeft.FlatAppearance.BorderSize = 0;
            this.buttonDeadzoneLeft.FlatAppearance.MouseDownBackColor = System.Drawing.Color.FromArgb(((int)(((byte)(224)))), ((int)(((byte)(224)))), ((int)(((byte)(224)))));
            this.buttonDeadzoneLeft.FlatAppearance.MouseOverBackColor = System.Drawing.Color.Gray;
            this.buttonDeadzoneLeft.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.buttonDeadzoneLeft.Font = new System.Drawing.Font("Arial Black", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.buttonDeadzoneLeft.ForeColor = System.Drawing.Color.White;
            this.buttonDeadzoneLeft.Location = new System.Drawing.Point(16, 264);
            this.buttonDeadzoneLeft.Name = "buttonDeadzoneLeft";
            this.buttonDeadzoneLeft.Size = new System.Drawing.Size(162, 28);
            this.buttonDeadzoneLeft.TabIndex = 16;
            this.buttonDeadzoneLeft.Text = "DEADZONE L: 23.9%";
            this.buttonDeadzoneLeft.UseVisualStyleBackColor = false;
            this.buttonDeadzoneLeft.Click += new System.EventHandler(this.buttonDeadzoneLeft_Click);
            // 
            // buttonDeadzoneRight
            // 
            this.buttonDeadzoneRight.AutoSize = true;
            this.buttonDeadzoneRight.AutoSizeMode = System.Windows.Forms.AutoSizeMode.GrowAndShrink;
            this.buttonDeadzoneRight.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(20)))), ((int)(((byte)(20)))), ((int)(((byte)(20)))));
            this.buttonDeadzoneRight.FlatAppearance.BorderSize = 0;
            this.buttonDeadzoneRight.FlatAppearance.MouseDownBackColor = System.Drawing.Color.FromArgb(((int)(((byte)(224)))), ((int)(((byte)(224)))), ((int)(((byte)(224)))));
            this.buttonDeadzoneRight.FlatAppearance.MouseOverBackColor = System.Drawing.Color.Gray;
            this.buttonDeadzoneRight.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.buttonDeadzoneRight.Font = new System.Drawing.Font("Arial Black", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.buttonDeadzoneRight.ForeColor = System.Drawing.Color.White;
            this.buttonDeadzoneRight.Location = new System.Drawing.Point(16, 295);
            this.buttonDeadzoneRight.Name = "buttonDeadzoneRight";
            this.buttonDeadzoneRight.Size = new System.Drawing.Size(163, 28);
            this.buttonDeadzoneRight.TabIndex = 17;
            this.buttonDeadzoneRight.Text = "DEADZONE R: 26.5%";
            this.buttonDeadzoneRight.UseVisualStyleBackColor = false;
            this.buttonDeadzoneRight.Click += new System.EventHandler(this.buttonDeadzoneRight_Click);
            // 
            // buttonRumble
            // 
            this.buttonRumble.AutoSize = true;
            this.buttonRumble.AutoSizeMode = System.Windows.Forms.AutoSizeMode.GrowAndShrink;
            this.buttonRumble.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(20)))), ((int)(((byte)(20)))), ((int)(((byte)(20)))));
            this.buttonRumble.FlatAppearance.BorderSize = 0;
            this.buttonRumble.FlatAppearance.MouseDownBackColor = System.Drawing.Color.FromArgb(((int)(((byte)(224)))), ((int)(((byte)(224)))), ((int)(((byte)(224)))));
            this.buttonRumble.FlatAppearance.MouseOverBackColor = System.Drawing.Color.Gray;
            this.buttonRumble.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.buttonRumble.Font = new System.Drawing.Font("Arial Black", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.buttonRumble.ForeColor = System.Drawing.Color.White;
            this.buttonRumble.Location = new System.Drawing.Point(37, 388);
            this.buttonRumble.Name = "buttonRumble";
            this.buttonRumble.Size = new System.Drawing.Size(127, 28);
            this.buttonRumble.TabIndex = 19;
            this.buttonRumble.Text = "RUMBLE: 100%";
            this.buttonRumble.UseVisualStyleBackColor = false;
            // 
            // buttonRadialLeft
            // 
            this.buttonRadialLeft.AutoSize = true;
            this.buttonRadialLeft.AutoSizeMode = System.Windows.Forms.AutoSizeMode.GrowAndShrink;
            this.buttonRadialLeft.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(20)))), ((int)(((byte)(20)))), ((int)(((byte)(20)))));
            this.buttonRadialLeft.FlatAppearance.BorderSize = 0;
            this.buttonRadialLeft.FlatAppearance.MouseDownBackColor = System.Drawing.Color.FromArgb(((int)(((byte)(224)))), ((int)(((byte)(224)))), ((int)(((byte)(224)))));
            this.buttonRadialLeft.FlatAppearance.MouseOverBackColor = System.Drawing.Color.Gray;
            this.buttonRadialLeft.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.buttonRadialLeft.Font = new System.Drawing.Font("Arial Black", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.buttonRadialLeft.ForeColor = System.Drawing.Color.White;
            this.buttonRadialLeft.Location = new System.Drawing.Point(45, 326);
            this.buttonRadialLeft.Name = "buttonRadialLeft";
            this.buttonRadialLeft.Size = new System.Drawing.Size(115, 28);
            this.buttonRadialLeft.TabIndex = 20;
            this.buttonRadialLeft.Text = "RADIAL L: ON";
            this.buttonRadialLeft.UseVisualStyleBackColor = false;
            // 
            // buttonRadialRight
            // 
            this.buttonRadialRight.AutoSize = true;
            this.buttonRadialRight.AutoSizeMode = System.Windows.Forms.AutoSizeMode.GrowAndShrink;
            this.buttonRadialRight.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(20)))), ((int)(((byte)(20)))), ((int)(((byte)(20)))));
            this.buttonRadialRight.FlatAppearance.BorderSize = 0;
            this.buttonRadialRight.FlatAppearance.MouseDownBackColor = System.Drawing.Color.FromArgb(((int)(((byte)(224)))), ((int)(((byte)(224)))), ((int)(((byte)(224)))));
            this.buttonRadialRight.FlatAppearance.MouseOverBackColor = System.Drawing.Color.Gray;
            this.buttonRadialRight.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.buttonRadialRight.Font = new System.Drawing.Font("Arial Black", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.buttonRadialRight.ForeColor = System.Drawing.Color.White;
            this.buttonRadialRight.Location = new System.Drawing.Point(45, 357);
            this.buttonRadialRight.Name = "buttonRadialRight";
            this.buttonRadialRight.Size = new System.Drawing.Size(116, 28);
            this.buttonRadialRight.TabIndex = 21;
            this.buttonRadialRight.Text = "RADIAL R: ON";
            this.buttonRadialRight.UseVisualStyleBackColor = false;
            // 
            // buttonAdvanced
            // 
            this.buttonAdvanced.AutoSize = true;
            this.buttonAdvanced.AutoSizeMode = System.Windows.Forms.AutoSizeMode.GrowAndShrink;
            this.buttonAdvanced.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(20)))), ((int)(((byte)(20)))), ((int)(((byte)(20)))));
            this.buttonAdvanced.FlatAppearance.BorderSize = 0;
            this.buttonAdvanced.FlatAppearance.MouseDownBackColor = System.Drawing.Color.FromArgb(((int)(((byte)(224)))), ((int)(((byte)(224)))), ((int)(((byte)(224)))));
            this.buttonAdvanced.FlatAppearance.MouseOverBackColor = System.Drawing.Color.Gray;
            this.buttonAdvanced.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.buttonAdvanced.Font = new System.Drawing.Font("Arial Black", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.buttonAdvanced.ForeColor = System.Drawing.Color.White;
            this.buttonAdvanced.Location = new System.Drawing.Point(45, 225);
            this.buttonAdvanced.Name = "buttonAdvanced";
            this.buttonAdvanced.Size = new System.Drawing.Size(115, 33);
            this.buttonAdvanced.TabIndex = 22;
            this.buttonAdvanced.Text = "ADVANCED";
            this.buttonAdvanced.UseVisualStyleBackColor = false;
            this.buttonAdvanced.Click += new System.EventHandler(this.buttonAdvanced_Click);
            // 
            // Form_SteamHelper
            // 
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.None;
            this.BackColor = System.Drawing.Color.Black;
            this.ClientSize = new System.Drawing.Size(640, 480);
            this.Controls.Add(this.buttonAdvanced);
            this.Controls.Add(this.buttonRadialRight);
            this.Controls.Add(this.buttonRadialLeft);
            this.Controls.Add(this.buttonRumble);
            this.Controls.Add(this.buttonDeadzoneRight);
            this.Controls.Add(this.buttonDeadzoneLeft);
            this.Controls.Add(this.buttonReset);
            this.Controls.Add(this.buttonSetControls);
            this.Controls.Add(this.buttonInput);
            this.Controls.Add(this.buttonMods);
            this.Controls.Add(this.buttonExit);
            this.Controls.Add(this.buttonPlay);
            this.Controls.Add(this.buttonDisplay);
            this.Controls.Add(this.buttonSound);
            this.Controls.Add(this.buttonSwitchDeviceLeft);
            this.Controls.Add(this.buttonSwitchPlayer);
            this.Controls.Add(this.buttonSwitchDevice);
            this.Controls.Add(this.buttonSwitchDeviceRight);
            this.Controls.Add(this.labelCursor);
            this.Controls.Add(this.panelLeftMenu);
            this.Controls.Add(this.labelShowHint);
            this.Controls.Add(this.labelCurrentDevice);
            this.Controls.Add(this.pictureBoxController);
            this.Controls.Add(this.panelTopMenu);
            this.DoubleBuffered = true;
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.None;
            this.KeyPreview = true;
            this.Margin = new System.Windows.Forms.Padding(2);
            this.MaximizeBox = false;
            this.MinimizeBox = false;
            this.Name = "Form_SteamHelper";
            this.ShowIcon = false;
            this.SizeGripStyle = System.Windows.Forms.SizeGripStyle.Hide;
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "SADX Launcher";
            this.KeyDown += new System.Windows.Forms.KeyEventHandler(this.Form_SteamHelper_KeyDown);
            this.MouseMove += new System.Windows.Forms.MouseEventHandler(this.Form_SteamHelper_MouseMove);
            ((System.ComponentModel.ISupportInitialize)(this.pictureBoxController)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion
        private System.ComponentModel.BackgroundWorker backgroundWorker1;
        private System.Windows.Forms.Button buttonInput;
        private System.Windows.Forms.Button buttonDisplay;
        private System.Windows.Forms.Button buttonSound;
        private System.Windows.Forms.Button buttonPlay;
        private System.Windows.Forms.Button buttonMods;
        private System.Windows.Forms.PictureBox pictureBoxController;
        private System.Windows.Forms.Button buttonExit;
        private System.Windows.Forms.Panel panelTopMenu;
        private System.Windows.Forms.Label labelCurrentDevice;
        private System.Windows.Forms.Label labelShowHint;
        private System.Windows.Forms.Button buttonSwitchDevice;
        private System.Windows.Forms.Button buttonSwitchDeviceLeft;
        private System.Windows.Forms.Button buttonSwitchDeviceRight;
        private System.Windows.Forms.Button buttonSwitchPlayer;
        private System.Windows.Forms.Panel panelLeftMenu;
        private System.Windows.Forms.Label labelCursor;
        private System.Windows.Forms.Button buttonSetControls;
        private System.Windows.Forms.Button buttonReset;
        private System.Windows.Forms.Button buttonDeadzoneLeft;
        private System.Windows.Forms.Button buttonDeadzoneRight;
        private System.Windows.Forms.Button buttonRumble;
        private System.Windows.Forms.Button buttonRadialLeft;
        private System.Windows.Forms.Button buttonRadialRight;
        private System.Windows.Forms.Button buttonAdvanced;
    }
}