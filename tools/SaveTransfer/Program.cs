﻿using System;
using System.Collections.Generic;
using System.IO;

namespace SaveTransfer
{
	static class Program
	{
		static List<byte[]> saves;

		static ushort CalcSaveChecksum(byte[] a2)
		{
			ushort v1; // eax@1
			int v2; // ecx@1
			byte v3; // edi@2
			ushort v4; // eax@3
			ushort v6; // [sp+0h] [bp-4h]@2

			v1 = 65535;
			v2 = 4;
			do
			{
				v3 = a2[v2];
				v6 = (ushort)(v3 ^ v1);
				if (((v3 ^ (byte)v1) & 1) != 0)
				{
					v4 = (ushort)((v6 >> 1) ^ 0x8408);
					v6 = (ushort)((v6 >> 1) ^ 0x8408);
				}
				else
				{
					v4 = (ushort)(v6 >> 1);
					v6 = (ushort)(v6 >> 1);
				}
				v4 = (ushort)(v4 >> 1);
				if ((v6 & 1) != 0)
				{
					v4 ^= 0x8408;
					v6 = v4;
				}
				else
				{
					v6 = v4;
				}
				v4 = (ushort)(v4 >> 1);
				if ((v6 & 1) != 0)
				{
					v4 ^= 0x8408;
					v6 = v4;
				}
				else
				{
					v6 = v4;
				}
				v4 = (ushort)(v4 >> 1);
				if ((v6 & 1) != 0)
				{
					v4 ^= 0x8408;
					v6 = v4;
				}
				else
				{
					v6 = v4;
				}
				v4 = (ushort)(v4 >> 1);
				if ((v6 & 1) != 0)
				{
					v4 ^= 0x8408;
					v6 = v4;
				}
				else
				{
					v6 = v4;
				}
				v4 = (ushort)(v4 >> 1);
				if ((v6 & 1) != 0)
				{
					v4 ^= 0x8408;
					v6 = v4;
				}
				else
				{
					v6 = v4;
				}
				v4 = (ushort)(v4 >> 1);
				if ((v6 & 1) != 0)
				{
					v4 ^= 0x8408;
					v6 = v4;
				}
				else
				{
					v6 = v4;
				}
				v4 = (ushort)(v4 >> 1);
				if ((v6 & 1) != 0)
				{
					v1 = (ushort)(v4 ^ 0x8408);
				}
				else
				{
					v6 = v4;
					v1 = v6;
				}
				++v2;
			}
			while (v2 < 0x570);
			return (ushort)~v1;
		}

		static bool Compare(byte[] a1, byte[] a2)
		{
			if (a1.Length != a2.Length)
				return false;

			for (int i = 0; i < a1.Length; i++)
				if (a1[i] != a2[i])
					return false;

			return true;
		}

		static bool CheckSaveDuplicate(byte[] file)
		{
			foreach (var item in saves)
			{
				if (Compare(file, item))
				{
					Console.WriteLine("Duplicate save data found, file not added");
					return true;
				}
			}
			return false;
		}

		static void AddSave(byte[] file)
		{
			if (!CheckSaveDuplicate(file))
				saves.Add(file);
		}

		static void ConvertSave()
		{
			saves = new List<byte[]>();
			string path_sadx2004 = Environment.CurrentDirectory;
			string path_steam = Path.Combine(Environment.GetFolderPath(Environment.SpecialFolder.MyDocuments), "SEGA", "Sonic Adventure DX", "SAVEDATA");
			string path_dccol = Path.Combine(Environment.GetFolderPath(Environment.SpecialFolder.MyDocuments), "SEGA", "Dreamcast Collection", "Sonic Adventure DX", "SAVEDATA");
			string path_savefolder = path_steam;
			bool dosaves = false;
			if (Directory.Exists(path_steam))
			{
				Console.WriteLine("Found Steam saves at " + path_steam);
				dosaves = true;
			}
			else if (Directory.Exists(path_dccol))
			{
				Console.WriteLine("Found Dreamcast Collection saves" + path_dccol);
				path_savefolder = path_dccol;
				dosaves = true;
			}
			if (!dosaves)
			{
				Console.WriteLine("No save data found");
				return;
			}
			// Create 2004 savedata folder
			if (!Directory.Exists(Path.Combine(Environment.CurrentDirectory, "savedata"))) Directory.CreateDirectory(Path.Combine(Environment.CurrentDirectory, "savedata"));
			// Copy Chao Garden file
			if (File.Exists(Path.Combine(path_sadx2004, "savedata", "SONICADVENTURE_DX_CHAOGARDEN.snc")))
			{
				Console.WriteLine("The Chao save file was not converted because your SADX 2004 savedata folder already contains a Chao save file.");
			}
			else if (File.Exists(Path.Combine(path_savefolder, "SonicAdventureChaoGarden.snc")))
			{
				Console.WriteLine("Copying Chao Garden file...");
				Console.WriteLine(Path.Combine(path_savefolder, "SonicAdventureChaoGarden.snc") + " > " + Path.Combine(Environment.CurrentDirectory, "savedata", "SONICADVENTURE_DX_CHAOGARDEN.snc"));
				File.Copy(Path.Combine(path_savefolder, "SonicAdventureChaoGarden.snc"), Path.Combine(Environment.CurrentDirectory, "savedata", "SONICADVENTURE_DX_CHAOGARDEN.snc"));
			}
			// Add regular saves from SADX2004 folder
			for (int u = 1; u < 100; u++)
			{
				string savefile = Path.Combine(path_sadx2004, "savedata", "SonicDX" + u.ToString("D2") + ".snc");
				if (File.Exists(savefile))
				{
					Console.WriteLine(" Adding save: " + savefile);
					AddSave(File.ReadAllBytes(savefile));
				}
			}
			// Add regular saves from Steam folder
			Console.WriteLine("Converting regular saves...");
			for (int i = 1; i < 100; i++)
			{
				string path_save = Path.Combine(path_savefolder, "SonicAdventure" + i.ToString("D2") + ".snc");
				if (File.Exists(path_save))
				{
					byte[] newsave = new byte[1392];
					Array.Copy(File.ReadAllBytes(path_save), newsave, newsave.Length);
					BitConverter.GetBytes(CalcSaveChecksum(newsave)).CopyTo(newsave, 0);
					Console.WriteLine(" Checking/adding save: " + path_save);
					AddSave(newsave);
				}
			}
			// Write out the saves
			Console.WriteLine("Writing out saves...");
			int s = 0;
			foreach (var item in saves)
			{
				File.WriteAllBytes(Path.Combine(path_sadx2004, "savedata", "SonicDX" + (s + 1).ToString("D2") + ".snc"), saves[s]);
				Console.WriteLine(" Converted save file:\n" + Path.Combine(path_sadx2004, "savedata", "SonicDX" + (s + 1).ToString("D2") + ".snc"));
				s++;
			}
		}

		static void Main(string[] args)
		{
			ConvertSave();
			System.Threading.Thread.Sleep(500);
		}
	}
}