# Save Transfer for the SADX Mod Installer

This tool converts save files from the Steam version of Sonic Adventure DX to make them compatible with the 2004 version. This tool was used internally by the SADX Mod Installer, however it can also work independently as a command line tool.

As of 2024, the Mod Loader's Extended Save Support can load Steam save files, so this tool is no longer necessary.