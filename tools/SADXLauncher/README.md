# SADX Launcher

This tool replaces the default launcher for Sonic Adventure DX (Steam). 

It's made in [Clickteam Fusion 2.5](https://www.clickteam.com/clickteam-fusion-2-5) and requires the Pro version to build.