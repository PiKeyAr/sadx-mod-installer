﻿using System.Windows.Forms;

namespace SteamHelper
{
	static class Program
	{
		static void Main(string[] args)
		{
			Form_SteamHelper form;
			Application.EnableVisualStyles();
			Application.SetCompatibleTextRenderingDefault(false);
			string wf = System.Environment.CurrentDirectory;
			string sf = System.Environment.CurrentDirectory;
			if (args.Length > 1)
			{
				wf = args[0];
				sf = args[1];
			}
			form = new Form_SteamHelper(wf, sf);
			form.ShowDialog();
		}
	}
}