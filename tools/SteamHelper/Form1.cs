﻿using System;
using System.ComponentModel;
using System.Windows.Forms;
using System.Diagnostics;
using System.IO;
using System.Security.Cryptography;
using static SteamHelper.PatchHashes;

namespace SteamHelper
{
    public partial class Form_SteamHelper : Form
    {
        enum SuccessValue
        {
            InProcess = 0,
            Success = 1,
            Error = 2,
        }

        static bool DontCheckExe = false;
        static string WorkFolder;
        static string StartFolder;
        static SuccessValue Success = SuccessValue.InProcess;
		static TextWriter logger;
		delegate void SetTextCallback(string text);
		delegate void SetTextCallbackFull(System.Windows.Forms.Label control, string text);
		delegate void SetValueCallback(int val);

        public Form_SteamHelper(string wfolder, string startfolder)
        {
            StartFolder = startfolder;
            WorkFolder = wfolder;
            InitializeComponent();
            UpdateProgressDetails("Creating log file...");
            logger = File.CreateText(Path.Combine(WorkFolder, "SADX Steam Conversion.log"));
            addURLHandler();
            AddProgress(10);
            backgroundWorker1.RunWorkerAsync();
		}

		private void UpdateProgressDetails(string str)
		{
			if (ProgressLabel.InvokeRequired)
			{
				SetTextCallback d = new SetTextCallback(UpdateProgressDetails);
				this.Invoke(d, new object[] { str });
			}
			else
			{
				ProgressLabel.Text = str;
                if (logger != null)
                {
                    logger.WriteLine(str);
                    logger.Flush();
                }
			}
		}

        private void EncodeMPG(string dir, string namenoext, int bitrate)
        {
            string destname = namenoext;
            if (destname.ToLowerInvariant() == "re-us2")
                destname = "RE-US";
            else if (destname.ToLowerInvariant() == "re-jp2")
                destname = "RE-JP";
            // Check if the destination file already exists
            if (File.Exists(Path.Combine(WorkFolder, "system", destname + ".mpg")))
            {
                logger.WriteLine("Destination file already exists: " + Path.Combine(WorkFolder, "system", destname + ".mpg"));
                return;
            }
            // Error check: folder
            string fulldir = Path.Combine(WorkFolder, dir);
            if (!Directory.Exists(fulldir))
            {
                logger.WriteLine("Could not locate the folder: " + fulldir);
                MessageBox.Show("Unable to locate SADX folder at: " + fulldir + ".\nVerify integrity of your SADX installation and run the installer again.", "Error: system folder not found", MessageBoxButtons.OK, MessageBoxIcon.Warning, MessageBoxDefaultButton.Button1, MessageBoxOptions.DefaultDesktopOnly);
                return;
            }

            string fullpath = Path.Combine(fulldir, namenoext);

            // Error check: source file
            if (!File.Exists(fullpath + ".sfd"))
            {
                logger.WriteLine("File not found: " + fullpath + ".sfd");
                return;
            }

            // Error check: ffmpeg.exe
            if (!File.Exists(Path.Combine(StartFolder, "ffmpeg.exe")))
            {
                logger.WriteLine("Critical error: FFMPEG not found.");
                MessageBox.Show("Critical error: unable to locate ffmpeg.exe.", "Error: FFMPEG not found", MessageBoxButtons.OK, MessageBoxIcon.Error, MessageBoxDefaultButton.Button1, MessageBoxOptions.DefaultDesktopOnly);
                return;
            }

            // Encode
            UpdateProgressDetails("Converting video: " + namenoext);
            Process pProcess = new System.Diagnostics.Process();
            pProcess.StartInfo.FileName = "ffmpeg.exe";
            pProcess.StartInfo.Arguments = "-y -hide_banner -loglevel error -r 29.97 -c:a adpcm_adx -i " + "\"" + fullpath + ".sfd\"" + " -vcodec mpeg1video -s 640x480 -aspect 4/3 -acodec mp2 -b:v " + bitrate.ToString() + " \"" + Path.Combine(WorkFolder, "system", destname) + ".mpg\"";
            logger.WriteLine("FFMPEG command line: ffmpeg " + pProcess.StartInfo.Arguments + System.Environment.NewLine);
            pProcess.StartInfo.UseShellExecute = false;
            pProcess.StartInfo.RedirectStandardError = true;
            pProcess.StartInfo.WorkingDirectory = StartFolder;
            pProcess.StartInfo.CreateNoWindow = true;
            pProcess.Start();
            string strOutput = pProcess.StandardError.ReadToEnd();
            logger.WriteLine(strOutput);
            pProcess.WaitForExit();
            logger.WriteLine("FFMPEG exited");
            // Check converted file
            if (!File.Exists(Path.Combine(WorkFolder, "system", destname + ".mpg")))
            {
                logger.WriteLine("Destination file not found: " + Path.Combine(WorkFolder, "system", destname + ".mpg"));
                return;
            }
        }

        private void ConvertSFD()
        {
            UpdateProgressDetails("Encoding videos...");
            EncodeMPG("DLC", "re-us2", 6400000);
            AddProgress(30);
            logger.WriteLine();
            EncodeMPG("DLC", "re-jp2", 6400000);
            AddProgress(30);
            logger.WriteLine();
            for (int i = 1; i < 9; i++)
            {
                EncodeMPG("system", "SA" + i.ToString(), 4800000);
                AddProgress(10);
                logger.WriteLine();
            }
        }

        private void PatchSteam()
        {
            UpdateProgressDetails("Checking Steam resources...");
            string message = string.Empty;
            MD5 md5 = MD5.Create();
            foreach (PatchHashData patchHash in SteamSourceData)
            {
                UpdateProgressDetails("Checking file " + patchHash.filename);
                string checkpath = Path.Combine(WorkFolder, patchHash.filename);
                if (!File.Exists(checkpath))
                {
                    message = "File '" + checkpath + "' was not found.\n\nVerify game integrity on Steam and try again.";
                    logger.WriteLine(message);
                    logger.Flush();
                    logger.Close();
                    MessageBox.Show("Error running Steam Conversion Helper: \n\n" + message, "Error", MessageBoxButtons.OK, MessageBoxIcon.Error);
                    Success = SuccessValue.Error;
                    Application.Exit();
                }
                byte[] filehash_bytes = md5.ComputeHash(File.ReadAllBytes(checkpath));
                string filehash = string.Empty;
                foreach (byte item in filehash_bytes)
                    filehash += item.ToString("x2");
                if (filehash != patchHash.checksum)
                {
                    message = "File '" + patchHash.filename + "' has a different checksum.\n\nVerify game integrity on Steam and try again.\n\n" + "File checksum: " + filehash + "\nExpected: " + patchHash.checksum;
                    logger.WriteLine(message);
                    logger.Flush();
                    logger.Close();
                    MessageBox.Show("Error running Steam Conversion Helper: \n" + message, "Error", MessageBoxButtons.OK, MessageBoxIcon.Error);
                    Success = SuccessValue.Error;
                    Application.Exit();
                }
            }
            UpdateProgressDetails("Patching Steam resources...");
            // Error check: hpatchz.exe
            if (!File.Exists(Path.Combine(StartFolder, "hpatchz.exe")))
            {
                logger.WriteLine("Critical error: hpatchz not found.");
                MessageBox.Show("Critical error: unable to locate hpatchz.exe.", "Error: hpatchz not found", MessageBoxButtons.OK, MessageBoxIcon.Error, MessageBoxDefaultButton.Button1, MessageBoxOptions.DefaultDesktopOnly);
                return;
            }
            Process pProcess = new System.Diagnostics.Process();
            pProcess.StartInfo.FileName = "hpatchz.exe";
            pProcess.StartInfo.Arguments = "-f " + "\"" + WorkFolder + "\"" + " \"" + Path.Combine(StartFolder, "patch_steam_inst.dat")+ "\" " + "\"" + WorkFolder + "\"";
            logger.WriteLine("hpatchz command line: hpatchz " + pProcess.StartInfo.Arguments + System.Environment.NewLine);
            pProcess.StartInfo.UseShellExecute = false;
            pProcess.StartInfo.RedirectStandardError = true;
            pProcess.StartInfo.WorkingDirectory = StartFolder;
            pProcess.StartInfo.CreateNoWindow = true;
            pProcess.Start();
            string strOutput = pProcess.StandardError.ReadToEnd();
            logger.WriteLine(strOutput);
            pProcess.WaitForExit();
            logger.WriteLine("hpatchz exited");
        }

        private void AddProgress(int value)
        {
            if (progressBar1.InvokeRequired)
            {
                SetValueCallback d = new SetValueCallback(AddProgress);
                this.Invoke(d, new object[] { value });
            }
            else
                progressBar1.Value = Math.Min(progressBar1.Maximum, progressBar1.Value + value);
        }

        private void Patch2004()
        {
            logger.WriteLine("2004 EXE found, checking version...");
            string exepath = Path.Combine(WorkFolder, "sonic.exe");
            MD5 md5 = MD5.Create();
            byte[] sonicexe = File.ReadAllBytes(exepath);
            uint VirtualAddress = BitConverter.ToUInt32(sonicexe, 0x254);
            uint VirtualSize = BitConverter.ToUInt32(sonicexe, 0x250);
            logger.WriteLine("\tVirtual address: " + VirtualAddress.ToString("X"));
            logger.WriteLine("\tVirtual size: " + VirtualSize.ToString("X"));
            if (VirtualAddress == 0x3DB000 && VirtualSize == 0xB6B88)
            {
                logger.WriteLine("Found US version, no patch required");
                DontCheckExe = true;
            }
            else
            {
                byte[] filehash_bytes = md5.ComputeHash(sonicexe);
                string filehash = string.Empty;
                foreach (byte item in filehash_bytes)
                    filehash += item.ToString("x2");
                string patchname = string.Empty;
                logger.WriteLine("MD5: " + filehash);
                switch (filehash)
                {
                    case "6e2e64ebf62787af47ed813221040898": // JP
                        logger.WriteLine("Found Japanese version, unsupported");
                        break;
                    case "1b65b196137b5a853d781ba93a3046a2": // Sold Out
                        logger.WriteLine("Found Sold Out Software version, unsupported");
                        break;
                    case "ad6388fa1a703e90d0fc693c5cdb4c5d": // EU original
                        patchname = "patch_eu_orig_inst.dat";
                        logger.WriteLine("Found EU (original) version");
                        break;
                    case "57f987014504e0f0dcfc154dfd48fead": // EU Best Buy
                        patchname = "patch_bestbuy_inst.dat";
                        logger.WriteLine("Found EU (Best Buy) version");
                        break;
                    case "9c1184502ad6d1bed8b2833822e3a477": // EU Sonic PC Collection
                        logger.WriteLine("Found EU (Sonic PC Collection) version");
                        patchname = "patch_eu_pccol_inst.dat";
                        break;
                    case "6b3c7a0013cbfd9b12c3765e9ba3a73e": // KR
                        logger.WriteLine("Found Korean version");
                        patchname = "patch_kr_inst.dat";
                        break;
                }
                if (patchname == string.Empty)
                {
                    logger.WriteLine("No patch data found for this sonic.exe");
                }
                else
                {
                    UpdateProgressDetails("Patching 2004 resources...");
                    // Error check: hpatchz.exe
                    if (!File.Exists(Path.Combine(StartFolder, "hpatchz.exe")))
                    {
                        logger.WriteLine("Critical error: hpatchz not found.");
                        MessageBox.Show("Critical error: unable to locate hpatchz.exe.", "Error: hpatchz not found", MessageBoxButtons.OK, MessageBoxIcon.Error, MessageBoxDefaultButton.Button1, MessageBoxOptions.DefaultDesktopOnly);
                        return;
                    }
                    Process pProcess = new System.Diagnostics.Process();
                    pProcess.StartInfo.FileName = "hpatchz.exe";
                    pProcess.StartInfo.Arguments = "-f " + "\"" + WorkFolder + "\"" + " \"" + Path.Combine(StartFolder, patchname) + "\" " + "\"" + WorkFolder + "\"";
                    logger.WriteLine("hpatchz command line: hpatchz " + pProcess.StartInfo.Arguments + System.Environment.NewLine);
                    pProcess.StartInfo.UseShellExecute = false;
                    pProcess.StartInfo.RedirectStandardError = true;
                    pProcess.StartInfo.WorkingDirectory = StartFolder;
                    pProcess.StartInfo.CreateNoWindow = true;
                    pProcess.Start();
                    string strOutput = pProcess.StandardError.ReadToEnd();
                    logger.WriteLine(strOutput);
                    pProcess.WaitForExit();
                    logger.WriteLine("hpatchz exited");
                }
            }
        }

        private void Cleanup()
        {
            try
            {
                foreach (string file in SteamDeleteFilesList)
                {
                    if (File.Exists(Path.Combine(WorkFolder, file)))
                        File.Delete(Path.Combine(WorkFolder, file));
                }
                foreach (string folder in SteamDeleteFolderList)
                {
                    if (Directory.Exists(Path.Combine(WorkFolder, folder)))
                        Directory.Delete(Path.Combine(WorkFolder, folder), true);
                }
            }
            catch (Exception ex)
            {
                logger.WriteLine(ex.Message.ToString());
                MessageBox.Show("Error deleting leftover files or folders" +
                       "See the file `SADX Steam Conversion.log` in the game directory for details.",
                       "Error running Steam Conversion Helper", MessageBoxButtons.OK, MessageBoxIcon.Error, MessageBoxDefaultButton.Button1, MessageBoxOptions.DefaultDesktopOnly);
            }
        }

        private void backgroundWorker1_DoWork(object sender, DoWorkEventArgs e)
        {
            logger.WriteLine("Work folder: " + WorkFolder);
            logger.WriteLine("Start folder: " + StartFolder);
            MD5 md5 = MD5.Create();
            bool done = false;
			while (!done)
			{
				try
				{
                    // 2004 version
                    if (File.Exists(Path.Combine(WorkFolder, "sonic.exe")) && !File.Exists(Path.Combine(WorkFolder, "Sonic Adventure DX.exe")))
                        Patch2004();
                    // Convert EXE
                    else                   
                       PatchSteam();
                    // Convert videos
                    ConvertSFD();
                    // Check if all files exist
                    UpdateProgressDetails("Verifying converted files...");
                    bool error = false;
                    foreach (PatchHashData file in ResultVerifyList)
                    {
                        if (file.filename == "sonic.exe" && DontCheckExe)
                            continue;
                        // If Mod Loader is installed, change the filename of CHRMODELS.DLL
                        if (file.filename == "system\\CHRMODELS.DLL" && File.Exists(Path.Combine(WorkFolder, "system\\CHRMODELS_orig.DLL")))
                            file.filename = "system\\CHRMODELS_orig.DLL";
                        // Check files
                        if (!File.Exists(Path.Combine(WorkFolder, file.filename)))
                        {
                            error = true;
                            logger.WriteLine(string.Format("File {0} is missing", file.filename));
                        }
                        else
                        {
                            byte[] filehash_bytes = md5.ComputeHash(File.ReadAllBytes(Path.Combine(WorkFolder, file.filename)));
                            string filehash = string.Empty;
                            foreach (byte item in filehash_bytes)
                                filehash += item.ToString("x2");
                            if (filehash != file.checksum)
                            {
                                error = true;
                                logger.WriteLine(string.Format("File {0} has an incorrect checksum: expected {1}, got {2}", file.filename, file.checksum, filehash));
                            }
                        }
                    }
                    // Check if the intro exists
                    if (!File.Exists(Path.Combine(WorkFolder, "system", "RE-US.mpg")))
                    {
                        logger.WriteLine("File system\\RE-US.mpg is missing");
                        error = true;
                    }
                    // Check if SA FMVs exist
                    for (int i = 1; i < 9; i++)
                    {
                        if (!File.Exists(Path.Combine(WorkFolder, "system", "SA" + i.ToString() + ".mpg")))
                        {
                            error = true;
                            logger.WriteLine(string.Format("File system\\SA{0}.mpg is missing", i.ToString()));
                        }
                    }
                    if (!error)
                        UpdateProgressDetails("Done!");
                    else
                    MessageBox.Show("Conversion has failed.\n" +
                        "See the file `SADX Steam Conversion.log` in the game directory for details.", 
                        "Error running Steam Conversion Helper", MessageBoxButtons.OK, MessageBoxIcon.Error, MessageBoxDefaultButton.Button1, MessageBoxOptions.DefaultDesktopOnly);
                    logger.Flush();
                    logger.Close();
					System.Threading.Thread.Sleep(1000);
					done = true;
				}
				catch (Exception ex)
				{
                    logger.Flush();
                    DialogResult result;
					result = MessageBox.Show(this, "Error running Steam Conversion Helper: \n" + ex.Message.ToString() + 
                        "\n\nSome files in the game's folder may be blocked by another program, or your anti-virus is interfering. " +
                        "Close all programs accessing the game's folder, verify game integrity on Steam and restart.\n\nIf you skip this step, the game may not work.\n\n" +
                        "Try again?", "Error running Steam Conversion Helper", MessageBoxButtons.RetryCancel, MessageBoxIcon.Error, 
                        MessageBoxDefaultButton.Button1, MessageBoxOptions.DefaultDesktopOnly);
                    if (result == DialogResult.Cancel)
                    {
                        logger.Flush();
                        logger.Close();
                        Success = SuccessValue.Error;
                        Application.Exit();
                    }
				}
			}
            Cleanup();
            Success = SuccessValue.Success;
            Application.Exit();
		}

        private void addURLHandler()
        {
            try
            {
                logger.WriteLine("Adding 1-click install support...");
                string managerPath = Path.Combine(WorkFolder, "SAModManager.exe");
                if (!File.Exists(managerPath))
                    managerPath = Path.Combine(WorkFolder, "SADXModManager.exe");
                if (!File.Exists(managerPath))
                {
                    logger.WriteLine("Mod Manager not found, skipping 1-click URL handler");
                    return;
                }
                logger.WriteLine("Mod Manager path: " + managerPath);
                using (var hkcu = Microsoft.Win32.Registry.CurrentUser)
                {
                    var key = hkcu.CreateSubKey("Software\\Classes\\sadxmm");
                    key.SetValue(null, "URL:SADX Mod Manager Protocol");
                    key.SetValue("URL Protocol", string.Empty);
                    using (var k2 = key.CreateSubKey("DefaultIcon"))
                        k2.SetValue(null, managerPath + ",1");
                    using (var k3 = key.CreateSubKey("shell"))
                    using (var k4 = k3.CreateSubKey("open"))
                    using (var k5 = k4.CreateSubKey("command"))
                        k5.SetValue(null, $"\"{managerPath}\" \"%1\"");
                    key.Close();
                }
            }
            catch (Exception ex)
            {
                logger.WriteLine("Unable to register SADX Mod Manager URL handler. Are you not running the installer as administrator? Exception code:\n" + ex.Message.ToString());
                //MessageBox.Show(this, "Unable to register SADX Mod Manager URL handler. Are you not running the installer as administrator? Exception code:\n\n" + ex.Message.ToString(), "SADX Steam Helper Error", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
            logger.WriteLine();
        }

        private void Form_SteamHelper_FormClosing(object sender, FormClosingEventArgs e)
        {
            if (Success == SuccessValue.InProcess)
            {
                DialogResult res = MessageBox.Show(this, "The game will not work if you cancel the conversion.\nAre you sure you want to cancel?", "Steam Helper", MessageBoxButtons.YesNo, MessageBoxIcon.Question);
                if (res == DialogResult.No)
                {
                    e.Cancel = true;
                }
                else
                {
                    logger.WriteLine("Conversion cancelled by the user");
                    logger.Flush();
                    logger.Close();
                    Success = SuccessValue.Error;
                }
            }
        }
    }
}