# Steam Helper for the SADX Mod Installer

This tool converts assets from the Steam version of Sonic Adventure DX to make them compatible with the 2004 version.